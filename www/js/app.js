angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCordova', 'ngIOS9UIWebViewPatch', 'pascalprecht.translate', 'ionic-material', 'ionic-cache-src', 'ja.qr', 'ionic.rating', 'ion-datetime-picker'])

.run(function($ionicPlatform, $state, $cordovaSQLite, $http, $cordovaDevice, $timeout, $rootScope, $cordovaPushV5, $ionicSlideBoxDelegate, $ionicConfig, $cordovaFile, $window, $ionicHistory, $ionicPickerI18n, $ionicPopup, $localStorage) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }

        if (ionic.Platform.grade.toLowerCase()!='a') {
            $ionicConfig.views.transition('none');
        }

        // console.log('START')

        var testing = false;

        $rootScope.db;

        $rootScope.uuid = $cordovaDevice.getUUID();
        $rootScope.model = $cordovaDevice.getModel();
        $rootScope.platform = $cordovaDevice.getPlatform();
        $rootScope.version = $cordovaDevice.getVersion();

        if($rootScope.platform == 'Android') {
          // Works on Android
          $rootScope.db = $cordovaSQLite.openDB({name: "theone.db", iosDatabaseLocation: 'default'});
        }
        else if($rootScope.platform == 'iOS') {
          // Works on iOS 
          $rootScope.db = $cordovaSQLite.openDB({name: "theone.db", location: 2, createFromLocation: 1});
        }

        $rootScope.institution = 25;
        $rootScope.inst_dir = 'stolle';
        var gcm_android = '960473335101';
		
        $rootScope.generalscript = 'http://www.olegtronics.com/appscripts/getappdata.php';
        $rootScope.pushgotlink = 'http://www.olegtronics.com/admin/coms/pushgot.php';

        $rootScope.appName = 'Штолле';
        $rootScope.iosLink = 'https://itunes.apple.com/us/app/stolle';
        $rootScope.androidLink = 'https://play.google.com/store/apps/details?id=by.olegtronics.stolle';
        $rootScope.companyicon = 'http://www.olegtronics.com/admin/img/icons/stolle.png';

        $rootScope.smallscreen = ((window.innerHeight <= 480) ? true : false);

        $rootScope.intconnect = true;
        $rootScope.intconnectupd = true;
        $rootScope.regform = false;

        $ionicPickerI18n.ok = "Применить";
        $ionicPickerI18n.cancel = "Отмена";
        $ionicPickerI18n.weekdays = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];
        $ionicPickerI18n.months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];

        var points = 0;
        var wallet = 0;
        var goods = 0;
        var cat = 0;
        var menue = 0;
        var ingrs = 0;
        var news = 0;
        var revs = 0;
        var asks = 0;
        var gifts = 0;
        var chat = 1;
        var order = 0;
        var room = 0;

        var fifthgift = 0;
        var reservs = 0;
        var organization = 0;

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        function ifNotExists(tablename, columnname, columntype, defaultvalue) {
          // FOR OLDER VERSION OF APP - ADD SOME NEW MENUE COLUMNS
          var queryAlter = "SELECT * FROM "+tablename+" WHERE "+columnname+"=?";
          $cordovaSQLite.execute($rootScope.db, queryAlter, [0]).then(function(suc) {}, function(er) {

              var queryAdd = "ALTER TABLE "+tablename+" ADD COLUMN "+columnname+" "+columntype;
              $cordovaSQLite.execute($rootScope.db, queryAdd, []).then(function() {

                // alert('added')

                var queryUpd = "UPDATE "+tablename+" SET "+columnname+"="+defaultvalue;
                $cordovaSQLite.execute($rootScope.db, queryUpd, []).then(function() {
                  // alert('updated')
                }, function(er) {
                  // alert('not updated '+JSON.stringify(er))
                });

              }, function(error) {
                // alert(JSON.stringify(error))
              });

          });
        }
            
        $rootScope.getCalender = function(data, notconf) {

          var dataorders = data;

          // DB ORDER
          var orderArr = dataorders[0].ordersArr;
          if(orderArr.length > 0) {

            var id = 0;
            
            function orderArrFuncIns(id) {

              var order_id = orderArr[id]['order_id'];
              var order_user = orderArr[id]['order_user'];
              var order_user_name_phone = orderArr[id]['order_user_name_phone'];
              var order_user_surname_phone = orderArr[id]['order_user_surname_phone'];
              var order_user_middlename_phone = orderArr[id]['order_user_middlename_phone'];
              var order_user_adress_phone = orderArr[id]['order_user_adress_phone'];
              var order_user_comment_phone = orderArr[id]['order_user_comment_phone'];
              var order_name = orderArr[id]['order_name'];
              var order_name_phone = orderArr[id]['order_name_phone'];
              var order_user_phone_phone = orderArr[id]['order_user_phone_phone'];
              var order_user_email_phone = orderArr[id]['order_user_email_phone'];
              var order_desc = orderArr[id]['order_desc'];
              var order_worker = orderArr[id]['order_worker'];
              var order_worker_name_phone = orderArr[id]['order_worker_name_phone'];
              var order_worker_pic_phone = orderArr[id]['order_worker_pic_phone'];
              var order_worker_profession_phone = orderArr[id]['order_worker_profession_phone'];
              var order_reminder_phone = orderArr[id]['order_reminder_phone'];
              var order_institution = orderArr[id]['order_institution'];
              var order_office = orderArr[id]['order_office'];
              var order_room = orderArr[id]['order_room'];
              var order_bill = orderArr[id]['order_bill'];
              var order_goods = orderArr[id]['order_goods'];
              var order_cats = orderArr[id]['order_cats'];
              var order_order = orderArr[id]['order_order'];
              var order_status = orderArr[id]['order_status'];
              var order_start = orderArr[id]['order_start'];
              var order_start_name_phone = orderArr[id]['order_start_name_phone'];
              var order_end = orderArr[id]['order_end'];
              var order_allday = orderArr[id]['order_allday'];
              var order_mobile_confirm = orderArr[id]['order_mobile_confirm'];
              var order_mobile = orderArr[id]['order_mobile'];
              var order_when = orderArr[id]['order_when'];
              var order_del = orderArr[id]['order_del'];

              var queryOrdering = "SELECT * FROM ordering WHERE order_id = ?";
              $cordovaSQLite.execute($rootScope.db, queryOrdering, [order_id]).then(function(suc) {
                if(suc.rows.length > 0) {

                  // DB ORDERS
                  var orderingUpd = "UPDATE ordering SET order_user=?, order_user_name_phone=?, order_user_surname_phone=?, order_user_middlename_phone=?, order_user_adress_phone=?, order_user_comment_phone=?, order_name=?, order_name_phone=?, order_user_phone_phone=?, order_user_email_phone=?, order_desc=?, order_worker=?, order_worker_name_phone=?, order_worker_pic_phone=?, order_worker_profession_phone=?, order_reminder_phone=?, order_institution=?, order_office=?, order_room=?, order_bill=?, order_goods=?, order_cats=?, order_order=?, order_status=?,  order_start=?, order_start_name_phone=?, order_end=?, order_allday=?, order_mobile_confirm=?, order_mobile=?, order_when=?, order_del=? WHERE order_id=?";
                  $cordovaSQLite.execute($rootScope.db, orderingUpd, [order_user, order_user_name_phone, order_user_surname_phone, order_user_middlename_phone, order_user_adress_phone, order_user_comment_phone, order_name, order_name_phone, order_user_phone_phone, order_user_email_phone, order_desc, order_worker, order_worker_name_phone, order_worker_pic_phone, order_worker_profession_phone, order_reminder_phone, order_institution, order_office, order_room, order_bill, order_goods, order_cats, order_order, order_status, order_start, order_start_name_phone, order_end, order_allday, order_mobile_confirm, order_mobile, order_when, order_del, order_id]).then(function() {
                    id++;
                    if(id < orderArr.length) {
                    orderArrFuncIns(id);
                    }
                  }, function() {});

                }
                else {

                  var orderIns = "INSERT INTO ordering (order_id, order_user, order_user_name_phone, order_user_surname_phone, order_user_middlename_phone, order_user_adress_phone, order_user_comment_phone, order_name, order_name_phone, order_user_phone_phone, order_user_email_phone, order_desc, order_worker, order_worker_name_phone, order_worker_pic_phone, order_worker_profession_phone, order_reminder_phone, order_institution, order_office, order_room, order_bill, order_goods, order_cats, order_order, order_status, order_start, order_start_name_phone, order_end, order_allday, order_mobile_confirm, order_mobile, order_when, order_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                  $cordovaSQLite.execute($rootScope.db, orderIns, [order_id, order_user, order_user_name_phone, order_user_surname_phone, order_user_middlename_phone, order_user_adress_phone, order_user_comment_phone, order_name, order_name_phone, order_user_phone_phone, order_user_email_phone, order_desc, order_worker, order_worker_name_phone, order_worker_pic_phone, order_worker_profession_phone, order_reminder_phone, order_institution, order_office, order_room, order_bill, order_goods, order_cats, order_order, order_status, order_start, order_start_name_phone, order_end, order_allday, order_mobile_confirm, order_mobile, order_when, order_del]).then(function(ins) {
                    id++;
                    if(id < orderArr.length) {
                    orderArrFuncIns(id);
                    }
                  }, function() {});

                }
              }, function() {});

            };
            orderArrFuncIns(0);

          }

          $timeout(function() {

            // if(notconf != 1) {
              // $state.go('login');
              // $rootScope.regform = true;
            // }
            // else {
            $ionicHistory.nextViewOptions({
              disableAnimate: true,
              disableBack: true
            });
            
              $state.go('app.main');
            // }

          }, 2000);
          
        };
        
        $rootScope.getWaiter = function(data, notconf) {
          
          var dataworkers = data;

          // DB WAITERS
          var usrArr = dataworkers[0].usrArr;
          if(usrArr.length > 0) {

            var id = 0;

            function usrArrFunc(id) {

              var user_id = usrArr[id]['user_id'];
              var user_name = usrArr[id]['user_name'];
              var user_surname = usrArr[id]['user_surname'];
              var user_middlename = usrArr[id]['user_middlename'];
              var user_mob = usrArr[id]['user_mob'];
              var user_work_pos = usrArr[id]['user_work_pos'];
              var user_menue_exe = usrArr[id]['user_menue_exe'];
              var user_pic = usrArr[id]['user_pic'];
              var user_gender = usrArr[id]['user_gender'];
              var user_institution = usrArr[id]['user_institution'];
              var user_office = usrArr[id]['user_office'];
              var user_reg = usrArr[id]['user_reg'];
              var user_del = usrArr[id]['user_del'];

              var queryWaiters = "SELECT * FROM users WHERE user_real_id = ?";
              $cordovaSQLite.execute($rootScope.db, queryWaiters, [user_id]).then(function(suc) {

                if(suc.rows.length > 0) {
                  // DB WAITERS
                  var usrUpd = "UPDATE users SET user_name=?, user_surname=?, user_middlename=?, user_mob=?, user_work_pos=?, user_menue_exe=?, user_pic=?, user_gender=?, user_institution=?, user_office=?, user_reg=?, user_del=? WHERE user_real_id=?";
                  $cordovaSQLite.execute($rootScope.db, usrUpd, [user_name, user_surname, user_middlename, user_mob, user_work_pos, user_menue_exe, user_pic, user_gender, user_institution, user_office, user_reg, user_del, user_id]).then(function() {
                    id++;
                    if(id < usrArr.length) {
                    usrArrFunc(id);
                    }
                  }, function() {});
                }
                else {
                  // DB WAITERS
                  var usrIns = "INSERT INTO users (user_real_id, user_name, user_surname, user_middlename, user_mob, user_work_pos, user_menue_exe, user_pic, user_gender, user_institution, user_office, user_reg, user_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
                  $cordovaSQLite.execute($rootScope.db, usrIns, [user_id, user_name, user_surname, user_middlename, user_mob, user_work_pos, user_menue_exe, user_pic, user_gender, user_institution, user_office, user_reg, user_del]).then(function() {
                    // alert('ins ok')
                    id++;
                    if(id < usrArr.length) {
                    usrArrFunc(id);
                    }
                  }, function() {});
                }

              }, function() {});

            }
            usrArrFunc(0);

          }

          // DB PROFESSIONS
          var profArr = dataworkers[0].profArr;
          if(profArr.length > 0) {

            var id = 0;

            function profArrFunc(id) {

              var prof_id = profArr[id]['prof_id'];
              var prof_name = profArr[id]['prof_name'];
              var prof_desc = profArr[id]['prof_desc'];
              var prof_institution = profArr[id]['prof_institution'];
              var prof_when = profArr[id]['prof_when'];
              var prof_del = profArr[id]['prof_del'];

              var queryProfs = "SELECT * FROM professions WHERE prof_id = ?";
              $cordovaSQLite.execute($rootScope.db, queryProfs, [prof_id]).then(function(suc) {
              if(suc.rows.length > 0) {
              // DB PROFESSIONS
              var profUpd = "UPDATE professions SET prof_name=?, prof_desc=?, prof_institution=?, prof_when=?, prof_del=? WHERE prof_id=?";
              $cordovaSQLite.execute($rootScope.db, profUpd, [prof_name, prof_desc, prof_institution, prof_when, prof_del, prof_id]).then(function() {
                // alert('upd ok')
                id++;
                if(id < profArr.length) {
                profArrFunc(id);
                }
              }, function() {});
              }
              else {
              // DB PROFESSIONS
              var profIns = "INSERT INTO professions (prof_id, prof_name, prof_desc, prof_institution, prof_when, prof_del) VALUES (?,?,?,?,?,?)";
              $cordovaSQLite.execute($rootScope.db, profIns, [prof_id, prof_name, prof_desc, prof_institution, prof_when, prof_del]).then(function() {
                // alert('ins ok')
                id++;
                if(id < profArr.length) {
                profArrFunc(id);
                }
              }, function() {});
              }
              }, function() {});

            }
            profArrFunc(0);

          }

          // DB OFFICE
          var offArr = dataworkers[0].offArr;
          if(offArr.length > 0) {

            var id = 0;

            function offArrFunc(id) {

              var office_id = offArr[id]['office_id'];
              var office_name = offArr[id]['office_name'];
              var office_start = offArr[id]['office_start'];
              var office_stop = offArr[id]['office_stop'];
              var office_bus_hours = offArr[id]['office_bus_hours'];
              var office_country = offArr[id]['office_country'];
              var office_city = offArr[id]['office_city'];
              var office_adress = offArr[id]['office_adress'];
              var office_lat = offArr[id]['office_lat'];
              var office_lon = offArr[id]['office_lon'];
              var office_desc = offArr[id]['office_desc'];
              var office_timezone = offArr[id]['office_timezone'];
              var office_tel = offArr[id]['office_tel'];
              var office_tel_n = offArr[id]['office_tel_n'];
              var office_fax = offArr[id]['office_fax'];
              var office_mob = offArr[id]['office_mob'];
              var office_email = offArr[id]['office_email'];
              var office_orders = offArr[id]['office_orders'];
              var office_skype = offArr[id]['office_skype'];
              var office_site = offArr[id]['office_site'];
              var office_logo = offArr[id]['office_logo'];
              var office_institution = offArr[id]['office_institution'];
              var office_del = offArr[id]['office_del'];

              var queryOffice = "SELECT * FROM organizations_office WHERE office_id = ?";
              $cordovaSQLite.execute($rootScope.db, queryOffice, [office_id]).then(function(suc) {
              if(suc.rows.length > 0) {
                // DB OFFICE
                var officeUpd = "UPDATE organizations_office SET office_name=?, office_start=?, office_stop=?, office_bus_hours=?, office_country=?, office_city=?, office_adress=?, office_lat=?, office_lon=?, office_desc=?, office_timezone=?, office_tel=?, office_tel_n=?, office_fax=?, office_mob=?, office_email=?, office_orders=?, office_skype=?, office_site=?, office_logo=?, office_institution=?, office_del=? WHERE office_id=?";
                $cordovaSQLite.execute($rootScope.db, officeUpd, [office_name, office_start, office_stop, office_bus_hours, office_country, office_city, office_adress, office_lat, office_lon, office_desc, office_timezone, office_tel, office_tel_n, office_fax, office_mob, office_email, office_orders, office_skype, office_site, office_logo, office_institution, office_del, office_id]).then(function() {
                  // alert('upd ok')
                  id++;
                  if(id < offArr.length) {
                  offArrFunc(id);
                  }
                }, function() {});
              }
              else {
                // DB OFFICE
                var officeIns = "INSERT INTO organizations_office (office_id, office_name, office_start, office_stop, office_bus_hours, office_country, office_city, office_adress, office_lat, office_lon, office_desc, office_timezone, office_tel, office_tel_n, office_fax, office_mob, office_email, office_orders, office_skype, office_site, office_logo, office_institution, office_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $cordovaSQLite.execute($rootScope.db, officeIns, [office_id, office_name, office_start, office_stop, office_bus_hours, office_country, office_city, office_adress, office_lat, office_lon, office_desc, office_timezone, office_tel, office_tel_n, office_fax, office_mob, office_email, office_orders, office_skype, office_site, office_logo, office_institution, office_del]).then(function() {
                  // alert('ins ok')
                  id++;
                  if(id < offArr.length) {
                  offArrFunc(id);
                  }
                }, function() {});
              }
              }, function() {});

            }
            offArrFunc(0);

          }

          // DB ROOMS
          var roomArr = dataworkers[0].roomArr;
          if(roomArr.length > 0) {

            var id = 0;

            function roomArrFunc(id) {

              var room_id = roomArr[id]['room_id'];
              var room_name = roomArr[id]['room_name'];
              var room_desc = roomArr[id]['room_desc'];
              var room_employee = roomArr[id]['room_employee'];
              var room_menue_exe = roomArr[id]['room_menue_exe'];
              var room_priority = roomArr[id]['room_priority'];
              var room_institution = roomArr[id]['room_institution'];
              var room_office = roomArr[id]['room_office'];
              var room_upd = roomArr[id]['room_upd'];
              var room_when = roomArr[id]['room_when'];
              var room_del = roomArr[id]['room_del'];

              var queryRooms = "SELECT * FROM rooms WHERE room_id = ?";
              $cordovaSQLite.execute($rootScope.db, queryRooms, [room_id]).then(function(suc) {
              if(suc.rows.length > 0) {
                // DB ROOMS
                var roomUpd = "UPDATE rooms SET room_name=?, room_desc=?, room_employee=?, room_menue_exe=?, room_priority=?, room_institution=?, room_office=?, room_upd=?, room_when=?, room_del=? WHERE room_id=?";
                $cordovaSQLite.execute($rootScope.db, roomUpd, [room_name, room_desc, room_employee, room_menue_exe, room_priority, room_institution, room_office, room_upd, room_when, room_del, room_id]).then(function() {
                  // alert('upd ok')
                  id++;
                  if(id < roomArr.length) {
                  offArrFunc(id);
                  }
                }, function() {});
              }
              else {
                // DB ROOMS
                var roomIns = "INSERT INTO rooms (room_id, room_name, room_desc, room_employee, room_menue_exe, room_priority, room_institution, room_office, room_upd, room_when, room_del) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                $cordovaSQLite.execute($rootScope.db, roomIns, [room_id, room_name, room_desc, room_employee, room_menue_exe, room_priority, room_institution, room_office, room_upd, room_when, room_del]).then(function() {
                  // alert('ins ok')
                  id++;
                  if(id < roomArr.length) {
                  roomArrFunc(id);
                  }
                }, function() {});
              }
              }, function() {});

            }
            roomArrFunc(0);

          }

          // DB SCHEDULE
          var schArr = dataworkers[0].schArr;
          if(schArr.length > 0) {

            var id = 0;

            function schArrFunc(id) {

              var schedule_id = schArr[id]['schedule_id'];
              var schedule_employee = schArr[id]['schedule_employee'];
              var schedule_menue = schArr[id]['schedule_menue'];
              var schedule_office = schArr[id]['schedule_office'];
              var schedule_start = schArr[id]['schedule_start'];
              var schedule_stop = schArr[id]['schedule_stop'];
              var schedule_institution = schArr[id]['schedule_institution'];
              var schedule_when = schArr[id]['schedule_when'];
              var schedule_del = schArr[id]['schedule_del'];

              var querySchedule = "SELECT * FROM schedule WHERE schedule_id = ?";
              $cordovaSQLite.execute($rootScope.db, querySchedule, [schedule_id]).then(function(suc) {
              if(suc.rows.length > 0) {
              // DB SCHEDULE
              var scheduleUpd = "UPDATE schedule SET schedule_employee=?, schedule_menue=?, schedule_office=?, schedule_start=?, schedule_stop=?, schedule_institution=?, schedule_when=?, schedule_del=? WHERE schedule_id=?";
              $cordovaSQLite.execute($rootScope.db, scheduleUpd, [schedule_employee, schedule_menue, schedule_office, schedule_start, schedule_stop, schedule_institution, schedule_when, schedule_del, schedule_id]).then(function() {
                // alert('upd ok')
                id++;
                if(id < schArr.length) {
                schArrFunc(id);
                }
              }, function() {});
              }
              else {
              // DB SCHEDULE
              var scheduleIns = "INSERT INTO schedule (schedule_id, schedule_employee, schedule_menue, schedule_office, schedule_start, schedule_stop, schedule_institution, schedule_when, schedule_del) VALUES (?,?,?,?,?,?,?,?,?)";
              $cordovaSQLite.execute($rootScope.db, scheduleIns, [schedule_id, schedule_employee, schedule_menue, schedule_office, schedule_start, schedule_stop, schedule_institution, schedule_when, schedule_del]).then(function() {
                // alert('ins ok')
                id++;
                if(id < schArr.length) {
                schArrFunc(id);
                }
              }, function() {});
              }
              }, function() {});

            }
            schArrFunc(0);

          }

          var calendstr = JSON.stringify({
            device: $rootScope.model,
            device_id: $rootScope.uuid,
            device_version: $rootScope.version,
            device_os: $rootScope.platform,
            inst_id: $rootScope.institution,
            newusr: 'calender'
          });
            
          $http.post($rootScope.generalscript, calendstr).then(function(calendsuc) {

            var calenddata = calendsuc.data;
            $rootScope.getCalender(calenddata, notconf);

          }, 
          function(er) {
            // alert('error Calendar: '+JSON.stringify(er));
          });
            
        };
        
        $rootScope.getNew = function(data) {
          
          // DB USER
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS users (user_id integer primary key, user_real_id integer, user_name text, user_surname text, user_middlename text, user_email text, user_email_confirm text, user_pwd text, user_tel integer, user_mob_confirm text, user_mob integer, user_work_pos integer, user_menue_exe text, user_institution integer, user_office integer, user_pic text, user_gender integer, user_birthday text, user_country integer, user_region integer, user_city integer, user_adress text, user_install_where integer, user_log_key text, user_gcm text, user_device text, user_device_id text, user_device_version text, user_device_os text, user_discount text, user_promo integer, user_conf_req integer, user_log integer, user_upd integer, user_reg integer, user_del integer)").then(function() {

            if(data[0].newusr == '1') {

              var user_id = data[0].usrData['user_id'];
              var user_name = data[0].usrData['user_name'];
              var user_surname = data[0].usrData['user_surname'];
              var user_middlename = data[0].usrData['user_middlename'];
              var user_email = data[0].usrData['user_email'];
              var user_email_confirm = data[0].usrData['user_email_confirm'];
              var user_pwd = data[0].usrData['user_pwd'];
              var user_tel = data[0].usrData['user_tel'];
              var user_mob_confirm = data[0].usrData['user_mob_confirm'];
              var user_mob = data[0].usrData['user_mob'];
              var user_work_pos = data[0].usrData['user_work_pos'];
              var user_menue_exe = data[0].usrData['user_menue_exe'];
              var user_institution = data[0].usrData['user_institution'];
              var user_office = data[0].usrData['user_office'];
              var user_pic = data[0].usrData['user_pic'];
              var user_gender = data[0].usrData['user_gender'];
              var user_birthday = data[0].usrData['user_birthday'];
              var user_country = data[0].usrData['user_country'];
              var user_region = data[0].usrData['user_region'];
              var user_city = data[0].usrData['user_city'];
              var user_adress = data[0].usrData['user_adress'];
              var user_install_where = data[0].usrData['user_install_where'];
              var user_log_key = data[0].usrData['user_log_key'];
              var user_gcm = data[0].usrData['user_gcm'];
              var user_device = data[0].usrData['user_device'];
              var user_device_id = data[0].usrData['user_device_id'];
              var user_device_version = data[0].usrData['user_device_version'];
              var user_device_os = data[0].usrData['user_device_os'];
              var user_discount = data[0].usrData['user_discount'];
              var user_promo = data[0].usrData['user_promo'];
              var user_conf_req = data[0].usrData['user_conf_req'];
              var user_log = data[0].usrData['user_log'];
              var user_upd = data[0].usrData['user_upd'];
              var user_reg = data[0].usrData['user_reg'];
              var user_del = data[0].usrData['user_del'];

              // DB USER
              var usrIns = "INSERT INTO users (user_real_id, user_name, user_surname, user_middlename, user_email, user_email_confirm, user_pwd, user_tel, user_mob_confirm, user_mob, user_work_pos, user_menue_exe, user_institution, user_office, user_pic, user_gender, user_birthday, user_country, user_region, user_city, user_adress, user_install_where, user_log_key, user_gcm, user_device, user_device_id, user_device_version, user_device_os, user_discount, user_promo, user_conf_req, user_log, user_upd, user_reg, user_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
              $cordovaSQLite.execute($rootScope.db, usrIns, [user_id, user_name, user_surname, user_middlename, user_email, user_email_confirm, user_pwd, user_tel, user_mob_confirm, user_mob, user_work_pos, user_menue_exe, user_institution, user_office, user_pic, user_gender, user_birthday, user_country, user_region, user_city, user_adress, user_install_where, user_log_key, user_gcm, user_device, user_device_id, user_device_version, user_device_os, user_discount, user_promo, user_conf_req, user_log, user_upd, user_reg, user_del]).then(function() {

                $timeout(function() {
                  
                  var waiterstr = JSON.stringify({
                    inst_id: $rootScope.institution,
                    newusr: 'waiter'
                  });
                    
                  $http.post($rootScope.generalscript, waiterstr).then(function(waitsuc) {

                    var waitdata = waitsuc.data;
                    $rootScope.getWaiter(waitdata, user_mob_confirm);

                  }, 
                  function(er) {
                  });

                }, 2000);

              }, function() {});

            }

          }, function() {});

          // DB COUNTRY
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS country (id_country integer primary key, name text, country_del integer)").then(function() {}, function() {});

          // DB REGION
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS region (id_region integer primary key, id_country integer, name text, region_del integer)").then(function() {}, function() {});

          // DB CITY
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS city (id_city integer primary key, id_region integer, id_country integer, name text, city_del integer)").then(function() {}, function() {});

          // DB POINTS
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS points (points_id integer primary key, points_user integer, points_bill integer, points_discount integer, points_points integer, points_got_spend integer, points_waiter integer, points_institution integer, points_office integer,points_status integer, points_comment integer, points_proofed integer, points_gift integer, points_check integer, points_waitertime integer, points_usertime integer, points_when integer, points_time integer, points_del integer)").then(function() {

            if(data[0].newusr == '1') {

                // DB POINTS
              var pointsArr = data[0].pointsArr;
              
              if(pointsArr.length > 0) {

                var id = 0;

                function pointsArrFuncIns(id) {

                  var points_id = pointsArr[id]['points_id'];
                  var points_user = pointsArr[id]['points_user'];
                  var points_bill = pointsArr[id]['points_bill'];
                  var points_discount = pointsArr[id]['points_discount'];
                  var points_points = pointsArr[id]['points_points'];
                  var points_got_spend = pointsArr[id]['points_got_spend'];
                  var points_waiter = pointsArr[id]['points_waiter'];
                  var points_institution = pointsArr[id]['points_institution'];
                  var points_office = pointsArr[id]['points_office'];
                  var points_status = pointsArr[id]['points_status'];
                  var points_comment = pointsArr[id]['points_comment'];
                  var points_proofed = pointsArr[id]['points_proofed'];
                  var points_gift = pointsArr[id]['points_gift'];
                  var points_check = pointsArr[id]['points_check'];
                  var points_waitertime = pointsArr[id]['points_waitertime'];
                  var points_usertime = pointsArr[id]['points_usertime'];
                  var points_when = pointsArr[id]['points_when'];
                  var points_time = pointsArr[id]['points_time'];
                  var points_del = pointsArr[id]['points_del'];

                  var pointsIns = "INSERT INTO points (points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_office, points_status, points_comment, points_proofed, points_gift, points_check, points_waitertime, points_usertime, points_when, points_time, points_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                  $cordovaSQLite.execute($rootScope.db, pointsIns, [points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_office, points_status, points_comment, points_proofed, points_gift, points_check, points_waitertime, points_usertime, points_when, points_time, points_del]).then(function() {
                    id++;
                    if(id < pointsArr.length) {
                      pointsArrFuncIns(id);
                    }
                  }, function() {});

                }
                pointsArrFuncIns(0);

              }

            }

          }, function() {});

          // DB WALLET
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS wallet (wallet_id integer primary key, wallet_user integer, wallet_institution integer, wallet_total integer, wallet_warn integer, wallet_when integer, wallet_del integer)").then(function() {
            if(data[0].newusr == '1') {
                // DB WALLET
              var walletArr = data[0].walletArr;

              if(walletArr.length > 0) {

                var wallet_user = data[0].walletArr[0]['wallet_user'];
                var wallet_institution = data[0].walletArr[0]['wallet_institution'];
                var wallet_total = data[0].walletArr[0]['wallet_total'];
                var wallet_warn = data[0].walletArr[0]['wallet_warn'];
                var wallet_when = data[0].walletArr[0]['wallet_when'];
                var wallet_del = data[0].walletArr[0]['wallet_del'];

                var walletIns = "INSERT INTO wallet (wallet_user, wallet_institution, wallet_total, wallet_warn, wallet_when, wallet_del) VALUES (?,?,?,?,?,?)";
                $cordovaSQLite.execute($rootScope.db, walletIns, [wallet_user, wallet_institution, wallet_total, wallet_warn, wallet_when, wallet_del]).then(function() {}, function() {});

              }
            }
          }, function() {});

          // DB GOODS
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS goods (goods_id integer primary key, goods_name text, goods_desc text, goods_pic text, goods_type integer, goods_institution integer, goods_when integer, goods_del integer)").then(function() {
            if(data[0].newusr == '1') {
              // DB GOODS
              var goodsArr = data[0].goodsArr;

              if(goodsArr.length > 0) {

                var id = 0;

                function goodsArrFuncIns(id) {

                  var goods_id = goodsArr[id]['goods_id'];
                  var goods_when = goodsArr[id]['goods_when'];
                  var goods_name = goodsArr[id]['goods_name'];
                  var goods_desc = goodsArr[id]['goods_desc'];
                  var goods_pic = goodsArr[id]['goods_pic'];
                  var goods_type = goodsArr[id]['goods_type'];
                  var goods_institution = goodsArr[id]['goods_institution'];
                  var goods_del = goodsArr[id]['goods_del'];

                  var goodsIns = "INSERT INTO goods (goods_id, goods_name, goods_desc, goods_pic, goods_type, goods_institution, goods_when, goods_del) VALUES (?,?,?,?,?,?,?,?)";
                  $cordovaSQLite.execute($rootScope.db, goodsIns, [goods_id, goods_name, goods_desc, goods_pic, goods_type, goods_institution, goods_when, goods_del]).then(function() {
                    id++;
                    if(id < goodsArr.length) {
                      goodsArrFuncIns(id);
                    }
                  }, function() {});

                }
                goodsArrFuncIns(0);

              }
            }
          }, function() {});

          // DB CATEGORIES
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS categories (cat_id integer primary key, cat_xid text, cat_name text, cat_desc text, cat_pic text, cat_ingr integer, cat_order integer, cat_institution integer, cat_when integer, cat_del integer)").then(function() {
              if(data[0].newusr == '1') {
                // DB CATEGORIES
                var catArr = data[0].catArr;

                if(catArr.length > 0) {

                  var id = 0;

                  function catArrFuncIns(id) {

                    var cat_id = catArr[id]['cat_id'];
                    var cat_xid = catArr[id]['cat_xid'];
                    var cat_name = catArr[id]['cat_name'];
                    var cat_desc = catArr[id]['cat_desc'];
                    var cat_pic = catArr[id]['cat_pic'];
                    var cat_ingr = catArr[id]['cat_ingr'];
                    var cat_order = catArr[id]['cat_order'];
                    var cat_institution = catArr[id]['cat_institution'];
                    var cat_when = catArr[id]['cat_when'];
                    var cat_del = catArr[id]['cat_del'];

                    var catsIns = "INSERT INTO categories (cat_id, cat_xid, cat_name, cat_desc, cat_pic, cat_ingr, cat_order, cat_institution, cat_when, cat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, catsIns, [cat_id, cat_xid, cat_name, cat_desc, cat_pic, cat_ingr, cat_order, cat_institution, cat_when, cat_del]).then(function() {
                      id++;
                      if(id < catArr.length) {
                        catArrFuncIns(id);
                      }
                    }, function() {});

                  }
                  catArrFuncIns(0);

                }
              }
          }, function() {});

          // DB MENUE
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS menue (menue_id integer primary key, menue_xid text, menue_cat_xid text, menue_cat integer, menue_name text, menue_desc text, menue_size integer, menue_cost integer, menue_costs text, menue_ingr text, menue_addition text, menue_addition_auto integer, menue_package text, menue_weight integer, menue_interval integer, menue_discount integer, menue_action text, menue_code text, menue_pic text, menue_institution integer, menue_when integer, menue_del integer)").then(function() {
            if(data[0].newusr == '1') {
              // DB MENU
              var menueArr = data[0].menueArr;
              
              if(menueArr.length > 0) {

                var id = 0;

                function menueArrFuncIns(id) {

                  var menue_id = menueArr[id]['menue_id'];
                  var menue_xid = menueArr[id]['menue_xid'];
                  var menue_cat_xid = menueArr[id]['menue_cat_xid'];
                  var menue_cat = menueArr[id]['menue_cat'];
                  var menue_name = menueArr[id]['menue_name'];
                  var menue_desc = menueArr[id]['menue_desc'];
                  var menue_size = menueArr[id]['menue_size'];
                  var menue_cost = menueArr[id]['menue_cost'];
                  var menue_costs = menueArr[id]['menue_costs'];
                  var menue_ingr = menueArr[id]['menue_ingr'];
                  var menue_addition = menueArr[id]['menue_addition'];
                  var menue_addition_auto = menueArr[id]['menue_addition_auto'];
                  var menue_package = menueArr[id]['menue_package'];
                  var menue_weight = menueArr[id]['menue_weight'];
                  var menue_interval = menueArr[id]['menue_interval'];
                  var menue_discount = menueArr[id]['menue_discount'];
                  var menue_action = menueArr[id]['menue_action'];
                  var menue_code = menueArr[id]['menue_code'];
                  var menue_pic = menueArr[id]['menue_pic'];
                  var menue_institution = menueArr[id]['menue_institution'];
                  var menue_when = menueArr[id]['menue_when'];
                  var menue_del = menueArr[id]['menue_del'];

                  var menueIns = "INSERT INTO menue (menue_id, menue_xid, menue_cat_xid, menue_cat, menue_name, menue_desc, menue_size, menue_cost, menue_costs, menue_ingr, menue_addition, menue_addition_auto, menue_package, menue_weight, menue_interval, menue_discount, menue_action, menue_code, menue_pic, menue_institution, menue_when, menue_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                  $cordovaSQLite.execute($rootScope.db, menueIns, [menue_id, menue_xid, menue_cat_xid, menue_cat, menue_name, menue_desc, menue_size, menue_cost, menue_costs, menue_ingr, menue_addition,menue_addition_auto, menue_package, menue_weight, menue_interval, menue_discount, menue_action, menue_code, menue_pic, menue_institution, menue_when, menue_del]).then(function() {
                    id++;
                    if(id < menueArr.length) {
                      menueArrFuncIns(id);
                    }
                  }, function() {});

                }
                menueArrFuncIns(0);

              }
            }
          }, function() {});
      
          // DB GIFTS
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS gifts (gifts_id integer primary key, gifts_name text, gifts_desc text, gifts_points integer, gifts_pic text, gifts_institution integer, gifts_when integer, gifts_del integer)").then(function() {
              if(data[0].newusr == '1') {
                // DB GIFTS
                var giftsArr = data[0].giftsArr;
                
                if(giftsArr.length > 0) {

                  var id = 0;

                  function giftsArrFuncIns(id) {

                    var gifts_id = giftsArr[id]['gifts_id'];
                    var gifts_name = giftsArr[id]['gifts_name'];
                    var gifts_desc = giftsArr[id]['gifts_desc'];
                    var gifts_points = giftsArr[id]['gifts_points'];
                    var gifts_pic = giftsArr[id]['gifts_pic'];
                    var gifts_institution = giftsArr[id]['gifts_institution'];
                    var gifts_when = giftsArr[id]['gifts_when'];
                    var gifts_del = giftsArr[id]['gifts_del'];

                    var giftsIns = "INSERT INTO gifts (gifts_id, gifts_name, gifts_desc, gifts_points, gifts_pic, gifts_institution, gifts_when, gifts_del) VALUES (?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, giftsIns, [gifts_id, gifts_name, gifts_desc, gifts_points, gifts_pic, gifts_institution, gifts_when, gifts_del]).then(function() {
                      id++;
                      if(id < giftsArr.length) {
                      giftsArrFuncIns(id);
                      }
                    }, function() {});

                  }
                  giftsArrFuncIns(0);

                }
              }
          }, function() {});

          // DB INGREDIENTS
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS ingredients (ingr_id integer primary key, ingr_xid text, ingr_name text, ingr_desc text, ingr_cat integer, ingr_code text, ingr_pic text, ingr_size integer, ingr_cost integer, ingr_institution integer, ingr_when integer, ingr_del)").then(function() {
              if(data[0].newusr == '1') {
                // DB INGREDIENTS
                var ingrArr = data[0].ingrArr;

                if(ingrArr.length > 0) {

                  var id = 0;

                  function ingrArrFuncIns(id) {

                    var ingr_id = ingrArr[id]['ingr_id'];
                    var ingr_xid = ingrArr[id]['ingr_xid'];
                    var ingr_name = ingrArr[id]['ingr_name'];
                    var ingr_desc = ingrArr[id]['ingr_desc'];
                    var ingr_cat = ingrArr[id]['ingr_cat'];
                    var ingr_code = ingrArr[id]['ingr_code'];
                    var ingr_pic = ingrArr[id]['ingr_pic'];
                    var ingr_size = ingrArr[id]['ingr_size'];
                    var ingr_cost = ingrArr[id]['ingr_cost'];
                    var ingr_institution = ingrArr[id]['ingr_institution'];
                    var ingr_when = ingrArr[id]['ingr_when'];
                    var ingr_del = ingrArr[id]['ingr_del'];

                    var ingrsIns = "INSERT INTO ingredients (ingr_id, ingr_xid, ingr_name, ingr_desc, ingr_cat, ingr_code, ingr_pic, ingr_size, ingr_cost, ingr_institution, ingr_when, ingr_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, ingrsIns, [ingr_id, ingr_xid, ingr_name, ingr_desc, ingr_cat, ingr_code, ingr_pic, ingr_size, ingr_cost, ingr_institution, ingr_when, ingr_del]).then(function() {
                      id++;
                      if(id < ingrArr.length) {
                      ingrArrFuncIns(id);
                      }
                    }, function() {});

                  }
                  ingrArrFuncIns(0);

                }
              }
          }, function() {});

          // DB NEWS
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS news (news_id integer primary key, news_name text, news_message text, news_pic text, news_institution integer, news_state integer, news_menue_id integer, news_cost integer, news_user integer, news_used integer, news_begin integer, news_end integer, news_when integer, news_del integer)").then(function() {
              if(data[0].newusr == '1') {
                // DB NEWS
                var newsArr = data[0].newsArr;

                if(newsArr.length > 0) {

                  var id = 0;

                  function newsArrFuncIns(id) {

                    var news_id = newsArr[id]['news_id'];
                    var news_state = newsArr[id]['news_state'];
                    var news_menue_id = 0;
                    var news_cost = 0;
                    var news_user = 0;
                    var news_used = 0;
                    var news_begin = 0;
                    var news_end = 0;
                    var news_when = newsArr[id]['news_when'];
                    var news_del = newsArr[id]['news_del'];
                    var news_name = 0;
                    var news_message = 0;
                    var news_pic = 0;
                    var news_institution = 0;

                    if(news_state == '1') {
                      news_name = newsArr[id]['news_name'];
                      news_message = newsArr[id]['news_message'];
                      news_pic = newsArr[id]['news_pic'];
                      news_institution = newsArr[id]['news_institution'];
                      news_menue_id = newsArr[id]['news_menue_id'];
                      news_cost = newsArr[id]['news_cost'];
                      news_user = newsArr[id]['news_user'];
                      news_used = newsArr[id]['news_used'];
                      news_begin = newsArr[id]['news_begin'];
                      news_end = newsArr[id]['news_end'];
                    }

                    var queryNews = "SELECT * FROM news WHERE news_id = ?";
                    $cordovaSQLite.execute($rootScope.db, queryNews, [news_id]).then(function(suc) {
                      if(suc.rows.length > 0) {
                        var newsUpd = "UPDATE news SET news_name=?, news_message=?, news_pic=?, news_institution=?, news_state=?, news_menue_id=?, news_cost=?, news_user=?, news_used=?, news_begin=?, news_end=?, news_when=?, news_del=? WHERE news_id=?";
                        $cordovaSQLite.execute($rootScope.db, newsUpd, [news_name, news_message, news_pic, news_institution, news_state, news_menue_id, news_cost, news_user, news_used, news_begin, news_end, news_when, news_del, news_id]).then(function() {
                          id++;
                          if(id < newsArr.length) {
                          newsArrFuncIns(id);
                          }
                        }, function() {});
                      }
                      else {
                        var newsIns = "INSERT INTO news (news_id, news_name, news_message, news_pic, news_institution, news_state, news_menue_id, news_cost, news_user, news_used, news_begin, news_end, news_when, news_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                        $cordovaSQLite.execute($rootScope.db, newsIns, [news_id, news_name, news_message, news_pic, news_institution, news_state, news_menue_id, news_cost, news_user, news_used, news_begin, news_end, news_when, news_del]).then(function() {
                          id++;
                          if(id < newsArr.length) {
                          newsArrFuncIns(id);
                          }
                        }, function() {});
                      }
                    }, function() {});

                  }
                  newsArrFuncIns(0);

                }
              }
          }, function() {});

          // DB REVIEWS
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS reviews (reviews_id integer primary key, reviews_from integer, reviews_to integer, reviews_message text, reviews_pic text, reviews_institution integer, reviews_answered integer, reviews_opened integer, reviews_when integer, reviews_del integer)").then(function() {
              if(data[0].newusr == '1') {
                // DB REVIEWS
                var reviewsArr = data[0].reviewsArr;

                if(reviewsArr.length > 0) {

                  var id = 0;

                  function reviewsArrFuncIns(id) {

                    var reviews_id = reviewsArr[id]['reviews_id'];
                    var reviews_from = reviewsArr[id]['reviews_from'];
                    var reviews_to = reviewsArr[id]['reviews_to'];
                    var reviews_message = reviewsArr[id]['reviews_message'];
                    var reviews_pic = reviewsArr[id]['reviews_pic'];
                    var reviews_institution = reviewsArr[id]['reviews_institution'];
                    var reviews_answered = reviewsArr[id]['reviews_answered'];
                    var reviews_opened = reviewsArr[id]['reviews_opened'];
                    var reviews_when = reviewsArr[id]['reviews_when'];
                    var reviews_del = reviewsArr[id]['reviews_del'];

                    var newsIns = "INSERT INTO reviews (reviews_id, reviews_from, reviews_to, reviews_message, reviews_pic, reviews_institution, reviews_answered, reviews_opened, reviews_when, reviews_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, newsIns, [reviews_id, reviews_from, reviews_to, reviews_message, reviews_pic, reviews_institution, reviews_answered, reviews_opened, reviews_when, reviews_del]).then(function() {
                      id++;
                      if(id < reviewsArr.length) {
                      reviewsArrFuncIns(id);
                      }
                    }, function() {});

                  }
                  reviewsArrFuncIns(0);

                }
              }
          }, function() {});

          // DB ASKS
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS asks (asks_id integer primary key, asks_name text, asks_message text, asks_type integer, asks_chained integer, asks_active integer, asks_img text, asks_answ text, asks_yes integer, asks_no integer, asks_reply text, asks_institution integer, asks_when integer, asks_del integer)").then(function() {
              if(data[0].newusr == '1') {
                // DB ASKS
                var asksArr = data[0].asksArr;

                if(asksArr.length > 0) {

                  var id = 0;

                  function asksArrFuncIns(id) {

                    var asks_id = asksArr[id]['asks_id'];
                    var asks_name = asksArr[id]['asks_name'];
                    var asks_message = asksArr[id]['asks_message'];
                    var asks_type = asksArr[id]['asks_type'];
                    var asks_chained = asksArr[id]['asks_chained'];
                    var asks_active = asksArr[id]['asks_active'];
                    var asks_img = asksArr[id]['asks_img'];
                    var asks_answ = asksArr[id]['asks_answ'];
                    var asks_yes = asksArr[id]['asks_yes'];
                    var asks_no = asksArr[id]['asks_no'];
                    var asks_institution = asksArr[id]['asks_institution'];
                    var asks_when = asksArr[id]['asks_when'];
                    var asks_del = asksArr[id]['asks_del'];

                    var asksIns = "INSERT INTO asks (asks_id, asks_name, asks_message, asks_type, asks_chained, asks_active, asks_img, asks_answ, asks_yes, asks_no, asks_reply, asks_institution, asks_when, asks_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, asksIns, [asks_id, asks_name, asks_message, asks_type, asks_chained, asks_active, asks_img, asks_answ, asks_yes, asks_no, '0', asks_institution, asks_when, asks_del]).then(function() {
                      id++;
                      if(id < asksArr.length) {
                        asksArrFuncIns(id);
                      }
                    }, function() {});

                  }
                  asksArrFuncIns(0);

                }
              }
          }, function() {});

          // DB CHAT
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS chat (chat_id integer primary key, chat_from integer, chat_to integer, chat_name text, chat_message text, chat_read integer, chat_institution integer, chat_answered integer, chat_when integer, chat_del integer)").then(function() {
              if(data[0].newusr == '1') {
                // DB CHAT
                var chatArr = data[0].chatArr;
                
                if(chatArr.length > 0) {

                  var id = 0;

                  function chatArrFuncIns(id) {

                    var chat_id = chatArr[id]['chat_id'];
                    var chat_from = chatArr[id]['chat_from'];
                    var chat_to = chatArr[id]['chat_to'];
                    var chat_name = chatArr[id]['chat_name'];
                    var chat_message = chatArr[id]['chat_message'];
                    var chat_read = chatArr[id]['chat_read'];
                    var chat_institution = chatArr[id]['chat_institution'];
                    var chat_answered = chatArr[id]['chat_answered'];
                    var chat_when = chatArr[id]['chat_when'];
                    var chat_del = chatArr[id]['chat_del'];

                    var chatIns = "INSERT INTO chat (chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, chatIns, [chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del]).then(function() {
                      id++;
                      if(id < chatArr.length) {
                      chatArrFuncIns(id);
                      }
                    }, function() {});

                  }
                  chatArrFuncIns(0);

                }
              }
          }, function() {});

          // DB ORDER
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS ordering (order_id integer primary key, order_user text, order_user_name_phone text, order_user_surname_phone text, order_user_middlename_phone text, order_user_adress_phone text, order_user_comment_phone text, order_name text, order_name_phone text, order_user_phone_phone text, order_user_email_phone text, order_desc text, order_worker integer, order_worker_name_phone text, order_worker_pic_phone text, order_worker_profession_phone text, order_reminder_phone text, order_institution integer, order_office integer, order_room integer, order_bill integer, order_goods integer, order_cats integer, order_order text, order_status integer, order_start integer, order_start_name_phone text, order_end integer, order_allday integer, order_mobile integer, order_mobile_confirm text, order_when integer, order_del integer)").then(function() {
              if(data[0].newusr == '1') {

              }
          }, function() {});

          // DB PROFESSIONS
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS professions (prof_id integer primary key, prof_name text, prof_desc text, prof_institution integer, prof_when integer, prof_del integer)").then(function() {
              if(data[0].newusr == '1') {

              }
          }, function() {});

          // DB OFFICES
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS organizations_office (office_id integer primary key, office_name text, office_start text, office_stop text, office_bus_hours text, office_country integer, office_city text, office_adress text, office_lat text, office_lon text, office_desc text, office_timezone integer, office_tel text, office_tel_n text, office_fax integer, office_mob integer, office_email text, office_pwd text, office_orders integer, office_skype text, office_site text, office_tax_id text, office_logo text, office_institution integer, office_log integer, office_reg integer, office_del integer)").then(function() {
              if(data[0].newusr == '1') {

              }
          }, function() {});

          // DB SCHEDULE
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS schedule (schedule_id integer primary key, schedule_employee integer, schedule_menue integer, schedule_office integer, schedule_start integer, schedule_stop integer, schedule_institution integer, schedule_when integer, schedule_del integer)").then(function() {
              if(data[0].newusr == '1') {

              }
          }, function() {});

          // DB FIFTHGIFT
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS fifthgift (fifth_id integer primary key, fifth_name text, fifth_desc text, fifth_user integer, fifth_menue_id integer, fifth_bill integer, fifth_got_spend integer, fifth_institution integer, fifth_office integer, fifth_when integer, fifth_del integer)").then(function() {
            
            if(data[0].newusr == '1') {

              var fifthArr = data[0].fifthArr;
              
              if(fifthArr.length > 0) {

                var id = 0;

                function fifthArrFuncIns(id) {

                  var fifth_id = fifthArr[id]['fifth_id'];
                  var fifth_name = fifthArr[id]['fifth_name'];
                  var fifth_desc = fifthArr[id]['fifth_desc'];
                  var fifth_user = fifthArr[id]['fifth_user'];
                  var fifth_menue_id = fifthArr[id]['fifth_menue_id'];
                  var fifth_bill = fifthArr[id]['fifth_bill'];
                  var fifth_got_spend = fifthArr[id]['fifth_got_spend'];
                  var fifth_institution = fifthArr[id]['fifth_institution'];
                  var fifth_office = fifthArr[id]['fifth_office'];
                  var fifth_when = fifthArr[id]['fifth_when'];
                  var fifth_del = fifthArr[id]['fifth_del'];
        
                  var queryFifth = "SELECT * FROM fifthgift WHERE fifth_id = ?";
                  $cordovaSQLite.execute($rootScope.db, queryFifth, [fifth_id]).then(function(suc) {
                  if(suc.rows.length > 0) {
                      var fifthUpd = "UPDATE fifthgift SET fifth_name=?, fifth_desc=?, fifth_user=?, fifth_menue_id=?, fifth_bill=?, fifth_got_spend=?, fifth_institution=?, fifth_office=?, fifth_when=?, fifth_del=? WHERE fifth_id=?";
                      $cordovaSQLite.execute($rootScope.db, fifthUpd, [fifth_name, fifth_desc, fifth_user, fifth_menue_id, fifth_bill, fifth_got_spend, fifth_institution, fifth_office, fifth_when, fifth_del, fifth_id]).then(function() {
                        id++;
                        if(id < fifthArr.length) {
                          fifthArrFuncIns(id);
                        }
                      }, function() {});
                    }
                    else {
                      var fifthIns = "INSERT INTO fifthgift (fifth_id, fifth_name, fifth_desc, fifth_user, fifth_menue_id, fifth_bill, fifth_got_spend, fifth_institution, fifth_office, fifth_when, fifth_del) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                      $cordovaSQLite.execute($rootScope.db, fifthIns, [fifth_id, fifth_name, fifth_desc, fifth_user, fifth_menue_id, fifth_bill, fifth_got_spend, fifth_institution, fifth_office, fifth_when, fifth_del]).then(function() {
                        id++;
                        if(id < fifthArr.length) {
                          fifthArrFuncIns(id);
                        }
                      }, function() {});
                    }
                  }, function() {});

                }
                fifthArrFuncIns(0);

              }

            }

          }, function() {});

          // DB RESERVATION
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS reservation (reservation_id integer primary key, reservation_userid integer, reservation_surname text, reservation_name text, reservation_middlename text, reservation_mobile integer, reservation_date integer, reservation_time integer, reservation_comment text, reservation_institution integer, reservation_when integer, reservation_del integer)").then(function() {
            if(data[0].newusr == '1') {

              var reservArr = data[0].reservArr;
              
              if(reservArr.length > 0) {

                var id = 0;

                function reservArrFuncIns(id) {

                  var reservation_id = reservArr[id]['reservation_id'];
                  var reservation_userid = reservArr[id]['reservation_userid'];
                  var reservation_surname = reservArr[id]['reservation_surname'];
                  var reservation_name = reservArr[id]['reservation_name'];
                  var reservation_middlename = reservArr[id]['reservation_middlename'];
                  var reservation_mobile = reservArr[id]['reservation_mobile'];
                  var reservation_date = reservArr[id]['reservation_date'];
                  var reservation_time = reservArr[id]['reservation_time'];
                  var reservation_comment = reservArr[id]['reservation_comment'];
                  var reservation_institution = reservArr[id]['reservation_institution'];
                  var reservation_when = reservArr[id]['reservation_when'];
                  var reservation_del = reservArr[id]['reservation_del'];
        
                  var queryReserv = "SELECT * FROM reservation WHERE reservation_id = ?";
                  $cordovaSQLite.execute($rootScope.db, queryReserv, [reservation_id]).then(function(suc) {
                  if(suc.rows.length > 0) {
                      var fifthUpd = "UPDATE fifthgift SET reservation_userid=?, reservation_surname=?, reservation_name=?, reservation_middlename=?, reservation_mobile=?, reservation_date=?, reservation_time=?, reservation_comment=?, reservation_institution=?, reservation_when=?, reservation_del=? WHERE reservation_id=?";
                      $cordovaSQLite.execute($rootScope.db, fifthUpd, [reservation_userid, reservation_surname, reservation_name, reservation_middlename, reservation_mobile, reservation_date, reservation_time, reservation_comment, reservation_institution, reservation_when, reservation_del, reservation_id]).then(function() {
                        id++;
                        if(id < reservArr.length) {
                          reservArrFuncIns(id);
                        }
                      }, function() {});
                    }
                    else {
                      var reservIns = "INSERT INTO reservation (reservation_id, reservation_userid, reservation_surname, reservation_name, reservation_middlename, reservation_mobile, reservation_date, reservation_time, reservation_comment, reservation_institution, reservation_when, reservation_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                      $cordovaSQLite.execute($rootScope.db, reservIns, [reservation_id, reservation_userid, reservation_surname, reservation_name, reservation_middlename, reservation_mobile, reservation_date, reservation_time, reservation_comment, reservation_institution, reservation_when, reservation_del]).then(function() {
                        id++;
                        if(id < reservArr.length) {
                          reservArrFuncIns(id);
                        }
                      }, function() {});
                    }
                  }, function() {});

                }
                reservArrFuncIns(0);

              }

            }

          }, function() {});

          // DB ORGANIZATION
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS organizations (org_id integer primary key, org_name text, org_country integer, org_city integer, org_adress text, org_timezone integer, org_tel text, org_fax text, org_mob text, org_email text, org_skype text, org_site text, org_tax_id text, org_logo text, org_sound integer, org_development integer, org_money_points integer, org_starting_points integer, org_money_percent integer, org_points_points integer, org_promo_points_owner integer, org_promo_points_scan_owner integer, org_promo_points_involved integer, org_promo_points_scan_involved integer, org_max_points integer, org_risk_summ integer, org_autoaprove integer, org_appvers integer, org_appurl text, org_log integer, org_reg integer, org_del integer)").then(function() {
            if(data[0].newusr == '1') {

              // DB ORGANIZATION
              var orgArr = data[0].orgArr;
              
              if(orgArr.length > 0) {

                var id = 0;

                function orgArrFuncIns(id) {

                  var org_id = orgArr[id]['org_id'];
                  var org_name = orgArr[id]['org_name'];
                  var org_country = orgArr[id]['org_country'];
                  var org_city = orgArr[id]['org_city'];
                  var org_adress = orgArr[id]['org_adress'];
                  var org_timezone = orgArr[id]['org_timezone'];
                  var org_tel = orgArr[id]['org_tel'];
                  var org_fax = orgArr[id]['org_fax'];
                  var org_mob = orgArr[id]['org_mob'];
                  var org_email = orgArr[id]['org_email'];
                  var org_skype = orgArr[id]['org_skype'];
                  var org_site = orgArr[id]['org_site'];
                  var org_tax_id = orgArr[id]['org_tax_id'];
                  var org_logo = orgArr[id]['org_logo'];
                  var org_sound = orgArr[id]['org_sound'];
                  var org_development = orgArr[id]['org_development'];
                  var org_money_points = orgArr[id]['org_money_points'];
                  var org_starting_points = orgArr[id]['org_starting_points'];
                  var org_money_percent = orgArr[id]['org_money_percent'];
                  var org_points_points = orgArr[id]['org_points_points'];
                  var org_promo_points_owner = orgArr[id]['org_promo_points_owner'];
                  var org_promo_points_scan_owner = orgArr[id]['org_promo_points_scan_owner'];
                  var org_promo_points_involved = orgArr[id]['org_promo_points_involved'];
                  var org_promo_points_scan_involved = orgArr[id]['org_promo_points_scan_involved'];
                  var org_max_points = orgArr[id]['org_max_points'];
                  var org_risk_summ = orgArr[id]['org_risk_summ'];
                  var org_autoaprove = orgArr[id]['org_autoaprove'];
                  var org_appvers = orgArr[id]['org_appvers'];
                  var org_appurl = orgArr[id]['org_appurl'];
                  var org_log = orgArr[id]['org_log'];
                  var org_reg = orgArr[id]['org_reg'];
                  var org_del = orgArr[id]['org_del'];
        
                  var queryOrg = "SELECT * FROM organizations WHERE org_id = ?";
                  $cordovaSQLite.execute($rootScope.db, queryOrg, [org_id]).then(function(suc) {
                    if(suc.rows.length > 0) {
                      var orgUpd = "UPDATE organizations SET org_name=?, org_country=?, org_city=?, org_adress=?, org_timezone=?, org_tel=?, org_fax=?, org_mob=?, org_email=?, org_skype=?, org_site=?, org_tax_id=?, org_logo=?, org_sound=?, org_development=?, org_money_points=?, org_starting_points=?, org_money_percent=?, org_points_points=?, org_promo_points_owner=?, org_promo_points_scan_owner=?, org_promo_points_involved=?, org_promo_points_scan_involved=?, org_max_points=?, org_risk_summ=?, org_autoaprove=?, org_appvers=?, org_appurl=?, org_log=?, org_reg=?, org_del=? WHERE org_id=?";
                      $cordovaSQLite.execute($rootScope.db, orgUpd, [org_name, org_country, org_city, org_adress, org_timezone, org_tel, org_fax, org_mob, org_email, org_skype, org_site, org_tax_id, org_logo, org_sound, org_development, org_money_points, org_starting_points, org_money_percent, org_points_points, org_promo_points_owner, org_promo_points_scan_owner, org_promo_points_involved, org_promo_points_scan_involved, org_max_points, org_risk_summ, org_autoaprove, org_appvers, org_appurl, org_log, org_reg, org_del, org_id]).then(function() {
                        id++;
                        if(id < orgArr.length) {
                          orgArrFuncIns(id);
                        }
                      }, function() {});
                    }
                    else {
                      var orgIns = "INSERT INTO organizations (org_id, org_name, org_country, org_city, org_adress, org_timezone, org_tel, org_fax, org_mob, org_email, org_skype, org_site, org_tax_id, org_logo, org_sound, org_development, org_money_points, org_starting_points, org_money_percent, org_points_points, org_promo_points_owner, org_promo_points_scan_owner, org_promo_points_involved, org_promo_points_scan_involved, org_max_points, org_risk_summ, org_autoaprove, org_appvers, org_appurl, org_log, org_reg, org_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                      $cordovaSQLite.execute($rootScope.db, orgIns, [org_id, org_name, org_country, org_city, org_adress, org_timezone, org_tel, org_fax, org_mob, org_email, org_skype, org_site, org_tax_id, org_logo, org_sound, org_development, org_money_points, org_starting_points, org_money_percent, org_points_points, org_promo_points_owner, org_promo_points_scan_owner, org_promo_points_involved, org_promo_points_scan_involved, org_max_points, org_risk_summ, org_autoaprove, org_appvers, org_appurl, org_log, org_reg, org_del]).then(function() {
                        id++;
                        if(id < orgArr.length) {
                          orgArrFuncIns(id);
                        }
                      }, function() {});
                    }
                  }, function() {});

                }
                orgArrFuncIns(0);

              }

            }
          }, function() {});

          // DB ROOMS
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS rooms (room_id integer primary key, room_name text, room_desc text, room_employee integer, room_menue_exe text, room_priority integer, room_institution integer, room_office integer, room_upd integer, room_when integer, room_del integer)").then(function() {}, function() {});

          // data = JSON.parse(e.data);
          
          // alert(JSON.stringify(data));

        };
        
        $rootScope.getUpdate = function(data, userupd) {
          // console.log(JSON.stringify(data))
          if(data[0].check == '1') {

            // USER LOG
            if(data[0].user_log > '0') {

              var usrlog = data[0].user_log;

              var queryUsrLog = "SELECT * FROM users WHERE user_id=? AND user_log=? LIMIT 1";
              $cordovaSQLite.execute($rootScope.db, queryUsrLog, [1, usrlog]).then(function(suc) {
                if(suc.rows.length == 0) {
                  
                  var queryUsrLogUpd = "UPDATE users SET user_log=? WHERE user_id=?";
                  $cordovaSQLite.execute($rootScope.db, queryUsrLogUpd, [usrlog, 1]).then(function(suc) {}, function() {});

                }
              }, function() {});

            }

            // USER DISCOUNT
            if(data[0].user_discount > '0') {

              var usrdiscount = data[0].user_discount;

              var queryUsrDiscount = "SELECT * FROM users WHERE user_id=? AND user_discount=? LIMIT 1";
              $cordovaSQLite.execute($rootScope.db, queryUsrDiscount, [1, usrdiscount]).then(function(suc) {
                if(suc.rows.length == 0) {
                  
                  var queryUsrDiscountUpd = "UPDATE users SET user_discount=? WHERE user_id=?";
                  $cordovaSQLite.execute($rootScope.db, queryUsrDiscountUpd, [usrdiscount, 1]).then(function(suc) {

                    $ionicPopup.alert({
                      title: 'Внимание',
                      template: 'Дисконтная карта добавленна!'
                    });

                  }, function() {});

                }
              }, function() {});

            }

            // USER WORK POSITION
            if(data[0].user_work_pos >= '2') {

              // alert(data[0].user_discount);

              var user_work_pos = data[0].user_work_pos;

              var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? LIMIT 1";
              $cordovaSQLite.execute($rootScope.db, queryUsrWP, [1, user_work_pos]).then(function(suc) {
                if(suc.rows.length == 0) {
                  
                  var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                  $cordovaSQLite.execute($rootScope.db, queryUsrWPUpd, [user_work_pos, 1]).then(function(suc) {

                    $ionicPopup.alert({
                      title: 'Внимание',
                      template: 'Вы заблокированны!'
                    });

                  }, function() {});

                }
              }, function() {});

            }
            // IF NO CONNECTION TO DB OR SERVER IS DOWN
            else if(data[0].user_work_pos != '1000') {

              var user_work_pos = data[0].user_work_pos;

              var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? LIMIT 1";
              $cordovaSQLite.execute($rootScope.db, queryUsrWP, [1, user_work_pos]).then(function(suc) {
                if(suc.rows.length == 0) {
                  
                  var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                  $cordovaSQLite.execute($rootScope.db, queryUsrWPUpd, [user_work_pos, 1]).then(function(suc) {

                    $ionicPopup.alert({
                      title: 'Внимание',
                      template: 'Вы разблокированны!'
                    });

                  }, function() {});

                }
              }, function() {});

            }
            
            // USER DATA UPDATE IF ON PHONE IS OLDER THAN ONLINE
            if(data[0].user_upd > userupd.user_upd && data[0].user_id > 0) {

              var user_id = data[0].user_id;
              var user_name = data[0].user_name;
              var user_surname = data[0].user_surname;
              var user_middlename = data[0].user_middlename;
              var user_email = data[0].user_email;
              var user_email_confirm = data[0].user_email_confirm;
              var user_pwd = data[0].user_pwd;
              var user_tel = data[0].user_tel;
              var user_mob_confirm = data[0].user_mob_confirm;
              var user_mob = data[0].user_mob;
              var user_work_pos = data[0].user_work_pos;
              var user_menue_exe = data[0].user_menue_exe;
              var user_institution = data[0].user_institution;
              var user_office = data[0].user_office;
              var user_pic = data[0].user_pic;
              var user_gender = data[0].user_gender;
              var user_birthday = data[0].user_birthday;
              var user_country = data[0].user_country;
              var user_region = data[0].user_region;
              var user_city = data[0].user_city;
              var user_adress = data[0].user_adress;
              var user_install_where = data[0].user_install_where;
              var user_log_key = data[0].user_log_key;
              var user_gcm = data[0].user_gcm;
              var user_device = data[0].user_device;
              var user_device_id = data[0].user_device_id;
              var user_device_version = data[0].user_device_version;
              var user_device_os = data[0].user_device_os;
              var user_discount = data[0].user_discount;
              var user_promo = data[0].user_promo;
              var user_conf_req = data[0].user_conf_req;
              var user_log = data[0].user_log;
              var user_upd = data[0].user_upd;
              var user_reg = data[0].user_reg;
              var user_del = data[0].user_del;

              var queryUsrLog = "SELECT * FROM users WHERE user_id=? LIMIT 1";
              $cordovaSQLite.execute($rootScope.db, queryUsrLog, [1]).then(function(suc) {
                if(suc.rows.length > 0) {
                  
                  var queryUsrDataUpd = "UPDATE users SET user_real_id=?, user_name=?, user_surname=?, user_middlename=?, user_email=?, user_email_confirm=?, user_pwd=?, user_tel=?, user_mob_confirm=?, user_mob=?, user_work_pos=?, user_menue_exe=?, user_institution=?, user_office=?, user_pic=?, user_gender=?, user_birthday=?, user_country=?, user_region=?, user_city=?, user_adress=?, user_install_where=?, user_log_key=?, user_gcm=?, user_device=?, user_device_id=?, user_device_version=?, user_device_os=?, user_discount=?, user_promo=?, user_conf_req=?, user_log=?, user_upd=?, user_reg=?, user_del=? WHERE user_id=?";
                  $cordovaSQLite.execute($rootScope.db, queryUsrDataUpd, [user_id, user_name, user_surname, user_middlename, user_email, user_email_confirm, user_pwd, user_tel, user_mob_confirm, user_mob, user_work_pos, user_menue_exe, user_institution, user_office, user_pic, user_gender, user_birthday, user_country, user_region, user_city, user_adress, user_install_where, user_log_key, user_gcm, user_device, user_device_id, user_device_version, user_device_os, user_discount, user_promo, user_conf_req, user_log, user_upd, user_reg, user_del, 1]).then(function(suc) {}, function() {});

                }
              }, function() {});

            }

            // DB POINTS
            var pointsArr = data[0].pointsArr;

            if(pointsArr.length > 0) {

              var id = 0;

              function pointsArrFunc(id) {

                var points_id = pointsArr[id]['points_id'];
                var points_user = pointsArr[id]['points_user'];
                var points_bill = pointsArr[id]['points_bill'];
                var points_discount = pointsArr[id]['points_discount'];
                var points_points = pointsArr[id]['points_points'];
                var points_got_spend = pointsArr[id]['points_got_spend'];
                var points_waiter = pointsArr[id]['points_waiter'];
                var points_institution = pointsArr[id]['points_institution'];
                var points_office = pointsArr[id]['points_office'];
                var points_status = pointsArr[id]['points_status'];
                var points_comment = pointsArr[id]['points_comment'];
                var points_proofed = pointsArr[id]['points_proofed'];
                var points_gift = pointsArr[id]['points_gift'];
                var points_check = pointsArr[id]['points_check'];
                var points_waitertime = pointsArr[id]['points_waitertime'];
                var points_usertime = pointsArr[id]['points_usertime'];
                var points_when = pointsArr[id]['points_when'];
                var points_time = pointsArr[id]['points_time'];
                var points_del = pointsArr[id]['points_del'];

                var queryPoints = "SELECT * FROM points WHERE points_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryPoints, [points_id]).then(function(suc) {
                  if(suc.rows.length > 0) {
                    var pointsUpd = "UPDATE points SET points_status=?, points_proofed=?, points_when=?, points_del=? WHERE points_id=?";
                    $cordovaSQLite.execute($rootScope.db, pointsUpd, [points_status, points_proofed, points_when, points_del, points_id]).then(function() {
                      id++;
                      if(id < pointsArr.length) {
                        pointsArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var pointsIns = "INSERT INTO points (points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_office, points_status, points_comment, points_proofed, points_gift, points_check, points_waitertime, points_usertime, points_when, points_time, points_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, pointsIns, [points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_office, points_status, points_comment, points_proofed, points_gift, points_check, points_waitertime, points_usertime, points_when, points_time, points_del]).then(function() {
                      id++;
                      if(id < pointsArr.length) {
                        pointsArrFunc(id);
                      }
                    }, function() {});
                  }
                }, function() {});

              }
              pointsArrFunc(0);

            }

            // DB WALLET
            var walletArr = data[0].walletArr;

            if(walletArr.length > 0) {

                var wallet_user = data[0].walletArr[0]['wallet_user'];
                var wallet_institution = data[0].walletArr[0]['wallet_institution'];
                var wallet_total = data[0].walletArr[0]['wallet_total'];
                var wallet_warn = data[0].walletArr[0]['wallet_warn'];
                var wallet_when = data[0].walletArr[0]['wallet_when'];
                var wallet_del = data[0].walletArr[0]['wallet_del'];

                var queryWallet = "SELECT * FROM wallet WHERE wallet_when < ?";
                $cordovaSQLite.execute($rootScope.db, queryWallet, [wallet_when]).then(function(suc) {
                  if(suc.rows.length > 0) {
                    var walletUpd = "UPDATE wallet SET wallet_total=?, wallet_when=?, wallet_warn=?, wallet_del=?";
                    $cordovaSQLite.execute($rootScope.db, walletUpd, [wallet_total, wallet_when, wallet_warn,wallet_del]).then(function() {}, function() {});
                  }
                },
                function() {});

            }

            // DB GOODS
            var goodsArr = data[0].goodsArr;

            if(goodsArr.length > 0) {

              var id = 0;

              function goodsArrFunc(id) {
                var goods_id = goodsArr[id]['goods_id'];
                var goods_when = goodsArr[id]['goods_when'];
                var goods_del = goodsArr[id]['goods_del'];
                var goods_name = 0;
                var goods_desc = 0;
                var goods_pic = 0;
                var goods_type = 0;
                var goods_institution = 0;

                if(goods_when != '1') {
                  goods_name = goodsArr[id]['goods_name'];
                  goods_desc = goodsArr[id]['goods_desc'];
                  goods_pic = goodsArr[id]['goods_pic'];
                  goods_type = goodsArr[id]['goods_type'];
                  goods_institution = goodsArr[id]['goods_institution'];
                }

                var queryGoods = "SELECT * FROM goods WHERE goods_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryGoods, [goods_id]).then(function(suc) {
                  if(suc.rows.length > 0) {
                    var goodsUpd = "UPDATE goods SET goods_name=?, goods_desc=?, goods_pic=?, goods_type=?, goods_institution=?, goods_when=?, goods_del=? WHERE goods_id=?";
                    $cordovaSQLite.execute($rootScope.db, goodsUpd, [goods_name, goods_desc, goods_pic, goods_type, goods_institution, goods_when, goods_del, goods_id]).then(function() {
                      id++;
                      if(id < goodsArr.length) {
                        goodsArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var goodsIns = "INSERT INTO goods (goods_id, goods_name, goods_desc, goods_pic, goods_type, goods_institution, goods_when, goods_del) VALUES (?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, goodsIns, [goods_id, goods_name, goods_desc, goods_pic, goods_type, goods_institution, goods_when, goods_del]).then(function() {
                      id++;
                      if(id < goodsArr.length) {
                        goodsArrFunc(id);
                      }
                    }, function() {});
                  }
                }, function() {});
              }
              goodsArrFunc(0);

            }

            // DB CATEGORIES
            var catArr = data[0].catArr;

            if(catArr.length > 0) {

              var id = 0;

              function catArrFunc(id) {
                var cat_id = catArr[id]['cat_id'];
                var cat_xid = 0;
                var cat_when = catArr[id]['cat_when'];
                var cat_del = catArr[id]['cat_del'];
                var cat_name = 0;
                var cat_desc = 0;
                var cat_pic = 0;
                var cat_ingr = 0;
                var cat_order = 0;
                var cat_institution = 0;

                if(cat_when != '1') {
                  cat_xid = catArr[id]['cat_xid'];
                  cat_name = catArr[id]['cat_name'];
                  cat_desc = catArr[id]['cat_desc'];
                  cat_pic = catArr[id]['cat_pic'];
                  cat_ingr = catArr[id]['cat_ingr'];
                  cat_order = catArr[id]['cat_order'];
                  cat_institution = catArr[id]['cat_institution'];
                }

                var queryCats = "SELECT * FROM categories WHERE cat_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryCats, [cat_id]).then(function(suc) {
                  if(suc.rows.length > 0) {
                    var catsUpd = "UPDATE categories SET cat_xid=?, cat_name=?, cat_desc=?, cat_pic=?, cat_ingr=?, cat_order=?, cat_institution=?, cat_when=?, cat_del=? WHERE cat_id=?";
                    $cordovaSQLite.execute($rootScope.db, catsUpd, [cat_xid, cat_name, cat_desc, cat_pic, cat_ingr, cat_order, cat_institution, cat_when, cat_del, cat_id]).then(function() {
                      id++;
                      if(id < catArr.length) {
                        catArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var catsIns = "INSERT INTO categories (cat_id, cat_xid, cat_name, cat_desc, cat_pic, cat_ingr, cat_order, cat_institution, cat_when, cat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, catsIns, [cat_id, cat_xid, cat_name, cat_desc, cat_pic, cat_ingr, cat_order, cat_institution, cat_when, cat_del]).then(function() {
                      id++;
                      if(id < catArr.length) {
                        catArrFunc(id);
                      }
                    }, function() {});
                  }
                }, function() {});
              }
              catArrFunc(0);

            }

            // DB MENU
            var menueArr = data[0].menueArr;
            
            if(menueArr.length > 0) {

              var id = 0;

              function menueArrFunc(id) {

                var menue_id = menueArr[id]['menue_id'];
                var menue_xid = 0;
                var menue_cat_xid = 0;
                var menue_when = menueArr[id]['menue_when'];
                var menue_del = menueArr[id]['menue_del'];
                var menue_cat = 0;
                var menue_name = 0;
                var menue_desc = 0;
                var menue_size = 0;
                var menue_cost = 0;
                var menue_costs = 0;
                var menue_ingr = 0;
                var menue_addition = 0;
                var menue_addition_auto = 0;
                var menue_package = 0;
                var menue_weight = 0;
                var menue_interval = 0;
                var menue_discount = 0;
                var menue_action = 0;
                var menue_code = 0;
                var menue_pic = 0;
                var menue_institution = 0;

                if(menue_when != '1') {
                  menue_xid = menueArr[id]['menue_xid'];
                  menue_cat_xid = menueArr[id]['menue_cat_xid'];
                  menue_cat = menueArr[id]['menue_cat'];
                  menue_name = menueArr[id]['menue_name'];
                  menue_desc = menueArr[id]['menue_desc'];
                  menue_size = menueArr[id]['menue_size'];
                  menue_cost = menueArr[id]['menue_cost'];
                  menue_costs = menueArr[id]['menue_costs'];
                  menue_ingr = menueArr[id]['menue_ingr'];
                  menue_addition = menueArr[id]['menue_addition'];
                  menue_addition_auto = menueArr[id]['menue_addition_auto'];
                  menue_package = menueArr[id]['menue_package'];
                  menue_weight = menueArr[id]['menue_weight'];
                  menue_discount = menueArr[id]['menue_discount'];
                  menue_action = menueArr[id]['menue_action'];
                  menue_code = menueArr[id]['menue_code'];
                  menue_interval = menueArr[id]['menue_interval'];
                  menue_pic = menueArr[id]['menue_pic'];
                  menue_institution = menueArr[id]['menue_institution'];
                }

                var queryMenue = "SELECT * FROM menue WHERE menue_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryMenue, [menue_id]).then(function(suc) {
                  if(suc.rows.length > 0) {
                    var menueUpd = "UPDATE menue SET menue_xid=?, menue_cat_xid=?, menue_cat=?, menue_name=?, menue_desc=?, menue_size=?, menue_cost=?, menue_costs=?, menue_ingr=?, menue_addition=?, menue_addition_auto=?, menue_package=?, menue_weight=?, menue_discount=?, menue_action=?, menue_code=?, menue_interval=?, menue_pic=?, menue_institution=?, menue_when=?, menue_del=? WHERE menue_id=?";
                    $cordovaSQLite.execute($rootScope.db, menueUpd, [menue_xid, menue_cat_xid, menue_cat, menue_name, menue_desc, menue_size, menue_cost, menue_costs, menue_ingr, menue_addition, menue_addition_auto, menue_package, menue_weight, menue_discount, menue_action, menue_code, menue_interval, menue_pic, menue_institution, menue_when, menue_del, menue_id]).then(function() {
                      id++;
                      if(id < menueArr.length) {
                        menueArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var menueIns = "INSERT INTO menue (menue_id, menue_xid, menue_cat_xid, menue_cat, menue_name, menue_desc, menue_size, menue_cost, menue_costs, menue_ingr, menue_addition, menue_addition_auto, menue_package, menue_weight, menue_discount, menue_action, menue_code, menue_interval, menue_pic, menue_institution, menue_when, menue_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, menueIns, [menue_id, menue_xid, menue_cat_xid, menue_cat, menue_name, menue_desc, menue_size, menue_cost, menue_costs, menue_ingr, menue_addition, menue_addition_auto, menue_package, menue_weight, menue_discount, menue_action, menue_code, menue_interval, menue_pic, menue_institution, menue_when, menue_del]).then(function() {
                      id++;
                      if(id < menueArr.length) {
                        menueArrFunc(id);
                      }
                    }, function() {});
                  }
                }, function() {});

              }
              menueArrFunc(0);

            }

            // DB INGREDIENTS
            var ingrArr = data[0].ingrArr;

            if(ingrArr.length > 0) {

              var id = 0;

              function ingrArrFunc(id) {

                var ingr_id = ingrArr[id]['ingr_id'];
                var ingr_xid = 0;
                var ingr_when = ingrArr[id]['ingr_when'];
                var ingr_del = ingrArr[id]['ingr_del'];
                var ingr_name = 0;
                var ingr_desc = 0;
                var ingr_cat = 0;
                var ingr_code = 0;
                var ingr_pic = 0;
                var ingr_size = 0;
                var ingr_cost = 0;
                var ingr_institution = 0;

                if(ingr_when != '1') {
                  ingr_xid = ingrArr[id]['ingr_xid'];
                  ingr_name = ingrArr[id]['ingr_name'];
                  ingr_desc = ingrArr[id]['ingr_desc'];
                  ingr_cat = ingrArr[id]['ingr_cat'];
                  ingr_code = ingrArr[id]['ingr_code'];
                  ingr_pic = ingrArr[id]['ingr_pic'];
                  ingr_size = ingrArr[id]['ingr_size'];
                  ingr_cost = ingrArr[id]['ingr_cost'];
                  ingr_institution = ingrArr[id]['ingr_institution'];
                }

                var queryIngrs = "SELECT * FROM ingredients WHERE ingr_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryIngrs, [ingr_id]).then(function(suc) {
                  if(suc.rows.length > 0) {
                    var menueUpd = "UPDATE ingredients SET ingr_xid=?, ingr_name=?, ingr_desc=?, ingr_cat=?, ingr_code=?, ingr_pic=?, ingr_size=?, ingr_cost=?, ingr_institution=?, ingr_when=?, ingr_del=? WHERE ingr_id=?";
                    $cordovaSQLite.execute($rootScope.db, menueUpd, [ingr_xid, ingr_name, ingr_desc, ingr_cat, ingr_code, ingr_pic, ingr_size, ingr_cost, ingr_institution, ingr_when, ingr_del, ingr_id]).then(function() {
                      id++;
                      if(id < ingrArr.length) {
                        ingrArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var ingrsIns = "INSERT INTO ingredients (ingr_id, ingr_xid, ingr_name, ingr_desc, ingr_cat, ingr_code, ingr_pic, ingr_size, ingr_cost, ingr_institution, ingr_when, ingr_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, ingrsIns, [ingr_id, ingr_xid, ingr_name, ingr_desc, ingr_cat, ingr_code, ingr_pic, ingr_size, ingr_cost, ingr_institution, ingr_when, ingr_del]).then(function() {
                      id++;
                      if(id < ingrArr.length) {
                        ingrArrFunc(id);
                      }
                    }, function() {});
                  }
                }, function() {});

              }
              ingrArrFunc(0);

            }

            // DB NEWS
            var newsArr = data[0].newsArr;

            if(newsArr.length > 0) {

              var id = 0;

              function newsArrFunc(id) {

                var news_id = newsArr[id]['news_id'];
                var news_state = newsArr[id]['news_state'];
                var news_menue_id = 0;
                var news_cost = 0;
                var news_user = 0;
                var news_used = 0;
                var news_begin = 0;
                var news_end = 0;
                var news_when = newsArr[id]['news_when'];
                var news_del = newsArr[id]['news_del'];
                var news_name = 0;
                var news_message = 0;
                var news_pic = 0;
                var news_institution = 0;

                if(news_state == '1') {
                  news_name = newsArr[id]['news_name'];
                  news_message = newsArr[id]['news_message'];
                  news_pic = newsArr[id]['news_pic'];
                  news_institution = newsArr[id]['news_institution'];
                  news_menue_id = newsArr[id]['news_menue_id'];
                  news_cost = newsArr[id]['news_cost'];
                  news_user = newsArr[id]['news_user'];
                  news_used = newsArr[id]['news_used'];
                  news_begin = newsArr[id]['news_begin'];
                  news_end = newsArr[id]['news_end'];
                }

                var queryNews = "SELECT * FROM news WHERE news_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryNews, [news_id]).then(function(suc) {
                  if(suc.rows.length > 0) {
                    var newsUpd = "UPDATE news SET news_name=?, news_message=?, news_pic=?, news_institution=?, news_state=?, news_menue_id=?, news_cost=?, news_user=?, news_used=?, news_begin=?, news_end=?, news_when=?, news_del=? WHERE news_id=?";
                    $cordovaSQLite.execute($rootScope.db, newsUpd, [news_name, news_message, news_pic, news_institution, news_state, news_menue_id, news_cost, news_user, news_used, news_begin, news_end, news_when, news_del, news_id]).then(function() {
                      id++;
                      if(id < newsArr.length) {
                      newsArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var newsIns = "INSERT INTO news (news_id, news_name, news_message, news_pic, news_institution, news_state, news_menue_id, news_cost, news_user, news_used, news_begin, news_end, news_when, news_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, newsIns, [news_id, news_name, news_message, news_pic, news_institution, news_state, news_menue_id, news_cost, news_user, news_used, news_begin, news_end, news_when, news_del]).then(function() {
                      id++;
                      if(id < newsArr.length) {
                      newsArrFunc(id);
                      }
                    }, function() {});
                  }
                }, function() {});

              }
              newsArrFunc(0);

            }

            // DB REVIEWS
            var reviewsArr = data[0].reviewsArr;

            if(reviewsArr.length > 0) {

              var id = 0;

              function reviewsArrFunc(id) {

                var reviews_id = reviewsArr[id]['reviews_id'];
                var reviews_from = reviewsArr[id]['reviews_from'];
                var reviews_to = reviewsArr[id]['reviews_to'];
                var reviews_message = reviewsArr[id]['reviews_message'];
                var reviews_pic = reviewsArr[id]['reviews_pic'];
                var reviews_institution = reviewsArr[id]['reviews_institution'];
                var reviews_answered = reviewsArr[id]['reviews_answered'];
                var reviews_opened = reviewsArr[id]['reviews_opened'];
                var reviews_when = reviewsArr[id]['reviews_when'];
                var reviews_del = reviewsArr[id]['reviews_del'];

                var queryReviews = "SELECT * FROM reviews WHERE reviews_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryReviews, [reviews_id]).then(function(suc) {
                  if(suc.rows.length > 0) {
                    var reviewsUpd = "UPDATE reviews SET reviews_from=?, reviews_to=?, reviews_message=?, reviews_pic=?, reviews_institution=?, reviews_answered=?, reviews_opened=?, reviews_when=?, reviews_del=? WHERE reviews_id=?";
                    $cordovaSQLite.execute($rootScope.db, reviewsUpd, [reviews_from, reviews_to, reviews_message, reviews_pic, reviews_institution, reviews_answered, reviews_opened, reviews_when, reviews_del, reviews_id]).then(function() {
                      id++;
                      if(id < reviewsArr.length) {
                        reviewsArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var newsIns = "INSERT INTO reviews (reviews_id, reviews_from, reviews_to, reviews_message, reviews_pic, reviews_institution, reviews_answered, reviews_opened, reviews_when, reviews_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, newsIns, [reviews_id, reviews_from, reviews_to, reviews_message, reviews_pic, reviews_institution, reviews_answered, reviews_when, reviews_del]).then(function() {
                      id++;
                      if(id < reviewsArr.length) {
                        reviewsArrFunc(id);
                      }
                    }, function() {});
                  }
                }, function() {});

              }
              reviewsArrFunc(0);

            }

            // DB ASKS
            var asksArr = data[0].asksArr;
            // console.log('=========================>>>>>>>>>>>> DATA: '+JSON.stringify(asksArr))
            if(asksArr.length > 0) {

              var id = 0;

              function asksArrFunc(id) {

                var asks_id = asksArr[id]['asks_id'];
                var asks_name = asksArr[id]['asks_name'];
                var asks_message = asksArr[id]['asks_message'];
                var asks_type = asksArr[id]['asks_type'];
                var asks_chained = asksArr[id]['asks_chained'];
                var asks_active = asksArr[id]['asks_active'];
                var asks_img = asksArr[id]['asks_img'];
                var asks_answ = asksArr[id]['asks_answ'];
                var asks_yes = asksArr[id]['asks_yes'];
                var asks_no = asksArr[id]['asks_no'];
                var asks_institution = asksArr[id]['asks_institution'];
                var asks_del = asksArr[id]['asks_del'];
                var asks_when = asksArr[id]['asks_when'];

                var queryAsks = "SELECT * FROM asks WHERE asks_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryAsks, [asks_id]).then(function(suc) {
                  if(suc.rows.length == 0) {
                    var newsIns = "INSERT INTO asks (asks_id, asks_name, asks_message, asks_type, asks_chained, asks_active, asks_img, asks_answ, asks_yes, asks_no, asks_reply, asks_institution, asks_when, asks_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, newsIns, [asks_id, asks_name, asks_message, asks_type, asks_chained, asks_active, asks_img, asks_answ, asks_yes, asks_no, 0, asks_institution, asks_when, asks_del]).then(function() {
                      id++;
                      if(id < asksArr.length) {
                        asksArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var asksIns = "UPDATE asks SET asks_name=?, asks_message=?, asks_type=?, asks_chained=?, asks_active=?, asks_img=?, asks_answ=?, asks_yes=?, asks_no=?, asks_institution=?, asks_when=?, asks_del=? WHERE asks_id=?";
                    $cordovaSQLite.execute($rootScope.db, asksIns, [asks_name, asks_message, asks_type, asks_chained, asks_active, asks_img, asks_answ, asks_yes, asks_no, asks_institution, asks_when, asks_del, asks_id]).then(function(ins) {
                      id++;
                      if(id < asksArr.length) {
                        asksArrFunc(id);
                      }
                    }, function() {});
                  }
                }, function() {});

              }
              asksArrFunc(0);

            }

            // DB GIFTS
            var giftsArr = data[0].giftsArr;
            
            if(giftsArr.length > 0) {

              var id = 0;

              function giftsArrFunc(id) {

                var gifts_id = giftsArr[id]['gifts_id'];
                var gifts_name = giftsArr[id]['gifts_name'];
                var gifts_desc = giftsArr[id]['gifts_desc'];
                var gifts_points = giftsArr[id]['gifts_points'];
                var gifts_pic = giftsArr[id]['gifts_pic'];
                var gifts_institution = giftsArr[id]['gifts_institution'];
                var gifts_when = giftsArr[id]['gifts_when'];
                var gifts_del = giftsArr[id]['gifts_del'];
      
                var queryGifts = "SELECT * FROM gifts WHERE gifts_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryGifts, [gifts_id]).then(function(suc) {
                if(suc.rows.length > 0) {
                    var giftsUpd = "UPDATE gifts SET gifts_name=?, gifts_desc=?, gifts_points=?, gifts_pic=?, gifts_institution=?, gifts_when=?, gifts_del=? WHERE gifts_id=?";
                    $cordovaSQLite.execute($rootScope.db, giftsUpd, [gifts_name, gifts_desc, gifts_points, gifts_pic, gifts_institution, gifts_when, gifts_del, gifts_id]).then(function() {
                      id++;
                      if(id < giftsArr.length) {
                        giftsArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var giftsIns = "INSERT INTO gifts (gifts_id, gifts_name, gifts_desc, gifts_points, gifts_pic, gifts_institution, gifts_when, gifts_del) VALUES (?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, giftsIns, [gifts_id, gifts_name, gifts_desc, gifts_points, gifts_pic, gifts_institution, gifts_when, gifts_del]).then(function() {
                    id++;
                    if(id < giftsArr.length) {
                      giftsArrFunc(id);
                    }
                    }, function() {});
                  }
                }, function() {});

              }
              giftsArrFunc(0);

            }

            // DB CHAT
            var chatArr = data[0].chatArr;
            
            if(chatArr.length > 0) {

              var id = 0;

              function chatArrFunc(id) {

                var chat_id = chatArr[id]['chat_id'];
                var chat_from = chatArr[id]['chat_from'];
                var chat_to = chatArr[id]['chat_to'];
                var chat_name = chatArr[id]['chat_name'];
                var chat_message = chatArr[id]['chat_message'];
                var chat_read = chatArr[id]['chat_read'];
                var chat_institution = chatArr[id]['chat_institution'];
                var chat_answered = chatArr[id]['chat_answered'];
                var chat_when = chatArr[id]['chat_when'];
                var chat_del = chatArr[id]['chat_del'];
      
                  var queryChat = "SELECT * FROM chat WHERE chat_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryChat, [chat_id]).then(function(suc) {
                if(suc.rows.length > 0) {
                    var chatUpd = "UPDATE chat SET chat_read=?, chat_answered=?, chat_when=?, chat_del=? WHERE chat_id=?";
                    $cordovaSQLite.execute($rootScope.db, chatUpd, [chat_read, chat_answered, chat_when, chat_del, chat_id]).then(function() {
                      id++;
                      if(id < chatArr.length) {
                        chatArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var chatIns = "INSERT INTO chat (chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, chatIns, [chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del]).then(function() {
                      id++;
                      if(id < chatArr.length) {
                        chatArrFunc(id);
                      }
                    }, function() {});
                  }
                }, function() {});

              }
              chatArrFunc(0);

            }

            // DB FIFTHGIFT
            var fifthArr = data[0].fifthArr;
            
            if(fifthArr.length > 0) {

              var id = 0;

              function fifthArrFunc(id) {

                var fifth_id = fifthArr[id]['fifth_id'];
                var fifth_name = fifthArr[id]['fifth_name'];
                var fifth_desc = fifthArr[id]['fifth_desc'];
                var fifth_user = fifthArr[id]['fifth_user'];
                var fifth_menue_id = fifthArr[id]['fifth_menue_id'];
                var fifth_bill = fifthArr[id]['fifth_bill'];
                var fifth_got_spend = fifthArr[id]['fifth_got_spend'];
                var fifth_institution = fifthArr[id]['fifth_institution'];
                var fifth_office = fifthArr[id]['fifth_office'];
                var fifth_when = fifthArr[id]['fifth_when'];
                var fifth_del = fifthArr[id]['fifth_del'];
      
                var queryFifth = "SELECT * FROM fifthgift WHERE fifth_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryFifth, [fifth_id]).then(function(suc) {
                if(suc.rows.length > 0) {
                    var chatUpd = "UPDATE fifthgift SET fifth_name=?, fifth_desc=?, fifth_user=?, fifth_menue_id=?, fifth_bill=?, fifth_got_spend=?, fifth_institution=?, fifth_office=?, fifth_when=?, fifth_del=? WHERE fifth_id=?";
                    $cordovaSQLite.execute($rootScope.db, chatUpd, [fifth_name, fifth_desc, fifth_user, fifth_menue_id, fifth_bill, fifth_got_spend, fifth_institution, fifth_office, fifth_when, fifth_del, fifth_id]).then(function() {
                      id++;
                      if(id < fifthArr.length) {
                        fifthArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var fifthIns = "INSERT INTO fifthgift (fifth_id, fifth_name, fifth_desc, fifth_user, fifth_menue_id, fifth_bill, fifth_got_spend, fifth_institution, fifth_office, fifth_when, fifth_del) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, fifthIns, [fifth_id, fifth_name, fifth_desc, fifth_user, fifth_menue_id, fifth_bill, fifth_got_spend, fifth_institution, fifth_office, fifth_when, fifth_del]).then(function() {
                      id++;
                      if(id < fifthArr.length) {
                        fifthArrFunc(id);
                      }
                    }, function() {});
                  }
                }, function() {});

              }
              fifthArrFunc(0);

            }

            // DB RESERVATION
            var reservArr = data[0].reservArr;
            
            if(reservArr.length > 0) {

              var id = 0;

              function reservArrFunc(id) {

                var reservation_id = reservArr[id]['reservation_id'];
                var reservation_userid = reservArr[id]['reservation_userid'];
                var reservation_surname = reservArr[id]['reservation_surname'];
                var reservation_name = reservArr[id]['reservation_name'];
                var reservation_middlename = reservArr[id]['reservation_middlename'];
                var reservation_mobile = reservArr[id]['reservation_mobile'];
                var reservation_date = reservArr[id]['reservation_date'];
                var reservation_time = reservArr[id]['reservation_time'];
                var reservation_comment = reservArr[id]['reservation_comment'];
                var reservation_institution = reservArr[id]['reservation_institution'];
                var reservation_when = reservArr[id]['reservation_when'];
                var reservation_del = reservArr[id]['reservation_del'];
      
                var queryReserv = "SELECT * FROM reservation WHERE reservation_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryReserv, [reservation_id]).then(function(suc) {
                if(suc.rows.length > 0) {
                    var chatUpd = "UPDATE chat SET reservation_userid=?, reservation_surname=?, reservation_name=?, reservation_middlename=?, reservation_mobile=?, reservation_date=?, reservation_time=?, reservation_comment=?, reservation_institution=?, reservation_when=?, reservation_del=? WHERE reservation_id=?";
                    $cordovaSQLite.execute($rootScope.db, chatUpd, [reservation_userid, reservation_surname, reservation_name, reservation_middlename, reservation_mobile, reservation_date, reservation_time, reservation_comment, reservation_institution, reservation_when, reservation_del, reservation_id]).then(function() {
                      id++;
                      if(id < reservArr.length) {
                        reservArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var reservIns = "INSERT INTO reservation (reservation_id, reservation_userid, reservation_surname, reservation_name, reservation_middlename, reservation_mobile, reservation_date, reservation_time, reservation_comment, reservation_institution, reservation_when, reservation_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, reservIns, [reservation_id, reservation_userid, reservation_surname, reservation_name, reservation_middlename, reservation_mobile, reservation_date, reservation_time, reservation_comment, reservation_institution, reservation_when, reservation_del]).then(function() {
                      id++;
                      if(id < reservArr.length) {
                        reservArrFunc(id);
                      }
                    }, function() {});
                  }
                }, function() {});

              }
              reservArrFunc(0);

            }

            // DB ORGANIZATION
            var orgArr = data[0].orgArr;
            
            if(orgArr.length > 0) {

              var id = 0;

              function orgArrFunc(id) {

                var org_id = orgArr[id]['org_id'];
                var org_name = orgArr[id]['org_name'];
                var org_country = orgArr[id]['org_country'];
                var org_city = orgArr[id]['org_city'];
                var org_adress = orgArr[id]['org_adress'];
                var org_timezone = orgArr[id]['org_timezone'];
                var org_tel = orgArr[id]['org_tel'];
                var org_fax = orgArr[id]['org_fax'];
                var org_mob = orgArr[id]['org_mob'];
                var org_email = orgArr[id]['org_email'];
                var org_skype = orgArr[id]['org_skype'];
                var org_site = orgArr[id]['org_site'];
                var org_tax_id = orgArr[id]['org_tax_id'];
                var org_logo = orgArr[id]['org_logo'];
                var org_sound = orgArr[id]['org_sound'];
                var org_development = orgArr[id]['org_development'];
                var org_money_points = orgArr[id]['org_money_points'];
                var org_starting_points = orgArr[id]['org_starting_points'];
                var org_money_percent = orgArr[id]['org_money_percent'];
                var org_points_points = orgArr[id]['org_points_points'];
                var org_promo_points_owner = orgArr[id]['org_promo_points_owner'];
                var org_promo_points_scan_owner = orgArr[id]['org_promo_points_scan_owner'];
                var org_promo_points_involved = orgArr[id]['org_promo_points_involved'];
                var org_promo_points_scan_involved = orgArr[id]['org_promo_points_scan_involved'];
                var org_max_points = orgArr[id]['org_max_points'];
                var org_risk_summ = orgArr[id]['org_risk_summ'];
                var org_autoaprove = orgArr[id]['org_autoaprove'];
                var org_appvers = orgArr[id]['org_appvers'];
                var org_appurl = orgArr[id]['org_appurl'];
                var org_log = orgArr[id]['org_log'];
                var org_reg = orgArr[id]['org_reg'];
                var org_del = orgArr[id]['org_del'];
      
                var queryOrg = "SELECT * FROM organizations WHERE org_id = ?";
                $cordovaSQLite.execute($rootScope.db, queryOrg, [org_id]).then(function(suc) {
                  if(suc.rows.length > 0) {
                    var orgUpd = "UPDATE organizations SET org_name=?, org_country=?, org_city=?, org_adress=?, org_timezone=?, org_tel=?, org_fax=?, org_mob=?, org_email=?, org_skype=?, org_site=?, org_tax_id=?, org_logo=?, org_sound=?, org_development=?, org_money_points=?, org_starting_points=?, org_money_percent=?, org_points_points=?, org_promo_points_owner=?, org_promo_points_scan_owner=?, org_promo_points_involved=?, org_promo_points_scan_involved=?, org_max_points=?, org_risk_summ=?, org_autoaprove=?, org_appvers=?, org_appurl=?, org_log=?, org_reg=?, org_del=? WHERE org_id=?";
                    $cordovaSQLite.execute($rootScope.db, orgUpd, [org_name, org_country, org_city, org_adress, org_timezone, org_tel, org_fax, org_mob, org_email, org_skype, org_site, org_tax_id, org_logo, org_sound, org_development, org_money_points, org_starting_points, org_money_percent, org_points_points, org_promo_points_owner, org_promo_points_scan_owner, org_promo_points_involved, org_promo_points_scan_involved, org_max_points, org_risk_summ, org_autoaprove, org_appvers, org_appurl, org_log, org_reg, org_del, org_id]).then(function() {
                      id++;
                      if(id < orgArr.length) {
                        orgArrFunc(id);
                      }
                    }, function() {});
                  }
                  else {
                    var orgIns = "INSERT INTO organizations (org_id, org_name, org_country, org_city, org_adress, org_timezone, org_tel, org_fax, org_mob, org_email, org_skype, org_site, org_tax_id, org_logo, org_sound, org_development, org_money_points, org_starting_points, org_money_percent, org_points_points, org_promo_points_owner, org_promo_points_scan_owner, org_promo_points_involved, org_promo_points_scan_involved, org_max_points, org_risk_summ, org_autoaprove, org_appvers, org_appurl, org_log, org_reg, org_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, orgIns, [org_id, org_name, org_country, org_city, org_adress, org_timezone, org_tel, org_fax, org_mob, org_email, org_skype, org_site, org_tax_id, org_logo, org_sound, org_development, org_money_points, org_starting_points, org_money_percent, org_points_points, org_promo_points_owner, org_promo_points_scan_owner, org_promo_points_involved, org_promo_points_scan_involved, org_max_points, org_risk_summ, org_autoaprove, org_appvers, org_appurl, org_log, org_reg, org_del]).then(function() {
                      id++;
                      if(id < orgArr.length) {
                        orgArrFunc(id);
                      }
                    }, function() {});
                  }
                }, function() {});

              }
              orgArrFunc(0);

            }

            $timeout(function() {

              var waiterstr = JSON.stringify({
                inst_id: $rootScope.institution,
                newusr: 'waiter'
              });
                
              $http.post($rootScope.generalscript, waiterstr).then(function(waitsuc) {

                var waitdata = waitsuc.data;
                // alert('success android Waiter: '+JSON.stringify(waitdata));
                
                if(userupd.user_mob_confirm == 1 && validateEmail(userupd.user_email) && userupd.user_name) {
                  $rootScope.getWaiter(waitdata, 1);
                }
                else {
                  $rootScope.getWaiter(waitdata, 0);
                }

              }, 
              function(er) {
                // if(userupd.user_mob_confirm == 1 && validateEmail(userupd.user_email) && userupd.user_name) {
                  $state.go('app.main');
                // }
                // else {
                //   $state.go('login');
                  // $rootScope.regform = false;
                  // $rootScope.intconnect = true;
                // }
                // alert('error Waiter: '+JSON.stringify(er));
              });

            }, 2000);

          }

        };

        // console.log('TIMEOUT 1')

        var queryMeSel = "SELECT * FROM users WHERE user_id = ?";
        $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {

          // DB ROOMS
          $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS rooms (room_id integer primary key, room_name text, room_desc text, room_employee integer, room_menue_exe text, room_priority integer, room_institution integer, room_office integer, room_upd integer, room_when integer, room_del integer)").then(function() {}, function() {});

          ifNotExists('organizations_office', 'office_orders', 'integer', 0);
          ifNotExists('goods', 'goods_type', 'integer', 0);
          ifNotExists('ordering', 'order_room', 'integer', 0);

          // console.log('========================================> USER EXISTS')

          var queryPoints = "SELECT * FROM points ORDER BY points_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryPoints, []).then(function(suc) {
            if(suc.rows.length > 0) {
              points = suc.rows.item(0).points_when;
            }
          }, function() {});

          var queryWallet = "SELECT * FROM wallet ORDER BY wallet_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryWallet, []).then(function(suc) {
            if(suc.rows.length > 0) {
              wallet = suc.rows.item(0).wallet_when;
            }
          }, function() {});

          var queryGoods = "SELECT * FROM goods ORDER BY goods_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryGoods, []).then(function(suc) {
            if(suc.rows.length > 0) {
              goods = suc.rows.item(0).goods_when;
            }
          }, function() {});

          var queryCat = "SELECT * FROM categories ORDER BY cat_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryCat, []).then(function(suc) {
            if(suc.rows.length > 0) {
              cat = suc.rows.item(0).cat_when;
            }
          }, function() {});

          var queryMenue = "SELECT * FROM menue ORDER BY menue_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryMenue, []).then(function(suc) {
            if(suc.rows.length > 0) {
              menue = suc.rows.item(0).menue_when;
            }
          }, function() {});

          var queryIngrs = "SELECT * FROM ingredients ORDER BY ingr_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryIngrs, []).then(function(suc) {
            if(suc.rows.length > 0) {
              ingrs = suc.rows.item(0).ingr_when;
            }
          }, function() {});

          var queryNews = "SELECT * FROM news ORDER BY news_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryNews, []).then(function(suc) {
            if(suc.rows.length > 0) {
              news = suc.rows.item(0).news_when;
            }
          }, function() {});

          var queryRevs = "SELECT * FROM reviews ORDER BY reviews_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryRevs, []).then(function(suc) {
            if(suc.rows.length > 0) {
              revs = suc.rows.item(0).reviews_when;
            }
          }, function() {});

          var queryAsks = "SELECT * FROM asks ORDER BY asks_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryAsks, []).then(function(suc) {
            if(suc.rows.length > 0) {
              asks = suc.rows.item(0).asks_when;
            }
          }, function() {});

          var queryGifts = "SELECT * FROM gifts ORDER BY gifts_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryGifts, []).then(function(suc) {
            if(suc.rows.length > 0) {
              gifts = suc.rows.item(0).gifts_when;
            }
          }, function() {});

          var queryChats = "SELECT * FROM chat ORDER BY chat_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryChats, []).then(function(suc) {
            if(suc.rows.length > 0) {
              chat = suc.rows.item(0).chat_when;
            }
          }, function() {});

          var queryOrdering = "SELECT * FROM ordering ORDER BY order_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryOrdering, []).then(function(suc) {
            if(suc.rows.length > 0) {
              order = suc.rows.item(0).order_when;
            }
          }, function() {});

          var queryRooms = "SELECT * FROM rooms ORDER BY room_upd DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryRooms, []).then(function(suc) {
            if(suc.rows.length > 0) {
              room = suc.rows.item(0).room_upd;
            }
          }, function() {});

          var queryFifthGift = "SELECT * FROM fifthgift ORDER BY fifth_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryFifthGift, []).then(function(suc) {
            if(suc.rows.length > 0) {
              fifthgift = suc.rows.item(0).fifth_when;
            }
          }, function() {});

          var queryReservation = "SELECT * FROM reservation ORDER BY reservation_when DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryReservation, []).then(function(suc) {
            if(suc.rows.length > 0) {
              reservs = suc.rows.item(0).reservation_when;
            }
          }, function() {});

          var queryOrganization = "SELECT * FROM organizations ORDER BY org_log DESC LIMIT 1";
          $cordovaSQLite.execute($rootScope.db, queryOrganization, []).then(function(suc) {
            if(suc.rows.length > 0) {
              organization = suc.rows.item(0).org_log;
            }
          }, function() {

            // DB ORGANIZATION
            $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS organizations (org_id integer primary key, org_name text, org_country integer, org_city integer, org_adress text, org_timezone integer, org_tel text, org_fax text, org_mob text, org_email text, org_skype text, org_site text, org_tax_id text, org_logo text, org_sound integer, org_development integer, org_money_points integer, org_starting_points integer, org_money_percent integer, org_points_points integer, org_promo_points_owner integer, org_promo_points_scan_owner integer, org_promo_points_involved integer, org_promo_points_scan_involved integer, org_max_points integer, org_risk_summ integer, org_autoaprove integer, org_appvers integer, org_appurl text, org_log integer, org_reg integer, org_del integer)").then(function() {}, function() {});

          });

          $timeout(function() {

            if(!testing) {

              var pushConfig = {
                android: {
                  senderID: gcm_android
                },
                ios: {
                  alert: "true",
                  badge: "true",
                  clearBadge: "true",
                  sound: "true"
                },
                windows: {}
              };

              $cordovaPushV5.initialize(pushConfig).then(function() {
                // sets Badge number to zero on iOS
                $cordovaPushV5.setBadgeNumber(0);
                // start listening for new notifications
                $cordovaPushV5.onNotification();
                // start listening for errors
                $cordovaPushV5.onError();
                // register to get registrationId
                $cordovaPushV5.register().then(function(data) {
                    var gcmstr = JSON.stringify({
                      device: $rootScope.model,
                      device_id: $rootScope.uuid,
                      device_version: $rootScope.version,
                      device_os: $rootScope.platform,
                      inst_id: $rootScope.institution,
                      gcm: data,
                      newusr: 'gcmreg'
                    });
                    $http.post($rootScope.generalscript, gcmstr).then(function() {}, function(er) {});
                });
              }, function() {});

              $rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data) {
                if(!data.additionalData.foreground) {
                  $localStorage.push_id = data.additionalData.push_id;
                }
                if($localStorage.push_id) {
                  var pushobj = {"push_usr": $rootScope.uuid, "push_inst": $rootScope.institution, "push_push": $localStorage.push_id, "push_open": 1};
                  var pushjson = JSON.stringify(pushobj);
                  $http.post(pushgotlink, pushjson).then(function(suc) {$localStorage.push_id = [];}, function(er) {});
                }
              });
              $rootScope.$on('$cordovaPushV5:errorOcurred', function(event, e){
                // e.message
              });

            }

            var jsonedstr = JSON.stringify({
                device: $rootScope.model,
                device_id: $rootScope.uuid,
                device_version: $rootScope.version,
                device_os: $rootScope.platform,
                inst_id: $rootScope.institution,
                points: points,
                wallet: wallet,
                goods: goods,
                cat: cat,
                menue: menue,
                ingrs: ingrs,
                news: news,
                revs: revs,
                asks: asks,
                gifts: gifts,
                chat: chat,
                order: order,
                room: room,
                fifthgift: fifthgift,
                reservs: reservs,
                organization: organization,
                newusr: 'check'
            });

            $http.post($rootScope.generalscript, jsonedstr).then(function(suc) {
              $rootScope.getUpdate(suc.data, success.rows.item(0));
            }, 
            function(er) {
                $rootScope.intconnectupd = false;
                $ionicHistory.nextViewOptions({
                  disableAnimate: true,
                  disableBack: true
                });
                // if(success.rows.item(0).user_mob_confirm == 1) {
                  $state.go('app.main');
                // }
                // else {
                //   $state.go('login');
                //   $rootScope.regform = false;
                // }
            });

          }, 1000);

        },
        function(error) {

          // console.log('========================================> NOT USER EXISTS')

          // console.log('ERROR')

          // CHECK AND CREATE DIRECTION
          $cordovaFile.checkDir(cordova.file.documentsDirectory, $rootScope.inst_dir).then(function () {}, function () {
            $cordovaFile.createDir(cordova.file.documentsDirectory, $rootScope.inst_dir, false).then(function () {}, function () {});
          });
          // CHECK AND CREATE DIRECTION
          $cordovaFile.checkDir(cordova.file.dataDirectory, $rootScope.inst_dir).then(function () {}, function () {
            $cordovaFile.createDir(cordova.file.dataDirectory, $rootScope.inst_dir, false).then(function () {}, function () {});
          });

          if(!testing) {

            var pushConfig = {
              android: {
              senderID: gcm_android
              },
              ios: {
              alert: "true",
              badge: "true",
              clearBadge: "true",
              sound: "true"
              },
              windows: {}
            };

            $cordovaPushV5.initialize(pushConfig).then(function() {
              // sets Badge number to zero on iOS
              $cordovaPushV5.setBadgeNumber(0);
              // start listening for new notifications
              $cordovaPushV5.onNotification();
              // start listening for errors
              $cordovaPushV5.onError();
              // register to get registrationId
              $cordovaPushV5.register().then(function(data) {
              // `data.registrationId` save it somewhere;
              var gcmstr = JSON.stringify({
                device: $rootScope.model,
                device_id: $rootScope.uuid,
                device_version: $rootScope.version,
                device_os: $rootScope.platform,
                inst_id: $rootScope.institution,
                gcm: data,
                newusr: 'gcmreg'
              });
              
              $http.post($rootScope.generalscript, gcmstr).then(function(gcmsuc) {}, function(er) {});
              
              })

            }, function() {});

            $rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data) {
              // alert(JSON.stringify(data.additionalData));
              if(!data.additionalData.foreground) {
              $localStorage.push_id = data.additionalData.push_id;
              // var pushobj = {"push_usr": $rootScope.uuid, "push_inst": $rootScope.institution, "push_push": data.additionalData.push_id, "push_rec": 1};
              // var pushjson = JSON.stringify(pushobj);
              // window.cordovaHTTP.post(pushgotlink, pushjson).then(function(suc) {}, function(er) {});
              }
              if($localStorage.push_id) {
              var pushobj = {"push_usr": $rootScope.uuid, "push_inst": $rootScope.institution, "push_push": $localStorage.push_id, "push_open": 1};
              var pushjson = JSON.stringify(pushobj);
              $http.post(pushgotlink, pushjson).then(function(suc) {$localStorage.push_id = [];}, function(er) {});

              }

            });
            // triggered every time error occurs
            $rootScope.$on('$cordovaPushV5:errorOcurred', function(event, e){
              // e.message
            });

          }

          var jsonedstr = JSON.stringify({
            device: $rootScope.model,
            device_id: $rootScope.uuid,
            device_version: $rootScope.version,
            device_os: $rootScope.platform,
            inst_id: $rootScope.institution,
            newusr: 'newusr'
          });

          $http.post($rootScope.generalscript, jsonedstr).then(function(suc) {

            // console.log('========================================> success android: '+JSON.stringify(suc.data));
            $rootScope.getNew(suc.data);

          }, 
          function(er) {
            $ionicPopup.alert({title: 'Ошибка', template: er.code});
            $rootScope.intconnectupd = false;
          });

        });
          

    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.views.maxCache(1);

    $ionicConfigProvider.scrolling.jsScrolling(true);

    $ionicConfigProvider.views.swipeBackEnabled(false);
    
    $ionicConfigProvider.tabs.position('top');

    $ionicConfigProvider.navBar.alignTitle('center');

    $ionicConfigProvider.views.transition('none');
    
    $ionicConfigProvider.backButton.previousTitleText(false);
    

    $stateProvider.state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })

    .state('app.main', {
        // cache: false,
        url: '/main',
        views: {
            'menuContent': {
                templateUrl: 'templates/main.html',
                controller: 'MainCtrl'
            }
        }
    })

    .state('app.bill', {
        url: '/bill',
        views: {
            'menuContent': {
                templateUrl: 'templates/bill.html',
                controller: 'BillCtrl'
            }
        }
    })

    .state('app.qr-code', {
        url: '/qr-code',
        views: {
            'menuContent': {
                templateUrl: 'templates/qr-code.html',
                controller: 'QRCodeCtrl'
            }
        }
    })

    .state('app.gifts', {
        url: '/gifts',
        views: {
            'menuContent': {
                templateUrl: 'templates/gifts.html',
                controller: 'GiftsCtrl'
            }
        }
    })

    .state('app.gifts-detail', {
        url: '/gifts-detail/:giftId',
        views: {
            'menuContent': {
                templateUrl: 'templates/gifts-detail.html',
                controller: 'GiftsDetailCtrl'
            }
        }
    })

    .state('app.store-categories', {
        cache: false,
        url: '/store-categories/:catIngr',
        views: {
            'menuContent': {
                templateUrl: 'templates/store-categories.html',
                controller: 'StoreCatCtrl'
            }
        }
    })

    .state('app.store', {
        cache: false,
        url: '/store/:catId',
        views: {
            'menuContent': {
                templateUrl: 'templates/store.html',
                controller: 'StoreCtrl'
            }
        }
    })

    .state('app.store-detail', {
        cache: false,
        url: '/store-detail/:storeId/:storeSize',
        views: {
            'menuContent': {
                templateUrl: 'templates/store-detail.html',
                controller: 'StoreDetailCtrl'
            }
        }
    })

    .state('app.constructor', {
        cache: false,
        url: '/constructor/:constId',
        views: {
            'menuContent': {
                templateUrl: 'templates/constructor.html',
                controller: 'ConstructorCtrl'
            }
        }
    })

    .state('app.store-confirm', {
        cache: false,
        url: '/store-confirm/:orderType',
        views: {
            'menuContent': {
                templateUrl: 'templates/store-confirm.html',
                controller: 'StoreConfirmCtrl'
            }
        }
    })

    .state('app.store-buy', {
        cache: false,
        url: '/store-buy/:orderType',
        views: {
            'menuContent': {
                templateUrl: 'templates/store-buy.html',
                controller: 'StoreBuyCtrl'
            }
        }
    })

    .state('app.news', {
        cache: false,
        url: '/news',
        views: {
            'menuContent': {
                templateUrl: 'templates/news.html',
                controller: 'NewsCtrl'
            }
        }
    })

    .state('app.news-detail', {
        cache: false,
        url: '/news-detail/:newsId',
        views: {
            'menuContent': {
                templateUrl: 'templates/news-detail.html',
                controller: 'NewsDetailCtrl'
            }
        }
    })

    .state('app.termsofdelivery', {
        url: '/termsofdelivery',
        views: {
            'menuContent': {
                templateUrl: 'templates/termsofdelivery.html',
                controller: 'TermsCtrl'
            }
        }
    })

    .state('app.delivery-location', {
        url: '/delivery-location',
        views: {
            'menuContent': {
                templateUrl: 'templates/delivery-location.html',
                controller: 'DeliveryCtrl'
            }
        }
    })

    .state('app.asks', {
        url: '/asks',
        views: {
            'menuContent': {
                templateUrl: 'templates/asks.html',
                controller: 'AsksCtrl'
            }
        }
    })

    .state('app.like', {
        url: '/like',
        views: {
            'menuContent': {
                templateUrl: 'templates/like.html',
                controller: 'ShareCtrl'
            }
        }
    })

    .state('app.share', {
        url: '/share',
        views: {
            'menuContent': {
                templateUrl: 'templates/share.html',
                controller: 'ShareCtrl'
            }
        }
    })

    .state('app.promo', {
        url: '/promo',
        views: {
            'menuContent': {
                templateUrl: 'templates/promo.html',
                controller: 'ShareCtrl'
            }
        }
    })

    .state('app.group', {
        url: '/group',
        views: {
            'menuContent': {
                templateUrl: 'templates/group.html',
                controller: 'ShareCtrl'
            }
        }
    })

    .state('app.termsloyality', {
        url: '/termsloyality',
        views: {
            'menuContent': {
                templateUrl: 'templates/termsloyality.html',
                controller: 'TermsCtrl'
            }
        }
    })

    .state('app.rules', {
        cache: false,
        url: '/rules',
        views: {
            'menuContent': {
                templateUrl: 'templates/rules.html',
                controller: 'RulesCtrl'
            }
        }
    })

    .state('app.about', {
        url: '/about',
        views: {
            'menuContent': {
                templateUrl: 'templates/about.html',
                controller: 'AboutCtrl'
            }
        }
    })

    .state('app.profile', {
        url: '/profile',
        views: {
            'menuContent': {
                templateUrl: 'templates/profile.html',
                controller: 'ProfileCtrl'
            }
        }
    })

    .state('app.profile1', {
        url: '/profile1',
        views: {
            'menuContent': {
                templateUrl: 'templates/profile1.html',
                controller: 'ProfileCtrl1'
            }
        }
    })

    .state('app.contact', {
        url: '/contact',
        views: {
            'menuContent': {
                templateUrl: 'templates/contact.html',
                controller: 'ContactCtrl'
            }
        }
    })

    .state('app.contact2', {
        url: '/contact2',
        views: {
            'menuContent': {
                templateUrl: 'templates/contact2.html',
                controller: 'Contact2Ctrl'
            }
        }
    })

    .state('app.support', {
        url: '/support',
        views: {
            'menuContent': {
                templateUrl: 'templates/support.html',
                controller: 'SupportCtrl'
            }
        }
    })

    .state('app.activity', {
        url: '/activity',
        views: {
            'menuContent': {
                templateUrl: 'templates/activity.html',
                controller: 'ActivityCtrl'
            }
        }
    })

    .state('app.order_category', {
        url: '/order_category',
        views: {
            'menuContent': {
                templateUrl: 'templates/order_category.html',
                controller: 'OrderCatCtrl'
            }
        }
    })

    .state('app.order_menue', {
        url: '/order_menue/:catId/:goodId',
        views: {
            'menuContent': {
                templateUrl: 'templates/order_menue.html',
                controller: 'OrderMenueCtrl'
            }
        }
    })

    .state('app.order_worker', {
        url: '/order_worker/:menId',
        views: {
            'menuContent': {
                templateUrl: 'templates/order_worker.html',
                controller: 'OrderWorkerCtrl'
            }
        }
    })

    .state('app.order_time', {
        url: '/order_time/:menId/:workId',
        views: {
            'menuContent': {
                templateUrl: 'templates/order_time.html',
                controller: 'OrderTimeCtrl'
            }
        }
    })

    .state('app.order', {
        url: '/order/:menId/:workId/:orderHour',
        views: {
            'menuContent': {
                templateUrl: 'templates/order.html',
                controller: 'OrderCtrl'
            }
        }
    })

    .state('app.order_final', {
        url: '/order_final/:menId/:workId/:orderHour',
        views: {
            'menuContent': {
                templateUrl: 'templates/order_final.html',
                controller: 'OrderFinalCtrl'
            }
        }
    })

    .state('app.order_bs_category', {
        url: '/order_bs_category',
        views: {
            'menuContent': {
                templateUrl: 'templates/order_bs_category.html',
                controller: 'OrderBSCatCtrl'
            }
        }
    })

    .state('app.order_bs_start', {
        url: '/order_bs_start/:catId/:goodId',
        views: {
            'menuContent': {
                templateUrl: 'templates/order_bs_start.html',
                controller: 'OrderBSStartCtrl'
            }
        }
    })

    .state('app.order_bs_worker', {
        url: '/order_bs_worker/:menId',
        views: {
            'menuContent': {
                templateUrl: 'templates/order_bs_worker.html',
                controller: 'OrderBSWorkerCtrl'
            }
        }
    })

    .state('app.order_bs_time', {
        url: '/order_bs_time/:menId/:workId',
        views: {
            'menuContent': {
                templateUrl: 'templates/order_bs_time.html',
                controller: 'OrderBSTimeCtrl'
            }
        }
    })

    .state('app.order_bs', {
        url: '/order_bs/:menId/:workId/:orderHour',
        views: {
            'menuContent': {
                templateUrl: 'templates/order_bs.html',
                controller: 'OrderBSCtrl'
            }
        }
    })

    .state('login2', {
      url: '/login2',
      templateUrl: 'templates/login2.html',
      controller: 'Login2Ctrl'
    })

    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
    })

    .state('policy', {
      url: '/policy',
      templateUrl: 'templates/policy.html',
      controller: 'PolicyCtrl'
    })

    ;

    // if none of the above states are matched, use this as the fallback
    // ORIGINAL
    // $urlRouterProvider.otherwise('/app/login');

    // COPY
    $urlRouterProvider.otherwise('login');
})

.config(function($translateProvider) {

    $translateProvider.translations("ru", {

      // FOR ALL
      org_name: "Штолле",
      go_website: "Перейти на сайт",
      org_website: "http://stolle.by",
      org_mail: "info@stolle.by",
      org_phone1: "80293317777",
      org_phone2: "80291444436",
      app_ios: "https://itunes.apple.com/us/app/stolle",
      app_android: "https://play.google.com/store/apps/details?id=by.olegtronics.stolle",
      dwn_app: "Установи приложение",
      get_bonuses: "и получи бонусы используя следующий промокод: ",
      icon_path: "http://www.olegtronics.com/admin/img/icons/stolle.png",
      instagram_hash: "#Штолле",
      internet_connection_title: "Сеть",
      internet_connection_body: "Соединение с интернетом прерванно!",
      all_attention: "Внимание!",
      all_phone_confirm_warn: "Необходимо подтвердить номер телефона и указать email в личном кабинете!",
      all_email_warn: "Необходимо указать email в личном кабинете!",
      all_phone_numbers: "Номера телефонов",
      all_go_to_email: "Указать email",
      all_one_number: "Единый номер",
      

      // LOGIN
      login_loading: "Загрузка..",
      login_txt_1: "Необходимо соединение с интернетом для первой загрузки приложения",
      login_txt_2: "Для самых свежих новостей необходимо включать приложение с подключеным интернетом!",
      login_goon: "Далее",
      login_db_tit: "База данных",
      login_db_tmp: "Ошибка с базой данных",
      login_serv_tit: "Ошибка сервера",
      login_serv_tmp: "Ошибка в ответе сервера",
      login_conf_tit: "Подтверждение",
      login_conf_tmp: "Подтвердите номер телефона",
      login_mail_tit: "Почта",
      login_mail_tmp: "Укажите Вашу почту",
      login_name_tit: "Имя",
      login_name_tmp: "Укажите Ваше имя",
      login_mob_tit: "Телефон",
      login_mob_tmp: "Укажите номер мобильного телефона",
      login_hello: "Здравствуйте",
      login_policy: "Осуществляя вход вы соглашаетесь с тем что прочли и принимаете ",
      login_policy_accept: "Пользовательское соглашение",

      // Surveys
      survey_radio_title: 'Опрос',
      survey_text_send: 'Отправить',
      survey_text_answer: 'Ваш ответ..',


      // MAIN
      main_card: "КАРТА ЛЮБИМОГО КЛИЕНТА",
      main_get_points: "ПРЕДЪЯВИ КАРТУ И ПОЛУЧИ БАЛЛЫ",
      main_get_points2: "ПОЛУЧИТЬ БАЛЛЫ",
      main_group_enter: "ВСТУПИТЬ",
      main_tell_friends: "ПОЛУЧИТЬ",
      main_rules: "БОНУСНАЯ ПРОГРАММА",
      main_others: "ЕЩЁ",
      main_gift_for_points: "ПОДАРКИ ЗА БАЛЛЫ",
      main_else: "ВСЁ",
      main_your_first_gift: "Ваш первый подарок",
      main_get_free_gifts: "ДЕЛИСЬ С ДРУЗЬЯМИ",
      main_delivery_area: "Зона доставки",
      main_menu: "Меню",
      main_location: "Местоположение",
      main_coffee: "Ароматный кофе",
      main_cocktail: "Коктейль",
      main_block_title: "Блок!",
      main_block_body: "Доступ к данной услуге был закрыт.",
      main_block_button: "ОК",
      main_phone_required_title: "Требуется номер телефона!",
      main_phone_required_body: "Для использования бонусной системы необходимо указать и подтвердить номер мобильного телефона в личном кабинете.",
      main_phone_required_button1: "Указать номер",
      main_phone_required_button2: "Отмена",
      main_ask_button1: "ДА",
      main_ask_button2: "НЕТ",
      main_order: "ЗАПИСЬ ОНЛАЙН",
      main_instagram: "МЫ В ИНСТАГРАМ",
      main_contacts: "КОНТАКТЫ",
      main_news_and_events: "НОВОСТИ И АКЦИИ",
      main_services_and_goods: "УСЛУГИ И ТОВАРЫ",
      main_rules_and_site: "ПРАВИЛА ПРОГРАММЫ",

      // MENU
      menu_main: "Главная",
      menu_about: "О нас",
      menu_share: "Поделиться с другом",
      menu_quest: "Опросы",
      menu_order: "Запись онлайн",
      menu_profile: "Личный кабинет",
      menu_support: "Техническая поддержка",
      menu_developers: "Разработчик",
      menu_lang_ru: "RU",
      menu_lang_de: "DE",

      // ABOUT
      about_contacts: "Контакт",

      about_name_1: "Кафе-пироговая в Минске на ул.Козлова",
      about_adress_1: "ул. Козлова, 8",
      about_work_time_1: "пн-сб: 10.00 - 21.00",
      about_work_time_1_1: "вс: 10.00 - 20.00",
      about_phone_1: "+375291624443",

      about_name_2: "Кафе-пироговая в Минске на ул.Раковской",
      about_adress_2: "ул.Раковская, 23",
      about_work_time_2: "пн-вс: 10.00-23.00",
      about_work_time_2_1: "",
      about_phone_2: "+375447508555",

      about_name_3: "Кафе-пироговая в Минске на пр.Независимости,53",
      about_adress_3: "пр. Независимости, 53",
      about_work_time_3: "пн-вс: 10.00-23.00",
      about_work_time_3_1: "",
      about_phone_3: "+375293317777",

      about_name_4: "Кафе-пироговая в Минске на ул. Интернациональная,23",
      about_adress_4: "ул.Интернациональная,23",
      about_work_time_4: "пн-сб: 10.00 - 23.00",
      about_work_time_4_1: "",
      about_phone_4: "+375447710051",

      about_name_5: "Кафе-пироговая в Минске на площади Победы",
      about_adress_5: "пр.Независимости,38",
      about_work_time_5: "пн-вс: 08.00-21.00",
      about_work_time_5_1: "",
      about_phone_5: "+375447212020",

      about_name_6: "Кафе-пироговая в Минске на ул.Свердлова",
      about_adress_6: "ул. Свердлова,22",
      about_work_time_6: "пн-сб: 10.00 - 22.00",
      about_work_time_6_1: "",
      about_phone_6: "+375296779012",

      about_name_7: "Кафе-пироговая в Минске на ул.Московская,22",
      about_adress_7: "ул. Московская,22 (ст.метро 'Институт Культуры')",
      about_work_time_7: "пн-вс: 09.00 - 21.00",
      about_work_time_7_1: "",
      about_phone_7: "+375293223340",

      about_name_8: "Кафе-пироговая в Минске на ул. В.Хоружей,8",
      about_adress_8: "ул. В. Хоружей,8 (возле Комаровского рынка)",
      about_work_time_8: "пн-сб: 09.00 - 21.00",
      about_work_time_8_1: "",
      about_phone_8: "+375296778868",

      about_name_9: "Кафе-пироговая в Минске на ул.К. Маркса",
      about_adress_9: "ул. К. Маркса, 20",
      about_work_time_9: "пн-сб: 09.00 - 22.00",
      about_work_time_9_1: "",
      about_phone_9: "+375296555350",

      about_name_10: "Кафе-пироговая в Минске на пр. Независимоcти, 19",
      about_adress_10: "пр. Независимоcти, 19",
      about_work_time_10: "пн-вс: 09.00 - 22.00",
      about_work_time_10_1: "",
      about_phone_10: "+375296098889",

      about_name_11: "Кафе-пироговая в Минске на ул.Сурганова, 50",
      about_adress_11: "ул. Сурганова, 50",
      about_work_time_11: "пн-вс: 09.00 - 22.00",
      about_work_time_11_1: "",
      about_phone_11: "+375293254090",

      about_top_5: "О нас",
      about_on_map: "Найти нас на карте",
      about_call: "Позвонить",
      about_letter: "Письмо",
      about_web: "Наш сайт",

      // SHARE
      share_with_friend: "Поделиться с другом",
      share_recommend_us: "Рекомендуйте нас друзьям и получайте баллы",
      share_if_friend_install: "Вам, если друг введет Ваш промокод",
      share_if_shared: "Другу, который ввел Ваш промокод",
      share_if_used: "Вам, если друг воспользуется нашими услугами",
      share_selfie_button1: "Отмена",
      share_selfie_button2: "Поделиться",

      // PROMO
      promo_top: "Поделиться с другом",
      promo_your_code: "Ваш промокод",
      promo_share_promo: "Поделитесь своим промокодом с друзьями",

      // ORDER
      order_top_1: "1/6 Выберите услугу",
      order_top_2: "2/6 Выберите услугу",
      order_top_3: "3/6 Выберите Мастера",
      order_top_4: "4/6 Выберите дату и время",
      order_top_5: "5/6 Введите контакты",
      order_top_6: "6/6 Подтвердите запись",
      order_bs_top_1: "Выберите услугу",
      order_bs_top_2: "Записаться",
      order_bs_top_3: "Выберите Мастера",
      order_bs_top_4: "Выберите дату и время",
      order_bs_top_5: "Введите контакты",
      order_bs_top_6: "Подтвердите запись",
      order_bs_go_on: "Далее",
      order_bs_online_reg_1: "Онлайн запись",
      order_bs_online_reg_2: "Beauty bar VL",
      order_bs_add_order: "Добавить запись",
      order_bs_your_name: "Ваше Имя",
      order_bs_your_name_ex: "Имя",
      order_bs_phone: "Телефон",
      order_bs_ex_phone_1: "Например: 79061234567, 375291234567",
      order_bs_ex_phone_2: "Только цифры! Пример: 79061234567, 375291234567",
      order_bs_email: "Email",
      order_bs_email_ex: "email@email.ru",
      order_bs_comment: "Комментарий",
      order_bs_comment_ex: "Мой комментарий",
      order_bs_confirm: "Записаться",
      order_cntr_pop_nottake_ord_tit: 'Заказы не принимаются!',
      order_cntr_pop_nottake_ord_tmp: 'В данный момент заказы не принимаются!',
      order_cntr_pop_nottake_ord_btn1: 'Закрыть',

      // PROFILE
      profile_top: "Личный кабинет",
      profile_my_promo: "Мой промокод:",
      profile_promo: "Промокод",
      profile_change_photo: "Поменять фото",
      profile_profile: "Профиль",
      profile_confirm_cancel: "Отменить",
      profile_confirm_wait: "Ожидает подтверждение",
      profile_confirm_ok: "Подтверждено",
      profile_confirm_not_ok: "Отклоненно",
      profile_confirm_canceled: "Отмененно пользователем",
      profile_my_points: "Мои баллы",
      profile_passed: "Одобрено",
      profile_stopped: "Отклонено",
      profile_profing: "Проверка",
      profile_money_sign: "руб",
      profile_gend_0: "Не указанно",
      profile_gend_1: "Мужской",
      profile_gend_2: "Женский",
      profile_fromwhere_0: "Не указанно",
      profile_fromwhere_1: "Заведение",
      profile_fromwhere_2: "Журналы",
      profile_fromwhere_3: "Биг борд",
      profile_fromwhere_4: "Интернет",
      profile_fromwhere_5: "Друзья",
      profile_myname: "Имя",
      profile_save_pic_button1: "Отмена",
      profile_save_pic_button2: "Применить",
      profile_restore_account_title: "Восстановить аккаунт",
      profile_restore_account_body: "Введите свой e-mail. Он должен совпадать с введенным на предыдущем телефоне.",
      profile_restore_account_button1: "НЕ ПОМНЮ",
      profile_restore_account_button2: "ПРОДОЛЖИТЬ",
      profile_orderpop_title: "Заказать",
      profile_orderpop_body: "Вы хотите повторить заказ?",
      profile_orderpop_button1: "НЕТ",
      profile_orderpop_button2: "ДА",
      profile_saved_title: "Сохраненно!",
      profile_saved_body: "Данные обновленны!",
      profile_repeat_title: "Повторить заказ?",
      profile_repeat_body: "Хотите повторить заказ?",
      profile_button_ok: "ОК",
      profile_additional_points_title: "Благодарим",
      profile_additional_points_body: "Вам будут начислены дополнительные баллы!",
      profile_block_title: "Блок!",
      profile_block_body: "Доступ к данной услуге был закрыт.",
      profile_wrongdata_title: "Неверные данные",
      profile_wrongdata_body: "Неверно задан промокод!",
      profile_promocode_title: "У Вас есть промо-код?",
      profile_promocode_button1: "НЕТ",
      profile_promocode_button2: "ПРИМЕНИТЬ",
      profile_phonenumber_title: "Требуется номер телефона!",
      profile_phonenumber_body: "Для внесения промокода необходимо указать и подтвердить номер мобильного телефона в личном кабинете.",
      profile_phonenumber_button1: "Указать номер",
      profile_phonenumber_button2: "Отмена",
      profile_cancel_order_title: "Отмена записи",
      profile_cancel_order_txt: "Вы хотите действительно отменить данную запись?",
      profile_cancel_order_btn_1: "Отмена",
      profile_cancel_order_btn_2: "Да",
      profile_modal_pop_notallowed_tit: "Внимание!",
      profile_modal_pop_notallowed_tmp: "Нельзя задавать промокод привлеченного Вами друга!",
      profile_modal_pop_notallowed_btn: "Закрыть",

      // PROFILE SETTINGS
      profile1_top: "Личный кабинет",
      profile1_settings_title: "Настройки",
      profile1_name: "Имя",
      profile1_surname: "Фамилия",
      profile1_middlename: "Отчество",
      profile1_gender: "Пол",
      profile1_birthday: "День рождения",
      profile1_mobile: "Моб. телефон",
      profile1_mobile_warn: "Только цифры! Пример: 375хххххххх",
      profile1_confirm_btn: "Подтвердить",
      profile1_mobile_placeholder: "Моб. телефон: 375хххххххх",
      profile1_confirm_sms: "Прислать СМС",
      profile1_confirm: "Подтвердить",
      profile1_phone: "Телефон",
      profile1_phone_placeholder: "Телефон: 375хххххххх",
      profile1_phone_warn: "Только цифры! Пример: 375хххххххх",
      profile1_email: "Email",
      profile1_email_placeholder: "email@email.com",
      profile1_email_warn: "Укажите правильный email!",
      profile1_street: "Улица",
      profile1_house: "Дом/корпус",
      profile1_entry: "Подъезд",
      profile1_flat: "№ квартиры",
      profile1_floor: "Этаж",
      profile1_from_where: "Откуда узнали?",
      profile1_save: "Сохранить",
      profile1_order_discount: "Заказать скидочную карту",
      profile1_gend_0: "Не указанно",
      profile1_gend_1: "Мужской",
      profile1_gend_2: "Женский",
      profile1_fromwhere_0: "Не указанно",
      profile1_fromwhere_1: "Заведение",
      profile1_fromwhere_2: "Журналы",
      profile1_fromwhere_3: "Биг борд",
      profile1_fromwhere_4: "Интернет",
      profile1_fromwhere_5: "Друзья",
      profile1_myname: "Имя",
      profile1_save_pic_button1: "Отмена",
      profile1_save_pic_button2: "Применить",
      profile1_restore_account_title: "Восстановить аккаунт",
      profile1_restore_account_body: "Введите свой e-mail. Он должен совпадать с введенным на предыдущем телефоне.",
      profile1_restore_account_button1: "НЕ ПОМНЮ",
      profile1_restore_account_button2: "ПРОДОЛЖИТЬ",
      profile1_orderpop_title: "Заказать",
      profile1_orderpop_body: "Вы хотите повторить заказ?",
      profile1_orderpop_button1: "НЕТ",
      profile1_orderpop_button2: "ДА",
      profile1_saved_title: "Сохраненно!",
      profile1_saved_body: "Данные обновленны!",
      profile1_phonevalid_title: "Внимание",
      profile1_phonevalid_body: "Повторный запрос только через 5 минут!",
      profile1_phonerepeat_title: "Код подтверждения",
      profile1_phonerepeat_body: "Вам придет СМС с кодом",
      profile1_phonerepeat_button1: "Отмена",
      profile1_phonerepeat_button2: "Подтвердить",
      profile1_phoneenter_button1: "Внимание",
      profile1_phoneenter_button2: "Укажите Ваш номер телефона!",
      profile1_phoneconf_title: "Спасибо!",
      profile1_phoneconf_body: "Номер подтвержден.",
      profile1_discount_title: "Дисконтная карта",
      profile1_discount_body: "Дисконтная карта добавленна!",
      profile1_blocked_title: "Внимание!",
      profile1_blocked_body: "Вы заблокированны!",
      profile1_unblocked_title: "Внимание!",
      profile1_unblocked_body: "Вы разблокированны!",
      profile1_wrongcode_title: "Внимание!",
      profile1_wrongcode_body: "Неверно заданный код!",
      profile1_waitconfirm_title: "Внимание!",
      profile1_waitconfirm_body: "Ожидайте одобрения.",
      profile1_whenready_title: "Запрос принят",
      profile1_whenready_body: "Как только Ваша скидочная карточка будет включена, Вы получете уведомление при открытие приложения и сможете сразу ею воспользоваться.<br/>Одобрение в течение 24 часов.",
      profile1_whenready_button: "ОК",
      profile1_shortphone_title1: "Слишком короткий номер.",
      profile1_shortphone_title2: "Пример: 375хххххххх",
      profile1_shortphone_body: "Длина номера телефона",
      profile1_shortphone_button: "ОК",
      profile1_nophone_title1: "Номер телефона не указан.",
      profile1_nophone_title2: "Пример: 375хххххххх",
      profile1_nophone_body: "Номера телефона",
      profile1_nophone_button: "ОК",
      profile1_smsconf_body: "Вам будет выслана СМС с кодом, который необходимо внести в строку \"код\" и нажать \"подтвердить\" в личном профиле.",
      profile1_smsconf_title: "Подтверждение мобильного телефона",
      profile1_smsconf_button1: "Отмена",
      profile1_smsconf_button2: "Прислать СМС",
      profile1_enter_phonenumber_title: "Номер телефона",
      profile1_enter_phonenumber_body: "Укажите номер Вашего мобильного телефона!",
      profile1_discountpop_nonum_title: "Номер телефона",
      profile1_discountpop_nonum_body: "Укажите номер Вашего мобильного телефона!",
      profile1_discountpop_nobirth_title: "День рождения",
      profile1_discountpop_nobirth_body: "Укажите Ваше день рождения!",
      profile1_discountpop_nogend_title: "Пол",
      profile1_discountpop_nogend_body: "Укажите Ваш пол!",
      profile1_discountpop_nosurname_title: "Отчество",
      profile1_discountpop_nosurname_body: "Укажите Ваше отчество!",
      profile1_discountpop_nofamily_title: "Фамилия",
      profile1_discountpop_nofamily_body: "Укажите Вашу фамилию!",
      profile1_discountpop_noname_title: "Имя",
      profile1_discountpop_noname_body: "Укажите Ваше имя!",
      profile1_restore_account_title: "Восстановить аккаунт",
      profile1_restore_account_body: "Введите свой e-mail. Он должен совпадать с введенным на предыдущем телефоне.",
      profile1_restore_account_button1: "НЕ ПОМНЮ",
      profile1_restore_account_button2: "ПРОДОЛЖИТЬ",
      profile1_order_again_title: "Заказать",
      profile1_order_again_body: "Вы хотите повторить заказ?",
      profile1_order_again_button1: "НЕТ",
      profile1_order_again_button2: "ДА",
      profile1_data_saved_title: "Сохраненно!",
      profile1_data_saved_body: "Данные обновленны!",
      profile1_repeat_order_title: "Повторить заказ?",
      profile1_repeat_order_body: "Вы хотите повторить заказ?",

      // SUPPORT
      support_top: "Техподдержка",
      support_call: "Позвонить",
      support_your_message: "Ваше сообщение",
      support_first_message: "Добрый день!",
      support_request_discount_name: "Запрос",
      support_request_discount_message: "Скидочная карта",
      support_pop_title: "Написать в техподдержку",
      support_pop_body: "Приготовьтесь сообщить оператору Ваш ID:",
      support_pop_button1: "ОТМЕНА",
      support_pop_button2: "ПРОДОЛЖИТЬ",


      // TERMS OF LOYALITY
      terms_top: "Условия доставки",
      terms_use: "Правила бонусной программы",
      terms_bonus: "Правила пользования бонусной программы, действующей в",
      terms_delivery_body1: "Еда с доставкой. Заказ еды на дом. Заказать пирог в офис. Сеть пироговых \"Штолле\"",
      terms_delivery_body2: "Лучшей гарантией того, что вам привезут пирог прямо из печи и в указанное вами время, является заблаговременный заказ еды на дом по телефону или на сайте. Заказы на сайте принимаются с 09.30 до 22.00, заказы по телефону принимаются с 9.30 до 20.00.",
      terms_delivery_body3: "Доставка по предварительным заказам осуществляется с 11.00 до 19.00 ежедневно.",
      terms_delivery_body4: "С 1-го декабря 2016 года доставка пирогов \"Штолле\" в пределах кольцевой г.Минска- 4 руб., а за МКАД- 7 руб. При заказе пирогов на сумму свыше 100 руб., доставка пирогов бесплатна в пределах г. Минска.",
      terms_delivery_body5: "Обратите внимание, что все пироги «Штолле» Вам доставят только в фирменных коробках «Штолле» с целью обеспечить нужные условия транспортировки пирогов. При доставке пирогов фирменная коробка \"Штолле\" бесплатна.",
      terms_delivery_body6: "При покупке пирогов навынос, стоимость большой, средней и маленькой коробки составляет 50 коп., 40 коп. и 30 коп.",
      terms_delivery_body7: "1. Необходимо учитывать, что при заказе \"сегодня на сегодня\" заявки принимаются на пироги, имеющиеся в наличии и свободное время по доставке предлагается оператором.",
      terms_delivery_body8: "2. При заказе \"сегодня на завтра\" вы можете выбрать любые пироги из всего указанного ассортимента, а так же если заявка необходима на первую половину дня.",
      terms_delivery_body9: "3. При доставке пироги не режутся, а доставляются целыми. Указанный вес пирогов и начинка не меняется.",
      terms_delivery_body10: "4. Для оформления заказа на сайте просто поместите выбранные пироги в корзину. Также необходимо указывать в комментариях дату и время, а также адрес, по которому вы бы хотели получить ваш заказ домой или в офис. Мы принимаем заявки на неделю-две вперед, к планируемой дате торжества, на завтрашний или в тот же день.",
      terms_delivery_body11: "5. Администратор сайта должен звонком подтвердить ваш заказ в течение часа. Если звонка от администратора не поступило в течение часа, рекомендуем продублировать ваш заказ по телефонам: +375293317777 либо +375291444436.",
      terms_delivery_body12: "6. ВАЖНО! Оплата пирогов при доставке осуществляется только наличными!",
      terms_loyality_body1: "Бонусная система",
      terms_loyality_body2: "Уважаемые клиенты (посетители) и пользователи мобильного приложения «Штолле»!",
      terms_loyality_body3: " В мобильном приложении Штолле есть Ваша персональная карта любимого клиента (бонусная программа лояльности).",
      terms_loyality_body4: " Предъявляйте ее персоналу каждый раз, когда заходите к нам.",
      terms_loyality_body5: " Карта любимого клиента позволяет Вам накапливать баллы за приобретенные пироги и обменивать их на подарки из списка подарков в мобильном приложение.",
      terms_loyality_body6: " Баллы начисляются в эквиваленте 10% от суммы, потраченной на пироги. Максимальный предел накопления – 1000 баллов. Для продолжения накопления необходимо израсходовать часть баллов. Баллы не начисляются при заказе пирогов с доставкой.",
      terms_loyality_body9: " Получить подарки за баллы можно только в «Штолле».",
      terms_loyality_body11: " В случае, если Вы не воспользуетесь картой постоянного клиента для начисления баллов или получения подарков в течении 6 месяцев неиспользованные баллы будут аннулированы.",
      terms_loyality_body12: " Одновременное использование дисконтной программы лояльности и бонусной программы лояльности (в мобильном приложении) запрещено.",
      terms_loyality_body13: " Администрация оставляет за собой право изменять условия бонусной программы лояльности в одностороннем порядке.",
      terms_loyality_body14: " Начисление баллов не распространяется на обеденное меню, специальные и акционные предложения!",
      terms_loyality_body15: " В праздничные и предпраздничные дни начисление баллов и выдача подарков за баллы (кроме первого подарка за регистрацию) не осуществляется.",
      terms_install_and_get1: "Установи приложение ",
      terms_install_and_get2: " и получи бонусы используя следующий промо-код: ",

      // BILL
      bill_enter: "Введите сумму, которую Вы потратили",
      bill_ready: "готово",
      bill_money: "р",
      bill_money_rest: "к",
      bill_card_use_title: "Внимание!",
      bill_card_use_body: "Данная карточка уже используется другим пользователем!",
      bill_calcbill_title: "Внимание!",
      bill_calcbill_body: "Введите сумму чека!",
      bill_discount_title: "Завести скидочную карточку?",
      bill_discount_body: "При наличие скидочной карточки, Вы можете сохранить ее в телефоне.",
      bill_discount_button1: "ВНЕСТИ СУЩЕСТВУЮЩУЮ",
      bill_discount_button2: "ОТМЕНА",
      bill_wrong_code_title: "Неверный код!",
      bill_wrong_code_body: "Код который Вы считали не является кодом нашей компании.",
      bill_wrong_code_button1: "ЕЩЕ РАЗ",
      bill_wrong_code_button2: "ОТМЕНА",
      bill_checkcode_error_title: "Ошибка!",
      bill_checkcode_error_body: "Произошла ошибка! Пожалуйста сообщите о следующей ошибки администратору:",
      bill_checkcode_error_button: "ОК",

      // CATEGORIES
      cat_top: "Меню",

      // STORE
      store_money: "руб",
      store_code_terms_top: "Условия доставки",
      store_code_terms_body: "Прежде чем сделать заказ, пожалуйста, ознакомьтесь с зоной доставки",
      store_code_terms_button: "Зона доставки",

      // STORE DETAIL
      store_detail_money: "руб",
      store_detail_weight: "г",
      store_detail_amount: "1 шт",
      store_detail_amount_txt: "шт",

      // STORE CONFIRM
      store_confirm_top: "Проверка заказа",
      store_confirm_top_takeaway: "Самовывоз",
      store_confirm_top_order: "Доставка",
      store_confirm_cart_empty: "Ваша карзина пуста",
      store_confirm_divider: "Дополнительно можно заказать:",
      store_confirm_free_souces_1: "Вам доступно:",
      store_confirm_free_souces_2: "бесплатных соуса.",
      store_confirm_free_souces_desc: "К каждой пицце и некоторым закускам можно заказать бесплатный порционный соус. Также вы можете заказать дополнительный платный соус.",
      store_confirm_money: "руб",
      store_confirm_weight: "г",
      store_confirm_amount: "шт",
      store_confirm_ready: "ОФОРМИТЬ",
      store_confirm_minsum_title: "Внимание!",
      store_confirm_minsum_body: "Минимальная сумма заказа",
      store_confirm_cart_empty_title: "Внимание!",
      store_confirm_cart_empty_body: "Добавьте товар в карзину!",
      store_confirm_delivery_cost: "Стоимость доставки 4 руб в пределах МКАД, 7 руб за МКАД",

      // STORE BUY
      store_buy_top: "Сделать заказ",
      store_buy_desc: "ОБРАТИТЕ ВНИМАНИЕ! Заказы на доставку блюд из основного меню принимаются с 10.00 до 23.00. Минимальная сума заказа: 11,5.",
      store_buy_money: "руб",
      store_buy_f_name: "Ваше имя",
      store_buy_l_name: "Ваша фамилия",
      store_buy_company: "Компания",
      store_buy_m_name: "Ваше отчество",
      store_buy_phone_1: "Ваш номер телефона",
      store_mobile_warn: "Только цифры! Пример: 375хххххххх",
      store_buy_town: "Минск",
      store_buy_street: "Ваша улица",
      store_buy_house: "Номер дома",
      store_buy_building: "Строительство",
      store_buy_entry: "Подъезд",
      store_buy_codeentry: "Код двери",
      store_buy_floor: "Этаж",
      store_buy_flat: "Квартира",
      store_buy_email: "Ваш email",
      store_buy_order_type_0: "Адрес доставки",
      store_buy_order_type_1: "Адрес самовывоза",
      store_buy_adress: "Адрес доставки",
      store_buy_adv_info_1: "Форма оплаты: ",
      store_buy_adv_info_2: "Комментарий: ",
      store_buy_advert: "Откуда узнали?",
      store_buy_d_type: "Доставка или самовывоз?",
      store_buy_summ: "Сумма к оплате",
      store_buy_wait_time: "Время ожидания",
      store_buy_p_type: "Способ оплаты",
      store_buy_amount: "С какой суммы подготовить сдачу? Пример: ",
      store_buy_out_money: "Наличными курьеру",
      store_buy_out_card: "Картой курьеру (платежный терминал)",
      store_buy_out_beznal: "Безналичный расчет",
      store_buy_in_money: "Наличными в заведении",
      store_buy_in_card: "Картой в заведении (платежный терминал)",
      store_buy_in_beznal: "Безналичный расчет",
      store_buy_in_street: "Заведение",
      store_buy_submit: "РАЗМЕСТИТЬ ЗАКАЗ",
      store_buy_go_basket: "КОРЗИНА",
      store_buy_must: "обязательно к заполнению!",
      store_buy_place_title: "Разместить заказ?",
      store_buy_place_body1: "ИТОГО: ",
      store_buy_place_body2: " руб.",
      store_buy_place_body3: "Оформить заказ?",
      store_buy_place_button1: "ОТМЕНА",
      store_buy_place_button2: "ОК",
      store_buy_placed_title: "Заказ принят!",
      store_buy_placed_body: "Ваш заказ принят! Ожидайте звонка!",
      store_buy_place_error_title: "Что-то пошло не так!",
      store_buy_placed_error_body: "Произошла ошибка!",
      store_buy_placed_info_title1: "Внимание!",
      store_buy_placed_info_body1: "Укажите пожалуйста Ваш Email.",
      store_buy_placed_info_body2: "Укажите пожалуйста способ оплаты.",
      store_buy_placed_info_body3: "Укажите пожалуйста номер дома.",
      store_buy_placed_info_body4: "Укажите пожалуйста улицу доставки.",
      store_buy_placed_info_body5: "Укажите пожалуйста Ваше имя.",
      store_buy_placed_info_body6: "Укажите пожалуйста Ваш номер телефона.",
      store_buy_placed_info_body7: "Минимум 10 цифр с префиксом!",
      store_buy_placed_info_body8: "Пример: 375хххххххх",

      // NEWS
      news_top: "Новости",


      // QR-CODE
      qrcode_money: "руб",
      qrcode_ready: "готово",
      qrcode_review_title: "Спасибо!",
      qrcode_review_body: "Ваш отзыв принят!",
      qrcode_warn_title: "Внимание!",
      qrcode_warn_body: "Возможно каждые 12 часов!",
      qrcode_review_pic: "Прикрепить фото к отзыву?",
      qrcode_review_button1: "НЕТ",
      qrcode_review_button2: "ДА",
      qrcode_review_location: "Не выбрано заведение",
      qrcode_review_visit_title: "Пожалуйста оцените Ваш сегодняшний визит:",
      qrcode_review_visit_choice: "Выбор Заведения",
      qrcode_review_visit_comment: "Вы можете добавить комментарий. Увидит только руководство.",
      qrcode_review_visit_button1: "НЕ СЕЙЧАС",
      qrcode_review_visit_button2: "ОЦЕНИТЬ",
      qrcode_review_visit_pic_button1: "Отмена",
      qrcode_review_visit_pic_button2: "Отправить",

      // GROUP
      group_top: "Наша группа",
      group_enter: "Вступите в группу и будьте с нами",
      group_fb: "Вступить Facebook",
      group_ig: "Вступить Instagram",
      group_vk: "Вступить Вконтакте",
      group_link_fb: "http://www.facebook.com/pages/pages/",
      group_link_ig: "http://www.instagram.com/",
      group_link_vk: "http://www.vk.com/",

      // LIKE
      like_link_vk: "http://www.vk.com/",
      like_link_fb: "http://www.fb.com/",
      like_txt_1: "Поставьте лайк и получите 5 баллов",
      like_txt_2: "Поставьте Like на нашей странице",
      like_txt_3: "Нравиться",

      // CONTACT
      contact_top: "Наши адреса",

      // CONTACT 2
      contact2_top: "Контакт",
      contact2_call: "ПОЗВОНИТЬ",
      contact2_vk: "МЫ В ВК",
      contact2_fb: "МЫ В FACEBOOK",

      // GIFTS
      gifts_top: "Подарки за баллы",

      // GIFTS DETAIL
      gifts_detail_get: "Получить",
      gifts_detail_block_title: "Блок!",
      gifts_detail_block_body: "Доступ к данной услуге был закрыт.",
      gifts_detail_block_button: "ОК",
      gifts_detail_use_points_title: "Использовать баллы?",
      gifts_detail_use_points_body1: "При продолжении будет потрачено ",
      gifts_detail_use_points_body2: " баллов.",
      gifts_detail_use_points_body3: "Если Вы хотите потратить баллы сейчас, скажите об этом нашему сотруднику и нажмите \"Продолжить\".",
      gifts_detail_use_points_body4: "Если Вы хотите потратить 'ПЕРВЫЙ ПОДАРОК' сейчас, скажите об этом нашему сотруднику и нажмите \"Продолжить\".",
      gifts_detail_use_points_button1: "ОТМЕНА",
      gifts_detail_use_points_button2: "ПРОДОЛЖИТЬ",
      gifts_detail_need_phone_title: "Требуется номер телефона!",
      gifts_detail_need_phone_body: "Для использования бонусной системы необходимо указать и подтвердить номер мобильного телефона в личном кабинете.",
      gifts_detail_need_phone_button1: "Указать номер",
      gifts_detail_need_phone_button2: "Отмена",
      gifts_detail_waiter_scan_title: "Дайте сотруднику отсканировать данный код. Будет потрачено ",
      gifts_detail_waiter_scan_title2: " баллов.",
      gifts_detail_waiter_scan_button: "ГОТОВО"

    });

    $translateProvider.translations("en", {

      // FOR ALL
      org_name: "Штолле",
      go_website: "Visit website",
      org_website: "http://stolle.by",
      org_mail: "info@stolle.by",
      org_phone1: "80293317777",
      org_phone2: "80291444436",
      app_ios: "https://itunes.apple.com/us/app/stolle/",
      app_android: "https://play.google.com/store/apps/details?id=by.olegtronics.stolle",
      dwn_app: "Установи приложение",
      get_bonuses: "и получи бонусы используя следующий промокод: ",
      icon_path: "http://www.olegtronics.com/admin/img/icons/stolle.png",
      instagram_hash: "#Штолле",
      internet_connection_title: "Сеть",
      internet_connection_body: "Соединение с интернетом прерванно!",
      all_attention: "Внимание!",
      all_phone_confirm_warn: "Необходимо подтвердить номер телефона и указать email в личном кабинете!",
      all_email_warn: "Необходимо указать email в личном кабинете!",
      all_phone_numbers: "Номера телефонов",
      all_go_to_email: "Указать email",
      all_one_number: "Единый номер",

      // LOGIN
      login_loading: "Загрузка..",
      login_txt_1: "Необходимо соединение с интернетом для первой загрузки приложения",
      login_txt_2: "Для самых свежих новостей необходимо включать приложение с подключеным интернетом!",
      login_goon: "Далее",
      login_db_tit: "База данных",
      login_db_tmp: "Ошибка с базой данных",
      login_serv_tit: "Ошибка сервера",
      login_serv_tmp: "Ошибка в ответе сервера",
      login_conf_tit: "Подтверждение",
      login_conf_tmp: "Подтвердите номер телефона",
      login_mail_tit: "Почта",
      login_mail_tmp: "Укажите Вашу почту",
      login_name_tit: "Имя",
      login_name_tmp: "Укажите Ваше имя",
      login_mob_tit: "Телефон",
      login_mob_tmp: "Укажите номер мобильного телефона",
      login_hello: "Здравствуйте",
      login_policy: "Осуществляя вход вы соглашаетесь с тем что прочли и принимаете ",
      login_policy_accept: "Пользовательское соглашение",

      // Surveys
      survey_radio_title: 'Опрос',
      survey_text_send: 'Отправить',
      survey_text_answer: 'Ваш ответ..',

      // MAIN
      main_card: "КАРТА ЛЮБИМОГО КЛИЕНТА",
      main_get_points: "ПРЕДЪЯВИ КАРТУ И ПОЛУЧИ БАЛЛЫ",
      main_get_points2: "ПОЛУЧИТЬ БАЛЛЫ",
      main_group_enter: "ВСТУПИТЬ",
      main_tell_friends: "ПОЛУЧИТЬ",
      main_rules: "БОНУСНАЯ ПРОГРАММА",
      main_others: "ЕЩЁ",
      main_gift_for_points: "ПОДАРКИ ЗА БЫЛЛЫ",
      main_else: "ВСЁ",
      main_your_first_gift: "Ваш первый подарок",
      main_get_free_gifts: "ДЕЛИСЬ С ДРУЗЬЯМИ",
      main_delivery_area: "Зона доставки",
      main_menu: "Меню",
      main_location: "Местоположение",
      main_coffee: "Ароматный кофе",
      main_cocktail: "Коктейль",
      main_block_title: "Блок!",
      main_block_body: "Доступ к данной услуге был закрыт.",
      main_block_button: "ОК",
      main_phone_required_title: "Требуется номер телефона!",
      main_phone_required_body: "Для использования бонусной системы необходимо указать и подтвердить номер мобильного телефона в личном кабинете.",
      main_phone_required_button1: "Указать номер",
      main_phone_required_button2: "Отмена",
      main_ask_button1: "ДА",
      main_ask_button2: "НЕТ",
      main_order: "ЗАПИСЬ ОНЛАЙН",
      main_instagram: "МЫ В ИНСТАГРАМ",
      main_contacts: "КОНТАКТЫ",
      main_news_and_events: "НОВОСТИ И АКЦИИ",
      main_services_and_goods: "УСЛУГИ И ТОВАРЫ",
      main_rules_and_site: "ПРАВИЛА ПРОГРАММЫ",

      // MENU
      menu_main: "Главная",
      menu_about: "О нас",
      menu_share: "Поделиться с другом",
      menu_quest: "Опросы",
      menu_order: "Запись онлайн",
      menu_profile: "Личный кабинет",
      menu_support: "Техническая поддержка",
      menu_developers: "Разработчик",
      menu_lang_ru: "RU",
      menu_lang_de: "DE",

      // ABOUT
      about_contacts: "Контакт",

      about_name_1: "Кафе-пироговая в Минске на ул.Козлова",
      about_adress_1: "ул. Козлова, 8",
      about_work_time_1: "пн-сб: 10.00 - 21.00",
      about_work_time_1_1: "вс: 10.00 - 20.00",
      about_phone_1: "+375291624443",

      about_name_2: "Кафе-пироговая в Минске на ул.Раковской",
      about_adress_2: "ул.Раковская, 23",
      about_work_time_2: "пн-вс: 10.00-23.00",
      about_work_time_2_1: "",
      about_phone_2: "+375447508555",

      about_name_3: "Кафе-пироговая в Минске на пр.Независимости,53",
      about_adress_3: "пр. Независимости, 53",
      about_work_time_3: "пн-вс: 10.00-23.00",
      about_work_time_3_1: "",
      about_phone_3: "+375293317777",

      about_name_4: "Кафе-пироговая в Минске на ул. Интернациональная,23",
      about_adress_4: "ул.Интернациональная,23",
      about_work_time_4: "пн-сб: 10.00 - 23.00",
      about_work_time_4_1: "",
      about_phone_4: "+375447710051",

      about_name_5: "Кафе-пироговая в Минске на площади Победы",
      about_adress_5: "пр.Независимости,38",
      about_work_time_5: "пн-вс: 08.00-21.00",
      about_work_time_5_1: "",
      about_phone_5: "+375447212020",

      about_name_6: "Кафе-пироговая в Минске на ул.Свердлова",
      about_adress_6: "ул. Свердлова,22",
      about_work_time_6: "пн-сб: 10.00 - 22.00",
      about_work_time_6_1: "",
      about_phone_6: "+375296779012",

      about_name_7: "Кафе-пироговая в Минске на ул.Московская,22",
      about_adress_7: "ул. Московская,22 (ст.метро 'Институт Культуры')",
      about_work_time_7: "пн-вс: 09.00 - 21.00",
      about_work_time_7_1: "",
      about_phone_7: "+375293223340",

      about_name_8: "Кафе-пироговая в Минске на ул.В. Хоружей,8",
      about_adress_8: "ул. В. Хоружей,8 (возле Комаровского рынка)",
      about_work_time_8: "пн-сб: 09.00 - 21.00",
      about_work_time_8_1: "",
      about_phone_8: "+375296778868",

      about_name_9: "Кафе-пироговая в Минске на ул.К. Маркса",
      about_adress_9: "ул. К. Маркса, 20",
      about_work_time_9: "пн-сб: 09.00 - 22.00",
      about_work_time_9_1: "",
      about_phone_9: "+375296555350",

      about_name_10: "Кафе-пироговая в Минске на пр. Независимоcти, 19",
      about_adress_10: "пр. Независимоcти, 19",
      about_work_time_10: "пн-вс: 09.00 - 22.00",
      about_work_time_10_1: "",
      about_phone_10: "+375296098889",

      about_name_11: "Кафе-пироговая в Минске на ул.Сурганова, 50",
      about_adress_11: "ул. Сурганова, 50",
      about_work_time_11: "пн-вс: 09.00 - 22.00",
      about_work_time_11_1: "",
      about_phone_11: "+375293254090",

      about_top_5: "О нас",
      about_on_map: "Найти нас на карте",
      about_call: "Позвонить",
      about_letter: "Письмо",
      about_web: "Наш сайт",

      // SHARE
      share_with_friend: "Поделиться с другом",
      share_recommend_us: "Рекомендуйте нас друзьям и получайте баллы",
      share_if_friend_install: "Вам, если друг введет Ваш промокод",
      share_if_shared: "Другу, который ввел Ваш промокод",
      share_if_used: "Вам, если друг воспользуется нашими услугами",
      share_selfie_button1: "Отмена",
      share_selfie_button2: "Поделиться",

      // PROMO
      promo_top: "Поделиться с другом",
      promo_your_code: "Ваш промокод",
      promo_share_promo: "Поделитесь своим промокодом с друзьями",

      // ORDER
      order_top_1: "1/6 Выберите услугу",
      order_top_2: "2/6 Выберите услугу",
      order_top_3: "3/6 Выберите Мастера",
      order_top_4: "4/6 Выберите дату и время",
      order_top_5: "5/6 Введите контакты",
      order_top_6: "6/6 Подтвердите запись",
      order_bs_top_1: "Выберите услугу",
      order_bs_top_2: "Записаться",
      order_bs_top_3: "Выберите Мастера",
      order_bs_top_4: "Выберите дату и время",
      order_bs_top_5: "Введите контакты",
      order_bs_top_6: "Подтвердите запись",
      order_bs_go_on: "Далее",
      order_bs_online_reg_1: "Онлайн запись",
      order_bs_online_reg_2: "Beauty bar VL",
      order_bs_add_order: "Добавить запись",
      order_bs_your_name: "Ваше Имя",
      order_bs_your_name_ex: "Имя",
      order_bs_phone: "Телефон",
      order_bs_ex_phone_1: "Например: 79061234567, 375291234567",
      order_bs_ex_phone_2: "Только цифры! Пример: 79061234567, 375291234567",
      order_bs_email: "Email",
      order_bs_email_ex: "email@email.ru",
      order_bs_comment: "Комментарий",
      order_bs_comment_ex: "Мой комментарий",
      order_bs_confirm: "Записаться",
      order_cntr_pop_nottake_ord_tit: 'Заказы не принимаются!',
      order_cntr_pop_nottake_ord_tmp: 'В данный момент заказы не принимаются!',
      order_cntr_pop_nottake_ord_btn1: 'Закрыть',

      // PROFILE
      profile_top: "Личный кабинет",
      profile_my_promo: "Мой промокод:",
      profile_promo: "Промокод",
      profile_change_photo: "Поменять фото",
      profile_profile: "Профиль",
      profile_confirm_cancel: "Отменить",
      profile_confirm_wait: "Ожидает подтверждение",
      profile_confirm_ok: "Подтверждено",
      profile_confirm_not_ok: "Отклоненно",
      profile_confirm_canceled: "Отмененно пользователем",
      profile_my_points: "Мои баллы",
      profile_passed: "Одобрено",
      profile_stopped: "Отклонено",
      profile_profing: "Проверка",
      profile_money_sign: "руб",
      profile_gend_0: "Не указанно",
      profile_gend_1: "Мужской",
      profile_gend_2: "Женский",
      profile_fromwhere_0: "Не указанно",
      profile_fromwhere_1: "Заведение",
      profile_fromwhere_2: "Журналы",
      profile_fromwhere_3: "Биг борд",
      profile_fromwhere_4: "Интернет",
      profile_fromwhere_5: "Друзья",
      profile_myname: "Имя",
      profile_save_pic_button1: "Отмена",
      profile_save_pic_button2: "Применить",
      profile_restore_account_title: "Восстановить аккаунт",
      profile_restore_account_body: "Введите свой e-mail. Он должен совпадать с введенным на предыдущем телефоне.",
      profile_restore_account_button1: "НЕ ПОМНЮ",
      profile_restore_account_button2: "ПРОДОЛЖИТЬ",
      profile_orderpop_title: "Заказать",
      profile_orderpop_body: "Вы хотите повторить заказ?",
      profile_orderpop_button1: "НЕТ",
      profile_orderpop_button2: "ДА",
      profile_saved_title: "Сохраненно!",
      profile_saved_body: "Данные обновленны!",
      profile_repeat_title: "Повторить заказ?",
      profile_repeat_body: "Хотите повторить заказ?",
      profile_button_ok: "ОК",
      profile_additional_points_title: "Благодарим",
      profile_additional_points_body: "Вам будут начислены дополнительные баллы!",
      profile_block_title: "Блок!",
      profile_block_body: "Доступ к данной услуге был закрыт.",
      profile_wrongdata_title: "Неверные данные",
      profile_wrongdata_body: "Неверно задан промокод!",
      profile_promocode_title: "У Вас есть промо-код?",
      profile_promocode_button1: "НЕТ",
      profile_promocode_button2: "ПРИМЕНИТЬ",
      profile_phonenumber_title: "Требуется номер телефона!",
      profile_phonenumber_body: "Для внесения промокода необходимо указать и подтвердить номер мобильного телефона в личном кабинете.",
      profile_phonenumber_button1: "Указать номер",
      profile_phonenumber_button2: "Отмена",
      profile_cancel_order_title: "Отмена записи",
      profile_cancel_order_txt: "Вы хотите действительно отменить данную запись?",
      profile_cancel_order_btn_1: "Отмена",
      profile_cancel_order_btn_2: "Да",
      profile_modal_pop_notallowed_tit: "Внимание!",
      profile_modal_pop_notallowed_tmp: "Нельзя задавать промокод привлеченного Вами друга!",
      profile_modal_pop_notallowed_btn: "Закрыть",

      // PROFILE SETTINGS
      profile1_top: "Личный кабинет",
      profile1_settings_title: "Настройки",
      profile1_name: "Имя",
      profile1_surname: "Фамилия",
      profile1_middlename: "Отчество",
      profile1_gender: "Пол",
      profile1_birthday: "День рождения",
      profile1_mobile: "Моб. телефон",
      profile1_mobile_warn: "Только цифры! Пример: 375хххххххх",
      profile1_confirm_btn: "Подтвердить",
      profile1_mobile_placeholder: "Моб. телефон: 375хххххххх",
      profile1_confirm_sms: "Прислать СМС",
      profile1_confirm: "Подтвердить",
      profile1_phone: "Телефон",
      profile1_phone_placeholder: "Телефон: 375хххххххх",
      profile1_phone_warn: "Только цифры! Пример: 375хххххххх",
      profile1_email: "Email",
      profile1_email_placeholder: "Email: email@email.com",
      profile1_email_warn: "Укажите правильный email!",
      profile1_street: "Улица",
      profile1_house: "Дом/корпус",
      profile1_entry: "Подъезд",
      profile1_flat: "№ квартиры",
      profile1_floor: "Этаж",
      profile1_from_where: "Откуда узнали?",
      profile1_save: "Сохранить",
      profile1_order_discount: "Заказать скидочную карту",
      profile1_gend_0: "Не указанно",
      profile1_gend_1: "Мужской",
      profile1_gend_2: "Женский",
      profile1_fromwhere_0: "Не указанно",
      profile1_fromwhere_1: "Заведение",
      profile1_fromwhere_2: "Журналы",
      profile1_fromwhere_3: "Биг борд",
      profile1_fromwhere_4: "Интернет",
      profile1_fromwhere_5: "Друзья",
      profile1_myname: "Имя",
      profile1_save_pic_button1: "Отмена",
      profile1_save_pic_button2: "Применить",
      profile1_restore_account_title: "Восстановить аккаунт",
      profile1_restore_account_body: "Введите свой e-mail. Он должен совпадать с введенным на предыдущем телефоне.",
      profile1_restore_account_button1: "НЕ ПОМНЮ",
      profile1_restore_account_button2: "ПРОДОЛЖИТЬ",
      profile1_orderpop_title: "Заказать",
      profile1_orderpop_body: "Вы хотите повторить заказ?",
      profile1_orderpop_button1: "НЕТ",
      profile1_orderpop_button2: "ДА",
      profile1_saved_title: "Сохраненно!",
      profile1_saved_body: "Данные обновленны!",
      profile1_phonevalid_title: "Внимание",
      profile1_phonevalid_body: "Повторный запрос только через 5 минут!",
      profile1_phonerepeat_title: "Код подтверждения",
      profile1_phonerepeat_body: "Вам придет СМС с кодом",
      profile1_phonerepeat_button1: "Отмена",
      profile1_phonerepeat_button2: "Подтвердить",
      profile1_phoneenter_button1: "Внимание",
      profile1_phoneenter_button2: "Укажите Ваш номер телефона!",
      profile1_phoneconf_title: "Спасибо!",
      profile1_phoneconf_body: "Номер подтвержден.",
      profile1_discount_title: "Дисконтная карта",
      profile1_discount_body: "Дисконтная карта добавленна!",
      profile1_blocked_title: "Внимание!",
      profile1_blocked_body: "Вы заблокированны!",
      profile1_unblocked_title: "Внимание!",
      profile1_unblocked_body: "Вы разблокированны!",
      profile1_wrongcode_title: "Внимание!",
      profile1_wrongcode_body: "Неверно заданный код!",
      profile1_waitconfirm_title: "Внимание!",
      profile1_waitconfirm_body: "Ожидайте одобрения.",
      profile1_whenready_title: "Запрос принят",
      profile1_whenready_body: "Как только Ваша скидочная карточка будет включена, Вы получете уведомление при открытие приложения и сможете сразу ею воспользоваться.<br/>Одобрение в течение 24 часов.",
      profile1_whenready_button: "ОК",
      profile1_shortphone_title1: "Слишком короткий номер.",
      profile1_shortphone_title2: "Пример: 375хххххххх",
      profile1_shortphone_body: "Длина номера телефона",
      profile1_shortphone_button: "ОК",
      profile1_nophone_title1: "Номер телефона не указан.",
      profile1_nophone_title2: "Пример: 375хххххххх",
      profile1_nophone_body: "Номера телефона",
      profile1_nophone_button: "ОК",
      profile1_smsconf_body: "Вам будет выслана СМС с кодом, который необходимо внести в строку \"код\" и нажать \"подтвердить\" в личном профиле.",
      profile1_smsconf_title: "Подтверждение мобильного телефона",
      profile1_smsconf_button1: "Отмена",
      profile1_smsconf_button2: "Прислать СМС",
      profile1_enter_phonenumber_title: "Номер телефона",
      profile1_enter_phonenumber_body: "Укажите номер Вашего мобильного телефона!",
      profile1_discountpop_nonum_title: "Номер телефона",
      profile1_discountpop_nonum_body: "Укажите номер Вашего мобильного телефона!",
      profile1_discountpop_nobirth_title: "День рождения",
      profile1_discountpop_nobirth_body: "Укажите Ваше день рождения!",
      profile1_discountpop_nogend_title: "Пол",
      profile1_discountpop_nogend_body: "Укажите Ваш пол!",
      profile1_discountpop_nosurname_title: "Отчество",
      profile1_discountpop_nosurname_body: "Укажите Ваше отчество!",
      profile1_discountpop_nofamily_title: "Фамилия",
      profile1_discountpop_nofamily_body: "Укажите Вашу фамилию!",
      profile1_discountpop_noname_title: "Имя",
      profile1_discountpop_noname_body: "Укажите Ваше имя!",
      profile1_restore_account_title: "Восстановить аккаунт",
      profile1_restore_account_body: "Введите свой e-mail. Он должен совпадать с введенным на предыдущем телефоне.",
      profile1_restore_account_button1: "НЕ ПОМНЮ",
      profile1_restore_account_button2: "ПРОДОЛЖИТЬ",
      profile1_order_again_title: "Заказать",
      profile1_order_again_body: "Вы хотите повторить заказ?",
      profile1_order_again_button1: "НЕТ",
      profile1_order_again_button2: "ДА",
      profile1_data_saved_title: "Сохраненно!",
      profile1_data_saved_body: "Данные обновленны!",
      profile1_repeat_order_title: "Повторить заказ?",
      profile1_repeat_order_body: "Вы хотите повторить заказ?",

      // SUPPORT
      support_top: "Техподдержка",
      support_call: "Позвонить",
      support_your_message: "Ваше сообщение",
      support_first_message: "Добрый день!",
      support_request_discount_name: "Запрос",
      support_request_discount_message: "Скидочная карта",
      support_pop_title: "Написать в техподдержку",
      support_pop_body: "Приготовьтесь сообщить оператору Ваш ID:",
      support_pop_button1: "ОТМЕНА",
      support_pop_button2: "ПРОДОЛЖИТЬ",

      // TERMS OF LOYALITY
      terms_top: "Условия доставки",
      terms_use: "Правила бонусной программы",
      terms_bonus: "Правила пользования бонусной программы, действующей в",
      terms_delivery_body1: "Еда с доставкой. Заказ еды на дом. Заказать пирог в офис. Сеть пироговых \"Штолле\"",
      terms_delivery_body2: "Лучшей гарантией того, что вам привезут пирог прямо из печи и в указанное вами время, является заблаговременный заказ еды на дом по телефону или на сайте. Заказы на сайте принимаются с 09.30 до 22.00, заказы по телефону принимаются с 9.30 до 20.00.",
      terms_delivery_body3: "Доставка по предварительным заказам осуществляется с 11.00 до 19.00 ежедневно.",
      terms_delivery_body4: "С 1-го декабря 2016 года доставка пирогов \"Штолле\" в пределах кольцевой г.Минска- 4 руб., а за МКАД- 7 руб. При заказе пирогов на сумму свыше 100 руб., доставка пирогов бесплатна в пределах г. Минска.",
      terms_delivery_body5: "Обратите внимание, что все пироги «Штолле» Вам доставят только в фирменных коробках «Штолле» с целью обеспечить нужные условия транспортировки пирогов. При доставке пирогов фирменная коробка \"Штолле\" бесплатна.",
      terms_delivery_body6: "При покупке пирогов навынос, стоимость большой, средней и маленькой коробки составляет 50 коп., 40 коп. и 30 коп.",
      terms_delivery_body7: "1. Необходимо учитывать, что при заказе \"сегодня на сегодня\" заявки принимаются на пироги, имеющиеся в наличии и свободное время по доставке предлагается оператором.",
      terms_delivery_body8: "2. При заказе \"сегодня на завтра\" вы можете выбрать любые пироги из всего указанного ассортимента, а так же если заявка необходима на первую половину дня.",
      terms_delivery_body9: "3. При доставке пироги не режутся, а доставляются целыми. Указанный вес пирогов и начинка не меняется.",
      terms_delivery_body10: "4. Для оформления заказа на сайте просто поместите выбранные пироги в корзину. Также необходимо указывать в комментариях дату и время, а также адрес, по которому вы бы хотели получить ваш заказ домой или в офис. Мы принимаем заявки на неделю-две вперед, к планируемой дате торжества, на завтрашний или в тот же день.",
      terms_delivery_body11: "5. Администратор сайта должен звонком подтвердить ваш заказ в течение часа. Если звонка от администратора не поступило в течение часа, рекомендуем продублировать ваш заказ по телефонам: +375293317777 либо +375291444436.",
      terms_delivery_body12: "6. ВАЖНО! Оплата пирогов при доставке осуществляется только наличными!",
      terms_loyality_body1: "Бонусная система",
      terms_loyality_body2: "Уважаемые клиенты (посетители) и пользователи мобильного приложения «Штолле»!",
      terms_loyality_body3: " В мобильном приложении Штолле есть Ваша персональная карта любимого клиента (бонусная программа лояльности).",
      terms_loyality_body4: " Предъявляйте ее персоналу каждый раз, когда заходите к нам.",
      terms_loyality_body5: " Карта любимого клиента позволяет Вам накапливать баллы за приобретенные пироги и обменивать их на подарки из списка подарков в мобильном приложение.",
      terms_loyality_body6: " Баллы начисляются в эквиваленте 10% от суммы, потраченной на пироги. Максимальный предел накопления – 1000 баллов. Для продолжения накопления необходимо израсходовать часть баллов. Баллы не начисляются при заказе пирогов с доставкой.",
      terms_loyality_body9: " Получить подарки за баллы можно только в «Штолле».",
      terms_loyality_body11: " В случае, если Вы не воспользуетесь картой постоянного клиента для начисления баллов или получения подарков в течении 6 месяцев неиспользованные баллы будут аннулированы.",
      terms_loyality_body12: " Одновременное использование дисконтной программы лояльности и бонусной программы лояльности (в мобильном приложении) запрещено.",
      terms_loyality_body13: " Администрация оставляет за собой право изменять условия бонусной программы лояльности в одностороннем порядке.",
      terms_loyality_body14: " Начисление баллов не распространяется на обеденное меню, специальные и акционные предложения!",
      terms_loyality_body15: " В праздничные и предпраздничные дни начисление баллов и выдача подарков за баллы (кроме первого подарка за регистрацию) не осуществляется.",
      terms_install_and_get1: "Установи приложение ",
      terms_install_and_get2: " и получи бонусы используя следующий промо-код: ",

      // BILL
      bill_enter: "Введите сумму, которую Вы потратили",
      bill_ready: "готово",
      bill_money: "р",
      bill_money_rest: "к",
      bill_card_use_title: "Внимание!",
      bill_card_use_body: "Данная карточка уже используется другим пользователем!",
      bill_calcbill_title: "Внимание!",
      bill_calcbill_body: "Введите сумму чека!",
      bill_discount_title: "Завести скидочную карточку?",
      bill_discount_body: "При наличие скидочной карточки, Вы можете сохранить ее в телефоне.",
      bill_discount_button1: "ВНЕСТИ СУЩЕСТВУЮЩУЮ",
      bill_discount_button2: "ОТМЕНА",
      bill_wrong_code_title: "Неверный код!",
      bill_wrong_code_body: "Код который Вы считали не является кодом нашей компании.",
      bill_wrong_code_button1: "ЕЩЕ РАЗ",
      bill_wrong_code_button2: "ОТМЕНА",
      bill_checkcode_error_title: "Ошибка!",
      bill_checkcode_error_body: "Произошла ошибка! Пожалуйста сообщите о следующей ошибки администратору:",
      bill_checkcode_error_button: "ОК",

      // CATEGORIES
      cat_top: "Меню",

      // STORE
      store_money: "руб",

      store_code_terms_top: "Условия доставки",
      store_code_terms_body: "Прежде чем сделать заказ, пожалуйста, ознакомьтесь с зоной доставки",
      store_code_terms_button: "Зона доставки",

      // STORE DETAIL
      store_detail_money: "руб",
      store_detail_weight: "г",
      store_detail_amount: "1 шт",
      store_detail_amount_txt: "шт",

      // STORE CONFIRM
      store_confirm_top: "Проверка заказа",
      store_confirm_top_takeaway: "Самовывоз",
      store_confirm_top_order: "Доставка",
      store_confirm_cart_empty: "Ваша карзина пуста",
      store_confirm_divider: "Дополнительно можно заказать:",
      store_confirm_free_souces_1: "Вам доступно:",
      store_confirm_free_souces_2: "бесплатных соуса.",
      store_confirm_free_souces_desc: "К каждой пицце и некоторым закускам можно заказать бесплатный порционный соус. Также вы можете заказать дополнительный платный соус.",
      store_confirm_money: "руб",
      store_confirm_weight: "г",
      store_confirm_amount: "шт",
      store_confirm_ready: "ОФОРМИТЬ",
      store_confirm_minsum_title: "Внимание!",
      store_confirm_minsum_body: "Минимальная сумма заказа",
      store_confirm_cart_empty_title: "Внимание!",
      store_confirm_cart_empty_body: "Добавьте товар в карзину!",
      store_confirm_delivery_cost: "Стоимость доставки 4 руб в пределах МКАД, 7 руб за МКАД",

      // STORE BUY
      store_buy_top: "Сделать заказ",
      store_buy_desc: "ОБРАТИТЕ ВНИМАНИЕ! Заказы на доставку блюд из основного меню принимаются с 10.00 до 23.00. Минимальная сума заказа: 11,5.",
      store_buy_money: "руб",
      store_buy_f_name: "Ваше имя",
      store_buy_l_name: "Ваша фамилия",
      store_buy_company: "Компания",
      store_buy_m_name: "Ваше отчество",
      store_buy_phone_1: "Ваш номер телефона",
      store_mobile_warn: "Только цифры! Пример: 375хххххххх",
      store_buy_town: "Минск",
      store_buy_street: "Ваша улица",
      store_buy_house: "Номер дома",
      store_buy_building: "Строительство",
      store_buy_entry: "Подъезд",
      store_buy_codeentry: "Код двери",
      store_buy_floor: "Этаж",
      store_buy_flat: "Квартира",
      store_buy_email: "Ваш email",
      store_buy_order_type_0: "Адрес доставки",
      store_buy_order_type_1: "Адрес самовывоза",
      store_buy_adress: "Адрес доставки",
      store_buy_adv_info_1: "Форма оплаты: ",
      store_buy_adv_info_2: "Комментарий: ",
      store_buy_advert: "От куда узнали?",
      store_buy_d_type: "Доставка или самовывоз?",
      store_buy_summ: "Сумма к оплате",
      store_buy_wait_time: "Время ожидания",
      store_buy_p_type: "Способ оплаты",
      store_buy_amount: "С какой суммы подготовить сдачу? Пример: ",
      store_buy_out_money: "Наличными курьеру",
      store_buy_out_card: "Картой курьеру (платежный терминал)",
      store_buy_out_beznal: "Безналичный расчет",
      store_buy_in_money: "Наличными в заведении",
      store_buy_in_card: "Картой в заведении (платежный терминал)",
      store_buy_in_beznal: "Безналичный расчет",
      store_buy_in_street: "Заведение",
      store_buy_submit: "РАЗМЕСТИТЬ ЗАКАЗ",
      store_buy_go_basket: "КОРЗИНА",
      store_buy_must: "обязательно к заполнению!",
      store_buy_place_title: "Разместить заказ?",
      store_buy_place_body1: "ИТОГО: ",
      store_buy_place_body2: " руб.",
      store_buy_place_body3: "Оформить заказ?",
      store_buy_place_button1: "ОТМЕНА",
      store_buy_place_button2: "ОК",
      store_buy_placed_title: "Заказ принят!",
      store_buy_placed_body: "Ваш заказ принят! Ожидайте звонка!",
      store_buy_place_error_title: "Что-то пошло не так!",
      store_buy_placed_error_body: "Произошла ошибка!",
      store_buy_placed_info_title1: "Внимание!",
      store_buy_placed_info_body1: "Укажите пожалуйста Ваш Email.",
      store_buy_placed_info_body2: "Укажите пожалуйста способ оплаты.",
      store_buy_placed_info_body3: "Укажите пожалуйста номер дома.",
      store_buy_placed_info_body4: "Укажите пожалуйста улицу доставки.",
      store_buy_placed_info_body5: "Укажите пожалуйста Ваше имя.",
      store_buy_placed_info_body6: "Укажите пожалуйста Ваш номер телефона.",
      store_buy_placed_info_body7: "Минимум 10 цифр с префиксом!",
      store_buy_placed_info_body8: "Пример: 375хххххххх",

      // NEWS
      news_top: "Новости",

      // QR-CODE
      qrcode_money: "руб",
      qrcode_ready: "готово",
      qrcode_review_title: "Спасибо!",
      qrcode_review_body: "Ваш отзыв принят!",
      qrcode_warn_title: "Внимание!",
      qrcode_warn_body: "Возможно каждые 12 часов!",
      qrcode_review_pic: "Прикрепить фото к отзыву?",
      qrcode_review_button1: "НЕТ",
      qrcode_review_button2: "ДА",
      qrcode_review_location: "Не выбрано заведение",
      qrcode_review_visit_title: "Пожалуйста оцените Ваш сегодняшний визит:",
      qrcode_review_visit_choice: "Выбор Заведения",
      qrcode_review_visit_comment: "Вы можете добавить комментарий. Увидит только руководство.",
      qrcode_review_visit_button1: "НЕ СЕЙЧАС",
      qrcode_review_visit_button2: "ОЦЕНИТЬ",
      qrcode_review_visit_pic_button1: "Отмена",
      qrcode_review_visit_pic_button2: "Отправить",

      // GROUP
      group_top: "Наша группа",
      group_enter: "Вступите в группу и будьте с нами",
      group_fb: "Вступить Facebook",
      group_ig: "Вступить Instagram",
      group_vk: "Вступить Вконтакте",
      group_link_fb: "http://www.facebook.com/pages/pages/",
      group_link_ig: "http://www.instagram.com/",
      group_link_vk: "http://www.vk.com/",

      // LIKE
      like_link_vk: "http://www.vk.com/",
      like_link_fb: "http://www.fb.com/",
      like_txt_1: "Поставьте лайк и получите 5 баллов",
      like_txt_2: "Поставьте Like на нашей странице",
      like_txt_3: "Нравиться",

      // CONTACT
      contact_top: "Наши адреса",

      // CONTACT 2
      contact2_top: "Контакт",
      contact2_call: "ПОЗВОНИТЬ",
      contact2_vk: "МЫ В ВК",
      contact2_fb: "МЫ В FACEBOOK",

      // GIFTS
      gifts_top: "Подарки за баллы",

      // GIFTS DETAIL
      gifts_detail_get: "Получить",

      gifts_detail_block_title: "Блок!",
      gifts_detail_block_body: "Доступ к данной услуге был закрыт.",
      gifts_detail_block_button: "ОК",
      gifts_detail_use_points_title: "Использовать баллы?",
      gifts_detail_use_points_body1: "При продолжении будет потрачено ",
      gifts_detail_use_points_body2: " баллов.",
      gifts_detail_use_points_body3: "Если Вы хотите потратить баллы сейчас, скажите об этом нашему сотруднику и нажмите \"Продолжить\".",
      gifts_detail_use_points_body4: "Если Вы хотите потратить 'ПЕРВЫЙ ПОДАРОК' сейчас, скажите об этом нашему сотруднику и нажмите \"Продолжить\".",
      gifts_detail_use_points_button1: "ОТМЕНА",
      gifts_detail_use_points_button2: "ПРОДОЛЖИТЬ",
      gifts_detail_need_phone_title: "Требуется номер телефона!",
      gifts_detail_need_phone_body: "Для использования бонусной системы необходимо указать и подтвердить номер мобильного телефона в личном кабинете.",
      gifts_detail_need_phone_button1: "Указать номер",
      gifts_detail_need_phone_button2: "Отмена",
      gifts_detail_waiter_scan_title: "Дайте сотруднику отсканировать данный код. Будет потрачено ",
      gifts_detail_waiter_scan_title2: " баллов.",
      gifts_detail_waiter_scan_button: "ГОТОВО"

    });

    $translateProvider.translations("de", {

      // FOR ALL
      org_name: "Штолле",
      go_website: "Homepage besuchen",
      org_website: "http://stolle.by",
      org_mail: "info@stolle.by",
      org_phone1: "80293317777",
      org_phone2: "80291444436",
      app_ios: "https://itunes.apple.com/us/app/stolle",
      app_android: "https://play.google.com/store/apps/details?id=by.olegtronics.stolle",
      dwn_app: "App laden",
      get_bonuses: "und Bonis kassieren mit folgenden Promo-code: ",
      icon_path: "http://www.olegtronics.com/admin/img/icons/stolle.png",
      instagram_hash: "#Штолле",
      internet_connection_title: "Сеть",
      internet_connection_body: "Соединение с интернетом прерванно!",
      all_attention: "Внимание!",
      all_phone_confirm_warn: "Необходимо подтвердить номер телефона и указать email в личном кабинете!",
      all_email_warn: "Необходимо указать email в личном кабинете!",
      all_phone_numbers: "Номера телефонов",
      all_go_to_email: "Указать email",
      all_one_number: "Единый номер",

      // LOGIN
      login_loading: "Laden..",
      login_txt_1: "Internetverbindung notwendig für Erststart",
      login_txt_2: "Für neueste Nachrichten Internetverbindung notwendig!",
      login_goon: "Далее",
      login_db_tit: "База данных",
      login_db_tmp: "Ошибка с базой данных",
      login_serv_tit: "Ошибка сервера",
      login_serv_tmp: "Ошибка в ответе сервера",
      login_conf_tit: "Подтверждение",
      login_conf_tmp: "Подтвердите номер телефона",
      login_mail_tit: "Почта",
      login_mail_tmp: "Укажите Вашу почту",
      login_name_tit: "Имя",
      login_name_tmp: "Укажите Ваше имя",
      login_mob_tit: "Телефон",
      login_mob_tmp: "Укажите номер мобильного телефона",
      login_hello: "Здравствуйте",
      login_policy: "Осуществляя вход вы соглашаетесь с тем что прочли и принимаете ",
      login_policy_accept: "Пользовательское соглашение",

      // Surveys
      survey_radio_title: 'Umfrage',
      survey_text_send: 'Senden',
      survey_text_answer: 'Ihre Antwort..',

      // MAIN
      main_card: "STAMMGAST-KARTE",
      main_get_points: "ПРЕДЪЯВИ КАРТУ И ПОЛУЧИ БАЛЛЫ",
      main_get_points2: "ПОЛУЧИТЬ БАЛЛЫ",
      main_group_enter: "BEITRETTEN",
      main_tell_friends: "BEKOMMEN",
      main_rules: "DAS BONUSPROGRAMМ",
      main_others: "NOCH",
      main_gift_for_points: "GESCHENKE FÜR PUNKTE",
      main_else: "ALLES",
      main_your_first_gift: "Ihr erstes Geschenk",
      main_get_free_gifts: "ДЕЛИСЬ С ДРУЗЬЯМИ",
      main_delivery_area: "Lieferungsbereich",
      main_menu: "Menü",
      main_location: "Standort",
      main_coffee: "Aromatischer Kaffee",
      main_cocktail: "Cocktail",
      main_block_title: "Блок!",
      main_block_body: "Доступ к данной услуге был закрыт.",
      main_block_button: "ОК",
      main_phone_required_title: "Требуется номер телефона!",
      main_phone_required_body: "Для использования бонусной системы необходимо указать и подтвердить номер мобильного телефона в личном кабинете.",
      main_phone_required_button1: "Указать номер",
      main_phone_required_button2: "Отмена",
      main_ask_button1: "ДА",
      main_ask_button2: "НЕТ",
      main_order: "ЗАПИСЬ ОНЛАЙН",
      main_instagram: "МЫ В ИНСТАГРАМ",
      main_contacts: "КОНТАКТЫ",
      main_news_and_events: "НОВОСТИ И АКЦИИ",
      main_services_and_goods: "УСЛУГИ И ТОВАРЫ",
      main_rules_and_site: "ПРАВИЛА ПРОГРАММЫ",

      // MENU
      menu_main: "Startseite",
      menu_about: "Über uns",
      menu_share: "Mit Freunden teilen",
      menu_quest: "Umfragen",
      menu_order: "Onlinetermin",
      menu_profile: "Profil",
      menu_support: "Technische Unterstützung",
      menu_developers: "Entwickler",
      menu_lang_ru: "RU",
      menu_lang_de: "DE",

      // ABOUT
      about_contacts: "Контакт",

      about_name_1: "Кафе-пироговая в Минске на ул.Козлова",
      about_adress_1: "ул. Козлова, 8",
      about_work_time_1: "пн-сб: 10.00 - 21.00",
      about_work_time_1_1: "вс: 10.00 - 20.00",
      about_phone_1: "+375291624443",

      about_name_2: "Кафе-пироговая в Минске на ул.Раковской",
      about_adress_2: "ул.Раковская, 23",
      about_work_time_2: "пн-вс: 10.00-23.00",
      about_work_time_2_1: "",
      about_phone_2: "+375447508555",

      about_name_3: "Кафе-пироговая в Минске на пр.Независимости,53",
      about_adress_3: "пр. Независимости, 53",
      about_work_time_3: "пн-вс: 10.00-23.00",
      about_work_time_3_1: "",
      about_phone_3: "+375293317777",

      about_name_4: "Кафе-пироговая в Минске на ул. Интернациональная,23",
      about_adress_4: "ул.Интернациональная,23",
      about_work_time_4: "пн-сб: 10.00 - 23.00",
      about_work_time_4_1: "",
      about_phone_4: "+375447710051",

      about_name_5: "Кафе-пироговая в Минске на площади Победы",
      about_adress_5: "пр.Независимости,38",
      about_work_time_5: "пн-вс: 08.00-21.00",
      about_work_time_5_1: "",
      about_phone_5: "+375447212020",

      about_name_6: "Кафе-пироговая в Минске на ул.Свердлова",
      about_adress_6: "ул. Свердлова,22",
      about_work_time_6: "пн-сб: 10.00 - 22.00",
      about_work_time_6_1: "",
      about_phone_6: "+375296779012",

      about_name_7: "Кафе-пироговая в Минске на ул.Московская,22",
      about_adress_7: "ул. Московская,22 (ст.метро 'Институт Культуры')",
      about_work_time_7: "пн-вс: 09.00 - 21.00",
      about_work_time_7_1: "",
      about_phone_7: "+375293223340",

      about_name_8: "Кафе-пироговая в Минске на ул.В. Хоружей,8",
      about_adress_8: "ул. В. Хоружей,8 (возле Комаровского рынка)",
      about_work_time_8: "пн-сб: 09.00 - 21.00",
      about_work_time_8_1: "",
      about_phone_8: "+375296778868",

      about_name_9: "Кафе-пироговая в Минске на ул.К. Маркса",
      about_adress_9: "ул. К. Маркса, 20",
      about_work_time_9: "пн-сб: 09.00 - 22.00",
      about_work_time_9_1: "",
      about_phone_9: "+375296555350",

      about_name_10: "Кафе-пироговая в Минске на пр. Независимоcти, 19",
      about_adress_10: "пр. Независимоcти, 19",
      about_work_time_10: "пн-вс: 09.00 - 22.00",
      about_work_time_10_1: "",
      about_phone_10: "+375296098889",

      about_name_11: "Кафе-пироговая в Минске на ул.Сурганова, 50",
      about_adress_11: "ул. Сурганова, 50",
      about_work_time_11: "пн-вс: 09.00 - 22.00",
      about_work_time_11_1: "",
      about_phone_11: "+375293254090",
      
      about_top_5: "О нас",
      about_on_map: "Auf Karte anzeigen",
      about_call: "Anrufen",
      about_letter: "E-mail",
      about_web: "Homepage",

      // SHARE
      share_with_friend: "Mit freunden teilen",
      share_recommend_us: "Freunde empfehlen und Punkte kassieren",
      share_if_friend_install: "Wenn ein Freund unser App mit diesem Promo-code installiert",
      share_if_shared: "Wenn Sie in einem Sozialen Netzwerk teilen",
      share_if_used: "Вам, если друг воспользуется нашими услугами",
      share_selfie_button1: "Отмена",
      share_selfie_button2: "Поделиться",

      // PROMO
      promo_top: "Mit Freund teilen",
      promo_your_code: "Ihr Promo-code",
      promo_share_promo: "Teilen Sie ihren Promo-code mit Freunden",

      // ORDER
      order_top_1: "1/6 Выберите услугу",
      order_top_2: "2/6 Выберите услугу",
      order_top_3: "3/6 Выберите Мастера",
      order_top_4: "4/6 Выберите дату и время",
      order_top_5: "5/6 Введите контакты",
      order_top_6: "6/6 Подтвердите запись",
      order_bs_top_1: "Выберите услугу",
      order_bs_top_2: "Записаться",
      order_bs_top_3: "Выберите Мастера",
      order_bs_top_4: "Выберите дату и время",
      order_bs_top_5: "Введите контакты",
      order_bs_top_6: "Подтвердите запись",
      order_bs_go_on: "Далее",
      order_bs_online_reg_1: "Онлайн запись",
      order_bs_online_reg_2: "Beauty bar VL",
      order_bs_add_order: "Добавить запись",
      order_bs_your_name: "Ваше Имя",
      order_bs_your_name_ex: "Имя",
      order_bs_phone: "Телефон",
      order_bs_ex_phone_1: "Например: 79061234567, 375291234567",
      order_bs_ex_phone_2: "Только цифры! Пример: 79061234567, 375291234567",
      order_bs_email: "Email",
      order_bs_email_ex: "email@email.ru",
      order_bs_comment: "Комментарий",
      order_bs_comment_ex: "Мой комментарий",
      order_bs_confirm: "Записаться",
      order_cntr_pop_nottake_ord_tit: 'Заказы не принимаются!',
      order_cntr_pop_nottake_ord_tmp: 'В данный момент заказы не принимаются!',
      order_cntr_pop_nottake_ord_btn1: 'Закрыть',

      // PROFILE
      profile_top: "Profil",
      profile_my_promo: "Mein Promo-code:",
      profile_promo: "Promo-code",
      profile_change_photo: "Neues Foto",
      profile_profile: "Profil",
      profile_confirm_cancel: "Отменить",
      profile_confirm_wait: "Ожидает подтверждение",
      profile_confirm_ok: "Подтверждено",
      profile_confirm_not_ok: "Отклоненно",
      profile_confirm_canceled: "Отмененно пользователем",
      profile_my_points: "Meine Punkte",
      profile_passed: "Geprüft",
      profile_stopped: "Abgelehnt",
      profile_profing: "Wird geprüft",
      profile_money_sign: "rub",
      profile_gend_0: "Не указанно",
      profile_gend_1: "Мужской",
      profile_gend_2: "Женский",
      profile_fromwhere_0: "Не указанно",
      profile_fromwhere_1: "Заведение",
      profile_fromwhere_2: "Журналы",
      profile_fromwhere_3: "Биг борд",
      profile_fromwhere_4: "Интернет",
      profile_fromwhere_5: "Друзья",
      profile_myname: "Имя",
      profile_save_pic_button1: "Отмена",
      profile_save_pic_button2: "Применить",
      profile_restore_account_title: "Восстановить аккаунт",
      profile_restore_account_body: "Введите свой e-mail. Он должен совпадать с введенным на предыдущем телефоне.",
      profile_restore_account_button1: "НЕ ПОМНЮ",
      profile_restore_account_button2: "ПРОДОЛЖИТЬ",
      profile_orderpop_title: "Заказать",
      profile_orderpop_body: "Вы хотите повторить заказ?",
      profile_orderpop_button1: "НЕТ",
      profile_orderpop_button2: "ДА",
      profile_saved_title: "Сохраненно!",
      profile_saved_body: "Данные обновленны!",
      profile_repeat_title: "Повторить заказ?",
      profile_repeat_body: "Хотите повторить заказ?",
      profile_button_ok: "ОК",
      profile_additional_points_title: "Благодарим",
      profile_additional_points_body: "Вам будут начислены дополнительные баллы!",
      profile_block_title: "Блок!",
      profile_block_body: "Доступ к данной услуге был закрыт.",
      profile_wrongdata_title: "Неверные данные",
      profile_wrongdata_body: "Неверно задан промокод!",
      profile_promocode_title: "У Вас есть промо-код?",
      profile_promocode_button1: "НЕТ",
      profile_promocode_button2: "ПРИМЕНИТЬ",
      profile_phonenumber_title: "Требуется номер телефона!",
      profile_phonenumber_body: "Для внесения промокода необходимо указать и подтвердить номер мобильного телефона в личном кабинете.",
      profile_phonenumber_button1: "Указать номер",
      profile_phonenumber_button2: "Отмена",
      profile_cancel_order_title: "Отмена записи",
      profile_cancel_order_txt: "Вы хотите действительно отменить данную запись?",
      profile_cancel_order_btn_1: "Отмена",
      profile_cancel_order_btn_2: "Да",
      profile_modal_pop_notallowed_tit: "Внимание!",
      profile_modal_pop_notallowed_tmp: "Нельзя задавать промокод привлеченного Вами друга!",
      profile_modal_pop_notallowed_btn: "Закрыть",

      // PROFILE SETTINGS
      profile1_top: "Profil",
      profile1_settings_title: "Einstellungen",
      profile1_name: "Vorname",
      profile1_surname: "Nachname",
      profile1_middlename: "Vatersname",
      profile1_gender: "Geschlecht",
      profile1_birthday: "Geburtstag",
      profile1_mobile: "Handy",
      profile1_mobile_warn: "Nur Nummern! z.B.: 375хххххххх",
      profile1_confirm_btn: "Подтвердить",
      profile1_mobile_placeholder: "Handy Nummer: 375хххххххх",
      profile1_confirm_sms: "Прислать СМС",
      profile1_confirm: "Подтвердить",
      profile1_phone: "Telefon",
      profile1_phone_placeholder: "Telefon: 375хххххххх",
      profile1_phone_warn: "Nur Nummern! z.B.: 375хххххххх",
      profile1_email: "E-mail",
      profile1_email_placeholder: "E-mail: email@email.com",
      profile1_email_warn: "Bitte valide Email angeben!",
      profile1_street: "Straße",
      profile1_house: "Hausnummer",
      profile1_entry: "Eingang",
      profile1_flat: "Zimmernummer",
      profile1_floor: "Stock",
      profile1_from_where: "Woher erfahren?",
      profile1_save: "Speichern",
      profile1_order_discount: "Rabattkarte bestellen",
      profile1_gend_0: "Не указанно",
      profile1_gend_1: "Мужской",
      profile1_gend_2: "Женский",
      profile1_fromwhere_0: "Не указанно",
      profile1_fromwhere_1: "Заведение",
      profile1_fromwhere_2: "Журналы",
      profile1_fromwhere_3: "Биг борд",
      profile1_fromwhere_4: "Интернет",
      profile1_fromwhere_5: "Друзья",
      profile1_myname: "Имя",
      profile1_save_pic_button1: "Отмена",
      profile1_save_pic_button2: "Применить",
      profile1_restore_account_title: "Восстановить аккаунт",
      profile1_restore_account_body: "Введите свой e-mail. Он должен совпадать с введенным на предыдущем телефоне.",
      profile1_restore_account_button1: "НЕ ПОМНЮ",
      profile1_restore_account_button2: "ПРОДОЛЖИТЬ",
      profile1_orderpop_title: "Заказать",
      profile1_orderpop_body: "Вы хотите повторить заказ?",
      profile1_orderpop_button1: "НЕТ",
      profile1_orderpop_button2: "ДА",
      profile1_saved_title: "Сохраненно!",
      profile1_saved_body: "Данные обновленны!",
      profile1_phonevalid_title: "Внимание",
      profile1_phonevalid_body: "Повторный запрос только через 5 минут!",
      profile1_phonerepeat_title: "Код подтверждения",
      profile1_phonerepeat_body: "Вам придет СМС с кодом",
      profile1_phonerepeat_button1: "Отмена",
      profile1_phonerepeat_button2: "Подтвердить",
      profile1_phoneenter_button1: "Внимание",
      profile1_phoneenter_button2: "Укажите Ваш номер телефона!",
      profile1_phoneconf_title: "Спасибо!",
      profile1_phoneconf_body: "Номер подтвержден.",
      profile1_discount_title: "Дисконтная карта",
      profile1_discount_body: "Дисконтная карта добавленна!",
      profile1_blocked_title: "Внимание!",
      profile1_blocked_body: "Вы заблокированны!",
      profile1_unblocked_title: "Внимание!",
      profile1_unblocked_body: "Вы разблокированны!",
      profile1_wrongcode_title: "Внимание!",
      profile1_wrongcode_body: "Неверно заданный код!",
      profile1_waitconfirm_title: "Внимание!",
      profile1_waitconfirm_body: "Ожидайте одобрения.",
      profile1_whenready_title: "Запрос принят",
      profile1_whenready_body: "Как только Ваша скидочная карточка будет включена, Вы получете уведомление при открытие приложения и сможете сразу ею воспользоваться.<br/>Одобрение в течение 24 часов.",
      profile1_whenready_button: "ОК",
      profile1_shortphone_title1: "Слишком короткий номер.",
      profile1_shortphone_title2: "Пример: 375хххххххх",
      profile1_shortphone_body: "Длина номера телефона",
      profile1_shortphone_button: "ОК",
      profile1_nophone_title1: "Номер телефона не указан.",
      profile1_nophone_title2: "Пример: 375хххххххх",
      profile1_nophone_body: "Номера телефона",
      profile1_nophone_button: "ОК",
      profile1_smsconf_body: "Вам будет выслана СМС с кодом, который необходимо внести в строку \"код\" и нажать \"подтвердить\" в личном профиле.",
      profile1_smsconf_title: "Подтверждение мобильного телефона",
      profile1_smsconf_button1: "Отмена",
      profile1_smsconf_button2: "Прислать СМС",
      profile1_enter_phonenumber_title: "Номер телефона",
      profile1_enter_phonenumber_body: "Укажите номер Вашего мобильного телефона!",
      profile1_discountpop_nonum_title: "Номер телефона",
      profile1_discountpop_nonum_body: "Укажите номер Вашего мобильного телефона!",
      profile1_discountpop_nobirth_title: "День рождения",
      profile1_discountpop_nobirth_body: "Укажите Ваше день рождения!",
      profile1_discountpop_nogend_title: "Пол",
      profile1_discountpop_nogend_body: "Укажите Ваш пол!",
      profile1_discountpop_nosurname_title: "Отчество",
      profile1_discountpop_nosurname_body: "Укажите Ваше отчество!",
      profile1_discountpop_nofamily_title: "Фамилия",
      profile1_discountpop_nofamily_body: "Укажите Вашу фамилию!",
      profile1_discountpop_noname_title: "Имя",
      profile1_discountpop_noname_body: "Укажите Ваше имя!",
      profile1_restore_account_title: "Восстановить аккаунт",
      profile1_restore_account_body: "Введите свой e-mail. Он должен совпадать с введенным на предыдущем телефоне.",
      profile1_restore_account_button1: "НЕ ПОМНЮ",
      profile1_restore_account_button2: "ПРОДОЛЖИТЬ",
      profile1_order_again_title: "Заказать",
      profile1_order_again_body: "Вы хотите повторить заказ?",
      profile1_order_again_button1: "НЕТ",
      profile1_order_again_button2: "ДА",
      profile1_data_saved_title: "Сохраненно!",
      profile1_data_saved_body: "Данные обновленны!",
      profile1_repeat_order_title: "Повторить заказ?",
      profile1_repeat_order_body: "Вы хотите повторить заказ?",

      // SUPPORT
      support_top: "Technische Hilfe",
      support_call: "Anrufen",
      support_your_message: "Ihre Nachricht",
      support_first_message: "Добрый день!",
      support_request_discount_name: "Запрос",
      support_request_discount_message: "Скидочная карта",
      support_pop_title: "Написать в техподдержку",
      support_pop_body: "Приготовьтесь сообщить оператору Ваш ID:",
      support_pop_button1: "ОТМЕНА",
      support_pop_button2: "ПРОДОЛЖИТЬ",

      // TERMS OF LOYALITY
      terms_top: "Lieferungsbedingungen",
      terms_use: "Bonusprogramm - Richtlinien",
      terms_bonus: "Regeln für die Nutzung des Bonusprogramms, in der jeweils gültigen",
      terms_delivery_body1: "Еда с доставкой. Заказ еды на дом. Заказать пирог в офис. Сеть пироговых \"Штолле\"",
      terms_delivery_body2: "Лучшей гарантией того, что вам привезут пирог прямо из печи и в указанное вами время, является заблаговременный заказ еды на дом по телефону или на сайте. Заказы на сайте принимаются с 09.30 до 22.00, заказы по телефону принимаются с 9.30 до 20.00.",
      terms_delivery_body3: "Доставка по предварительным заказам осуществляется с 11.00 до 19.00 ежедневно.",
      terms_delivery_body4: "С 1-го декабря 2016 года доставка пирогов \"Штолле\" в пределах кольцевой г.Минска- 4 руб., а за МКАД- 7 руб. При заказе пирогов на сумму свыше 100 руб., доставка пирогов бесплатна в пределах г. Минска.",
      terms_delivery_body5: "Обратите внимание, что все пироги «Штолле» Вам доставят только в фирменных коробках «Штолле» с целью обеспечить нужные условия транспортировки пирогов. При доставке пирогов фирменная коробка \"Штолле\" бесплатна.",
      terms_delivery_body6: "При покупке пирогов навынос, стоимость большой, средней и маленькой коробки составляет 50 коп., 40 коп. и 30 коп.",
      terms_delivery_body7: "1. Необходимо учитывать, что при заказе \"сегодня на сегодня\" заявки принимаются на пироги, имеющиеся в наличии и свободное время по доставке предлагается оператором.",
      terms_delivery_body8: "2. При заказе \"сегодня на завтра\" вы можете выбрать любые пироги из всего указанного ассортимента, а так же если заявка необходима на первую половину дня.",
      terms_delivery_body9: "3. При доставке пироги не режутся, а доставляются целыми. Указанный вес пирогов и начинка не меняется.",
      terms_delivery_body10: "4. Для оформления заказа на сайте просто поместите выбранные пироги в корзину. Также необходимо указывать в комментариях дату и время, а также адрес, по которому вы бы хотели получить ваш заказ домой или в офис. Мы принимаем заявки на неделю-две вперед, к планируемой дате торжества, на завтрашний или в тот же день.",
      terms_delivery_body11: "5. Администратор сайта должен звонком подтвердить ваш заказ в течение часа. Если звонка от администратора не поступило в течение часа, рекомендуем продублировать ваш заказ по телефонам: +375293317777 либо +375291444436.",
      terms_delivery_body12: "6. ВАЖНО! Оплата пирогов при доставке осуществляется только наличными!",
      terms_loyality_body1: "Бонусная система",
      terms_loyality_body2: "Уважаемые клиенты (посетители) и пользователи мобильного приложения «Штолле»!",
      terms_loyality_body3: " В мобильном приложении Штолле есть Ваша персональная карта любимого клиента (бонусная программа лояльности).",
      terms_loyality_body4: " Предъявляйте ее персоналу каждый раз, когда заходите к нам.",
      terms_loyality_body5: " Карта любимого клиента позволяет Вам накапливать баллы за приобретенные пироги и обменивать их на подарки из списка подарков в мобильном приложение.",
      terms_loyality_body6: " Баллы начисляются в эквиваленте 10% от суммы, потраченной на пироги. Максимальный предел накопления – 1000 баллов. Для продолжения накопления необходимо израсходовать часть баллов. Баллы не начисляются при заказе пирогов с доставкой.",
      terms_loyality_body9: " Получить подарки за баллы можно только в «Штолле».",
      terms_loyality_body11: " В случае, если Вы не воспользуетесь картой постоянного клиента для начисления баллов или получения подарков в течении 6 месяцев неиспользованные баллы будут аннулированы.",
      terms_loyality_body12: " Одновременное использование дисконтной программы лояльности и бонусной программы лояльности (в мобильном приложении) запрещено.",
      terms_loyality_body13: " Администрация оставляет за собой право изменять условия бонусной программы лояльности в одностороннем порядке.",
      terms_loyality_body14: " Начисление баллов не распространяется на обеденное меню, специальные и акционные предложения!",
      terms_loyality_body15: " В праздничные и предпраздничные дни начисление баллов и выдача подарков за баллы (кроме первого подарка за регистрацию) не осуществляется.",
      terms_install_and_get1: "Установи приложение ",
      terms_install_and_get2: " и получи бонусы используя следующий промо-код: ",

      // BILL
      bill_enter: "Geben Sie den Betrag an, den Sie ausgegeben haben",
      bill_ready: "Weiter",
      bill_money: "r",
      bill_money_rest: "k",
      bill_card_use_title: "Внимание!",
      bill_card_use_body: "Данная карточка уже используется другим пользователем!",
      bill_calcbill_title: "Внимание!",
      bill_calcbill_body: "Введите сумму чека!",
      bill_discount_title: "Завести скидочную карточку?",
      bill_discount_body: "При наличие скидочной карточки, Вы можете сохранить ее в телефоне.",
      bill_discount_button1: "ВНЕСТИ СУЩЕСТВУЮЩУЮ",
      bill_discount_button2: "ОТМЕНА",
      bill_wrong_code_title: "Неверный код!",
      bill_wrong_code_body: "Код который Вы считали не является кодом нашей компании.",
      bill_wrong_code_button1: "ЕЩЕ РАЗ",
      bill_wrong_code_button2: "ОТМЕНА",
      bill_checkcode_error_title: "Ошибка!",
      bill_checkcode_error_body: "Произошла ошибка! Пожалуйста сообщите о следующей ошибки администратору:",
      bill_checkcode_error_button: "ОК",

      // CATEGORIES
      cat_top: "Menü",

      // STORE
      store_money: "rub",
      store_code_terms_top: "Условия доставки",
      store_code_terms_body: "Прежде чем сделать заказ, пожалуйста, ознакомьтесь с зоной доставки",
      store_code_terms_button: "Зона доставки",

      // STORE DETAIL
      store_detail_money: "rub",
      store_detail_weight: "g",
      store_detail_amount: "1 stck",
      store_detail_amount_txt: "stck",

      // STORE CONFIRM
      store_confirm_top: "Prüfung der Bestellung",
      store_confirm_top_takeaway: "Selbstabholung",
      store_confirm_top_order: "Lieferung",
      store_confirm_cart_empty: "Ihr Warenkorb ist leer",
      store_confirm_divider: "Zusätzlich kann man bestellen:",
      store_confirm_free_souces_1: "Ihnen stehen zur Verfügung:",
      store_confirm_free_souces_2: "kostenlose Souce.",
      store_confirm_free_souces_desc: "Zu jeder Pizza und einigen Vorspeisen können kostenlose Souce bestellt werden. Genauso können auch Souce für Geld bestellt werden.",
      store_confirm_money: "rub",
      store_confirm_weight: "g",
      store_confirm_amount: "stck",
      store_confirm_ready: "ABSENDEN",
      store_confirm_minsum_title: "Внимание!",
      store_confirm_minsum_body: "Минимальная сумма заказа!",
      store_confirm_cart_empty_title: "Внимание!",
      store_confirm_cart_empty_body: "Добавьте товар в карзину!",
      store_confirm_delivery_cost: "Стоимость доставки 4 руб в пределах МКАД, 7 руб за МКАД",

      // STORE BUY
      store_buy_top: "Bestellung machen",
      store_buy_desc: "ACHTUNG! Bestellungen zur Lieferung werden von 10.00 bis 23.00 angenommen. Minimale Bestellsumme: 11,5.",
      store_buy_money: "rub",
      store_buy_f_name: "Ihr Vorname",
      store_buy_l_name: "Ihr Nachname",
      store_buy_company: "Firma",
      store_buy_m_name: "Ihr Vatersname",
      store_buy_phone_1: "Ihre Telefonnummer",
      store_mobile_warn: "Nur Nummern! Beispiel: 375хххххххх",
      store_buy_town: "Minsk",
      store_buy_street: "Ihre Straße",
      store_buy_house: "Hausnummer",
      store_buy_building: "Bau",
      store_buy_entry: "Eingang",
      store_buy_codeentry: "Türcode",
      store_buy_floor: "Stockwerk",
      store_buy_flat: "Wohnung",
      store_buy_email: "Ihre E-mail",
      store_buy_order_type_0: "Bestelladresse",
      store_buy_order_type_1: "Selbstabholadresse",
      store_buy_adress: "Bestelladresse",
      store_buy_adv_info_1: "Zahlungsform: ",
      store_buy_adv_info_2: "Kommentar: ",
      store_buy_advert: "Von wo erfahren?",
      store_buy_d_type: "Lieferung oder Selbstabholung?",
      store_buy_summ: "Bestellsumme",
      store_buy_wait_time: "Wartezeit",
      store_buy_p_type: "Zahlungsform",
      store_buy_amount: "Von welcher Summe Wechselgeld? z.B.:",
      store_buy_out_money: "Bargeld dem Kurier",
      store_buy_out_card: "Bankkarte dem Kurier (Zahlterminal)",
      store_buy_out_beznal: "Banküberweisung",
      store_buy_in_money: "Bargeld im Lokal",
      store_buy_in_card: "Bankkarte im Lokal (Zahlterminal)",
      store_buy_in_beznal: "Banküberweisung",
      store_buy_in_street: "Lokal",
      store_buy_submit: "BESTELLEN",
      store_buy_go_basket: "BESTELLKORB",
      store_buy_must: "unbedingt zum Ausfüllen!",
      store_buy_place_title: "Разместить заказ?",
      store_buy_place_body1: "ИТОГО: ",
      store_buy_place_body2: " руб.",
      store_buy_place_body3: "Оформить заказ?",
      store_buy_place_button1: "ОТМЕНА",
      store_buy_place_button2: "ОК",
      store_buy_placed_title: "Заказ принят!",
      store_buy_placed_body: "Ваш заказ принят! Ожидайте звонка!",
      store_buy_place_error_title: "Что-то пошло не так!",
      store_buy_placed_error_body: "Произошла ошибка!",
      store_buy_placed_info_title1: "Внимание!",
      store_buy_placed_info_body1: "Укажите пожалуйста Ваш Email.",
      store_buy_placed_info_body2: "Укажите пожалуйста способ оплаты.",
      store_buy_placed_info_body3: "Укажите пожалуйста номер дома.",
      store_buy_placed_info_body4: "Укажите пожалуйста улицу доставки.",
      store_buy_placed_info_body5: "Укажите пожалуйста Ваше имя.",
      store_buy_placed_info_body6: "Укажите пожалуйста Ваш номер телефона.",
      store_buy_placed_info_body7: "Минимум 10 цифр с префиксом!",
      store_buy_placed_info_body8: "Пример: 375хххххххх",

      // NEWS
      news_top: "Новости",

      // QR-CODE
      qrcode_money: "rub",
      qrcode_ready: "weiter",
      qrcode_review_title: "Спасибо!",
      qrcode_review_body: "Ваш отзыв принят!",
      qrcode_warn_title: "Внимание!",
      qrcode_warn_body: "Возможно каждые 12 часов!",
      qrcode_review_pic: "Прикрепить фото к отзыву?",
      qrcode_review_button1: "НЕТ",
      qrcode_review_button2: "ДА",
      qrcode_review_location: "Не выбрано заведение",
      qrcode_review_visit_title: "Пожалуйста оцените Ваш сегодняшний визит:",
      qrcode_review_visit_choice: "Выбор Заведения",
      qrcode_review_visit_comment: "Вы можете добавить комментарий. Увидит только руководство.",
      qrcode_review_visit_button1: "НЕ СЕЙЧАС",
      qrcode_review_visit_button2: "ОЦЕНИТЬ",
      qrcode_review_visit_pic_button1: "Отмена",
      qrcode_review_visit_pic_button2: "Отправить",

      // GROUP
      group_top: "Unsere Gruppe",
      group_enter: "Gruppe beitretten und uns folgen",
      group_fb: "Facebook beitretten",
      group_ig: "Instagram beitretten",
      group_vk: "Вконтакте beitretten",
      group_link_fb: "http://www.facebook.com/pages/pages/",
      group_link_ig: "http://www.instagram.com/",
      group_link_vk: "http://www.vk.com/",

      // LIKE
      like_link_vk: "http://www.vk.com/",
      like_link_fb: "http://www.fb.com/",
      like_txt_1: "Like abgeben und 5 Punkte kassieren",
      like_txt_2: "Like auf unseren Homepage hinterlassen",
      like_txt_3: "Gefällt",

      // CONTACT
      contact_top: "Unsere Adressen",

      // CONTACT 2
      contact2_top: "Kontakt",
      contact2_call: "ПОЗВОНИТЬ",
      contact2_vk: "МЫ В ВК",
      contact2_fb: "МЫ В FACEBOOK",

      // GIFTS
      gifts_top: "Geschenke für Punkte",

      // GIFTS DETAIL
      gifts_detail_get: "Erhalten",

      gifts_detail_block_title: "Блок!",
      gifts_detail_block_body: "Доступ к данной услуге был закрыт.",
      gifts_detail_block_button: "ОК",
      gifts_detail_use_points_title: "Использовать баллы?",
      gifts_detail_use_points_body1: "При продолжении будет потрачено ",
      gifts_detail_use_points_body2: " баллов.",
      gifts_detail_use_points_body3: "Если Вы хотите потратить баллы сейчас, скажите об этом нашему сотруднику и нажмите \"Продолжить\".",
      gifts_detail_use_points_body4: "Если Вы хотите потратить 'ПЕРВЫЙ ПОДАРОК' сейчас, скажите об этом нашему сотруднику и нажмите \"Продолжить\".",
      gifts_detail_use_points_button1: "ОТМЕНА",
      gifts_detail_use_points_button2: "ПРОДОЛЖИТЬ",
      gifts_detail_need_phone_title: "Требуется номер телефона!",
      gifts_detail_need_phone_body: "Для использования бонусной системы необходимо указать и подтвердить номер мобильного телефона в личном кабинете.",
      gifts_detail_need_phone_button1: "Указать номер",
      gifts_detail_need_phone_button2: "Отмена",
      gifts_detail_waiter_scan_title: "Дайте сотруднику отсканировать данный код. Будет потрачено ",
      gifts_detail_waiter_scan_title2: " баллов.",
      gifts_detail_waiter_scan_button: "ГОТОВО"

    });

    $translateProvider.preferredLanguage("ru");
    $translateProvider.fallbackLanguage("ru");
});