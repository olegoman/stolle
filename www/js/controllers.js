/* global angular, document, window */
'use strict';

angular.module('starter.controllers', [])

.controller('AppCtrl', function($rootScope, $scope, $state, $ionicModal, $ionicPopover, $timeout, $translate, $cordovaGlobalization, $ionicSideMenuDelegate, $ionicHistory, Store) {

    // Form data for the login modal
    $scope.loginData = {};
    $scope.isExpanded = false;
    $scope.hasHeaderFabLeft = false;
    $scope.hasHeaderFabRight = false;

    var navIcons = document.getElementsByClassName('ion-navicon');
    for (var i = 0; i < navIcons.length; i++) {
        navIcons.addEventListener('click', function() {
            this.classList.toggle('active');
        });
    }

    $scope.ChangeLanguage = function(lang){
        $translate.use(lang);
    }

    ////////////////////////////////////////
    // Layout Methods
    ////////////////////////////////////////

    $scope.hideNavBar = function() {
        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
    };

    $scope.showNavBar = function() {
        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
    };

    $scope.noHeader = function() {
        var content = document.getElementsByTagName('ion-content');
        for (var i = 0; i < content.length; i++) {
            if (content[i].classList.contains('has-header')) {
                content[i].classList.toggle('has-header');
            }
        }
    };

    $scope.setExpanded = function(bool) {
        $scope.isExpanded = bool;
    };

    $scope.setHeaderFab = function(location) {
        var hasHeaderFabLeft = false;
        var hasHeaderFabRight = false;

        switch (location) {
            case 'left':
                hasHeaderFabLeft = true;
                break;
            case 'right':
                hasHeaderFabRight = true;
                break;
        }

        $scope.hasHeaderFabLeft = hasHeaderFabLeft;
        $scope.hasHeaderFabRight = hasHeaderFabRight;
    };

    $scope.hasHeader = function() {
        var content = document.getElementsByTagName('ion-content');
        for (var i = 0; i < content.length; i++) {
            if (!content[i].classList.contains('has-header')) {
                content[i].classList.toggle('has-header');
            }
        }

    };

    $scope.hideHeader = function() {
        $scope.hideNavBar();
        $scope.noHeader();
    };

    $scope.showHeader = function() {
        $scope.showNavBar();
        $scope.hasHeader();
    };

    $scope.clearFabs = function() {
        var fabs = document.getElementsByClassName('button-fab');
        if (fabs.length && fabs.length > 1) {
            fabs[0].remove();
        }
    };

    $scope.toggleLeftSideMenu = function() {
        if($rootScope.platform == 'Android') {
            $ionicSideMenuDelegate.toggleLeft();
        }
        else {
            $ionicHistory.nextViewOptions({
              disableAnimate: true,
              disableBack: true
            });
            $ionicSideMenuDelegate.toggleLeft();
        }
    };

    Store.deliverycats().then(function() {
        // ORDERS AT PANEL TOP
        $scope.order_0 = (Store.ordercheck(0)).toFixed(2);
        // $scope.order_1 = (Store.ordercheck(1)).toFixed(2);
    }, function() {});

    $scope.$on('cart:updated', function(event,data) {
        $scope.order_0 = (Store.ordercheck(0)).toFixed(2);
        // $scope.order_1 = (Store.ordercheck(1)).toFixed(2);
    });

    Store.sumcheck().then(function(data) {
        $scope.order = data;
    });

    // TRANSLATOR
    $cordovaGlobalization.getPreferredLanguage().then(
    function(result) {
        var lang = result.value.split('-');
        $translate.use(lang[0]);
    },
    function(error) {});

})

.controller('PolicyCtrl', function($rootScope, $scope, $state) {
    
})

.controller('LoginCtrl', function($rootScope, $scope, $state, $timeout, $stateParams, $ionicHistory, $window, $ionicPopup, $cordovaSQLite, $ionicPlatform, $http, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $ionicLoading, $ionicScrollDelegate, $ionicNavBarDelegate, $translate, $cordovaNetwork, $localStorage, ionicMaterialMotion, ionicMaterialInk) {

    $ionicPlatform.ready(function() {

        $translate(['profile1_gend_0', 'profile1_gend_1', 'profile1_gend_2', 'profile1_fromwhere_0', 'profile1_fromwhere_1', 'profile1_fromwhere_2', 'profile1_fromwhere_3', 'profile1_fromwhere_4', 'profile1_fromwhere_5', 'profile1_myname', 'profile1_save_pic_button1', 'profile1_save_pic_button2', 'profile1_orderpop_title', 'profile1_orderpop_body', 'profile1_orderpop_button1', 'profile1_orderpop_button2', 'profile1_saved_title', 'profile1_saved_body', 'internet_connection_title', 'internet_connection_body', 'profile1_phonevalid_title', 'profile1_phonevalid_body', 'profile1_phonerepeat_title', 'profile1_phonerepeat_body', 'profile1_phonerepeat_button1', 'profile1_phonerepeat_button2', 'profile1_phoneenter_button1', 'profile1_phoneenter_button2', 'profile1_phoneconf_title', 'profile1_phoneconf_body', 'profile1_discount_title', 'profile1_discount_body', 'profile1_blocked_title', 'profile1_blocked_body', 'profile1_unblocked_title', 'profile1_unblocked_body', 'profile1_wrongcode_title', 'profile1_wrongcode_body', 'profile1_waitconfirm_title', 'profile1_waitconfirm_body', 'profile1_whenready_title', 'profile1_whenready_body', 'profile1_whenready_button', 'profile1_shortphone_title1', 'profile1_shortphone_title2', 'profile1_shortphone_body', 'profile1_shortphone_button', 'profile1_nophone_title1', 'profile1_whenready_title', 'profile1_nophone_title2', 'profile1_nophone_body', 'profile1_nophone_button', 'profile1_smsconf_body', 'profile1_smsconf_title', 'profile1_smsconf_button1', 'profile1_smsconf_button2', 'profile1_enter_phonenumber_title', 'profile1_enter_phonenumber_body', 'profile1_discountpop_nonum_title', 'profile1_discountpop_nonum_body', 'profile1_discountpop_nobirth_title', 'profile1_discountpop_nobirth_body', 'profile1_discountpop_nogend_title', 'profile1_discountpop_nogend_body', 'profile1_discountpop_nosurname_title', 'profile1_discountpop_nosurname_body', 'profile1_discountpop_nofamily_title', 'profile1_discountpop_nofamily_body', 'profile1_discountpop_noname_title', 'profile1_discountpop_noname_body', 'profile1_restore_account_title', 'profile1_restore_account_body', 'profile1_restore_account_button1', 'profile1_restore_account_button2', 'profile1_order_again_title', 'profile1_order_again_body', 'profile1_order_again_button1', 'profile1_order_again_button2', 'profile1_data_saved_title', 'profile1_data_saved_body', 'profile1_repeat_order_title', 'profile1_repeat_order_body', 'login_goon', 'login_db_tit', 'login_db_tmp', 'login_serv_tit', 'login_serv_tmp', 'login_conf_tit', 'login_conf_tmp', 'login_mail_tit', 'login_mail_tmp', 'login_name_tit', 'login_name_tmp', 'login_mob_tit', 'login_mob_tmp']).then(function(res) {

            $scope.profile1_gend_0 = res.profile1_gend_0;
            $scope.profile1_gend_1 = res.profile1_gend_1;
            $scope.profile1_gend_2 = res.profile1_gend_2;
            $scope.profile1_fromwhere_0 = res.profile1_fromwhere_0;
            $scope.profile1_fromwhere_1 = res.profile1_fromwhere_1;
            $scope.profile1_fromwhere_2 = res.profile1_fromwhere_2;
            $scope.profile1_fromwhere_3 = res.profile1_fromwhere_3;
            $scope.profile1_fromwhere_4 = res.profile1_fromwhere_4;
            $scope.profile1_fromwhere_5 = res.profile1_fromwhere_5;
            $scope.profile1_myname = res.profile1_myname;
            $scope.profile1_save_pic_button1 = res.profile1_save_pic_button1;
            $scope.profile1_save_pic_button2 = res.profile1_save_pic_button2;
            $scope.profile1_orderpop_title = res.profile1_orderpop_title;
            $scope.profile1_orderpop_body = res.profile1_orderpop_body;

            $scope.internet_connection_title = res.internet_connection_title;
            $scope.internet_connection_body = res.internet_connection_body;

            $scope.profile1_phonevalid_title = res.profile1_phonevalid_title;
            $scope.profile1_phonevalid_body = res.profile1_phonevalid_body;
            $scope.profile1_phonerepeat_title = res.profile1_phonerepeat_title;
            $scope.profile1_phonerepeat_body = res.profile1_phonerepeat_body;
            $scope.profile1_phonerepeat_button1 = res.profile1_phonerepeat_button1;
            $scope.profile1_phonerepeat_button2 = res.profile1_phonerepeat_button2;
            $scope.profile1_phoneenter_button1 = res.profile1_phoneenter_button1;
            $scope.profile1_phoneenter_button2 = res.profile1_phoneenter_button2;
            $scope.profile1_phoneconf_title = res.profile1_phoneconf_title;
            $scope.profile1_phoneconf_body = res.profile1_phoneconf_body;
            $scope.profile1_discount_title = res.profile1_discount_title;
            $scope.profile1_discount_body = res.profile1_discount_body;
            $scope.profile1_blocked_title = res.profile1_blocked_title;
            $scope.profile1_blocked_body = res.profile1_blocked_body;
            $scope.profile1_unblocked_title = res.profile1_unblocked_title;
            $scope.profile1_unblocked_body = res.profile1_unblocked_body;
            $scope.profile1_wrongcode_title = res.profile1_wrongcode_title;
            $scope.profile1_wrongcode_body = res.profile1_wrongcode_body;
            $scope.profile1_waitconfirm_title = res.profile1_waitconfirm_title;
            $scope.profile1_waitconfirm_body = res.profile1_waitconfirm_body;
            $scope.profile1_whenready_title = res.profile1_whenready_title;
            $scope.profile1_whenready_body = res.profile1_whenready_body;
            $scope.profile1_whenready_button = res.profile1_whenready_button;
            $scope.profile1_shortphone_title1 = res.profile1_shortphone_title1;
            $scope.profile1_shortphone_title2 = res.profile1_shortphone_title2;
            $scope.profile1_shortphone_body = res.profile1_shortphone_body;
            $scope.profile1_shortphone_button = res.profile1_shortphone_button;
            $scope.profile1_nophone_title1 = res.profile1_nophone_title1;
            $scope.profile1_whenready_title = res.profile1_whenready_title;
            $scope.profile1_nophone_title2 = res.profile1_nophone_title2;
            $scope.profile1_nophone_body = res.profile1_nophone_body;
            $scope.profile1_nophone_button = res.profile1_nophone_button;
            $scope.profile1_smsconf_body = res.profile1_smsconf_body;
            $scope.profile1_smsconf_title = res.profile1_smsconf_title;
            $scope.profile1_smsconf_button1 = res.profile1_smsconf_button1;
            $scope.profile1_smsconf_button2 = res.profile1_smsconf_button2;
            $scope.profile1_enter_phonenumber_title = res.profile1_enter_phonenumber_title;
            $scope.profile1_enter_phonenumber_body = res.profile1_enter_phonenumber_body;
            $scope.profile1_discountpop_nonum_title = res.profile1_discountpop_nonum_title;
            $scope.profile1_discountpop_nonum_body = res.profile1_discountpop_nonum_body;
            $scope.profile1_discountpop_nobirth_title = res.profile1_discountpop_nobirth_title;
            $scope.profile1_discountpop_nobirth_body = res.profile1_discountpop_nobirth_body;
            $scope.profile1_discountpop_nogend_title = res.profile1_discountpop_nogend_title;
            $scope.profile1_discountpop_nogend_body = res.profile1_discountpop_nogend_body;
            $scope.profile1_discountpop_nosurname_title = res.profile1_discountpop_nosurname_title;
            $scope.profile1_discountpop_nosurname_body = res.profile1_discountpop_nosurname_body;
            $scope.profile1_discountpop_nofamily_title = res.profile1_discountpop_nofamily_title;
            $scope.profile1_discountpop_nofamily_body = res.profile1_discountpop_nofamily_body;
            $scope.profile1_discountpop_noname_title = res.profile1_discountpop_noname_title;
            $scope.profile1_discountpop_noname_body = res.profile1_discountpop_noname_body;
            $scope.profile1_restore_account_title = res.profile1_restore_account_title;
            $scope.profile1_restore_account_body = res.profile1_restore_account_body;
            $scope.profile1_restore_account_button1 = res.profile1_restore_account_button1;
            $scope.profile1_restore_account_button2 = res.profile1_restore_account_button2;
            $scope.profile1_order_again_title = res.profile1_order_again_title;
            $scope.profile1_order_again_body = res.profile1_order_again_body;
            $scope.profile1_order_again_button1 = res.profile1_order_again_button1;
            $scope.profile1_order_again_button2 = res.profile1_order_again_button2;
            $scope.profile1_data_saved_title = res.profile1_data_saved_title;
            $scope.profile1_data_saved_body = res.profile1_data_saved_body;
            $scope.profile1_repeat_order_title = res.profile1_repeat_order_title;
            $scope.profile1_repeat_order_body = res.profile1_repeat_order_body;
            $scope.login_goon = res.login_goon;
            $scope.login_db_tit = res.login_db_tit;
            $scope.login_db_tmp = res.login_db_tmp;
            $scope.login_serv_tit = res.login_serv_tit;
            $scope.login_serv_tmp = res.login_serv_tmp;
            $scope.login_conf_tit = res.login_conf_tit;
            $scope.login_conf_tmp = res.login_conf_tmp;
            $scope.login_mail_tit = res.login_mail_tit;
            $scope.login_mail_tmp = res.login_mail_tmp;
            $scope.login_name_tit = res.login_name_tit;
            $scope.login_name_tmp = res.login_name_tmp;
            $scope.login_mob_tit = res.login_mob_tit;
            $scope.login_mob_tmp = res.login_mob_tmp;            

        });

        $scope.$on('$ionicView.beforeLeave', function(){
            $scope.connectvar = 1;
        });

        $scope.smsOrdered = 0;
        $scope.smsConfirmed = 0;

        if($localStorage.smsOrdered) {
            if($localStorage.smsOrdered == 1) {
                $scope.smsOrdered = 1;
            }
        }
        else {
            $localStorage.smsOrdered = 0;
        }

        $scope.connectvar = 0;

        $scope.intconnect = true;

        $timeout(function() {
            $ionicScrollDelegate.resize();
        }, 2000);

        var version = parseInt($rootScope.version) * 100;
        var minvers = 4.3*100;

        if($rootScope.platform == 'Android' && version < minvers) {

        }
        else {
            ionicMaterialInk.displayEffect();
        }

        $scope.user = {
            name: '',
            surname: '',
            middlename: '',
            gender: '',
            birthday: '',
            mob: '',
            tel: '',
            email: '',
            adress: '',
            install_where: ''
        };

        $scope.adr = {
            adress1: '',
            adress2: '',
            adress3: '',
            adress4: '',
            adress5: ''
        };

        $scope.myID = '';
        
        $scope.myPic = 'http://www.olegtronics.com/admin/images/user.png';

        $scope.saving = true;

        $scope.adr.adress1 = '';
        $scope.adr.adress2 = '';
        $scope.adr.adress3 = '';
        $scope.adr.adress4 = '';
        $scope.adr.adress5 = '';
        $scope.user.name = '';
        $scope.user.surname = '';
        $scope.user.middlename = '';
        $scope.user.email = '';
        $scope.user.tel = '';
        $scope.user.mob = '';
        $scope.user.institution = $rootScope.institution;
        $scope.user.pic = '';
        $scope.user.mob_confirm = 1;
        $scope.user.email_confirm = 0;

        $scope.usrGender = [{id: 0,name: $scope.profile1_gend_0},{id: 1,name: $scope.profile1_gend_1},{id: 2,name: $scope.profile1_gend_2}];

        $scope.usrFromWhere = [{id: 0,name: $scope.profile1_fromwhere_0},{id: 1,name: $scope.profile1_fromwhere_1},{id: 2,name: $scope.profile1_fromwhere_2},{id: 3,name: $scope.profile1_fromwhere_3},{id: 4,name: $scope.profile1_fromwhere_4},{id: 5,name: $scope.profile1_fromwhere_5}];

        $scope.user.gender = $scope.usrGender[0];
        $scope.user.install_where = $scope.usrFromWhere[0];
        $scope.myName = $scope.profile1_myname;

        var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
        $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {

            $scope.user_real_id = success.rows.item(0).user_real_id;

            if(success.rows.item(0).user_name != '0' && success.rows.item(0).user_name != '') {
                $scope.user.name = success.rows.item(0).user_name;
            }

            if(success.rows.item(0).user_surname != '0' && success.rows.item(0).user_surname != '') {
                $scope.user.surname = success.rows.item(0).user_surname;
            }

            if(success.rows.item(0).user_middlename != '0' && success.rows.item(0).user_middlename != '') {
                $scope.user.middlename = success.rows.item(0).user_middlename;
            }

            if(success.rows.item(0).user_email != '0' && success.rows.item(0).user_email != '') {
                $scope.user.email = success.rows.item(0).user_email;
            }

            if(success.rows.item(0).user_email_confirm == '1') {
                $scope.user.email_confirm = success.rows.item(0).user_email_confirm;
            }

            if(success.rows.item(0).user_mob_confirm != '') {
                $scope.user.mob_confirm = success.rows.item(0).user_mob_confirm;
                $scope.smsConfirmed = success.rows.item(0).user_mob_confirm;
            }
            else {
                $scope.user.mob_confirm = 0;
            }

            if(success.rows.item(0).user_mob != '0' && success.rows.item(0).user_mob != '') {
                $scope.user.mob = success.rows.item(0).user_mob;
            }

            if(success.rows.item(0).user_institution != '0' && success.rows.item(0).user_institution != '') {
            $scope.user.institution = success.rows.item(0).user_institution;
            }

            if(success.rows.item(0).user_real_id != '0' && success.rows.item(0).user_real_id != '') {
                $scope.myID = success.rows.item(0).user_real_id;
            }

        }, function(er) {
            $scope.user.mob_confirm = 0;
        });

        /*
        $scope.saved = function() {

            $ionicPopup.alert({
                title: "Сохраненно",
                template: "Данные сохранены!"
            });


            $timeout(function() {savedPopup.close();}, 3000);

        } 
        */

        $scope.timecalc = function(x) {
            // ОПРЕДЕЛЕНИЕ ВРЕМЯ
            var nowtime = new Date();
            var gottime = x * 1000;
            var nowtimediff = nowtime.getTime() - gottime;
            var onltime = new Date(gottime);
            var onlDateTime;
            var onlMonth;
            var onlDay;
            var onlHour;
            var onlMin;

            // DAYS AGO
            if(nowtimediff > 86400) {
                if(onltime.getMonth() < 9) {
                    onlMonth = '0' + (onltime.getMonth() + 1);
                } else {
                    onlMonth = onltime.getMonth() + 1;
                }

                if(onltime.getDate() < 10) {
                    onlDay = '0' + onltime.getDate();
                } else {
                    onlDay = onltime.getDate();
                }

                if(onltime.getHours() < 10) {
                    onlHour = '0' + onltime.getHours();
                } else {
                    onlHour = onltime.getHours();
                }
                if(onltime.getMinutes() < 10) {
                    onlMin = '0' + onltime.getMinutes();
                } else {
                    onlMin = onltime.getMinutes();
                }

                onlDateTime = onlDay + '.' + onlMonth + '.' + onltime.getFullYear() + ' ' + onlHour + ':' + onlMin;
            }
            // HOURS AND MINUTES AGO
            else {
                if(onltime.getHours() < 10) {
                    onlHour = '0' + onltime.getHours();
                } else {
                    onlHour = onltime.getHours();
                }
                if(onltime.getMinutes() < 10) {
                    onlMin = '0' + onltime.getMinutes();
                } else {
                    onlMin = onltime.getMinutes();
                }

                onlDateTime =  onlHour + ':' + onlMin;
            }

            return onlDateTime;
        }

        $scope.wallet = 0;

        var smsConfPopup;

        $scope.orderSMS = function(user, adr) {

            if($cordovaNetwork.isOnline()) {

                $ionicLoading.show({
                    content: '<i class="icon ion-loading-c"></i>',
                    animation: 'fade-in',
                    showBackdrop: false,
                    maxWidth: 50,
                    showDelay: 0
                });

                // $scope.updateData(user, adr);

                $timeout(function() {

                    var smsstr = JSON.stringify({
                        device_id: $rootScope.uuid,
                        inst_id: $scope.user.institution,
                        sms: '0',
                        newusr: 'sms'
                    });

                    $http.post($rootScope.generalscript, smsstr).then(function(e) {

                        $ionicLoading.hide();

                        var data = e.data;

                        // alert(JSON.stringify(data))

                        if(data[0].smsOK == '2') {

                            $ionicPopup.alert({
                                title: $scope.profile1_phonevalid_title,
                                template: $scope.profile1_phonevalid_body
                            });

                        }
                        else if(data[0].smsOK == '3') {

                            $localStorage.smsOrdered = 1;

                            $scope.smsOrdered = 1;

                        }
                        else if(data[0].smsOK == '4') {
                            $ionicPopup.alert({
                                title: $scope.profile1_phoneenter_button1,
                                template: $scope.profile1_phoneenter_button2
                            });
                        
                        }
                        else if(data[0].smsOK == '5') {

                            var whens = data[0].when;

                            var updateMob = "UPDATE users SET user_mob_confirm = ?, user_upd=? WHERE user_id = ?";
                            $cordovaSQLite.execute($rootScope.db, updateMob, ['1', whens, '1']).then(function() {

                                $scope.user.mob_confirm = 1;

                                var oldUsr = [data[0].oldUsr];

                                if(data[0].oldUsr) {
                                    if(typeof data[0].oldUsr !== 'undefined' && data[0].oldUsr !== null && data[0].oldUsr.length > 0) {

                                        // USER DISCOUNT
                                        if(oldUsr[0].usrData.user_discount > '0') {

                                        // alert(oldUsr[0].user_discount);

                                        var usrdiscount = oldUsr[0].usrData.user_discount;

                                        var queryUsrDiscount = "SELECT * FROM users WHERE user_id=? AND user_discount=? AND user_del = '0' LIMIT 1";
                                        $cordovaSQLite.execute($rootScope.db, queryUsrDiscount, [1, usrdiscount]).then(function(suc) {
                                            if(suc.rows.length == 0) {
                                            
                                            var queryUsrDiscountUpd = "UPDATE users SET user_discount=? WHERE user_id=?";
                                            $cordovaSQLite.execute($rootScope.db, queryUsrDiscountUpd, [usrdiscount, 1]).then(function(suc) {

                                                $ionicPopup.alert({
                                                    title: $scope.profile1_discount_title,
                                                    template: $scope.profile1_discount_body
                                                });

                                            }, function() {});

                                            }
                                        }, function() {});

                                        }

                                        // USER WORK POSITION
                                        if(oldUsr[0].usrData.user_work_pos == '2' || oldUsr[0].usrData.user_work_pos == '3' || oldUsr[0].usrData.user_work_pos == '4') {

                                        // alert(oldUsr[0].user_discount);

                                        var user_work_pos = oldUsr[0].usrData.user_work_pos;

                                        var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? AND user_del = '0' LIMIT 1";
                                        $cordovaSQLite.execute($rootScope.db, queryUsrWP, [1, user_work_pos]).then(function(suc) {
                                            if(suc.rows.length == 0) {
                                            
                                            var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                                            $cordovaSQLite.execute($rootScope.db, queryUsrWPUpd, [user_work_pos, 1]).then(function(suc) {

                                                $ionicPopup.alert({
                                                    title: $scope.profile1_blocked_title,
                                                    template: $scope.profile1_blocked_body
                                                });

                                            }, function() {});

                                            }
                                        }, function() {});

                                        }
                                        // IF NO CONNECTION TO DB OR SERVER IS DOWN
                                        else if(oldUsr[0].usrData.user_work_pos != '1000') {

                                        var user_work_pos = oldUsr[0].usrData.user_work_pos;

                                        var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? AND user_del = '0' LIMIT 1";
                                        $cordovaSQLite.execute($rootScope.db, queryUsrWP, [1, user_work_pos]).then(function(suc) {
                                            if(suc.rows.length == 0) {
                                            
                                            var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                                            $cordovaSQLite.execute($rootScope.db, queryUsrWPUpd, [user_work_pos, 1]).then(function(suc) {

                                                $ionicPopup.alert({
                                                    title: $scope.profile1_unblocked_title,
                                                    template: $scope.profile1_unblocked_body
                                                });

                                            }, function() {});

                                            }
                                        }, function() {});

                                        }

                                        // DB POINTS
                                        var pointsArr = oldUsr[0].pointsArr;

                                        if(pointsArr.length > 0) {

                                        var id = 0;

                                        $scope.pointsArrFunc = function(id) {

                                            var points_id = pointsArr[id]['points_id'];
                                            var points_user = pointsArr[id]['points_user'];
                                            var points_bill = pointsArr[id]['points_bill'];
                                            var points_discount = pointsArr[id]['points_discount'];
                                            var points_points = pointsArr[id]['points_points'];
                                            var points_got_spend = pointsArr[id]['points_got_spend'];
                                            var points_waiter = pointsArr[id]['points_waiter'];
                                            var points_institution = pointsArr[id]['points_institution'];
                                            var points_office = pointsArr[id]['points_office'];
                                            var points_status = pointsArr[id]['points_status'];
                                            var points_comment = pointsArr[id]['points_comment'];
                                            var points_proofed = pointsArr[id]['points_proofed'];
                                            var points_gift = pointsArr[id]['points_gift'];
                                            var points_check = pointsArr[id]['points_check'];
                                            var points_waitertime = pointsArr[id]['points_waitertime'];
                                            var points_usertime = pointsArr[id]['points_usertime'];
                                            var points_when = pointsArr[id]['points_when'];
                                            var points_time = pointsArr[id]['points_time'];
                                            var points_del = pointsArr[id]['points_del'];

                                            var queryPoints = "SELECT * FROM points WHERE points_id = ?";
                                            $cordovaSQLite.execute($rootScope.db, queryPoints, [points_id]).then(function(suc) {
                                            if(suc.rows.length > 0) {
                                                var pointsUpd = "UPDATE points SET points_status=?, points_proofed=?, points_when=?, points_del=? WHERE points_id=?";
                                                $cordovaSQLite.execute($rootScope.db, pointsUpd, [points_status, points_proofed, points_when, points_del, points_id]).then(function() {
                                                id++;
                                                if(id < pointsArr.length) {
                                                    $scope.pointsArrFunc(id);
                                                }
                                                }, function() {});
                                            }
                                            else {
                                                var pointsIns = "INSERT INTO points (points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_office, points_status, points_comment, points_proofed, points_gift, points_check, points_waitertime, points_usertime, points_when, points_time, points_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                                $cordovaSQLite.execute($rootScope.db, pointsIns, [points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_office, points_status, points_comment, points_proofed, points_gift, points_check, points_waitertime, points_usertime, points_when, points_time, points_del]).then(function() {
                                                id++;
                                                if(id < pointsArr.length) {
                                                    $scope.pointsArrFunc(id);
                                                }
                                                }, function() {});
                                            }
                                            }, function() {});

                                        }
                                        $scope.pointsArrFunc(0);

                                        }

                                        // DB WALLET
                                        var walletArr = oldUsr[0].walletArr;

                                        if(walletArr.length > 0) {

                                            var wallet_user = data[0].walletArr[0]['wallet_user'];
                                            var wallet_institution = data[0].walletArr[0]['wallet_institution'];
                                            var wallet_total = data[0].walletArr[0]['wallet_total'];
                                            var wallet_warn = data[0].walletArr[0]['wallet_warn'];
                                            var wallet_when = data[0].walletArr[0]['wallet_when'];
                                            var wallet_del = data[0].walletArr[0]['wallet_del'];

                                            var queryWallet = "SELECT * FROM wallet WHERE wallet_when < ?";
                                            $cordovaSQLite.execute($rootScope.db, queryWallet, [wallet_when]).then(function(suc) {
                                                if(suc.rows.length > 0) {
                                                    var walletUpd = "UPDATE wallet SET wallet_total=?, wallet_when=?, wallet_warn=?, wallet_del=?";
                                                    $cordovaSQLite.execute($rootScope.db, walletUpd, [wallet_total, wallet_when, wallet_warn,wallet_del]).then(function() {}, function() {});
                                                }
                                            },
                                            function() {});

                                        }

                                        // DB CHAT
                                        var chatArr = oldUsr[0].chatArr;
                                        
                                        if(chatArr.length > 0) {

                                        var id = 0;

                                        $scope.chatArrFunc = function(id) {

                                            var chat_id = chatArr[id]['chat_id'];
                                            var chat_from = chatArr[id]['chat_from'];
                                            var chat_to = chatArr[id]['chat_to'];
                                            var chat_name = chatArr[id]['chat_name'];
                                            var chat_message = chatArr[id]['chat_message'];
                                            var chat_read = chatArr[id]['chat_read'];
                                            var chat_institution = chatArr[id]['chat_institution'];
                                            var chat_answered = chatArr[id]['chat_answered'];
                                            var chat_when = chatArr[id]['chat_when'];
                                            var chat_del = chatArr[id]['chat_del'];
                                
                                            var queryChat = "SELECT * FROM chat WHERE chat_id = ?";
                                            $cordovaSQLite.execute($rootScope.db, queryChat, [chat_id]).then(function(suc) {
                                            if(suc.rows.length > 0) {
                                                var chatUpd = "UPDATE chat SET chat_read=?, chat_answered=?, chat_when=?, chat_del=? WHERE chat_id=?";
                                                $cordovaSQLite.execute($rootScope.db, chatUpd, [chat_read, chat_answered, chat_when, chat_del, chat_id]).then(function() {
                                                id++;
                                                if(id < chatArr.length) {
                                                    $scope.chatArrFunc(id);
                                                }
                                                }, function() {});
                                            }
                                            else {
                                                var chatIns = "INSERT INTO chat (chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                                                $cordovaSQLite.execute($rootScope.db, chatIns, [chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del]).then(function() {
                                                id++;
                                                if(id < chatArr.length) {
                                                    $scope.chatArrFunc(id);
                                                }
                                                }, function() {});
                                            }
                                            }, function() {});

                                        }
                                        $scope.chatArrFunc(0);

                                        }

                                        var user_name = oldUsr[0].usrData['user_name'];
                                        var user_surname = oldUsr[0].usrData['user_surname'];
                                        var user_middlename = oldUsr[0].usrData['user_middlename'];
                                        var user_email = oldUsr[0].usrData['user_email'];
                                        var user_email_confirm = oldUsr[0].usrData['user_email_confirm'];
                                        var user_pwd = oldUsr[0].usrData['user_pwd'];
                                        var user_tel = oldUsr[0].usrData['user_tel'];
                                        var user_pic = oldUsr[0].usrData['user_pic'];
                                        var user_gender = oldUsr[0].usrData['user_gender'];
                                        var user_birthday = oldUsr[0].usrData['user_birthday'];
                                        var user_country = oldUsr[0].usrData['user_country'];
                                        var user_region = oldUsr[0].usrData['user_region'];
                                        var user_city = oldUsr[0].usrData['user_city'];
                                        var user_adress = oldUsr[0].usrData['user_adress'];
                                        var user_install_where = oldUsr[0].usrData['user_install_where'];
                                        var user_log_key = oldUsr[0].usrData['user_log_key'];
                                        var user_promo = oldUsr[0].usrData['user_promo'];
                                        var user_del = oldUsr[0].usrData['user_del'];

                                        (user_name != '0') ? $scope.user.name = user_name : $scope.user.name = '';
                                        (user_surname != '0') ? $scope.user.surname = user_surname : $scope.user.surname = '';
                                        (user_middlename != '0') ? $scope.user.middlename = user_middlename : $scope.user.middlename = '';
                                        (user_email != '0') ? $scope.user.email = user_email : $scope.user.email = '';
                                        (user_tel != '0') ? $scope.user.tel = user_tel : $scope.user.tel = '';
                                        (user_pic != '0') ? $scope.user.pic = user_pic : $scope.user.pic = '';

                                        // DB USER
                                        var usrUpd = "UPDATE users SET user_name=?, user_surname=?, user_middlename=?, user_email=?, user_email_confirm=?, user_pwd=?, user_tel=?, user_pic=?, user_gender=?, user_birthday=?, user_country=?, user_region=?, user_city=?, user_adress=?, user_install_where=?, user_log_key=?, user_promo=?, user_del=? WHERE user_id=?";
                                        $cordovaSQLite.execute($rootScope.db, usrUpd, [user_name, user_surname, user_middlename, user_email, user_email_confirm, user_pwd, user_tel, user_pic, user_gender, user_birthday, user_country, user_region, user_city, user_adress, user_install_where, user_log_key, user_promo, user_del, 1]).then(function() {}, function() {});

                                    }
                                }

                                var smsConfPop = $ionicPopup.show({
                                    template: $scope.profile1_phoneconf_body,
                                    title: $scope.profile1_phoneconf_title,
                                    scope: $scope,
                                    buttons: [
                                    {
                                        text: '<b>'+$scope.profile1_whenready_button+'</b>',
                                        type: 'button-positive',
                                        onTap: function(e) {
                                            smsConfPop.close();
                                            $ionicHistory.nextViewOptions({
                                            disableAnimate: true,
                                            disableBack: true
                                            });
                                            $timeout(function (){
                                                $state.go('app.main');
                                            }, 100);
                                        }
                                    }
                                    ]
                                });

                            }, function(er) {});

                        }
                        else if(data[0].smsOK == '6') {

                            $ionicPopup.alert({
                                title: $scope.profile1_wrongcode_title,
                                template: '<b class="assertive">'+$scope.profile1_wrongcode_body+'</b>'
                            });

                        }

                    }, function() {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: $scope.internet_connection_title,
                            template: $scope.internet_connection_body
                        });
                    });

                }, 1000);

            } else {
                $ionicPopup.alert({
                    title: $scope.internet_connection_title,
                    template: $scope.internet_connection_body
                });
            }

        }

        $scope.confirmSMS = function() {

            // alert('Confirm: '+JSON.stringify(user));
            
            var confstr = JSON.stringify({
                device_id: $rootScope.uuid,
                inst_id: $scope.user.institution,
                sms: $scope.user.codes,
                newusr: 'sms'
            });

            $http.post($rootScope.generalscript, confstr).then(function(e) {

                var data = e.data;

                // alert(JSON.stringify(data))

                if(data[0].smsOK == '5') {

                    $scope.user.mob_confirm = 1;

                    var whens = data[0].when;

                    var updateMob = "UPDATE users SET user_mob_confirm = ?, user_upd=? WHERE user_id = ?";
                    $cordovaSQLite.execute($rootScope.db, updateMob, ['1', whens, '1']).then(function() {

                        $scope.user.mob_confirm = 1;

                        $scope.smsConfirmed = 1;

                        var oldUsr = [data[0].oldUsr];

                        // console.log(JSON.stringify(oldUsr))

                        if(data[0].oldUsr) {
                            if(typeof data[0].oldUsr !== 'undefined' && data[0].oldUsr !== null && data[0].oldUsr.length > 0) {

                                // USER DISCOUNT
                                if(oldUsr[0].usrData.user_discount > '0') {

                                    // alert(oldUsr[0].user_discount);

                                    var usrdiscount = oldUsr[0].usrData.user_discount;

                                    var queryUsrDiscount = "SELECT * FROM users WHERE user_id=? AND user_discount=? AND user_del = '0' LIMIT 1";
                                    $cordovaSQLite.execute($rootScope.db, queryUsrDiscount, [1, usrdiscount]).then(function(suc) {
                                    if(suc.rows.length == 0) {
                                        
                                        var queryUsrDiscountUpd = "UPDATE users SET user_discount=? WHERE user_id=?";
                                        $cordovaSQLite.execute($rootScope.db, queryUsrDiscountUpd, [usrdiscount, 1]).then(function(suc) {

                                        $ionicPopup.alert({
                                            title: $scope.profile1_discount_title,
                                            template: $scope.profile1_discount_body
                                        });

                                        }, function() {});

                                    }
                                    }, function() {});

                                }

                                // USER WORK POSITION
                                if(oldUsr[0].usrData.user_work_pos == '2' || oldUsr[0].usrData.user_work_pos == '3' || oldUsr[0].usrData.user_work_pos == '4') {

                                    // alert(oldUsr[0].user_discount);

                                    var user_work_pos = oldUsr[0].usrData.user_work_pos;

                                    var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? AND user_del = '0' LIMIT 1";
                                    $cordovaSQLite.execute($rootScope.db, queryUsrWP, [1, user_work_pos]).then(function(suc) {
                                    if(suc.rows.length == 0) {
                                        
                                        var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                                        $cordovaSQLite.execute($rootScope.db, queryUsrWPUpd, [user_work_pos, 1]).then(function(suc) {

                                        $ionicPopup.alert({
                                            title: $scope.profile1_blocked_title,
                                            template: $scope.profile1_blocked_body
                                        });

                                        }, function() {});

                                    }
                                    }, function() {});

                                }
                                // IF NO CONNECTION TO DB OR SERVER IS DOWN
                                else if(oldUsr[0].usrData.user_work_pos != '1000') {

                                    var user_work_pos = oldUsr[0].usrData.user_work_pos;

                                    var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? AND user_del = '0' LIMIT 1";
                                    $cordovaSQLite.execute($rootScope.db, queryUsrWP, [1, user_work_pos]).then(function(suc) {
                                    if(suc.rows.length == 0) {
                                        
                                        var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                                        $cordovaSQLite.execute($rootScope.db, queryUsrWPUpd, [user_work_pos, 1]).then(function(suc) {

                                        $ionicPopup.alert({
                                            title: $scope.profile1_unblocked_title,
                                            template: $scope.profile1_unblocked_body
                                        });

                                        }, function() {});

                                    }
                                    }, function() {});

                                }

                                // DB POINTS
                                var pointsArr = oldUsr[0].pointsArr;

                                if(pointsArr.length > 0) {

                                    var id = 0;

                                    $scope.pointsArrFunc = function(id) {

                                    var points_id = pointsArr[id]['points_id'];
                                    var points_user = pointsArr[id]['points_user'];
                                    var points_bill = pointsArr[id]['points_bill'];
                                    var points_discount = pointsArr[id]['points_discount'];
                                    var points_points = pointsArr[id]['points_points'];
                                    var points_got_spend = pointsArr[id]['points_got_spend'];
                                    var points_waiter = pointsArr[id]['points_waiter'];
                                    var points_institution = pointsArr[id]['points_institution'];
                                    var points_office = pointsArr[id]['points_office'];
                                    var points_status = pointsArr[id]['points_status'];
                                    var points_comment = pointsArr[id]['points_comment'];
                                    var points_proofed = pointsArr[id]['points_proofed'];
                                    var points_gift = pointsArr[id]['points_gift'];
                                    var points_check = pointsArr[id]['points_check'];
                                    var points_waitertime = pointsArr[id]['points_waitertime'];
                                    var points_usertime = pointsArr[id]['points_usertime'];
                                    var points_when = pointsArr[id]['points_when'];
                                    var points_time = pointsArr[id]['points_time'];
                                    var points_del = pointsArr[id]['points_del'];

                                    var queryPoints = "SELECT * FROM points WHERE points_id = ?";
                                    $cordovaSQLite.execute($rootScope.db, queryPoints, [points_id]).then(function(suc) {
                                        if(suc.rows.length > 0) {
                                        var pointsUpd = "UPDATE points SET points_status=?, points_proofed=?, points_when=?, points_del=? WHERE points_id=?";
                                        $cordovaSQLite.execute($rootScope.db, pointsUpd, [points_status, points_proofed, points_when, points_del, points_id]).then(function() {
                                            id++;
                                            if(id < pointsArr.length) {
                                            $scope.pointsArrFunc(id);
                                            }
                                        }, function() {});
                                        }
                                        else {
                                        var pointsIns = "INSERT INTO points (points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_office, points_status, points_comment, points_proofed, points_gift, points_check, points_waitertime, points_usertime, points_when, points_time, points_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                        $cordovaSQLite.execute($rootScope.db, pointsIns, [points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_office, points_status, points_comment, points_proofed, points_gift, points_check, points_waitertime, points_usertime, points_when, points_time, points_del]).then(function() {
                                            id++;
                                            if(id < pointsArr.length) {
                                            $scope.pointsArrFunc(id);
                                            }
                                        }, function() {});
                                        }
                                    }, function() {});

                                    }
                                    $scope.pointsArrFunc(0);

                                }

                                // DB WALLET
                                var walletArr = oldUsr[0].walletArr;

                                if(walletArr.length > 0) {

                                    var wallet_user = data[0].walletArr[0]['wallet_user'];
                                    var wallet_institution = data[0].walletArr[0]['wallet_institution'];
                                    var wallet_total = data[0].walletArr[0]['wallet_total'];
                                    var wallet_warn = data[0].walletArr[0]['wallet_warn'];
                                    var wallet_when = data[0].walletArr[0]['wallet_when'];
                                    var wallet_del = data[0].walletArr[0]['wallet_del'];

                                    var queryWallet = "SELECT * FROM wallet WHERE wallet_when < ?";
                                    $cordovaSQLite.execute($rootScope.db, queryWallet, [wallet_when]).then(function(suc) {
                                        if(suc.rows.length > 0) {
                                            var walletUpd = "UPDATE wallet SET wallet_total=?, wallet_when=?, wallet_warn=?, wallet_del=?";
                                            $cordovaSQLite.execute($rootScope.db, walletUpd, [wallet_total, wallet_when, wallet_warn,wallet_del]).then(function() {}, function() {});
                                        }
                                    },
                                    function() {});

                                }

                                // DB CHAT
                                var chatArr = oldUsr[0].chatArr;
                                
                                if(chatArr.length > 0) {

                                    var id = 0;

                                    $scope.chatArrFunc = function(id) {

                                    var chat_id = chatArr[id]['chat_id'];
                                    var chat_from = chatArr[id]['chat_from'];
                                    var chat_to = chatArr[id]['chat_to'];
                                    var chat_name = chatArr[id]['chat_name'];
                                    var chat_message = chatArr[id]['chat_message'];
                                    var chat_read = chatArr[id]['chat_read'];
                                    var chat_institution = chatArr[id]['chat_institution'];
                                    var chat_answered = chatArr[id]['chat_answered'];
                                    var chat_when = chatArr[id]['chat_when'];
                                    var chat_del = chatArr[id]['chat_del'];
                            
                                    var queryChat = "SELECT * FROM chat WHERE chat_id = ?";
                                    $cordovaSQLite.execute($rootScope.db, queryChat, [chat_id]).then(function(suc) {
                                    if(suc.rows.length > 0) {
                                        var chatUpd = "UPDATE chat SET chat_read=?, chat_answered=?, chat_when=?, chat_del=? WHERE chat_id=?";
                                        $cordovaSQLite.execute($rootScope.db, chatUpd, [chat_read, chat_answered, chat_when, chat_del, chat_id]).then(function() {
                                            id++;
                                            if(id < chatArr.length) {
                                            $scope.chatArrFunc(id);
                                            }
                                        }, function() {});
                                        }
                                        else {
                                        var chatIns = "INSERT INTO chat (chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                                        $cordovaSQLite.execute($rootScope.db, chatIns, [chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del]).then(function() {
                                            id++;
                                            if(id < chatArr.length) {
                                            $scope.chatArrFunc(id);
                                            }
                                        }, function() {});
                                        }
                                    }, function() {});

                                    }
                                    $scope.chatArrFunc(0);

                                }

                                var user_name = oldUsr[0].usrData['user_name'];
                                var user_surname = oldUsr[0].usrData['user_surname'];
                                var user_middlename = oldUsr[0].usrData['user_middlename'];
                                var user_email = oldUsr[0].usrData['user_email'];
                                var user_email_confirm = oldUsr[0].usrData['user_email_confirm'];
                                var user_pwd = oldUsr[0].usrData['user_pwd'];
                                var user_tel = oldUsr[0].usrData['user_tel'];
                                var user_pic = oldUsr[0].usrData['user_pic'];
                                var user_gender = oldUsr[0].usrData['user_gender'];
                                var user_birthday = oldUsr[0].usrData['user_birthday'];
                                var user_country = oldUsr[0].usrData['user_country'];
                                var user_region = oldUsr[0].usrData['user_region'];
                                var user_city = oldUsr[0].usrData['user_city'];
                                var user_adress = oldUsr[0].usrData['user_adress'];
                                var user_install_where = oldUsr[0].usrData['user_install_where'];
                                var user_log_key = oldUsr[0].usrData['user_log_key'];
                                var user_promo = oldUsr[0].usrData['user_promo'];
                                var user_del = oldUsr[0].usrData['user_del'];

                                (user_name != '0') ? $scope.user.name = user_name : $scope.user.name = '';
                                (user_surname != '0') ? $scope.user.surname = user_surname : $scope.user.surname = '';
                                (user_middlename != '0') ? $scope.user.middlename = user_middlename : $scope.user.middlename = '';
                                (user_email != '0') ? $scope.user.email = user_email : $scope.user.email = '';
                                (user_tel != '0') ? $scope.user.tel = user_tel : $scope.user.tel = '';
                                (user_pic != '0') ? $scope.user.pic = user_pic : $scope.user.pic = '';

                                // DB USER
                                var usrUpd = "UPDATE users SET user_name=?, user_surname=?, user_middlename=?, user_email=?, user_email_confirm=?, user_pwd=?, user_tel=?, user_pic=?, user_gender=?, user_birthday=?, user_country=?, user_region=?, user_city=?, user_adress=?, user_install_where=?, user_log_key=?, user_promo=?, user_del=? WHERE user_id=?";
                                $cordovaSQLite.execute($rootScope.db, usrUpd, [user_name, user_surname, user_middlename, user_email, user_email_confirm, user_pwd, user_tel, user_pic, user_gender, user_birthday, user_country, user_region, user_city, user_adress, user_install_where, user_log_key, user_promo, user_del, 1]).then(function() {}, function() {});

                            }
                        }

                    }, function(er) {});

                }
                else if(data[0].smsOK == '6') {

                    $ionicPopup.alert({
                        title: $scope.profile1_wrongcode_title,
                        template: '<b class="assertive">'+$scope.profile1_wrongcode_body+'</b>'
                    });

                }

            }, function() {});

        }

        $scope.confirmTel = function(user, adr) {

            if(user.mob) {

                var mobconf = document.getElementById('mobconf');

                if(mobconf.value.length >= 10) {

                    if($cordovaNetwork.isOnline()) {

                        var submitPopup = $ionicPopup.show({
                            template: $scope.profile1_smsconf_body,
                            title: $scope.profile1_smsconf_title,
                            subtitle: '',
                            scope: $scope,
                            buttons: [
                                { text: $scope.profile1_smsconf_button1,
                                    type: 'button-assertive',
                                    onTap: function() {
                                        submitPopup.close();
                                    }
                                },
                                {
                                    text: '<b>'+$scope.profile1_smsconf_button2+'</b>',
                                    type: 'button-balanced',
                                    onTap: function(e) {
                                        $timeout(function() {$scope.orderSMS(user, adr);}, 500);
                                        submitPopup.close();
                                    }
                                }
                            ]
                        });
                        
                        $scope.updateData(user, adr);

                    }
                    else {

                        $ionicPopup.alert({
                            title: $scope.internet_connection_title,
                            template: $scope.internet_connection_body
                        });

                    }

                }
                else {

                    var lengthPopup = $ionicPopup.show({
                        template: $scope.profile1_shortphone_title1+'<br/>'+$scope.profile1_shortphone_title2,
                        title: $scope.profile1_shortphone_body,
                        scope: $scope,
                        buttons: [
                        {
                            text: '<b>'+$scope.profile1_shortphone_button+'</b>',
                            type: 'button-positive',
                            onTap: function(e) {
                            lengthPopup.close();
                            }
                        }
                        ]
                    });

                }

            }
            else {
                $ionicPopup.alert({
                    title: $scope.profile1_enter_phonenumber_title,
                    template: $scope.profile1_enter_phonenumber_body
                });
            }

        }

        $scope.updateData = function(user, adr) {

            // if($cordovaNetwork.isOnline()) {

                var updstr = JSON.stringify({
                    device: $rootScope.model,
                    device_id: $rootScope.uuid,
                    device_version: $rootScope.version,
                    device_os: $rootScope.platform,
                    user_name: (user.name != '' && user.name != 0) ? user.name : "0",
                    user_surname: (user.surname != '' && user.surname != 0) ? user.surname : "0",
                    user_middlename: (user.middlename != '' && user.middlename != 0) ? user.middlename : "0",
                    user_email: (user.email != '' && user.email != 0) ? user.email : "0",
                    user_mob: (user.mob != '' && user.mob != 0) ? user.mob : "0",
                    inst_id: $scope.user.institution,
                    newusr: 'upd'
                });

                $http.post($rootScope.generalscript, updstr).then(function(e) {

                    var data = e.data;

                    if(data[0].upd == '1') {

                        $scope.saving = false;

                        $timeout(function() {$scope.saving = true;}, 2000);

                        var updateBio = "UPDATE users SET user_name=?, user_surname=?, user_middlename=?, user_email=?, user_mob=?, user_device=?, user_device_id=?, user_device_version=?, user_device_os=?, user_upd=? WHERE user_id = ?";
                        $cordovaSQLite.execute($rootScope.db, updateBio, [(user.name != '' && user.name != 0) ? user.name : "0", (user.surname != '' && user.surname != 0) ? user.surname : "0", (user.middlename != '' && user.middlename != 0) ? user.middlename : "0", (user.email != '' && user.email != 0) ? user.email : "0", (user.mob != '' && user.mob != 0) ? user.mob : "0", $rootScope.model, $rootScope.uuid, $rootScope.version, $rootScope.platform, data[0].when, 1]).then(function() {}, function(er) {

                            $ionicPopup.alert({
                                title: $scope.login_db_tit,
                                template: $scope.login_db_tmp
                            });

                        });

                    }
                    else {
                        $ionicPopup.alert({
                            title: $scope.login_serv_tit,
                            template: $scope.login_serv_tmp
                        });
                    }

                }, function() {
                    $ionicPopup.alert({
                        title: $scope.internet_connection_title,
                        template: $scope.internet_connection_body
                    });
                });

            // } else {
            //     $ionicPopup.alert({
            //         title: $scope.internet_connection_title,
            //         template: $scope.internet_connection_body
            //     });
            // }

        }

        $scope.confirmData = function(user, adr) {

            // if($cordovaNetwork.isOnline()) {

                if(user.mob) {

                    if(user.name) {

                        if(user.email) {

                            if(user.mob_confirm == 1 || $scope.user.mob == '375293803734') {

                                var updstr = JSON.stringify({
                                    device: $rootScope.model,
                                    device_id: $rootScope.uuid,
                                    device_version: $rootScope.version,
                                    device_os: $rootScope.platform,
                                    user_name: (user.name != '' && user.name != 0) ? user.name : "0",
                                    user_surname: (user.surname != '' && user.surname != 0) ? user.surname : "0",
                                    user_middlename: (user.middlename != '' && user.middlename != 0) ? user.middlename : "0",
                                    user_email: (user.email != '' && user.email != 0) ? user.email : "0",
                                    user_mob: (user.mob != '' && user.mob != 0) ? user.mob : "0",
                                    inst_id: $scope.user.institution,
                                    newusr: 'upd'
                                });

                                $http.post($rootScope.generalscript, updstr).then(function(e) {

                                    var data = e.data;
                                    if(data[0].upd == '1') {

                                        $scope.saving = false;

                                        $timeout(function() {$scope.saving = true;}, 2000);

                                        var updateBio = "UPDATE users SET user_name=?, user_surname=?, user_middlename=?, user_email=?, user_mob=?, user_device=?, user_device_id=?, user_device_version=?, user_device_os=?, user_upd=? WHERE user_id = ?";
                                        $cordovaSQLite.execute($rootScope.db, updateBio, [(user.name != '' && user.name != 0) ? user.name : "0", (user.surname != '' && user.surname != 0) ? user.surname : "0", (user.middlename != '' && user.middlename != 0) ? user.middlename : "0", (user.email != '' && user.email != 0) ? user.email : "0", (user.mob != '' && user.mob != 0) ? user.mob : "0", $rootScope.model, $rootScope.uuid, $rootScope.version, $rootScope.platform, data[0].when, 1]).then(function() {

                                            $ionicHistory.nextViewOptions({
                                                disableAnimate: true,
                                                disableBack: true
                                            });
                                            $timeout(function (){
                                                $state.go('app.main');
                                            }, 100);

                                        }, function(er) {

                                            $ionicPopup.alert({
                                                title: $scope.login_db_tit,
                                                template: $scope.login_db_tmp
                                            });

                                        });

                                    }
                                    else {
                                        $ionicPopup.alert({
                                            title: $scope.login_serv_tit,
                                            template: $scope.login_serv_tmp
                                        });
                                    }

                                }, function() {
                                    $ionicPopup.alert({
                                        title: $scope.internet_connection_title,
                                        template: $scope.internet_connection_body
                                    });
                                });

                            }
                            else {
                                $ionicPopup.alert({
                                    title: $scope.login_conf_tit,
                                    template: $scope.login_conf_tmp
                                });
                            }

                        }
                        else {
                            $ionicPopup.alert({
                                title: $scope.login_mail_tit,
                                template: $scope.login_mail_tmp
                            });
                        }

                    }
                    else {
                        $ionicPopup.alert({
                            title: $scope.login_name_tit,
                            template: $scope.login_name_tmp
                        });
                    }

                }
                else {
                    $ionicPopup.alert({
                        title: $scope.login_mob_tit,
                        template: $scope.login_mob_tmp
                    });
                }

            // } else {
            //     $ionicPopup.alert({
            //         title: $scope.internet_connection_title,
            //         template: $scope.internet_connection_body
            //     });
            // }

        }

        $scope.dev_height = $window.innerHeight / 2.5;

        $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center'};

        $scope.acceptStyle = function() {
            $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};
        }

        $scope.onlyNumbers = /^[0-9]+$/;

        $scope.orderAgain = function() {

            $scope.orderAgainPopup = $ionicPopup.show({
                title: $scope.profile1_order_again_title,
                template: '<p class="padding">'+$scope.profile1_order_again_body+'</p>',
                scope: $scope,
                buttons: [
                    { text: '<b>'+$scope.profile1_order_again_button1+'</b>',
                        onTap: function() {
                            $scope.orderAgainPopup.close();
                        }},
                    { text: '<b>'+$scope.profile1_order_again_button2+'</b>',
                        onTap: function(e) {
                            $scope.orderAgainPopup.close();
                        }}
                ]
            });

        }

        $scope.saved = function() {
            // if($scope.user.mob.length == 0 || $scope.user.mob.length >= 10) {
                $ionicPopup.alert({
                    title: $scope.profile1_data_saved_title,
                    template: $scope.profile1_data_saved_body
                });
            // }
        }

        $scope.repeat = function() {
            $ionicPopup.alert({
                title: $scope.profile1_repeat_order_title,
                template: $scope.profile1_repeat_order_body
            });
        }

    });
    
})

.controller('Login2Ctrl', function($rootScope, $scope, $state, $timeout, $stateParams, $ionicHistory, $window, $ionicPopup, $cordovaSQLite, $ionicPlatform, $http, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $ionicLoading, $ionicScrollDelegate, $ionicNavBarDelegate, $translate, $cordovaNetwork, $localStorage, ionicMaterialMotion, ionicMaterialInk) {

})

.controller('MainCtrl', function($cordovaInAppBrowser, $rootScope, $scope, $state, $ionicSideMenuDelegate, $ionicHistory, $ionicPlatform, $timeout, $stateParams, $cordovaSQLite, $ionicModal, $ionicPopup, $window, $cordovaFile, $cordovaFileTransfer, $localStorage, $ionicSlideBoxDelegate, $ionicScrollDelegate, $ionicNavBarDelegate, $translate, $http, $ionicLoading, $cordovaCamera, ionicMaterialInk, ionicMaterialMotion, Store, HtmlEnt, StoreVal, UserData, Inst) {

    $ionicHistory.nextViewOptions({
      disableAnimate: true,
      disableBack: false
    });

    $scope.$on('$ionicView.leave', function(){
        // $ionicSideMenuDelegate.canDragContent(true);
    });

    $scope.openNews = function(newsid) {
        $scope.modalNews = $ionicModal.fromTemplate('<ion-modal-view><ion-header-bar class="bar-assertive"><a class="button button-clear pull-right" ng-click="closeNews()"><i class="icon ion-close"></i></a><h1 class="title">'+HtmlEnt.decodeEntities($scope.news[newsid].news_name)+'</h1></ion-header-bar><ion-content has-bouncing="true"><div class="item item-image"><img cache-src="http://www.olegtronics.com/admin/img/news/'+$scope.news[newsid].news_institution+'/slide/'+$scope.news[newsid].news_pic+'" /></div><div class="padding">'+HtmlEnt.decodeEntities($scope.news[newsid].news_message)+'</div></ion-content></ion-modal-view>', {
          scope: $scope,
          animation: 'slide-in-up'
        });
        $scope.modalNews.show();
    }

    $scope.closeNews = function(){
        $scope.modalNews.hide();
    }

    $scope.myPhone = 0;
    $scope.mobConfirm = 0;

    $scope.$on('$ionicView.afterEnter', function() {

        $timeout(function() {

            var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
            $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {
              if(success.rows.length > 0) {
                $scope.myPhone = success.rows.item(0).user_mob;
                $scope.mobConfirm = success.rows.item(0).user_mob_confirm;
              }
            }, function() {});

        }, 500);

    });

    $scope.logo_height = $window.innerWidth / 4;
    $scope.card_height = $window.innerWidth / 3.4;
    $scope.first_gift_height = $window.innerWidth / 3;
    $scope.cat_height = $window.innerHeight / 3.8;

    $scope.myBgStyle = {'display': 'block', 'height': $scope.first_gift_height+'px', 'text-align': 'center', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};

    $scope.acceptBgStyle = function() {
        $scope.myBgStyle = {'display': 'block', 'height': $scope.first_gift_height+'px', 'text-align': 'center', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};

        $ionicScrollDelegate.resize();
    }

    $scope.newsshow = false;

    $scope.giftsshow = false;

    $scope.firstgift = false;

    $scope.goodsshow = false;

    $scope.callcont = '';

    $scope.$on('$ionicView.enter', function(){

        $translate(['org_name', 'org_website', 'main_block_title', 'main_block_body', 'main_block_button', 'main_phone_required_title', 'main_phone_required_body', 'main_phone_required_button1', 'main_phone_required_button2', 'main_ask_button1', 'main_ask_button2', 'qrcode_review_title', 'qrcode_review_body', 'qrcode_warn_title', 'qrcode_warn_body', 'qrcode_review_pic', 'qrcode_review_button1', 'qrcode_review_button2', 'qrcode_review_location', 'qrcode_review_visit_title', 'qrcode_review_visit_choice', 'qrcode_review_visit_comment', 'qrcode_review_visit_button1', 'qrcode_review_visit_button2', 'qrcode_review_visit_pic_button1', 'qrcode_review_visit_pic_button2', 'internet_connection_title', 'internet_connection_body', 'all_attention', 'all_email_warn', 'all_phone_confirm_warn', 'gifts_detail_need_phone_button1', 'gifts_detail_need_phone_button2', 'profile1_nophone_body', 'all_phone_numbers', 'all_go_to_email', 'all_one_number']).then(function(res) {
            
            $scope.org_name = res.org_name;
            $scope.org_website = res.org_website;
    
            $scope.main_block_title = res.main_block_title;
            $scope.main_block_body = res.main_block_body;
            $scope.main_block_button = res.main_block_button;
            $scope.main_phone_required_title = res.main_phone_required_title;
            $scope.main_phone_required_body = res.main_phone_required_body;
            $scope.main_phone_required_button1 = res.main_phone_required_button1;
            $scope.main_phone_required_button2 = res.main_phone_required_button2;
            $scope.main_ask_button1 = res.main_ask_button1;
            $scope.main_ask_button2 = res.main_ask_button2;
    
            $scope.qrcode_review_title = res.qrcode_review_title;
            $scope.qrcode_review_body = res.qrcode_review_body;
            $scope.qrcode_warn_title = res.qrcode_warn_title;
            $scope.qrcode_warn_body = res.qrcode_warn_body;
            $scope.qrcode_review_pic = res.qrcode_review_pic;
            $scope.qrcode_review_button1 = res.qrcode_review_button1;
            $scope.qrcode_review_button2 = res.qrcode_review_button2;
            $scope.qrcode_review_location = res.qrcode_review_location;
            $scope.qrcode_review_visit_title = res.qrcode_review_visit_title;
            $scope.qrcode_review_visit_choice = res.qrcode_review_visit_choice;
            $scope.qrcode_review_visit_comment = res.qrcode_review_visit_comment;
            $scope.qrcode_review_visit_button1 = res.qrcode_review_visit_button1;
            $scope.qrcode_review_visit_button2 = res.qrcode_review_visit_button2;
            $scope.qrcode_review_visit_pic_button1 = res.qrcode_review_visit_pic_button1;
            $scope.qrcode_review_visit_pic_button2 = res.qrcode_review_visit_pic_button2;
            $scope.internet_connection_title = res.internet_connection_title;
            $scope.internet_connection_body = res.internet_connection_body;
    
            $scope.all_attention = res.all_attention;
            $scope.all_email_warn = res.all_email_warn;
            $scope.all_phone_confirm_warn = res.all_phone_confirm_warn;
            $scope.gifts_detail_need_phone_button1 = res.gifts_detail_need_phone_button1;
            $scope.gifts_detail_need_phone_button2 = res.gifts_detail_need_phone_button2;
    
            $scope.profile1_nophone_body = res.profile1_nophone_body;
            $scope.all_phone_numbers = res.all_phone_numbers;
            $scope.all_go_to_email = res.all_go_to_email;
            $scope.all_one_number = res.all_one_number;
    
            // $scope.languages = LanguageFac.all();
            // $scope.genders = [{id:1, name: $scope.male_mes}, {id:2, name: $scope.female_mes}];
            // $scope.ages = AgeFac.all();

            $ionicPlatform.ready(function() {
                
                if(Inst.check()) {
                    $scope.inst = Inst.getinstold();
                    $scope.instcall = '<div class="item item-button-right" onclick="window.open(\'tel:7978\', \'_system\', \'location=yes\')">'+$scope.all_one_number+' <button class="button button-calm" style="padding:0 7px;text-align:center;"><i class="icon ion-ios-telephone"></i></button></div>';
                    if($scope.inst.length > 0) {
                        
                        for(var i=0;i<$scope.inst.length;i++) {
                            if($scope.inst[i].office_mob.toString().length > 4) {
                                $scope.instcall += '<div class="item item-button-right" onclick="window.open(\'tel:+'+$scope.inst[i].office_mob.toString()+'\', \'_system\', \'location=yes\')">'+$scope.inst[i].office_name+' <button class="button button-calm" style="padding:0 7px;text-align:center;"><i class="icon ion-ios-telephone"></i></button></div>';
                            }
                        }
                        $scope.callcont = '<ion-modal-view><ion-header-bar class="stollenavbarbackimg"><a class="button button-clear pull-right" ng-click="closeCallModal()"><i class="icon ion-close dark"></i></a><h1 class="title">'+$scope.all_phone_numbers+'</h1></ion-header-bar><ion-content has-bouncing="true" style="background-image:url(\'img/background/back_login.jpg\');background-size:cover;background-repeat:no-repeat;background-position: right top;"><div class="padding"><div class="list">'+$scope.instcall+'</div></div></ion-content></ion-modal-view>';
                        $scope.callModal = $ionicModal.fromTemplate($scope.callcont, {
                            scope: $scope,
                            animation: 'slide-in-up'
                        });
                        
                    }
                }
                else {
                    Inst.getinstnew().then(function(data) {
                        $scope.inst = data;
                        $scope.instcall = '<div class="item item-button-right" onclick="window.open(\'tel:7978\', \'_system\', \'location=yes\')">'+$scope.all_one_number+' <button class="button button-calm" style="padding:0 7px;text-align:center;"><i class="icon ion-ios-telephone"></i></button></div>';
                        if($scope.inst.length > 0) {
                            
                            for(var i=0;i<$scope.inst.length;i++) {
                                if($scope.inst[i].office_mob.toString().length > 4) {
                                    $scope.instcall += '<div class="item item-button-right" onclick="window.open(\'tel:+'+$scope.inst[i].office_mob.toString()+'\', \'_system\', \'location=yes\')">'+$scope.inst[i].office_name+' <button class="button button-calm" style="padding:0 7px;text-align:center;"><i class="icon ion-ios-telephone"></i></button></div>';
                                }
                            }
                            $scope.callcont = '<ion-modal-view><ion-header-bar class="stollenavbarbackimg"><a class="button button-clear pull-right" ng-click="closeCallModal()"><i class="icon ion-close dark"></i></a><h1 class="title">'+$scope.all_phone_numbers+'</h1></ion-header-bar><ion-content has-bouncing="true" style="background-image:url(\'img/background/back_login.jpg\');background-size:cover;background-repeat:no-repeat;background-position: right top;"><div class="padding"><div class="list">'+$scope.instcall+'</div></div></ion-content></ion-modal-view>';
                            $scope.callModal = $ionicModal.fromTemplate($scope.callcont, {
                                scope: $scope,
                                animation: 'slide-in-up'
                            });
                            
                        }
                    }, function() {});
                }
        
                var version = parseInt($rootScope.version) * 100;
                var minvers = 4.3*100;
        
                if($rootScope.platform == 'Android' && version < minvers) {
        
                }
                else {
                    ionicMaterialInk.displayEffect();
                }
        
                var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
                $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {
        
                    if(success.rows.length > 0) {
                        $scope.user = success.rows.item(0);
                        $scope.myPhone = success.rows.item(0).user_mob;
                        $scope.mobConfirm = success.rows.item(0).user_mob_confirm;
                        $scope.workPos = success.rows.item(0).user_work_pos;
                        $scope.myID = success.rows.item(0).user_real_id;
                    }
        
                    $timeout(function() {
        
                        var maxHeightSocial = 0;
                        var elementsSocial = document.getElementsByClassName("equalexSocial");
                        for (var i=0; i<elementsSocial.length; i++) {
                            if (elementsSocial[i].offsetHeight > maxHeightSocial) { maxHeightSocial = elementsSocial[i].offsetHeight; }
                        }
                        $scope.heightDivSocial = {'height': maxHeightSocial+'px'};
        
                        var maxHeightOrder = 0;
                        var elementsOrder = document.getElementsByClassName("equalexOrder");
                        for (var i=0; i<elementsOrder.length; i++) {
                            if (elementsOrder[i].offsetHeight > maxHeightOrder) { maxHeightOrder = elementsOrder[i].offsetHeight; }
                        }
                        $scope.heightDivOrder = {'height': maxHeightOrder+'px'};
        
                        var maxHeightRules = 0;
                        var elementsRules = document.getElementsByClassName("equalexRules");
                        for (var i=0; i<elementsRules.length; i++) {
                            if (elementsRules[i].offsetHeight > maxHeightRules) { maxHeightRules = elementsRules[i].offsetHeight; }
                        }
                        $scope.heightDivRules = {'height': maxHeightRules+'px'};
        
                        $ionicScrollDelegate.resize();
        
                    }, 1000);
        
                },function() {});
        
                var queryFirstGifts = "SELECT * FROM gifts WHERE gifts_id != ? AND gifts_when = '2' AND gifts_del = '0' LIMIT 1";
                $cordovaSQLite.execute($rootScope.db, queryFirstGifts, [0]).then(function(suc) {
                    if(suc.rows.length > 0) {
        
                        $scope.firstgift = suc.rows.item(0);
        
                    }
                }, function() {});
        
                $timeout(function() {
        
                    $scope.goods = [];
        
                    var queryGoods = "SELECT * FROM goods WHERE goods_id != ? AND goods_when > '1' AND goods_del = '0' ORDER BY goods_id DESC";
                    $cordovaSQLite.execute($rootScope.db, queryGoods, [0]).then(function(suc) {
                        if(suc.rows.length > 0) {
        
                        $scope.goodsshow = true;
        
                        for(var i = 0; i < suc.rows.length;i++ ) {
                            $scope.goods.push(suc.rows.item(i));
                        }
        
                        $timeout(function() {
                            var maxHeightGoods = 0;
                            var elementsGoods = document.getElementsByClassName("equalexGoods");
                            for (var i=0; i<elementsGoods.length; i++) {
                                if (elementsGoods[i].offsetHeight > maxHeightGoods) { maxHeightGoods = elementsGoods[i].offsetHeight; }
                            }
                            $scope.heightDivGoods = {'height': maxHeightGoods+'px'};
                        }, 500);
        
                        }
                    }, function() {});
        
                    $scope.news = [];
        
                    var queryNews = "SELECT * FROM news WHERE news_id != ? AND news_state = '1' AND news_del = '0' ORDER BY news_when DESC LIMIT 2";
                    $cordovaSQLite.execute($rootScope.db, queryNews, [0]).then(function(suc) {
                        if(suc.rows.length > 0) {
        
                        $scope.newsshow = true;
        
                        for(var i = 0; i < suc.rows.length;i++ ) {
                            $scope.news.push(suc.rows.item(i));
                        }
                        // $ionicSlideBoxDelegate.update();
                        $timeout(function() {
                            var maxHeightNews = 0;
                            var elementsNews = document.getElementsByClassName("equalexNews");
                            for (var i=0; i<elementsNews.length; i++) {
                                if (elementsNews[i].offsetHeight > maxHeightNews) { maxHeightNews = elementsNews[i].offsetHeight; }
                            }
                            $scope.heightDivNews = {'height': maxHeightNews+'px'};
                        }, 500);
        
                        }
                    }, function() {});
        
                    $scope.gifts = [];
        
                    var queryGifts = "SELECT * FROM gifts WHERE gifts_when > ? AND gifts_del = '0' ORDER BY gifts_points DESC LIMIT 2";
                    $cordovaSQLite.execute($rootScope.db, queryGifts, [2]).then(function(suc) {
                        if(suc.rows.length > 0) {
        
                        $scope.giftsshow = true;
        
                        for(var i = 0; i < suc.rows.length;i++ ) {
                            // console.log('ROW ====================> '+JSON.stringify(suc.rows.item(i)))
                            if(suc.rows.item(i).gifts_when > 2) {
                                $scope.gifts.push(suc.rows.item(i));
                            }
        
                        }
        
                        $timeout(function() {
                            var maxHeightGifts = 0;
                            var elementsGifts = document.getElementsByClassName("equalexGifts");
                            for (var i=0; i<elementsGifts.length; i++) {
                                if (elementsGifts[i].offsetHeight > maxHeightGifts) { maxHeightGifts = elementsGifts[i].offsetHeight; }
                            }
                            $scope.heightDivGifts = {'height': maxHeightGifts+'px'};
                            // console.log('====================> '+JSON.stringify($scope.gifts))
                        }, 500);
        
                        }
                    }, function() {});
        
                    // var queryAsks = "SELECT * FROM asks WHERE asks_id != ? AND asks_del = '0' ORDER BY asks_id DESC LIMIT 1";
                    // $cordovaSQLite.execute($rootScope.db, queryAsks, [0]).then(function(success) {
        
                    //     if(success.rows.length > 0 && success.rows.item(0).asks_when == '0') {
                        
                    //         var askid = success.rows.item(0).asks_id;
        
                    //         $scope.asksPopup = $ionicPopup.show({
                    //             title: success.rows.item(0).asks_name,
                    //             template: '<p class="padding">'+success.rows.item(0).asks_message+'</p>',
                    //             scope: $scope,
                    //             buttons: [
                    //                 { text: $scope.main_ask_button1,
                    //                     onTap: function() {
                    //                         $scope.answAsks(askid, 1);
                    //                         $scope.asksPopup.close();
                    //                     }},
                    //                 { text: $scope.main_ask_button2,
                    //                     onTap: function(e) {
                    //                         $scope.answAsks(askid, 2);
                    //                         $scope.asksPopup.close();
                    //                     }}
                    //             ]
                    //         });
        
                    //     }
        
                    // },function() {});
        
                }, 1000);
        
                $scope.connectvar = 0;
        
                $scope.intconnect = true;
        
                $scope.answAsks = function(x, y) {
                    
                    var answsend = JSON.stringify({
                                    device_id: $rootScope.uuid,
                                    inst_id: $rootScope.institution,
                                    asks_id: x,
                                    asks_answ: y,
                                    newusr: 'asks'
                                });
        
                    $http.post($rootScope.generalscript, answsend).then(function(e) {
        
                        var data = e.data;
        
                        if(data[0].asksOK == '0') {
        
                        }
                        else if(data[0].asksOK == '1') {
                            var asksupd = "UPDATE asks SET asks_yes=?, asks_no=?, asks_when=?, asks_del=? WHERE asks_id=?";
                            $cordovaSQLite.execute($rootScope.db, asksupd, ['1', '0', data[0].asks_when, '0', data[0].asks_id]).then(function() {}, function() {});
                        }
                        else if(data[0].asksOK == '2') {
                            var asksupd = "UPDATE asks SET asks_yes=?, asks_no=?, asks_when=?, asks_del=? WHERE asks_id=?";
                            $cordovaSQLite.execute($rootScope.db, asksupd, ['0', '1', data[0].asks_when, '0', data[0].asks_id]).then(function() {}, function() {});
                        }
                        
                    }, function() {});
        
                }
        
                $scope.htmlEntities = function(x) {
                    return HtmlEnt.decodeEntities(x);
                }
        
                $scope.sendRate = function(x, y) {
                    
                    // ОКОШКО ПРЕДЛАГАЮЩЕЕ ПРИКРЕПИТЬ ФОТО
                    $scope.ratePicPopup = $ionicPopup.show({
                        template: $scope.qrcode_review_pic,
                        title: '',
                        scope: $scope,
                        buttons: [
                            { text: $scope.qrcode_review_button1,
                                onTap: function() {
                                    
                                    $scope.ratePicPopup.close();
                                    
                                    var ratestr = JSON.stringify({
                                        device_id: $rootScope.uuid,
                                        inst_id: $rootScope.institution,
                                        rating: x,
                                        ratingtxt: y,
                                        pic: 0,
                                        newusr: 'rate'
                                    });
        
                                    $http.post($rootScope.generalscript, ratestr).then(function(e) {
        
                                        var data = e.data;
        
                                        // console.log('======================> YES: '+JSON.stringify(data))
        
                                        if(data[0].reviewOK == '0') {
        
                                        }
                                        else if(data[0].reviewOK == '1') {
        
                                            $ionicPopup.alert({
                                                title: $scope.qrcode_review_title,
                                                template: $scope.qrcode_review_body
                                            });
        
                                        }
                                        else if(data[0].reviewOK == '2') {
                                            $ionicPopup.alert({
                                                title: $scope.qrcode_warn_title,
                                                template: $scope.qrcode_warn_body
                                            });
                                        }
        
                                    }, function(er) {
                                        // console.log('======================> ERROR: '+JSON.stringify(er))
                                    });
                                    
                                }
                            },
                            {
                                text: $scope.qrcode_review_button2,
                                onTap: function() {
                                    
                                    $scope.selfie(x, y);
                                    
                                }
                            }
                        ]
                    });
                    
                }
        
                $scope.rateVisit = function() {
        
                    UserData.get().then(function(res) {
        
                        if(res.user_work_pos == '2' || res.user_work_pos == '3' || res.user_work_pos == '4') {
        
                            $scope.forbidPopup = $ionicPopup.show({
                                title: $scope.main_block_title,
                                template: $scope.main_block_body,
                                scope: $scope,
                                buttons: [
                                { text: $scope.main_block_button,
                                    onTap: function() {
                                        $scope.forbidPopup.close();
                                    }}
                                ]
                            });
        
                        }
                        else {
        
                            if(res.user_mob_confirm == 1) {
        
                                var mailmatch = '[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})';
                                if(res.user_email.match(mailmatch)) {
        
                                    var instits = '';
                                    
                                    if($scope.inst.length > 0) {
                            
                                        for(var i=0;i<$scope.inst.length;i++) {
                                            if($scope.inst[i].office_mob.toString().length > 4) {
                                                if(instits == '') {
                                                    instits = '<option value="'+$scope.inst[i].office_name+'">'+$scope.inst[i].office_name+'</option>';
                                                }
                                                else {
                                                    instits += '<option value="'+$scope.inst[i].office_name+'">'+$scope.inst[i].office_name+'</option>';
                                                }
                                            }
                                        }
                                        
                                    }
        
                                    $scope.ratePopup = $ionicPopup.show({
                                        title: $scope.qrcode_review_visit_title,
                                        template: '<p style="text-align:center;max-width:100%;overflow:hidden;"><select ng-model="rating.street" ng-style="{\'height\':\'30px\',\'padding\':\'5px\'}"><option value="'+$scope.qrcode_review_location+'" ng-selected="true">'+$scope.qrcode_review_visit_choice+'</option>'+instits+'</select></p><p><rating class="energized" ng-model="rating.rate" max="rating.max"></rating></p><textarea id="ratetxt" placeholder="'+$scope.qrcode_review_visit_comment+'" rows="6" style="width:100%;border-bottom:2px solid #ff0000;resize:none;padding:4px;"></textarea>',
                                        scope: $scope,
                                        buttons: [
                                            { text: $scope.qrcode_review_visit_button1,
                                                onTap: function() {
                                                    $scope.ratePopup.close();
                                                }},
                                            { text: $scope.qrcode_review_visit_button2,
                                                onTap: function(e) {
                                                    var rtxt = document.getElementById('ratetxt').value;
                                                    if(rtxt) {
                                                        if(rtxt.length > 0) {
                                                            $scope.sendRate($scope.rating.rate, rtxt);
                                                            $scope.ratePopup.close();
                                                        }
                                                    }
                                                }}
                                        ]
                                    });
        
                                }
                                else {
                                    $scope.emailPopup = $ionicPopup.alert({
                                        title: $scope.all_attention,
                                        template: $scope.all_email_warn,
                                        scope: $scope,
                                        buttons: [
                                        { text: $scope.all_go_to_email,
                                            onTap: function() {
                                                $scope.emailPopup.close();
                                                $timeout(function() {$state.go('app.profile1');}, 100);
                                            }},
                                            { text: $scope.gifts_detail_need_phone_button2,
                                            onTap: function() {
                                                $scope.emailPopup.close();
                                            }}
                                        ]
                                    });
                                }
        
                            }
                            else {
        
                                $scope.phonePopup = $ionicPopup.show({
                                    title: $scope.all_attention,
                                    template: $scope.all_phone_confirm_warn,
                                    scope: $scope,
                                    buttons: [
                                    { text: $scope.gifts_detail_need_phone_button1,
                                        onTap: function() {
                                            $scope.phonePopup.close();
                                            $timeout(function() {$state.go('app.profile1');}, 100);
                                        }},
                                        { text: $scope.gifts_detail_need_phone_button2,
                                        onTap: function() {
                                            $scope.phonePopup.close();
                                        }}
                                    ]
                                });
        
                            }
        
                        }
        
                    });
        
                }
        
                // set the rate and max variables
                $scope.rating = {};
                $scope.rating.rate = 5;
                $scope.rating.max = 5;
        
                // НАСТРОЙКИ СЕЛФИ
                var optionsselfie = {
                    quality: 70,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    targetWidth: 700,
                    targetHeight: 700,
                    correctOrientation: true
                };
        
                $scope.selfieBtn = true;
        
                $scope.picPrev = 1;
                
                // СДЕЛАТЬ СЕЛФИ
                $scope.selfie = function(x, y) {
        
                        if($scope.selfieBtn) {
        
                            $scope.selfieBtn = false;
                            
                            $scope.picPrev = 1;
        
                            $cordovaCamera.getPicture(optionsselfie).then(function(imageURI) {
        
                                // DATE - TIME IN SECONDS
                                var when = Math.floor(new Date().getTime() / 1000);
        
                                var randpicname = $scope.myID + '_' + when;
        
                                var namefilesplit = imageURI.split('/');
                                var namefile = namefilesplit[namefilesplit.length-1];
                                var oldurlsplit = imageURI.split(namefile);
                                var oldurl = oldurlsplit[0];
                                var topath = cordova.file.dataDirectory + 'stolle/' + randpicname + '.jpg';
                                var tourl = cordova.file.dataDirectory + 'stolle/';
        
                                $cordovaFile.moveFile(oldurl, namefile, tourl, randpicname + '.jpg').then(function(success) {
                                    // console.log("OK ==============================> "+JSON.stringify(success))
                                    $scope.savePic(topath, randpicname, x, y);
        
                                }, function(er) {
                                    // console.log("ERROR ==============================> "+JSON.stringify(er))
                                });
        
        
                            }, function(err) {
                                $scope.selfieBtn = true;
                            });
        
                        }
                }
        
                // ЗАГРУЗКА СЕЛФИ
                $scope.savePic = function(imgpath, randpicname, x, y) {
        
                    if($scope.picPrev == '1') {
                    
                        $scope.ratePicPopup.close();
        
                        // Setup the loader
                        $ionicLoading.show({
                            content: '<i class="icon ion-loading-c"></i><span id="loadprog"></span>',
                            animation: 'fade-in',
                            showBackdrop: false,
                            maxWidth: 50,
                            showDelay: 0
                        });
        
                        // UPLOADING SOUND
                        var options = {
                            fileKey: "file",
                            fileName: randpicname + '.jpg',
                            chunkedMode: false,
                            mimeType: 'image/jpeg'
                        }
        
                        $cordovaFileTransfer.upload("http://www.olegtronics.com/admin/coms/upload.php?usrupl=1&preview="+$scope.picPrev+"&user_id=" + $scope.myID, imgpath, options).then(function(result) {
                            
                            var srtringify = JSON.stringify(result.response);
                            var parsingres = JSON.parse(JSON.parse(srtringify));
        
                            var messent = parsingres.user_upd;
        
                            if(messent > 0) {
        
                                    $scope.picPreview = 'http://www.olegtronics.com/admin/img/user/'+$rootScope.institution+'/pic/'+randpicname + '.jpg';
        
                                    // ОКОШКО ПРЕДЛАГАЮЩЕЕ СОХРАНИТЬ ФОТО
                                    $scope.picPopup = $ionicPopup.show({
                                        template: '<div class="list card"><div class="item item-image"><img cache-src="'+$scope.picPreview+'" /></div></div>',
                                        title: '',
                                        subtitle: '',
                                        scope: $scope,
                                        buttons: [
                                            { text: $scope.qrcode_review_visit_pic_button1,
                                                type: 'button-assertive',
                                                onTap: function() {
                                                    $cordovaFile.removeFile(cordova.file.dataDirectory + 'stolle/', randpicname + '.jpg').then(function() {
                                                        $scope.picPopup.close();
                                                        $scope.selfieBtn = true;
                                                        $timeout(function() {$scope.sendRate(x, y);}, 500);
                                                    }, function() {});
                                                }
                                            },
                                            {
                                                text: '<b>'+$scope.qrcode_review_visit_pic_button2+'</b>',
                                                type: 'button-balanced',
                                                onTap: function(e) {
                                                    $cordovaFile.removeFile(cordova.file.dataDirectory + 'stolle/', randpicname + '.jpg').then(function() {
                                                        $scope.picPrev = 0;
                                                        $scope.selfieBtn = true;
                                                        $timeout(function() {$scope.savePic(imgpath, randpicname, x, y);}, 500);
                                                        $scope.picPopup.close();
                                                    }, function() {});
                                                    
                                                }
                                            }
                                        ]
                                    });
        
                            }
        
                            $ionicLoading.hide();
        
                        }, function(err) {
        
                            $ionicLoading.hide();
        
                        }, function (progress) {
                            document.getElementById('loadprog').innerHTML = Math.round((progress.loaded / progress.total) * 100) + ' %';
                        });
                    
                    }
                    else if($scope.picPrev == '0') {
        
                            var picstr = JSON.stringify({
                                device_id: $rootScope.uuid,
                                inst_id: $rootScope.institution,
                                rating: x,
                                ratingtxt: y,
                                pic: randpicname + '.jpg',
                                newusr: 'rate'
                            });
        
                            $http.post($rootScope.generalscript, picstr).then(function(e) {
        
                                $ionicPopup.alert({
                                    title: $scope.qrcode_review_title,
                                    template: $scope.qrcode_review_body
                                });
        
                            }, function() {});
        
                            $scope.picPopup.close();
        
                    }
        
                }
        
            });
    
        });
        $ionicSideMenuDelegate.canDragContent(true);
        $timeout(function() {
            $ionicNavBarDelegate.align('center');
        }, 200);
    });

    $scope.gotoCard = function() {
        
        UserData.get().then(function(res) {

            if(res.user_work_pos == '2' || res.user_work_pos == '3' || res.user_work_pos == '4') {

                $scope.forbidPopup = $ionicPopup.show({
                    title: $scope.main_block_title,
                    template: $scope.main_block_body,
                    scope: $scope,
                    buttons: [
                    { text: $scope.main_block_button,
                        onTap: function() {
                            $scope.forbidPopup.close();
                        }}
                    ]
                });

            }
            else {

                if(res.user_mob_confirm == 1) {
                    var mailmatch = '[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})';
                    if(res.user_email.match(mailmatch)) {
                        // STANDART
                        // StoreVal.setbill(1234, '&'+$rootScope.uuid);
                        // SMALLER QR-CODE
                        StoreVal.setbill(1234, '&'+$scope.user.user_real_id);
                        $timeout(function() {$state.go('app.qr-code')}, 10);

                    }
                    else {
                        $scope.emailPopup = $ionicPopup.alert({
                            title: $scope.all_attention,
                            template: $scope.all_email_warn,
                            scope: $scope,
                            buttons: [
                            { text: $scope.all_go_to_email,
                                onTap: function() {
                                    $scope.emailPopup.close();
                                    $timeout(function() {$state.go('app.profile1');}, 100);
                                }},
                                { text: $scope.gifts_detail_need_phone_button2,
                                onTap: function() {
                                    $scope.emailPopup.close();
                                }}
                            ]
                        });
                    }
                }
                else {

                    $scope.phonePopup = $ionicPopup.show({
                        title: $scope.all_attention,
                        template: $scope.all_phone_confirm_warn,
                        scope: $scope,
                        buttons: [
                        { text: $scope.gifts_detail_need_phone_button1,
                            onTap: function() {
                                $scope.phonePopup.close();
                                $timeout(function() {$state.go('app.profile1');}, 100);
                            }},
                            { text: $scope.gifts_detail_need_phone_button2,
                            onTap: function() {
                                $scope.phonePopup.close();
                            }}
                        ]
                    });

                }

            }

        });

    }

    $scope.openCallModal = function(){
        $scope.callModal.show();
    }

    $scope.closeCallModal = function(){
        $scope.callModal.hide();
    }

})

.controller('QRCodeCtrl', function($rootScope, $scope, $state, $ionicHistory, $ionicPopup, $stateParams, $timeout, $ionicPlatform, $window, $ionicNavBarDelegate, $ionicLoading, $cordovaSQLite, $http, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $translate, ionicMaterialMotion, ionicMaterialInk, StoreVal, Inst) {

    $translate(['qrcode_review_title', 'qrcode_review_body', 'qrcode_warn_title', 'qrcode_warn_body', 'qrcode_review_pic', 'qrcode_review_button1', 'qrcode_review_button2', 'qrcode_review_location', 'qrcode_review_visit_title', 'qrcode_review_visit_choice', 'qrcode_review_visit_comment', 'qrcode_review_visit_button1', 'qrcode_review_visit_button2', 'qrcode_review_visit_pic_button1', 'qrcode_review_visit_pic_button2', 'internet_connection_title', 'internet_connection_body']).then(function(res) {

        $scope.qrcode_review_title = res.qrcode_review_title;
        $scope.qrcode_review_body = res.qrcode_review_body;
        $scope.qrcode_warn_title = res.qrcode_warn_title;
        $scope.qrcode_warn_body = res.qrcode_warn_body;
        $scope.qrcode_review_pic = res.qrcode_review_pic;
        $scope.qrcode_review_button1 = res.qrcode_review_button1;
        $scope.qrcode_review_button2 = res.qrcode_review_button2;
        $scope.qrcode_review_location = res.qrcode_review_location;
        $scope.qrcode_review_visit_title = res.qrcode_review_visit_title;
        $scope.qrcode_review_visit_choice = res.qrcode_review_visit_choice;
        $scope.qrcode_review_visit_comment = res.qrcode_review_visit_comment;
        $scope.qrcode_review_visit_button1 = res.qrcode_review_visit_button1;
        $scope.qrcode_review_visit_button2 = res.qrcode_review_visit_button2;
        $scope.qrcode_review_visit_pic_button1 = res.qrcode_review_visit_pic_button1;
        $scope.qrcode_review_visit_pic_button2 = res.qrcode_review_visit_pic_button2;
        $scope.internet_connection_title = res.internet_connection_title;
        $scope.internet_connection_body = res.internet_connection_body;

    });

    $ionicHistory.nextViewOptions({
      disableAnimate: true,
      disableBack: false
    });

    $scope.$on('$ionicView.beforeLeave', function(){
        $scope.connectvar = 1;
    });

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');

        // if($cordovaNetwork.isOnline()) {
            // $scope.socketConnection();
        // }
        // else {
        //     $scope.socketConnection();
        //     $scope.intconnect = false;
        //     $ionicPopup.alert({
        //         title: $scope.internet_connection_title,
        //         template: $scope.internet_connection_body
        //     });
        // }

      }, 100);
    });

    $scope.$on('$ionicView.enter', function() {
        $scope.qrcodeString = StoreVal.getbill();
    });

    if(Inst.check()) {
        $scope.inst = Inst.getinstold();
    }
    else {
        Inst.getinstnew().then(function(data) {
            $scope.inst = data;
        }, function() {});
    }

    $scope.intconnect = true;

    $scope.connectvar = 0;

    $scope.sendRate = function(x, y) {
        
        // ОКОШКО ПРЕДЛАГАЮЩЕЕ ПРИКРЕПИТЬ ФОТО
        $scope.ratePicPopup = $ionicPopup.show({
            template: $scope.qrcode_review_pic,
            title: '',
            scope: $scope,
            buttons: [
                { text: $scope.qrcode_review_button1,
                    onTap: function() {
                        
                        $scope.ratePicPopup.close();
                        
                        var ratestr = JSON.stringify({
                            device_id: $rootScope.uuid,
                            inst_id: $rootScope.institution,
                            rating: x,
                            ratingtxt: y,
                            pic: 0,
                            newusr: 'rate'
                        });

                        $http.post($rootScope.generalscript, ratestr).then(function(e) {

                            var data = e.data;

                            // console.log('======================> YES: '+JSON.stringify(data))

                            if(data[0].reviewOK == '0') {

                            }
                            else if(data[0].reviewOK == '1') {

                                $ionicPopup.alert({
                                    title: $scope.qrcode_review_title,
                                    template: $scope.qrcode_review_body
                                });

                                $ionicHistory.nextViewOptions({
                                  disableAnimate: true,
                                  disableBack: true
                                });
                                $state.go('app.main');
                            }
                            else if(data[0].reviewOK == '2') {
                                $ionicPopup.alert({
                                    title: $scope.qrcode_warn_title,
                                    template: $scope.qrcode_warn_body
                                });
                            }

                        }, function(er) {
                            // console.log('======================> ERROR: '+JSON.stringify(er))
                            $state.go('app.main');
                        });
                        
                    }
                },
                {
                    text: $scope.qrcode_review_button2,
                    onTap: function() {
                        
                        $scope.selfie(x, y);
                        
                    }
                }
            ]
        });
        
    }

    var version = parseInt($rootScope.version) * 100;
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.qrcodeString = StoreVal.getbill();
    $scope.size = 200;
    $scope.correctionLevel = '';
    $scope.typeNumber = 0;
    $scope.inputMode = '';
    $scope.image = false;

    $scope.billsum = (StoreVal.getsum()/100).toFixed(2).split('.')[0];
    $scope.remainer = (StoreVal.getsum()/100).toFixed(2).split('.')[1];

    // set the rate and max variables
    $scope.rating = {};
    $scope.rating.rate = 5;
    $scope.rating.max = 5;
    
    var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
    $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {

        $scope.user = success.rows.item(0);
        $scope.myID = success.rows.item(0).user_real_id;
        
    }, function() {});

    $scope.paid = function() {

        var instits = '';

        if($scope.inst.length > 0) {

            for(var i=0;i<$scope.inst.length;i++) {
                if($scope.inst[i].office_mob.toString().length > 4) {
                    if(instits == '') {
                        instits = '<option value="'+$scope.inst[i].office_name+'">'+$scope.inst[i].office_name+'</option>';
                    }
                    else {
                        instits += '<option value="'+$scope.inst[i].office_name+'">'+$scope.inst[i].office_name+'</option>';
                    }
                }
            }
            
        }

        $scope.ratePopup = $ionicPopup.show({
            title: $scope.qrcode_review_visit_title,
            template: '<p style="text-align:center;max-width:100%;overflow:hidden;"><select ng-model="rating.street" ng-style="{\'height\':\'30px\',\'padding\':\'5px\'}"><option value="'+$scope.qrcode_review_location+'" ng-selected="true">'+$scope.qrcode_review_visit_choice+'</option>'+instits+'</select></p><p><rating class="energized" ng-model="rating.rate" max="rating.max"></rating></p><textarea id="ratetxt" placeholder="'+$scope.qrcode_review_visit_comment+'" rows="6" style="width:100%;border-bottom:2px solid #ff0000;resize:none;padding:4px;"></textarea>',
            scope: $scope,
            buttons: [
                { text: $scope.qrcode_review_visit_button1,
                    onTap: function() {
                        $scope.ratePopup.close();
                        $ionicHistory.nextViewOptions({
                            disableAnimate: true,
                            disableBack: true
                        });
                        $timeout(function() {$state.go('app.main');}, 500);
                    }},
                { text: $scope.qrcode_review_visit_button2,
                    onTap: function(e) {
                        $ionicHistory.nextViewOptions({
                            disableAnimate: true,
                            disableBack: true
                        });
                        var rtxt = document.getElementById('ratetxt').value;
                        $scope.sendRate($scope.rating.rate, rtxt);
                        $scope.ratePopup.close();
                    }}
            ]
        });

    }

    // НАСТРОЙКИ СЕЛФИ
    var optionsselfie = {
        quality: 70,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: 700,
        targetHeight: 700,
        correctOrientation: true
    };

    $scope.selfieBtn = true;

    $scope.picPrev = 1;
    
    // СДЕЛАТЬ СЕЛФИ
    $scope.selfie = function(x, y) {

        // if($cordovaNetwork.isOnline()) {

            if($scope.selfieBtn) {

                $scope.selfieBtn = false;
                
                $scope.picPrev = 1;

                $cordovaCamera.getPicture(optionsselfie).then(function(imageURI) {

                    // DATE - TIME IN SECONDS
                    var when = Math.floor(new Date().getTime() / 1000);

                    var randpicname = $scope.myID + '_' + when;

                    var namefilesplit = imageURI.split('/');
                    var namefile = namefilesplit[namefilesplit.length-1];
                    var oldurlsplit = imageURI.split(namefile);
                    var oldurl = oldurlsplit[0];
                    var topath = cordova.file.dataDirectory + 'stolle/' + randpicname + '.jpg';
                    var tourl = cordova.file.dataDirectory + 'stolle/';

                    $cordovaFile.moveFile(oldurl, namefile, tourl, randpicname + '.jpg').then(function(success) {
                        // console.log("OK ==============================> "+JSON.stringify(success))
                        $scope.savePic(topath, randpicname, x, y);

                    }, function(er) {
                        // console.log("ERROR ==============================> "+JSON.stringify(er))
                    });


                }, function(err) {
                    $scope.selfieBtn = true;
                });

            }
        /*
        }
        else {
         $scope.intconnect = false;
         $ionicPopup.alert({
            title: $scope.internet_connection_title,
            template: $scope.internet_connection_body
         });
        }
        */
    }

    // ЗАГРУЗКА СЕЛФИ
    $scope.savePic = function(imgpath, randpicname, x, y) {

        if($scope.picPrev == '1') {
        
            $scope.ratePicPopup.close();

            // Setup the loader
            $ionicLoading.show({
                content: '<i class="icon ion-loading-c"></i><span id="loadprog"></span>',
                animation: 'fade-in',
                showBackdrop: false,
                maxWidth: 50,
                showDelay: 0
            });

            // UPLOADING SOUND
            var options = {
                fileKey: "file",
                fileName: randpicname + '.jpg',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }

            $cordovaFileTransfer.upload("http://www.olegtronics.com/admin/coms/upload.php?usrupl=1&preview="+$scope.picPrev+"&user_id=" + $scope.myID, imgpath, options).then(function(result) {
                
                    var srtringify = JSON.stringify(result.response);
                    var parsingres = JSON.parse(JSON.parse(srtringify));

                    var messent = parsingres.user_upd;

                    if(messent > 0) {

                            $scope.picPreview = 'http://www.olegtronics.com/admin/img/user/'+$rootScope.institution+'/pic/'+randpicname + '.jpg';

                            // ОКОШКО ПРЕДЛАГАЮЩЕЕ СОХРАНИТЬ ФОТО
                            $scope.picPopup = $ionicPopup.show({
                                template: '<div class="list card"><div class="item item-image"><img cache-src="'+$scope.picPreview+'" /></div></div>',
                                title: '',
                                subtitle: '',
                                scope: $scope,
                                buttons: [
                                    { text: $scope.qrcode_review_visit_pic_button1,
                                        type: 'button-assertive',
                                        onTap: function() {
                                            $cordovaFile.removeFile(cordova.file.dataDirectory + 'stolle/', randpicname + '.jpg').then(function() {
                                                $scope.picPopup.close();
                                                $scope.selfieBtn = true;
                                                $timeout(function() {$scope.sendRate(x, y);}, 500);
                                            }, function() {});
                                        }
                                    },
                                    {
                                        text: '<b>'+$scope.qrcode_review_visit_pic_button2+'</b>',
                                        type: 'button-balanced',
                                        onTap: function(e) {                                    
                                            $cordovaFile.removeFile(cordova.file.dataDirectory + 'stolle/', randpicname + '.jpg').then(function() {
                                                $scope.picPrev = 0;
                                                $scope.selfieBtn = true;
                                                $timeout(function() {$scope.savePic(imgpath, randpicname, x, y);}, 500);
                                                $scope.picPopup.close();
                                            }, function() {});
                                            
                                        }
                                    }
                                ]
                            });

                    }

                $ionicLoading.hide();

            }, function(err) {

                $ionicLoading.hide();

            }, function (progress) {
                document.getElementById('loadprog').innerHTML = Math.round((progress.loaded / progress.total) * 100) + ' %';
            });
        
        }
        else if($scope.picPrev == '0') {
                        
            // if($cordovaNetwork.isOnline()) {

                var picstr = JSON.stringify({
                    device_id: $rootScope.uuid,
                    inst_id: $rootScope.institution,
                    rating: x,
                    ratingtxt: y,
                    pic: randpicname + '.jpg',
                    newusr: 'rate'
                });

                $http.post($rootScope.generalscript, picstr).then(function(e) {

                    $ionicPopup.alert({
                        title: $scope.qrcode_review_title,
                        template: $scope.qrcode_review_body
                    });
                    $state.go('app.main');

                }, function() {
                    $state.go('app.main');
                });

                $scope.picPopup.close();
                $ionicHistory.nextViewOptions({
                  disableAnimate: true,
                  disableBack: true
                });
                
            // }
            // else {
            //     $scope.intconnect = false;
            //     $ionicPopup.alert({
            //         title: $scope.internet_connection_title,
            //         template: $scope.internet_connection_body
            //     });
            // }

        }

    }

})

.controller('BillCtrl', function($rootScope, $scope, $state, $timeout, $stateParams, $ionicHistory, $cordovaSQLite, $ionicPlatform, $cordovaBarcodeScanner, $ionicPopup, $ionicNavBarDelegate, $translate, $http, ionicMaterialInk, ionicMaterialMotion, StoreVal) {

    $translate(['bill_card_use_title', 'bill_card_use_body', 'bill_calcbill_title', 'bill_calcbill_body', 'bill_discount_title', 'bill_discount_body', 'bill_discount_button1', 'bill_discount_button2', 'bill_wrong_code_title', 'bill_wrong_code_body', 'bill_wrong_code_button1', 'bill_wrong_code_button2', 'bill_checkcode_error_title', 'bill_checkcode_error_body', 'bill_checkcode_error_button', 'internet_connection_title', 'internet_connection_body']).then(function(res) {

        $scope.bill_card_use_title = res.bill_card_use_title;
        $scope.bill_card_use_body = res.bill_card_use_body;
        $scope.bill_calcbill_title = res.bill_calcbill_title;
        $scope.bill_calcbill_body = res.bill_calcbill_body;
        $scope.bill_discount_title = res.bill_discount_title;
        $scope.bill_discount_body = res.bill_discount_body;
        $scope.bill_discount_button1 = res.bill_discount_button1;
        $scope.bill_discount_button2 = res.bill_discount_button2;
        $scope.bill_wrong_code_title = res.bill_wrong_code_title;
        $scope.bill_wrong_code_body = res.bill_wrong_code_body;
        $scope.bill_wrong_code_button1 = res.bill_wrong_code_button1;
        $scope.bill_wrong_code_button2 = res.bill_wrong_code_button2;
        $scope.bill_checkcode_error_title = res.bill_checkcode_error_title;
        $scope.bill_checkcode_error_body = res.bill_checkcode_error_body;
        $scope.bill_checkcode_error_button = res.bill_checkcode_error_button;
        $scope.internet_connection_title = res.internet_connection_title;
        $scope.internet_connection_body = res.internet_connection_body;

    });

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');

        // if($cordovaNetwork.isOnline()) {
            // $scope.socketConnection();
        // }
        // else {
        //     $scope.socketConnection();
        //     $scope.intconnect = false;
        //     $ionicPopup.alert({
        //         title: $scope.internet_connection_title,
        //         template: $scope.internet_connection_body
        //     });
        // }

      }, 500);
    });

    $scope.$on('$ionicView.beforeLeave', function(){
        $scope.connectvar = 1;
    });

    $scope.intconnect = true;

    $scope.usrdiscount = 0;

    $scope.connectvar = 0;

    $scope.sendDiscount = function(x) {

        $scope.usrdiscount = x;
        var discountstr = JSON.stringify({
            device_id: $rootScope.uuid,
            inst_id: $rootScope.institution,
            discount: x,
            newusr: 'discount'
        });

        $http.post($rootScope.generalscript, discountstr).then(function(e) {
            var data = JSON.parse(e.data);

            // alert(JSON.stringify(data))

            if(data[0].discountOK == '0') {

            }
            else if(data[0].discountOK == '1') {

                var queryUsrSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
                $cordovaSQLite.execute($rootScope.db, queryUsrSel, [1]).then(function(success) {

                  if(success.rows.length > 0) {

                    var queryUsrUpd = "UPDATE users SET user_discount = ?, user_upd = ? WHERE user_id = ?";
                    $cordovaSQLite.execute($rootScope.db, queryUsrUpd, [$scope.usrdiscount, data[0].discount_when, 1]).then(function() {

                        // var otherstring = '&' + $scope.user.user_real_id + '&' + $scope.user.user_device_id + '&' + $scope.user.user_mob;

                        // alert('1: '+otherstring+ ' 2: '+$scope.realexpression + ' 3: '+$scope.usrdiscount)

                        StoreVal.setdiscount($scope.realexpression, '&' + $scope.user.user_device_id, $scope.usrdiscount);

                        $timeout(function() {

                            $ionicHistory.nextViewOptions({
                              disableAnimate: true,
                              disableBack: true
                            });

                            $state.go('app.qr-code');

                        }, 500);

                    }, function() {});

                  }

                }, function() {});

            }
            else if(data[0].discountOK == '2') {
                $ionicPopup.alert({
                    title: $scope.bill_card_use_title,
                    template: $scope.bill_card_use_body
                });
            }

        }, function() {});

    }

    var version = parseInt($rootScope.version) * 100;
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.realexpression = 0;
    $scope.expression = 0;
    $scope.remainer = 0;

    $ionicPlatform.ready(function() {

        var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
        $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {

          if(success.rows.length > 0) {
            $scope.user = success.rows.item(0);
          }

        },function() {});

    });

    $scope.add = function(value) {
        if($scope.realexpression === "" || $scope.realexpression === undefined || $scope.realexpression === 0) {
            $scope.realexpression = value;
            $scope.expression = ($scope.realexpression / 100).toFixed(2).split('.')[0];
            $scope.remainer = ($scope.realexpression / 100).toFixed(2).split('.')[1];
        } else if($scope.realexpression.length === undefined || $scope.realexpression.length <= 7) {
            $scope.realexpression = $scope.realexpression + "" + value;
            $scope.expression = (parseInt($scope.realexpression) / 100).toFixed(2).split('.')[0];
            $scope.remainer = (parseInt($scope.realexpression) / 100).toFixed(2).split('.')[1];
        }
    }

    $scope.removeText = function() {

        if($scope.realexpression != "" && $scope.realexpression != undefined && $scope.realexpression != 0 && $scope.realexpression.length != undefined) {

            if($scope.realexpression.length == 1) {
                $scope.realexpression = 0;
                $scope.expression = 0;
                $scope.remainer = 0;
            }
            else {
                $scope.realexpression = $scope.realexpression.slice(0, -1);
                $scope.expression = ($scope.realexpression / 100).toFixed(2).split('.')[0];
                $scope.remainer = ($scope.realexpression / 100).toFixed(2).split('.')[1];
            }
            
        } else {
            $scope.realexpression = 0;
            $scope.expression = 0;
            $scope.remainer = 0;
        }
    }

    $scope.calcbill = function() {

        if($scope.realexpression != "" && $scope.realexpression != undefined && $scope.realexpression != 0 && $scope.realexpression.length != undefined) {

            // $scope.billPopup = $ionicPopup.show({
            //     template: '<div class="list card"><div class="item"><h2>Получить баллы</h2></div><a ng-click="setPoints();" class="item item-image"><img style="max-width:100%;max-height:100%;" src="img/btn/btn_points.png"></a></div>',
            //     scope: $scope,
            //     buttons: [
            //         { text: 'ОТМЕНА',
            //             onTap: function() {
            //                 $scope.billPopup.close();
            //             }}
            //     ]
            // });

            $scope.setPoints();

        }
        else {
            $ionicPopup.alert({
                title: $scope.bill_calcbill_title,
                template: $scope.bill_calcbill_body
            });
        }

    }

    $scope.proofDiscount = function() {

        $scope.billPopup.close();

        // alert($scope.user.user_discount)

        if($scope.user.user_discount != "" && $scope.user.user_discount != undefined && $scope.user.user_discount != 0 && $scope.user.user_discount.length != undefined) {

            $ionicHistory.nextViewOptions({
              disableAnimate: true,
              disableBack: false
            });

            // var otherstring = '&' + $scope.user.user_real_id + '&' + $scope.user.user_device_id + '&' + $scope.user.user_mob;

            StoreVal.setdiscount($scope.realexpression, '&'+$scope.user.user_device_id, $scope.user.user_discount);

            $timeout(function() {$state.go('app.qr-code');}, 200);

        }
        else {

            $scope.discountPopup = $ionicPopup.show({
                title: $scope.bill_discount_title,
                template: '<div class="padding">'+$scope.bill_discount_body+'</div>',
                scope: $scope,
                buttons: [
                        {   text: $scope.bill_discount_button1,
                            type: 'button-balanced',
                            onTap: function() {
                                $scope.discountPopup.close();
                                $timeout(function() {$scope.scanning();}, 200);
                            }},
                        // {   text: 'ЗАПРОСИТЬ',
                        //     type: 'button-positive',
                        //     onTap: function(e) {
                        //         $ionicHistory.nextViewOptions({
                        //           disableAnimate: true,
                        //           disableBack: true
                        //         });
                        //         $scope.discountPopup.close();
                        //         $timeout(function() {$state.go('app.profile1');}, 200);
                        //     }},
                        {   text: $scope.bill_discount_button2,
                            type: 'button-assertive',
                            onTap: function(e) {
                                $scope.discountPopup.close();
                            }}
                ]
            });

        }

    }

    $scope.setPoints = function() {

        // $scope.billPopup.close();

        // $ionicHistory.nextViewOptions({
        //   disableAnimate: true,
        //   disableBack: false
        // });

        // var otherstring = '&' + $scope.user.user_real_id + '&' + $scope.user.user_device_id + '&' + $scope.user.user_mob;

        StoreVal.setbill($scope.realexpression, '&'+$scope.user.user_device_id);

        $timeout(function() {$state.go('app.qr-code');}, 500);

    }

    $scope.scanning = function() {

        $cordovaBarcodeScanner.scan().then(function(barcodeData) {

            var barCode = barcodeData.text.toString();

                // alert(barCode.length + ' | ' + barcodeData.text)

            if(barCode.length >= 12 && barCode.length <= 13) {

                $scope.sendDiscount(barCode);

            }
            else {

              $scope.noEffortPopup = $ionicPopup.show({
                    title: $scope.bill_wrong_code_title,
                    template: '<p class="padding" style="text-align:center;">'+bill_wrong_code_body+'<p>',
                    scope: $scope,
                    buttons: [
                        { text: $scope.bill_wrong_code_button1,
                            onTap: function() {
                                $scope.noEffortPopup.close();
                                $timeout(function() {$scope.scanning();}, 200);
                            }},
                        { text: $scope.bill_wrong_code_button2,
                            onTap: function(e) {
                                $scope.noEffortPopup.close();
                            }}
                    ]
                });

            }

        }, 
        function(error) {

            $scope.errorPopup = $ionicPopup.show({
                title: $scope.bill_checkcode_error_title,
                template: '<p class="padding assertive" style="text-align:center;">'+$scope.bill_checkcode_error_body+'</p><p>'+JSON.stringify(error)+'</p>',
                scope: $scope,
                buttons: [
                    { text: $scope.bill_checkcode_error_button,
                        onTap: function(e) {
                            $scope.errorPopup.close();
                        }}
                ]
            });

        });

      }

})

.controller('GiftsCtrl', function($rootScope, $scope, $window, $ionicPlatform, $state, $timeout, $cordovaFile, $cordovaFileTransfer, $localStorage, $cordovaSQLite, $ionicScrollDelegate, $ionicNavBarDelegate, $translate, ionicMaterialInk, ionicMaterialMotion, HtmlEnt) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 500);
    });

    // $timeout(function() {

    //     var maxHeight = 0;
    //     var elements = document.getElementsByClassName("equalex");
    //     for (var i=0; i<elements.length; i++) {
    //         if (elements[i].offsetHeight > maxHeight) { maxHeight = elements[i].offsetHeight; }
    //     }
    //     $scope.heightDiv = {'height': maxHeight+'px'};

    //    $ionicScrollDelegate.resize();
    // }, 1000);

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    // $scope.allGifts = Gifts.all();

    $scope.dev_height = $window.innerHeight / 3;

    $timeout(function() {
        $scope.myStyle = {'height': document.getElementsByClassName("equalex")[0].offsetWidth+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};
    }, 500);

    $scope.gifts = [];

    var queryGifts = "SELECT * FROM gifts WHERE gifts_when > ? AND gifts_del = '0' ORDER BY gifts_points ASC";
    $cordovaSQLite.execute($rootScope.db, queryGifts, [2]).then(function(suc) {

      if(suc.rows.length > 0) {

        for(var i = 0; i < suc.rows.length;i++ ) {

            if(suc.rows.item(i).gifts_id > '1') {
                $scope.gifts.push(suc.rows.item(i));
            }
            
        }

      }
    }, function() {});

    $scope.htmlEntities = function(x) {
        return HtmlEnt.decodeEntities(x);
    }

})

.controller('GiftsDetailCtrl', function($rootScope, $scope, $stateParams, $ionicPlatform, $ionicPopup, $state, $timeout, $cordovaFile, $cordovaFileTransfer, $localStorage, $window, $ionicScrollDelegate, $cordovaSQLite, $ionicNavBarDelegate, $translate, ionicMaterialMotion, ionicMaterialInk, Gifts, StoreVal, HtmlEnt) {

    $translate(['gifts_detail_block_title', 'gifts_detail_block_body', 'gifts_detail_block_button', 'gifts_detail_use_points_title', 'gifts_detail_use_points_body1', 'gifts_detail_use_points_body2', 'gifts_detail_use_points_body3', 'gifts_detail_use_points_body4', 'gifts_detail_use_points_button1', 'gifts_detail_use_points_button2', 'gifts_detail_need_phone_title', 'gifts_detail_need_phone_body', 'gifts_detail_need_phone_button1', 'gifts_detail_need_phone_button2', 'gifts_detail_waiter_scan_title', 'gifts_detail_waiter_scan_title2', 'gifts_detail_waiter_scan_button', 'internet_connection_title', 'internet_connection_body']).then(function(res) {

        $scope.gifts_detail_block_title = res.gifts_detail_block_title;
        $scope.gifts_detail_block_body = res.gifts_detail_block_body;
        $scope.gifts_detail_block_button = res.gifts_detail_block_button;
        $scope.gifts_detail_use_points_title = res.gifts_detail_use_points_title;
        $scope.gifts_detail_use_points_body1 = res.gifts_detail_use_points_body1;
        $scope.gifts_detail_use_points_body2 = res.gifts_detail_use_points_body2;
        $scope.gifts_detail_use_points_body3 = res.gifts_detail_use_points_body3;
        $scope.gifts_detail_use_points_body4 = res.gifts_detail_use_points_body4;
        $scope.gifts_detail_use_points_button1 = res.gifts_detail_use_points_button1;
        $scope.gifts_detail_use_points_button2 = res.gifts_detail_use_points_button2;
        $scope.gifts_detail_need_phone_title = res.gifts_detail_need_phone_title;
        $scope.gifts_detail_need_phone_body = res.gifts_detail_need_phone_body;
        $scope.gifts_detail_need_phone_button1 = res.gifts_detail_need_phone_button1;
        $scope.gifts_detail_need_phone_button2 = res.gifts_detail_need_phone_button2;
        $scope.gifts_detail_waiter_scan_title = res.gifts_detail_waiter_scan_title;
        $scope.gifts_detail_waiter_scan_title2 = res.gifts_detail_waiter_scan_title2;
        $scope.gifts_detail_waiter_scan_button = res.gifts_detail_waiter_scan_button;
        $scope.internet_connection_title = res.internet_connection_title;
        $scope.internet_connection_body = res.internet_connection_body;

    });

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 500);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.dev_height = $window.innerHeight / 2;

    $scope.gift = [];

    $scope.selectpic = '';

    $scope.mobConfirm = 0;
    $scope.myPhone = 0;

    var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
    $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {

      if(success.rows.length > 0) {
        $scope.user = success.rows.item(0);
        $scope.myPhone = success.rows.item(0).user_mob;
        $scope.mobConfirm = success.rows.item(0).user_mob_confirm;
      }

    },function() {});

    var queryGiftSel = "SELECT * FROM gifts WHERE gifts_id = ? AND gifts_del = '0'";
    $cordovaSQLite.execute($rootScope.db, queryGiftSel, [$stateParams.giftId]).then(function(success) {

      if(success.rows.length > 0) {

        $scope.gift = success.rows.item(0);

        if($stateParams.giftId == '1') {
            $scope.selectpic = "http://www.olegtronics.com/admin/img/icons/"+success.rows.item(0).gifts_pic;
        }
        else {
            $scope.selectpic = "http://www.olegtronics.com/admin/img/gifts/"+success.rows.item(0).gifts_institution+"/pic/"+success.rows.item(0).gifts_pic;
        }

      }

    }, function() {});

    $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center'};

    $scope.acceptStyle = function() {
        $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};

        $ionicScrollDelegate.resize();
    }

    $scope.mapHeight = {'height': $scope.dev_height+'px', 'width': '100%'};

    $scope.getbonus = function(bonuscost, gifttype) {

        if($scope.user.user_work_pos == '2' || $scope.user.user_work_pos == '3' || $scope.user.user_work_pos == '4') {

            $scope.forbidPopup = $ionicPopup.show({
                    title: $scope.gifts_detail_block_title,
                    template: $scope.gifts_detail_block_body,
                    scope: $scope,
                    buttons: [
                      { text: $scope.gifts_detail_block_button,
                          onTap: function() {
                              $scope.forbidPopup.close();
                          }}
                    ]
                });

        }
        else if($scope.myPhone && $scope.myPhone != '0' && $scope.mobConfirm && $scope.mobConfirm == '1') {
            // BIG QR-CODE
            // var otherstring = $scope.gift.gifts_id + '&' + $scope.user.user_device_id;
            // NEW QR-CODE FOR STOLLE
            var otherstring = $scope.gift.gifts_id + '&' + $scope.user.user_real_id;

            // IS FIRST GIFT OR NOT
            var bonuscost = bonuscost;
            var bontit = $scope.gifts_detail_use_points_title;
            var bontemp = '<p class="padding">'+$scope.gifts_detail_use_points_body1+bonuscost+$scope.gifts_detail_use_points_body2+'<br/> '+$scope.gifts_detail_use_points_body3+'</p>';
            if(gifttype == 1) {
                bontemp = '<p class="padding">'+$scope.gifts_detail_use_points_body4+'</p>';
                bonuscost = 1;
                bontit = '';
            }

            StoreVal.setbill(bonuscost, otherstring);

            $scope.billsum = StoreVal.getsum();

            $scope.bonusPopup = $ionicPopup.show({
                title: bontit,
                template: bontemp,
                scope: $scope,
                buttons: [
                    { text: $scope.gifts_detail_use_points_button1,
                        onTap: function() {
                            $scope.bonusPopup.close();
                        }
                    },
                    { text: $scope.gifts_detail_use_points_button2,
                        onTap: function(e) {
                            $timeout(function() {
                                $scope.qrbonus(StoreVal.getbill(), gifttype);
                            }, 200);
                            $scope.bonusPopup.close();
                        }
                    }
                ]
            });

        }
        else {

            $scope.phonePopup = $ionicPopup.show({
                title: $scope.gifts_detail_need_phone_title,
                template: $scope.gifts_detail_need_phone_body,
                scope: $scope,
                buttons: [
                  { text: $scope.gifts_detail_need_phone_button1,
                      onTap: function() {
                          $scope.phonePopup.close();
                          $timeout(function() {$state.go('app.profile1');}, 100);
                      }},
                      { text: $scope.gifts_detail_need_phone_button2,
                      onTap: function() {
                          $scope.phonePopup.close();
                      }}
                ]
            });

        }

    }

    $scope.qrbonus = function(x, gifttype) {

        var splitx = x.split("&");

        $scope.qrcodeString = x;
        $scope.size = 200;
        $scope.correctionLevel = '';
        $scope.typeNumber = 0;
        $scope.inputMode = '';
        $scope.image = false;
        
        $scope.scantxt = $scope.gifts_detail_waiter_scan_title + splitx[4] + $scope.gifts_detail_waiter_scan_title2;
        if(gifttype == 1) {
            $scope.scantxt = '';
        }

        $scope.QRPopup = $ionicPopup.show({
            title: $scope.scantxt,
            template: '<div class="row"><div class="col" style="text-align: center;"><qr class="qrcodeString" text="qrcodeString" type-number="typeNumber" correction-level="correctionLevel" size="size" input-mode="inputMode" image="image"></qr></div></div>',
            scope: $scope,
            buttons: [
                { text: $scope.gifts_detail_waiter_scan_button,
                    onTap: function(e) {
                        $scope.QRPopup.close();
                    }}
            ]
        });

    }

    $scope.htmlEntities = function(x) {
        return HtmlEnt.decodeEntities(x);
    }

})

.controller('StoreCatCtrl', function($rootScope, $scope, $window, $stateParams, $ionicPlatform, $state, $timeout, $cordovaFile, $cordovaFileTransfer, $localStorage, $cordovaSQLite, $ionicScrollDelegate, $ionicNavBarDelegate, $translate, $ionicModal, ionicMaterialInk, ionicMaterialMotion, Store, HtmlEnt) {

    $translate(['store_code_terms_top', 'store_code_terms_body', 'store_code_terms_button']).then(function(res) {

        $scope.store_code_terms_top = res.store_code_terms_top;
        $scope.store_code_terms_body = res.store_code_terms_body;
        $scope.store_code_terms_button = res.store_code_terms_button;
        
    });

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
        $ionicNavBarDelegate.align('center');
    });

    $timeout(function() {

       $ionicScrollDelegate.resize();

    }, 1000);

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    if($stateParams.catIngr) {

        // alert($stateParams.catIngr)

        Store.getcats($stateParams.catIngr).then(function(data) {
            $scope.allCats = data;
            // alert(JSON.stringify(data))
        });

    }
    else {

        Store.getcats().then(function(data) {
            $scope.allCats = data;
            // alert(JSON.stringify(data))
        });

    }

    $scope.dev_height = $window.innerHeight / 3.8;

    $scope.card_height = $window.innerWidth / 3.5;

    $scope.myStyle = {'height': $scope.dev_height+'px'};

    // Store.sumcheck().then(function(data) {
    //     $scope.order = data;
    // });

    $scope.pieces = function(pcsid) {
        return Store.getpcs(pcsid);
    }

    $scope.catpcs = function(cats) {
        return Store.getcatpcs(cats);
    }

    $scope.recalc = function(x, y) {
        // if($scope.delivery.indexOf($stateParams.catId) == -1) {

        //     var discount = 1;

        //     if(parseInt(y) > 0) {
        //         discount = parseInt(y)/100;
        //     }

        //     return (x*discount/100).toFixed(2);
        // }
        // else if($scope.delivery.indexOf($stateParams.catId) != -1) {
            return (x/100).toFixed(2);
        // }
    }

    $scope.htmlEntities = function(x) {
        return HtmlEnt.decodeEntities(x);
    }

})

.controller('StoreCtrl', function($rootScope, $scope, $window, $stateParams, $ionicPlatform, $state, $timeout, $cordovaFile, $cordovaFileTransfer, $localStorage, $cordovaSQLite, $ionicScrollDelegate, $ionicNavBarDelegate, $translate, $ionicModal, ionicMaterialInk, ionicMaterialMotion, Store, HtmlEnt) {

    $scope.$on('$ionicView.enter', function(){
        $timeout(function() {
            $ionicNavBarDelegate.align('center');
        });
    });

    $translate(['store_code_terms_top', 'store_code_terms_body', 'store_code_terms_button']).then(function(res) {

        $scope.store_code_terms_top = res.store_code_terms_top;
        $scope.store_code_terms_body = res.store_code_terms_body;
        $scope.store_code_terms_button = res.store_code_terms_button;
        
    });

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
        $ionicNavBarDelegate.align('center');
    });

    $timeout(function() {

       $ionicScrollDelegate.resize();

    }, 1000);

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.allStore = [];

    $scope.getcat = '';

    $scope.delivery = Store.getdeliverycats();

    $scope.storeLoaded = false;

    if($stateParams.catId) {
        // console.log('CAT ID =========================> '+JSON.stringify($stateParams.catId))
        // CAT SELECTED
        Store.selcat($stateParams.catId).then(function(data) {
            $scope.allStore = data;
            $scope.storeLoaded = true;
            // console.log('ALL STORE =========================> '+JSON.stringify($scope.allStore))
            $timeout(function() {
                var elements = document.getElementsByClassName("equalex");
                var comparor = elements.length / 2;
                if (elements.length % 2 != 0) {
                   comparor = (elements.length+1) / 2;
                }
                for (var i=0; i<elements.length; i++) {
                    var ix = i+comparor;
                    if(elements[i] && elements[ix]) {
                        if (elements[i].offsetHeight > elements[ix].offsetHeight) {
                            elements[ix].style.height = elements[i].offsetHeight+'px';
                        }
                        else if (elements[i].offsetHeight < elements[ix].offsetHeight) {
                            elements[i].style.height = elements[ix].offsetHeight+'px';
                        }
                    }
                }
            }, 100);
        });

        // CATNAME
        Store.getcat($stateParams.catId).then(function(data) {
            $scope.getcat = data;
        });

    }

    $scope.dev_height = $window.innerHeight / 3.8;

    $scope.myStyle = {'height': $scope.dev_height+'px'};

    $scope.pieces = function(pcsid) {
        return Store.getpcs(pcsid);
    }

    $scope.catpcs = function(cats) {
        return Store.getcatpcs(cats);
    }

    $scope.recalc = function(x, y) {
        if($scope.delivery.indexOf($stateParams.catId) == -1) {

            var discount = 1;

            if(parseInt(y) > 0) {
                discount = parseInt(y)/100;
            }

            return (x*discount/100).toFixed(2);
        }
        else if($scope.delivery.indexOf($stateParams.catId) != -1) {
            return (x/100).toFixed(2);
        }
    }

    $scope.htmlEntities = function(x) {
        return HtmlEnt.decodeEntities(x);
    }

})

.controller('StoreDetailCtrl', function($rootScope, $scope, $stateParams, $ionicPlatform, $state, $timeout, $cordovaFile, $cordovaFileTransfer, $localStorage, $window, $cordovaSQLite, $ionicScrollDelegate, $ionicPosition, $ionicNavBarDelegate, $translate, $ionicPopup, ionicMaterialMotion, ionicMaterialInk, Store, HtmlEnt, Inst) {

    $translate(['order_cntr_pop_nottake_ord_tit', 'order_cntr_pop_nottake_ord_tmp', 'order_cntr_pop_nottake_ord_btn1']).then(function(res) {

        $scope.order_cntr_pop_nottake_ord_tit = res.order_cntr_pop_nottake_ord_tit;
        $scope.order_cntr_pop_nottake_ord_tmp = res.order_cntr_pop_nottake_ord_tmp;
        $scope.order_cntr_pop_nottake_ord_btn1 = res.order_cntr_pop_nottake_ord_btn1;

    });

    var buyitem = '';

    $scope.delivery = Store.getdeliverycats();

    $scope.$on('$ionicView.enter', function() {

        if(Inst.check()) {
            $scope.inst = Inst.getinstold();
        }
        else {
            Inst.getinstnew().then(function(data) {
                $scope.inst = data;
            }, function() {});
        }

        $timeout(function() {
            $ionicNavBarDelegate.align('center');
        });
        
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.storeid = $stateParams.storeId;

    $scope.storeitem = Store.getstore($stateParams.storeId);

    $scope.storeitemdiscount = 1;
    if($scope.storeitem.menue_discount != 0) {
        $scope.storeitemdiscount = parseInt($scope.storeitem.menue_discount)/100;
    }

    $scope.outside = false;

    if($scope.delivery.indexOf($scope.storeitem.menue_cat) != '-1' && $scope.storeitem.menue_xid != '0') {$scope.outside = true;} else {$scope.outside = false;}

    $timeout(function() {

        buyitem = {
            id: $stateParams.storeId,
            xid: $scope.storeitem.menue_xid,
            cat: $scope.storeitem.menue_cat,
            name: $scope.storeitem.menue_name,
            desc: $scope.storeitem.menue_desc,
            size: $scope.storeitem.menue_size,
            cost: $scope.delivery.indexOf($scope.storeitem.menue_cat) != '-1' ? ($scope.storeitem.menue_cost / 100).toFixed(2) : ($scope.storeitem.menue_cost*$scope.storeitemdiscount/100).toFixed(2),
            ingr: $scope.storeitem.menue_ingr,
            weigh: $scope.storeitem.menue_weight,
            pic: $scope.storeitem.menue_pic,
            inst: $scope.storeitem.menue_institution,
            quantity: 1
        };

    }, 200);

    $scope.recalc = function(x) {
        return ((x/100).toFixed(2));
    }

    // $scope.storesizes = Store.allsizes();

    // $scope.currsizeparam = $stateParams.storeSize - 1;
    // $scope.currsize = $scope.storesizes[$scope.currsizeparam].siz;
    // $scope.currweigh = $scope.storeitem.weigh[$scope.currsizeparam];
    // $scope.currprice = $scope.storeitem.cost[$scope.currsizeparam];

    // $scope.switchsize = function() {
    //     if($scope.currsizeparam == 3) {
    //         $scope.currsizeparam = 0;
    //         $scope.currsize = $scope.storesizes[$scope.currsizeparam].siz;
    //         $scope.currweigh = $scope.storeitem.weigh[$scope.currsizeparam];
    //         $scope.currprice = $scope.storeitem.cost[$scope.currsizeparam];

    //         buyitem = {
    //             id: $stateParams.storeId,
    //             cat: $scope.storeitem.cat,
    //             name: $scope.storeitem.name,
    //             th: $scope.storeitem.th,
    //             pic: $scope.storeitem.pic,
    //             desc: $scope.storeitem.desc,
    //             ingr: $scope.storeitem.ingr,
    //             cost: $scope.currprice,
    //             weigh: $scope.currweigh
    //         };
    //     }
    //     else {
    //         $scope.currsizeparam = $scope.currsizeparam + 1;
    //         $scope.currsize = $scope.storesizes[$scope.currsizeparam].siz;
    //         $scope.currweigh = $scope.storeitem.weigh[$scope.currsizeparam];
    //         $scope.currprice = $scope.storeitem.cost[$scope.currsizeparam];

    //         buyitem = {
    //             id: $stateParams.storeId,
    //             cat: $scope.storeitem.cat,
    //             name: $scope.storeitem.name,
    //             th: $scope.storeitem.th,
    //             pic: $scope.storeitem.pic,
    //             desc: $scope.storeitem.desc,
    //             ingr: $scope.storeitem.ingr,
    //             cost: $scope.currprice,
    //             weigh: $scope.currweigh
    //         };
    //     }
    // }

    // $scope.selectpic = $scope.storeitem.menue_pic;

    // $scope.getcat = Store.getcat($scope.storeitem.menue_cat);

    // $scope.getcatpath = $scope.getcat.path;

    $scope.dev_height = $window.innerHeight / 2.5;

    $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center'};

    $scope.acceptStyle = function() {
        $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};

        $ionicScrollDelegate.resize();

    }

    $scope.pieces = Store.getpcs($stateParams.storeId);

    $scope.inerheight = $window.innerHeight;
    $scope.inerwidth = $window.innerWidth;

    // var focusElementA = document.querySelector('#fuckoff');
    // var angularElementA = angular.element(focusElementA);
    // var offsetElementA = $ionicPosition.offset(angularElementA);

    // var btnwidth = offsetElementA.width;
    // var btnheight = offsetElementA.height;
    // var btntop = offsetElementA.top;
    // var btnleft = offsetElementA.left;

    // $scope.getcart = Store.getcart();
    
    $scope.addcart = function(idx) {

        if($scope.inst[0].office_orders != '1') {
            var errPopup = $ionicPopup.alert({
                title: $scope.order_cntr_pop_nottake_ord_tit,
                template: $scope.order_cntr_pop_nottake_ord_tmp,
                scope: $scope,
                buttons: [
                {
                    text: $scope.order_cntr_pop_nottake_ord_btn1,
                    type: 'button-positive',
                    onTap: function(e) {
                        errPopup.close();
                    }
                }
                ]
            });
        }
        else {

            // alert(buyitem.id)

            Store.addtocart(buyitem);

            $scope.pieces = Store.getpcs(buyitem.id);

            // $scope.getcart = Store.getcart();

            // var cart = document.getElementById('shopping-cart');
            var btnclicked = document.getElementById('addcartbtn');
            var btnclone = btnclicked.cloneNode(true);
                btnclone.style.top = btnclicked.getBoundingClientRect().top;
                btnclone.style.left = btnclicked.getBoundingClientRect().left;
                btnclone.style.position = 'absolute';
                // btnclone.style.border = '2px solid #888';
                // btnclone.style.backgroundColor = '#fff';
                btnclone.style.zIndex = 1000;
                document.body.appendChild(btnclone);

            var postop = btnclicked.getBoundingClientRect().top;
            var posleft = btnclicked.getBoundingClientRect().left;

            // alert('top: '+postop + ' left: '+posleft);

            // var endpostop = cart.getBoundingClientRect().top;
            // var endposleft = cart.getBoundingClientRect().left;
            
            var btnwidth = btnclone.offsetWidth;
            var btnheight = btnclone.offsetHeight;var endpostop = 20;
            var endposleft = $window.innerWidth - 50;
            var endposleft2 = 50;
            var fontsise = 120;
            var postopstep = (postop - endpostop) / 10;
            var posleftstep = (endposleft - posleft) / 10;
            var posleftstep2 = (endposleft2 - posleft) / 10;
            var widthstep = btnwidth / 10;
            var heightstep = btnheight / 10;

            var theint = setInterval(function() {

                var x = 0;
                x++;
                if (x == 50) {
                    clearInterval(theint);
                    document.body.removeChild(btnclone);
                } else {

                    if($scope.delivery.indexOf($scope.storeitem.menue_cat) == '-1') {
                        postop = postop - postopstep;
                        posleft = posleft + posleftstep2;
                        // btnwidth = btnwidth - widthstep;
                        // btnheight = btnheight - heightstep;
                        fontsise = fontsise - 10;
                        btnclone.style.top = postop + 'px'; 
                        btnclone.style.left = posleft + 'px';
                        // btnclone.style.width = btnwidth + 'px';
                        // btnclone.style.height = btnheight + 'px';
                        // btnclone.getElementsByTagName('button')[0].style.fontSize = fontsise + '%';
                        // btnclone.getElementsByTagName('button')[1].style.fontSize = fontsise + '%';
                        // btnclone.getElementsByTagName('button')[2].style.fontSize = fontsise + '%';

                        btnclone.style.fontSize = fontsise + '%';
                    }
                    else if($scope.delivery.indexOf($scope.storeitem.menue_cat) != '-1') {
                        postop = postop - postopstep;
                        posleft = posleft + posleftstep;
                        // btnwidth = btnwidth - widthstep;
                        // btnheight = btnheight - heightstep;
                        fontsise = fontsise - 10;
                        btnclone.style.top = postop + 'px'; 
                        btnclone.style.left = posleft + 'px';
                        // btnclone.style.width = btnwidth + 'px';
                        // btnclone.style.height = btnheight + 'px';
                        // btnclone.getElementsByTagName('button')[0].style.fontSize = fontsise + '%';
                        // btnclone.getElementsByTagName('button')[1].style.fontSize = fontsise + '%';
                        // btnclone.getElementsByTagName('button')[2].style.fontSize = fontsise + '%';

                        btnclone.style.fontSize = fontsise + '%';
                    }

                }

            }, 1);
            // console.log(postop + ' ' + posleft + ' ' + endpostop + ' ' + endposleft + ' ' + btnwidth + ' ' + btnheight);

        }

    }

    $scope.subcart = function() {

        if($scope.inst[0].office_orders != '1') {
            var errPopup = $ionicPopup.alert({
                title: $scope.order_cntr_pop_nottake_ord_tit,
                template: $scope.order_cntr_pop_nottake_ord_tmp,
                scope: $scope,
                buttons: [
                {
                    text: $scope.order_cntr_pop_nottake_ord_btn1,
                    type: 'button-positive',
                    onTap: function(e) {
                        errPopup.close();
                    }
                }
                ]
            });
        }
        else {
            Store.subcart(buyitem);
            $scope.pieces = Store.getpcs(buyitem.id);
            // $scope.getcart = Store.getcart();
        }
    }

    $scope.getingrs = function(itm) {
        return Store.getingrdetail(itm);
    }

    $scope.htmlEntities = function(x) {
        return HtmlEnt.decodeEntities(x);
    }

})

.controller('StoreConfirmCtrl', function($rootScope, $scope, $stateParams, $ionicPlatform, $ionicPopup, $state, $timeout, $cordovaFile, $cordovaFileTransfer, $localStorage, $cordovaSQLite, $ionicNavBarDelegate, $translate, $ionicSideMenuDelegate, ionicMaterialInk, ionicMaterialMotion, Store, HtmlEnt, UserData) {

    $translate(['store_confirm_top_takeaway', 'store_confirm_top_order', 'store_confirm_top', 'store_confirm_minsum_title', 'store_confirm_minsum_body', 'store_confirm_cart_empty_title', 'store_confirm_cart_empty_body', 'all_attention', 'all_email_warn', 'all_phone_confirm_warn', 'gifts_detail_need_phone_button1', 'gifts_detail_need_phone_button2', 'all_go_to_email']).then(function(res) {

        $scope.store_confirm_top_takeaway = res.store_confirm_top_takeaway;
        $scope.store_confirm_top_order = res.store_confirm_top_order;
        $scope.store_confirm_top = res.store_confirm_top;
        $scope.store_confirm_minsum_title = res.store_confirm_minsum_title;
        $scope.store_confirm_minsum_body = res.store_confirm_minsum_body;
        $scope.store_confirm_cart_empty_title = res.store_confirm_cart_empty_title;
        $scope.store_confirm_cart_empty_body = res.store_confirm_cart_empty_body;

        $scope.all_attention = res.all_attention;
        $scope.all_email_warn = res.all_email_warn;
        $scope.all_phone_confirm_warn = res.all_phone_confirm_warn;
        $scope.gifts_detail_need_phone_button1 = res.gifts_detail_need_phone_button1;
        $scope.gifts_detail_need_phone_button2 = res.gifts_detail_need_phone_button2;
        $scope.all_go_to_email = res.all_go_to_email;

    });

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
        $ionicSideMenuDelegate.canDragContent(true);
        $timeout(function() {
            $ionicNavBarDelegate.align('center');
        }, 500);
    });

    var buyitem = '';

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.orderType = $stateParams.orderType;

    $scope.order = Store.ordercheck($stateParams.orderType).toFixed(2);

    $scope.carts = Store.cartcheck($stateParams.orderType);

    $scope.delivery = Store.getdeliverycats();

    $scope.myStyle = {'background-repeat': 'no-repeat', 'background-position': 'center center', 'border-radius': '0px', 'background-size': 'cover'};

    $scope.acceptStyle = function() {
        $scope.myStyle = {'background-repeat': 'no-repeat', 'background-position': 'center center', 'border-radius': '0px', 'background-size': 'cover'};
    }

    $scope.getcat = function(cts) {
        Store.getcat(cts);
    }

    $scope.addcart = function(pcs) {
        // var pcsid = pcs.id;
        Store.addtocart(pcs);
        $scope.order = Store.ordercheck($stateParams.orderType).toFixed(2);
        $scope.storeaddrefresh();
        // $scope.pieces(pcsid);
    }

    $scope.addcartadd = function(article) {

        var menuediscount = 1;
        if(article.menue_discount != 0) {
            menuediscount = (article.menue_discount/100);
        }

        buyitem = {
            id: article.menue_id,
            cat: article.menue_cat,
            name: article.menue_name,
            desc: article.menue_desc,
            size: article.menue_size,
            cost: $scope.delivery.indexOf($stateParams.orderType) != '1' ? (article.menue_cost / 100).toFixed(2) : (article.menue_cost*menuediscount/100).toFixed(2),
            ingr: article.menue_ingr,
            weigh: article.menue_weight,
            pic: article.menue_pic,
            inst: article.menue_institution,
            quantity: 1
        };

        // var pcsid = pcs.id;
        $scope.addcart(buyitem);
        // $scope.pieces(pcsid);
    }

    $scope.subcartadd = function(article) {

        var menuediscount = 1;
        if(article.menue_discount != 0) {
            menuediscount = (article.menue_discount/100);
        }

        buyitem = {
            id: article.menue_id,
            cat: article.menue_cat,
            name: article.menue_name,
            desc: article.menue_desc,
            size: article.menue_size,
            cost: $scope.delivery.indexOf($stateParams.orderType) != '1' ? (article.menue_cost / 100).toFixed(2) : (article.menue_cost*menuediscount/100).toFixed(2),
            ingr: article.menue_ingr,
            weigh: article.menue_weight,
            pic: article.menue_pic,
            inst: article.menue_institution,
            quantity: 1
        };

        // var pcsid = pcs.id;
        $scope.subcart(buyitem);
        // $scope.pieces(pcsid);
    }

    $scope.subcart = function(pcs) {
        // var pcsid = pcs.id;
        Store.subcart(pcs);
        $scope.order = Store.ordercheck($stateParams.orderType).toFixed(2);
        $scope.storeaddrefresh();
        // $scope.pieces(pcsid);
    }

    $scope.remfromcart = function(pcsid) {
        Store.rmfromcart(pcsid);
        $scope.order = Store.ordercheck($stateParams.orderType).toFixed(2);
        $scope.carts = Store.cartcheck($stateParams.orderType);
    }

    $scope.getingrs = function(itm) {
        return Store.getingrname(itm);
    }

    $scope.gotoBuy = function() {
        if($stateParams.orderType == 0) {
            if($scope.order > 0) {
                UserData.get().then(function(res) {
                    if(res.user_mob_confirm == 1) {
                        var mailmatch = '[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})';
                        if(res.user_email.match(mailmatch)) {
                            $state.go('app.store-buy', {orderType: $scope.orderType});
                        }
                        else {
                            $scope.emailPopup = $ionicPopup.alert({
                                title: $scope.all_attention,
                                template: $scope.all_email_warn,
                                scope: $scope,
                                buttons: [
                                { text: $scope.all_go_to_email,
                                    onTap: function() {
                                        $scope.emailPopup.close();
                                        $timeout(function() {$state.go('app.profile1');}, 100);
                                    }},
                                    { text: $scope.gifts_detail_need_phone_button2,
                                    onTap: function() {
                                        $scope.emailPopup.close();
                                    }}
                                ]
                            });
                        }
                    }
                    else {

                        $scope.phonePopup = $ionicPopup.show({
                            title: $scope.all_attention,
                            template: $scope.all_phone_confirm_warn,
                            scope: $scope,
                            buttons: [
                            { text: $scope.gifts_detail_need_phone_button1,
                                onTap: function() {
                                    $scope.phonePopup.close();
                                    $timeout(function() {$state.go('app.profile1');}, 100);
                                }},
                                { text: $scope.gifts_detail_need_phone_button2,
                                onTap: function() {
                                    $scope.phonePopup.close();
                                }}
                            ]
                        });
                        
                    }
                });
            }
            else {
                $ionicPopup.alert({
                    title: $scope.store_confirm_minsum_title,
                    template: $scope.store_confirm_minsum_body
                });
            }
        }
        else if($stateParams.orderType == 1) {
            if($scope.order > 0) {
                $state.go('app.store-buy', {orderType: $scope.orderType});
            }
            else {
                $ionicPopup.alert({
                    title: $scope.store_confirm_cart_empty_title,
                    template: $scope.store_confirm_cart_empty_body
                });
            }
        }
        
    }

    $scope.getpics = function(articleid) {
        return Store.getpcs(articleid);
    }

    $scope.freesouces = 0;

    $timeout(function() {

        if($stateParams.orderType == 0) {
            $scope.store_confirm_top_text = $scope.store_confirm_top_order;
        }
        else if($stateParams.orderType == 1) {
            $scope.store_confirm_top_text = $scope.store_confirm_top_takeaway;
        }
        else {
            $scope.store_confirm_top_text = $scope.store_confirm_top;
        }

    }, 200);

    $scope.addStore = '';

    $scope.storeaddrefresh = function() {

        $scope.freesouces = Store.getfree($stateParams.orderType);

    }

    $scope.storeaddrefresh();
    
    if($stateParams.orderType == 0) {
        Store.selfreecat('81').then(function(data) {
            $scope.addStore = data;
        });
    }
    else if ($stateParams.orderType == 1) {
        Store.selfreecat('82').then(function(data) {
            $scope.addStore = data;
        });
    }

    $scope.htmlEntities = function(x) {
        return HtmlEnt.decodeEntities(x);
    }

})

.controller('StoreBuyCtrl', function($rootScope, $scope, $stateParams, $ionicPlatform, $ionicPopup, $state, $timeout, $cordovaFile, $cordovaFileTransfer, $localStorage, $cordovaSQLite, $ionicNavBarDelegate, $translate, $http, $ionicHistory, ionicMaterialInk, ionicMaterialMotion, Store, Inst) {

    $translate(['store_buy_in_card', 'store_buy_out_card', 'org_name', 'store_buy_town', 'store_buy_place_title', 'store_buy_place_body1', 'store_buy_place_body2', 'store_buy_place_body3', 'store_buy_place_button1', 'store_buy_place_button2', 'store_buy_placed_title', 'store_buy_placed_body', 'store_buy_place_error_title', 'store_buy_placed_error_body', 'store_buy_placed_info_title1', 'store_buy_placed_info_body1', 'store_buy_placed_info_body2', 'store_buy_placed_info_body3', 'store_buy_placed_info_body4', 'store_buy_placed_info_body5', 'store_buy_placed_info_body6', 'store_buy_placed_info_body7', 'store_buy_placed_info_body8', 'order_cntr_pop_nottake_ord_tit', 'order_cntr_pop_nottake_ord_tmp', 'order_cntr_pop_nottake_ord_btn1']).then(function(res) {

        $scope.store_buy_in_card = res.store_buy_in_card;
        $scope.store_buy_out_card = res.store_buy_out_card;
        $scope.org_name = res.org_name;
        $scope.store_buy_town = res.store_buy_town;

        $scope.store_buy_place_title = res.store_buy_place_title;
        $scope.store_buy_place_body1 = res.store_buy_place_body1;
        $scope.store_buy_place_body2 = res.store_buy_place_body2;
        $scope.store_buy_place_body3 = res.store_buy_place_body3;
        $scope.store_buy_place_button1 = res.store_buy_place_button1;
        $scope.store_buy_place_button2 = res.store_buy_place_button2;
        $scope.store_buy_placed_title = res.store_buy_placed_title;
        $scope.store_buy_placed_body = res.store_buy_placed_body;
        $scope.store_buy_place_error_title = res.store_buy_place_error_title;
        $scope.store_buy_placed_error_body = res.store_buy_placed_error_body;
        $scope.store_buy_placed_info_title1 = res.store_buy_placed_info_title1;
        $scope.store_buy_placed_info_body1 = res.store_buy_placed_info_body1;
        $scope.store_buy_placed_info_body2 = res.store_buy_placed_info_body2;
        $scope.store_buy_placed_info_body3 = res.store_buy_placed_info_body3;
        $scope.store_buy_placed_info_body4 = res.store_buy_placed_info_body4;
        $scope.store_buy_placed_info_body5 = res.store_buy_placed_info_body5;
        $scope.store_buy_placed_info_body6 = res.store_buy_placed_info_body6;
        $scope.store_buy_placed_info_body7 = res.store_buy_placed_info_body7;
        $scope.store_buy_placed_info_body8 = res.store_buy_placed_info_body8;
        $scope.order_cntr_pop_nottake_ord_tit = res.order_cntr_pop_nottake_ord_tit;
        $scope.order_cntr_pop_nottake_ord_tmp = res.order_cntr_pop_nottake_ord_tmp;
        $scope.order_cntr_pop_nottake_ord_btn1 = res.order_cntr_pop_nottake_ord_btn1;

    });

    $scope.streetNumb = '';

    $scope.orderform_0 = {
        device_id: $rootScope.uuid,
        inst_id: 0,
        newusr: 'purchase',
        userName: '',
        userSurname: '',
        userMiddlename: '',
        organization: $scope.org_name,
        userMobile: '',
        phone2: '',
        town: $scope.store_buy_town,
        userAdress: '',
        house: '',
        building: '',
        entry: '',
        codeentry: '',
        floor: '',
        flat: '',
        userEmail: '',
        userComment: '',
        advert: '',
        d_type: 0,
        cartsum: Store.ordercheck(0),
        wait_time: 122222222,
        p_type: '',
        pay_amount: '',
        mobilePurchase: Store.ordersend(0)
    }

    $scope.orderform_1 = {
        userName: '',
        userSurname: '',
        userMiddlename: '',
        organization: $scope.org_name,
        userMobile: '',
        phone2: '',
        town: $scope.store_buy_town,
        userAdress: '',
        house: '',
        building: '',
        entry: '',
        codeentry: '',
        floor: '',
        flat: '',
        userEmail: '',
        userComment: '',
        advert: '',
        d_type: 1,
        cartsum: Store.ordercheck(1),
        wait_time: 122222222,
        p_type: '',
        pay_amount: '',
        mobilePurchase: Store.ordersend(1)
    }

    $scope.$on('$ionicView.enter', function(event, viewData) {
        
        $scope.btnClick = true;

        if(Inst.check()) {
            $scope.inst = Inst.getinstold();
        }
        else {
            Inst.getinstnew().then(function(data) {
                $scope.inst = data;
            }, function() {});
        }

    });

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');

        $scope.streetNumb = '';

        var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
        $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {

            if(success.rows.item(0).user_name != '0' && success.rows.item(0).user_name != '') {
                $scope.orderform_0.userName = success.rows.item(0).user_name;
                $scope.orderform_1.f_name = success.rows.item(0).user_name;
            }

            if(success.rows.item(0).user_surname != '0' && success.rows.item(0).user_surname != '') {
                $scope.orderform_0.userSurname = success.rows.item(0).user_surname;
                $scope.orderform_1.l_name = success.rows.item(0).user_surname;
            }

            if(success.rows.item(0).user_middlename != '0' && success.rows.item(0).user_middlename != '') {
                $scope.orderform_0.userMiddlename = success.rows.item(0).user_middlename;
                $scope.orderform_1.m_name = success.rows.item(0).user_middlename;
            }

            if(success.rows.item(0).user_email != '0' && success.rows.item(0).user_email != '') {
                $scope.orderform_0.userEmail = success.rows.item(0).user_email;
            }

            if(success.rows.item(0).user_tel != '0' && success.rows.item(0).user_tel != '') {
                $scope.orderform_0.phone2 = success.rows.item(0).user_tel;
            }

            if(success.rows.item(0).user_mob != '0' && success.rows.item(0).user_mob != '') {
                $scope.orderform_0.userMobile = success.rows.item(0).user_mob;
                $scope.orderform_1.phone1 = success.rows.item(0).user_mob;
            }

            $scope.orderform_0.inst_id = success.rows.item(0).user_institution;

        }, function() {});

      }, 500);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.orderType = $stateParams.orderType;

    $scope.order = Store.ordercheck($stateParams.orderType).toFixed(2);

    $scope.rounding = Store.ordercheck($stateParams.orderType).toFixed(0);

    // $scope.streets = Store.getstreets();

    // $scope.streetNums = Store.getstreetnum(0);

    // $scope.getNums = function(x) {
    //     $scope.streetNums = Store.getstreetnum(x);
    //     var elt = document.getElementById('streetsel');
    //     $scope.orderform_0.street = elt.options[elt.selectedIndex].text;
    // }

    $scope.proofMob = function() {
        $scope.orderform_0.phone1 = $scope.orderform_0.phone1.replace(/[^0-9]+/g, "").slice(0,12);
    }

    $scope.proofMob2 = function() {
        $scope.orderform_1.phone1 = $scope.orderform_1.phone1.replace(/[^0-9]+/g, "").slice(0,12);
    }

    $scope.btnClick = true;

    $scope.buy = function(orderform) {
        if($scope.btnClick) {
            $scope.btnClick = false;
            if($scope.inst[0].office_orders != '1') {
                $scope.btnClick = true;
                var errPopup = $ionicPopup.alert({
                    title: $scope.order_cntr_pop_nottake_ord_tit,
                    template: $scope.order_cntr_pop_nottake_ord_tmp,
                    scope: $scope,
                    buttons: [
                    {
                        text: $scope.order_cntr_pop_nottake_ord_btn1,
                        type: 'button-positive',
                        onTap: function(e) {
                            errPopup.close();
                        }
                    }
                    ]
                });
            }
            else {
                if(orderform.userMobile && orderform.userMobile.toString().length >= 10) {

                    $scope.notphone = false;

                    if(orderform.userName) {

                        $scope.notphone = false;
                        $scope.notname = false;

                        if(orderform.userAdress) {

                            $scope.notphone = false;
                            $scope.notname = false;
                            $scope.notstreet = false;

                            if(orderform.userEmail && $scope.orderType == '0') {

                                // $scope.notphone = false;
                                // $scope.notname = false;
                                // $scope.notstreet = false;
                                // $scope.notstreetnum = false;

                                // if(orderform.p_type) {

                                    // $scope.notphone = false;
                                    // $scope.notname = false;
                                    // $scope.notstreet = false;
                                    // $scope.notstreetnum = false;
                                    // $scope.notptype = false;

                                    // if(orderform.pay_amount || orderform.p_type == $scope.store_buy_in_card || orderform.p_type == $scope.store_buy_out_card) {

                                            $scope.notphone = false;
                                            $scope.notname = false;
                                            $scope.notstreet = false;
                                            $scope.notstreetnum = false;
                                            $scope.notptype = false;
                                            $scope.notamount = false;
                                            $scope.notemail = false;

                                            $scope.buyPopup = $ionicPopup.show({
                                                title: $scope.store_buy_place_title,
                                                template: '<p class="padding">'+$scope.store_buy_place_body1+(orderform.cartsum).toFixed(2)+$scope.store_buy_place_body2+'</p><p>'+$scope.store_buy_place_body3+'</p>',
                                                scope: $scope,
                                                buttons: [
                                                    { text: $scope.store_buy_place_button1,
                                                        onTap: function() {
                                                            $scope.buyPopup.close();
                                                            $scope.btnClick = true;
                                                        }},
                                                    { text: $scope.store_buy_place_button2,
                                                        onTap: function(e) {
                                                            $scope.buyPopup.close();

                                                            $timeout(function() {

                                                                var jsonedstr = JSON.stringify(orderform);

                                                                var link = "http://www.olegtronics.com/appscripts/getappdata.php";

                                                                // alert(jsonedstr)

                                                                $http.post(link, jsonedstr).then(function(suc) {

                                                                    $ionicHistory.nextViewOptions({
                                                                        disableAnimate: true,
                                                                        disableBack: true
                                                                    });

                                                                    $ionicPopup.alert({
                                                                        title: $scope.store_buy_placed_title,
                                                                        template: $scope.store_buy_placed_body
                                                                    });

                                                                    Store.rmcarttype($scope.orderType);

                                                                    $timeout(function() {$state.go('app.main');}, 100);

                                                                    $scope.btnClick = true;

                                                                    // alert('success: '+JSON.stringify(suc.data));

                                                                }, 
                                                                function(er) {
                                                                    $scope.btnClick = true;

                                                                    $ionicPopup.alert({
                                                                        title: $scope.store_buy_place_error_title,
                                                                        template: $scope.store_buy_placed_error_body
                                                                    });
                                                                });

                                                            }, 500);
                                                        }}
                                                    ]
                                            });

                                        }
                                        else {
                                            $ionicPopup.alert({
                                                title: $scope.store_buy_placed_info_title1,
                                                template: $scope.store_buy_placed_info_body1
                                            });
                                            $scope.btnClick = true;
                                            $scope.notemail = true;
                                            // $scope.notamount = true;
                                        }


                                // }
                                // else {
                                //     $ionicPopup.alert({
                                //         title: $scope.store_buy_placed_info_title1,
                                //         template: $scope.store_buy_placed_info_body2
                                //     });

                                //     $scope.notptype = true;
                                // }

                            // }
                            // else {
                            //     $ionicPopup.alert({
                            //         title: $scope.store_buy_placed_info_title1,
                            //         template: $scope.store_buy_placed_info_body3
                            //     });

                            //     $scope.notstreetnum = true;
                            // }
                        }
                        else {
                            $ionicPopup.alert({
                                title: $scope.store_buy_placed_info_title1,
                                template: $scope.store_buy_placed_info_body4
                            });
                            $scope.btnClick = true;
                            $scope.notstreet = true;
                        }
                    }
                    else {
                        $ionicPopup.alert({
                            title: $scope.store_buy_placed_info_title1,
                            template: $scope.store_buy_placed_info_body5
                        });
                        $scope.btnClick = true;
                        $scope.notname = true;
                    }
                }
                else {
                    $ionicPopup.alert({
                        title: $scope.store_buy_placed_info_title1,
                        template: $scope.store_buy_placed_info_body6+"<br/>"+$scope.store_buy_placed_info_body7+"<br/>"+$scope.store_buy_placed_info_body8
                    });
                    $scope.btnClick = true;
                    $scope.notphone = true;
                }
            }
        }
    }

    $scope.onlyNumbers = /^[0-9]+$/;

})

.controller('NewsCtrl', function($rootScope, $scope, $window, $ionicPlatform, $state, $timeout, $cordovaFile, $cordovaFileTransfer, $localStorage, $cordovaSQLite, $ionicScrollDelegate, $ionicNavBarDelegate, $translate, ionicMaterialInk, ionicMaterialMotion, HtmlEnt) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 500);
    });

    // $timeout(function() {

    //     var maxHeight = 0;
    //     var elements = document.getElementsByClassName("equalex");
    //     for (var i=0; i<elements.length; i++) {
    //         if (elements[i].offsetHeight > maxHeight) { maxHeight = elements[i].offsetHeight; }
    //     }
    //     $scope.heightDiv = {'height': maxHeight+'px'};

    //    $ionicScrollDelegate.resize();
    // }, 1000);

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    // $scope.allGifts = Gifts.all();

    $scope.dev_height = $window.innerHeight / 3;

    $timeout(function() {
        $scope.myStyle = {'height': document.getElementsByClassName("equalex")[0].offsetWidth+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};
    }, 500);

    $scope.gifts = [];

    $scope.news = [];

    var queryNews = "SELECT * FROM news WHERE news_id != ? AND news_state = '1' AND news_del = '0' ORDER BY news_when DESC";
    $cordovaSQLite.execute($rootScope.db, queryNews, [0]).then(function(suc) {
      if(suc.rows.length > 0) {

        $scope.newsshow = true;

        for(var i = 0; i < suc.rows.length;i++ ) {
            $scope.news.push(suc.rows.item(i));

            if(i == suc.rows.length-1) {
                $timeout(function() {
                    var elements = document.getElementsByClassName("equalex");
                    var comparor = elements.length / 2;
                    if (elements.length % 2 != 0) {
                       comparor = (elements.length+1) / 2;
                    }
                    for (var i=0; i<elements.length; i++) {
                        var ix = i+comparor;
                        if(elements[i] && elements[ix]) {
                            if (elements[i].offsetHeight > elements[ix].offsetHeight) {
                                elements[ix].style.height = elements[i].offsetHeight+'px';
                            }
                            else if (elements[i].offsetHeight < elements[ix].offsetHeight) {
                                elements[i].style.height = elements[ix].offsetHeight+'px';
                            }
                        }
                    }
                }, 100);
            }

        }
        // $ionicSlideBoxDelegate.update();

      }
    }, function() {});

    $scope.htmlEntities = function(x) {
        return HtmlEnt.decodeEntities(x);
    }

})

.controller('NewsDetailCtrl', function($rootScope, $scope, $stateParams, $ionicPlatform, $ionicPopup, $state, $timeout, $cordovaFile, $cordovaFileTransfer, $localStorage, $window, $ionicScrollDelegate, $cordovaSQLite, $ionicNavBarDelegate, $translate, ionicMaterialMotion, ionicMaterialInk, HtmlEnt) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 500);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.dev_height = $window.innerHeight / 2;

    $scope.news = [];

    var queryNewsSel = "SELECT * FROM news WHERE news_id = ? AND news_del = '0'";
    $cordovaSQLite.execute($rootScope.db, queryNewsSel, [$stateParams.newsId]).then(function(success) {

      if(success.rows.length > 0) {

        $scope.news = success.rows.item(0);

      }

    }, function() {});

    $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};

    $scope.acceptStyle = function() {
        $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};

        $ionicScrollDelegate.resize();
    }

    $scope.mapHeight = {'min-height': $scope.dev_height+'px', 'width': '100%'};

    $scope.htmlEntities = function(x) {
        return HtmlEnt.decodeEntities(x);
    }

})

.controller('ConstructorCtrl', function($rootScope, $scope, $stateParams, $ionicPlatform, $state, $timeout, $cordovaFile, $cordovaFileTransfer, $localStorage, $window, $cordovaSQLite, $ionicSlideBoxDelegate, $ionicNavBarDelegate, $translate, ionicMaterialMotion, ionicMaterialInk, Store) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 500);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    // if($stateParams.constId != 0) {
    //     console.log(constId)
    // }

    $scope.next = function() {
        $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function() {
        $ionicSlideBoxDelegate.previous();
    };

    $scope.viewtitle = "Размер пиццы";

    $scope.slideHasChanged = function(index) {

        $scope.slideIndex = index;

        switch(index) {
            case 0:
                $scope.viewtitle = "Размер пиццы";
                break;
            case 1:
                $scope.viewtitle = "Сыры";
                break;
            case 2:
                $scope.viewtitle = "Соусы";
                break;
            case 3:
                $scope.viewtitle = "Мясо";
                break;
            case 4:
                $scope.viewtitle = "Овощи";
                break;
        }
    }

    $scope.dev_height = $window.innerHeight / 3;

    $scope.constr_div4 = $window.innerWidth * 0.75;
    $scope.constr_div3 = $scope.constr_div4 * 0.75;
    $scope.constr_div2 = $scope.constr_div3 * 0.75;
    $scope.constr_div1 = $scope.constr_div2 * 0.75;

    $scope.myStyle = {'height': $scope.dev_height+'px'};

    Store.sumcheck().then(function(data) {
        $scope.order = data;
    });

    $scope.storesizes = Store.allsizes();
    $scope.currsizename = $scope.storesizes[0].name;
    $scope.ings = Store.allingredients();
    $scope.ingrcheese = Store.getingrcat(1);
    $scope.ingrsouce = Store.getingrcat(2);
    $scope.ingrmeat = Store.getingrcat(3);
    $scope.ingrvegat = Store.getingrcat(4);

    $scope.size26 = false;
    $scope.size31 = false;
    $scope.size45 = false;

    $scope.pastecost = 30000;

    $scope.currsize = $scope.storesizes[0].siz;

    $scope.switchsize = function() {
        if($scope.size45) {
            $scope.size26 = false;
            $scope.size31 = false;
            $scope.size45 = false;
            $scope.currsize = 18;
            $scope.currsizeparam = 0;
            $scope.currsizename = $scope.storesizes[0].name;
            $scope.pastecost = 30000;
            $scope.currprice();
        }
        else if($scope.size31) {
            $scope.size26 = true;
            $scope.size31 = true;
            $scope.size45 = true;
            $scope.currsize = 45;
            $scope.currsizename = $scope.storesizes[3].name;
            $scope.pastecost = 70000;
            $scope.currprice();
        }
        else if($scope.size26) {
            $scope.size26 = true;
            $scope.size31 = true;
            $scope.size45 = false;
            $scope.currsize = 31;
            $scope.currsizename = $scope.storesizes[2].name;
            $scope.pastecost = 60000;
            $scope.currprice();
        } 
        else if(!$scope.size26) {
            $scope.size26 = true;
            $scope.size31 = false;
            $scope.size45 = false;
            $scope.currsize = 26;
            $scope.currsizename = $scope.storesizes[1].name;
            $scope.pastecost = 50000;
            $scope.currprice();
        }
    }

    $scope.addingred = function(x) {
            Store.addingr(x);
            $scope.pieces(x);
            $scope.currprice();
            $scope.getingrs();
    }

    $scope.remingred = function(x) {
            Store.rmingr(x);
            $scope.pieces(x);
            $scope.currprice();
            $scope.getingrs();
    }

    $scope.pieces = function(pcsid) {
        return Store.getingrpcs(pcsid);
    }

    $scope.ingrcost = function() {
        return Store.ingredientscost();
    }

    $scope.getingrs = function() {
        return Store.getingrnameconst();
    }

    $scope.currprice = function() {
        var sumprice = $scope.pastecost + $scope.ingrcost();
        return sumprice;
    }

    $scope.subcart = function() {
        Store.rmallingr();
        $scope.size26 = false;
        $scope.size31 = false;
        $scope.size45 = false;
        $scope.currsize = 18;
        $scope.currsizeparam = 0;
        $scope.currsizename = $scope.storesizes[0].name;
        $scope.pastecost = 30000;

        $scope.currprice();
    }

    $scope.addcart = function() {
        Store.addingrcart($scope.currprice());
        Store.sumcheck().then(function(data) {
        $scope.order = data;
    });
    }

})

.controller('AsksCtrl', function($rootScope, $scope, $stateParams, $state, $ionicPlatform, $timeout, $ionicPopup, $ionicHistory, $translate, Survey, UserData, HtmlEnt) {

    $translate(['org_name', 'org_website', 'main_block_title', 'main_block_body', 'main_block_button', 'main_phone_required_title', 'main_phone_required_body', 'main_phone_required_button1', 'main_phone_required_button2', 'main_ask_button1', 'main_ask_button2', 'qrcode_review_title', 'qrcode_review_body', 'qrcode_warn_title', 'qrcode_warn_body', 'qrcode_review_pic', 'qrcode_review_button1', 'qrcode_review_button2', 'qrcode_review_location', 'qrcode_review_visit_title', 'qrcode_review_visit_choice', 'qrcode_review_visit_comment', 'qrcode_review_visit_button1', 'qrcode_review_visit_button2', 'qrcode_review_visit_pic_button1', 'qrcode_review_visit_pic_button2', 'internet_connection_title', 'internet_connection_body', 'all_attention', 'all_email_warn', 'all_phone_confirm_warn', 'gifts_detail_need_phone_button1', 'gifts_detail_need_phone_button2', 'all_go_to_email']).then(function(res) {

        $scope.org_name = res.org_name;
        $scope.org_website = res.org_website;

        $scope.main_block_title = res.main_block_title;
        $scope.main_block_body = res.main_block_body;
        $scope.main_block_button = res.main_block_button;
        $scope.main_phone_required_title = res.main_phone_required_title;
        $scope.main_phone_required_body = res.main_phone_required_body;
        $scope.main_phone_required_button1 = res.main_phone_required_button1;
        $scope.main_phone_required_button2 = res.main_phone_required_button2;
        $scope.main_ask_button1 = res.main_ask_button1;
        $scope.main_ask_button2 = res.main_ask_button2;

        $scope.qrcode_review_title = res.qrcode_review_title;
        $scope.qrcode_review_body = res.qrcode_review_body;
        $scope.qrcode_warn_title = res.qrcode_warn_title;
        $scope.qrcode_warn_body = res.qrcode_warn_body;
        $scope.qrcode_review_pic = res.qrcode_review_pic;
        $scope.qrcode_review_button1 = res.qrcode_review_button1;
        $scope.qrcode_review_button2 = res.qrcode_review_button2;
        $scope.qrcode_review_location = res.qrcode_review_location;
        $scope.qrcode_review_visit_title = res.qrcode_review_visit_title;
        $scope.qrcode_review_visit_choice = res.qrcode_review_visit_choice;
        $scope.qrcode_review_visit_comment = res.qrcode_review_visit_comment;
        $scope.qrcode_review_visit_button1 = res.qrcode_review_visit_button1;
        $scope.qrcode_review_visit_button2 = res.qrcode_review_visit_button2;
        $scope.qrcode_review_visit_pic_button1 = res.qrcode_review_visit_pic_button1;
        $scope.qrcode_review_visit_pic_button2 = res.qrcode_review_visit_pic_button2;
        $scope.internet_connection_title = res.internet_connection_title;
        $scope.internet_connection_body = res.internet_connection_body;

        $scope.all_attention = res.all_attention;
        $scope.all_email_warn = res.all_email_warn;
        $scope.all_phone_confirm_warn = res.all_phone_confirm_warn;
        $scope.gifts_detail_need_phone_button1 = res.gifts_detail_need_phone_button1;
        $scope.gifts_detail_need_phone_button2 = res.gifts_detail_need_phone_button2;
        $scope.all_go_to_email = res.all_go_to_email;

        // $scope.languages = LanguageFac.all();
        // $scope.genders = [{id:1, name: $scope.male_mes}, {id:2, name: $scope.female_mes}];
        // $scope.ages = AgeFac.all();

    });

    $scope.htmlEntities = function(x) {
        return HtmlEnt.decodeEntities(x);
    }
    
    $scope.$on('$ionicView.enter', function (event, viewData) {

        Survey.check().then(function(data) {
            
            $scope.surveys = data;

        }, function() {});

        $scope.answers = {
            questionId: '',
            answer: ''
        }
    
    });

    $scope.sendAnswer = function() {

        UserData.get().then(function(res) {

            if(res.user_work_pos == '2' || res.user_work_pos == '3' || res.user_work_pos == '4') {

                $scope.forbidPopup = $ionicPopup.show({
                    title: $scope.main_block_title,
                    template: $scope.main_block_body,
                    scope: $scope,
                    buttons: [
                    { text: $scope.main_block_button,
                        onTap: function() {
                            $scope.forbidPopup.close();
                        }}
                    ]
                });

            }
            else {

                if(res.user_mob_confirm == 1) {
                    var mailmatch = '[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})';
                    if(res.user_email.match(mailmatch)) {

                        if($scope.answers.answer) {

                            Survey.answer($scope.answers.questionId, $scope.answers.answer);

                            $timeout(function() {

                                var myPopup = $ionicPopup.alert({
                                    title: 'Благодарим',
                                    template: 'Спасибо за ответы!',
                                    scope: $scope,
                                    buttons: [
                                        {
                                            text: '<b>закрыть</b>',
                                            type: 'positive',
                                            onTap: function(e) {
                                                myPopup.close();
                                            }
                                        }
                                    ]
                                });

                                $ionicHistory.nextViewOptions({
                                    disableAnimate: true,
                                    disableBack: true
                                });
                                $state.go('app.main');

                            }, 500);

                        }

                    }
                    else {
                        $scope.emailPopup = $ionicPopup.alert({
                            title: $scope.all_attention,
                            template: $scope.all_email_warn,
                            scope: $scope,
                            buttons: [
                            { text: $scope.all_go_to_email,
                                onTap: function() {
                                    $scope.emailPopup.close();
                                    $timeout(function() {$state.go('app.profile1');}, 100);
                                }},
                                { text: $scope.gifts_detail_need_phone_button2,
                                onTap: function() {
                                    $scope.emailPopup.close();
                                }}
                            ]
                        });
                    }
                }
                else {

                    $scope.phonePopup = $ionicPopup.show({
                        title: $scope.all_attention,
                        template: $scope.all_phone_confirm_warn,
                        scope: $scope,
                        buttons: [
                        { text: $scope.gifts_detail_need_phone_button1,
                            onTap: function() {
                                $scope.phonePopup.close();
                                $timeout(function() {$state.go('app.profile1');}, 100);
                            }},
                            { text: $scope.gifts_detail_need_phone_button2,
                            onTap: function() {
                                $scope.phonePopup.close();
                            }}
                        ]
                    });

                }

            }

        });

    }

})

.controller('TermsCtrl', function($rootScope, $scope, $state, $timeout, $ionicPlatform, $window, $ionicModal, $ionicSideMenuDelegate, $cordovaSQLite, $ionicNavBarDelegate, $translate, ionicMaterialMotion, ionicMaterialInk, $cordovaSocialSharing) {

    $translate(['org_name', 'org_website', 'terms_delivery_body1', 'terms_delivery_body2', 'terms_delivery_body3', 'terms_delivery_body4', 'terms_delivery_body5', 'terms_delivery_body6', 'terms_delivery_body7', 'terms_delivery_body8', 'terms_loyality_body1', 'terms_loyality_body2', 'terms_loyality_body3', 'terms_loyality_body4', 'terms_loyality_body5', 'terms_loyality_body6', 'terms_loyality_body9', 'terms_loyality_body11', 'terms_loyality_body12', 'terms_loyality_body13', 'terms_loyality_body14', 'terms_install_and_get1', 'terms_install_and_get2']).then(function(res) {

        $scope.org_name = res.org_name;
        $scope.org_website = res.org_website;

        $scope.terms_delivery_body1 = res.terms_delivery_body1;
        $scope.terms_delivery_body2 = res.terms_delivery_body2;
        $scope.terms_delivery_body3 = res.terms_delivery_body3;
        $scope.terms_delivery_body4 = res.terms_delivery_body4;
        $scope.terms_delivery_body5 = res.terms_delivery_body5;
        $scope.terms_delivery_body6 = res.terms_delivery_body6;
        $scope.terms_delivery_body7 = res.terms_delivery_body7;
        $scope.terms_delivery_body8 = res.terms_delivery_body8;
        $scope.terms_loyality_body1 = res.terms_loyality_body1;
        $scope.terms_loyality_body2 = res.terms_loyality_body2;
        $scope.terms_loyality_body3 = res.terms_loyality_body3;
        $scope.terms_loyality_body4 = res.terms_loyality_body4;
        $scope.terms_loyality_body5 = res.terms_loyality_body5;
        $scope.terms_loyality_body6 = res.terms_loyality_body6;
        $scope.terms_loyality_body9 = res.terms_loyality_body9;
        $scope.terms_loyality_body11 = res.terms_loyality_body11;
        $scope.terms_loyality_body12 = res.terms_loyality_body12;
        $scope.terms_loyality_body13 = res.terms_loyality_body13;
        $scope.terms_loyality_body14 = res.terms_loyality_body14;
        $scope.terms_install_and_get1 = res.terms_install_and_get1;
        $scope.terms_install_and_get2 = res.terms_install_and_get2;

    });

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');

        $scope.deliverycont = '<ion-modal-view><ion-header-bar class="bar-assertive"><a class="button button-clear pull-right" ng-click="closeDeliveryModal()"><i class="icon ion-close"></i></a><h1 class="title">'+$scope.terms_delivery_body1+'</h1></ion-header-bar><ion-content has-bouncing="true"><div class="padding"><p>'+$scope.terms_delivery_body2+'</p><p><strong>'+$scope.terms_delivery_body3+'</strong></p><p><ul><li>'+$scope.terms_delivery_body4+'</li><li>'+$scope.terms_delivery_body5+'</li></ul></p><p><strong>'+$scope.terms_delivery_body6+'</strong></p><p>'+$scope.terms_delivery_body7+'</p><p>'+$scope.terms_delivery_body8+'</p></div></ion-content></ion-modal-view>';

        $scope.loyalitycont = '<ion-modal-view><ion-header-bar class="bar-assertive"><a class="button button-clear pull-right" ng-click="closeLoyalityModal()"><i class="icon ion-close"></i></a><h1 class="title">'+$scope.terms_loyality_body1+'</h1></ion-header-bar><ion-content has-bouncing="true"><div class="padding"><p>'+$scope.terms_loyality_body2+'<br/>'+$scope.terms_loyality_body3+'<br/>'+$scope.terms_loyality_body4+'<br/>'+$scope.terms_loyality_body5+'<br/>'+$scope.terms_loyality_body6+'<br/>'+$scope.terms_loyality_body9+'<br/>'+$scope.terms_loyality_body11+'<br/>'+$scope.terms_loyality_body12+'<br/>'+$scope.terms_loyality_body13+'<br/>'+$scope.terms_loyality_body14+'</p></div></ion-content></ion-modal-view>';

        $scope.deliveryModal = $ionicModal.fromTemplate($scope.deliverycont, {
          scope: $scope,
          animation: 'slide-in-up'
        });

        $scope.loyalityModal = $ionicModal.fromTemplate($scope.loyalitycont, {
          scope: $scope,
          animation: 'slide-in-up'
        });

      }, 500);
    });

    // Set Header
    $scope.$parent.setHeaderFab(true);

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.dev_height = $window.innerHeight / 2;

    $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover', 'box-shadow': 'none'};

    $scope.acceptStyle = function() {
        $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover', 'box-shadow': 'none'};
    }

    $scope.mapHeight = {'height': $scope.dev_height+'px', 'width': '100%'};

    var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
    $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {

      if(success.rows.length > 0) {
        $scope.promo = success.rows.item(0).user_real_id;
      }

    },function() {});

    $scope.openDeliveryModal = function(){
        $scope.deliveryModal.show();
    }

    $scope.closeDeliveryModal = function(){
        $scope.deliveryModal.hide();
    }

    $scope.openLoyalityModal = function(){
        $scope.loyalityModal.show();
    }

    $scope.closeLoyalityModal = function(){
        $scope.loyalityModal.hide();
    }

    // PROMO SHARING
    $scope.promoSharing = function() {
        $cordovaSocialSharing.share($scope.terms_install_and_get1+$scope.org_name+$scope.$scope.terms_install_and_get2+$scope.promo, $scope.org_name, null, $scope.org_website) // Share via native share sheet
            .then(function(result) {

                // window.cordovaHTTP.get("http://www.olegtronics.com/echo/req/socialshare.php", {params: {"mem_id":mem_id, "share_where":1}}).then(function(promoData) {
                //     },
                //     function() {
                //     });

            }, function(err) {});
    }

})

.controller('DeliveryCtrl', function($rootScope, $scope, $stateParams, $timeout, $ionicPlatform, ionicMaterialInk, ionicMaterialMotion, $ionicNavBarDelegate, $window, $translate) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 500);
    });

    // Set Ink
    ionicMaterialInk.displayEffect();

    // if($cordovaNetwork.isOnline()) {

        ymaps.ready(function () {
            myMap = new ymaps.Map('map', {
                center: [53.9008, 27.5598],
                zoom: 10,
                // Обратите внимание, что в API 2.1 по умолчанию карта создается с элементами управления.
                // Если вам не нужно их добавлять на карту, в ее параметрах передайте пустой массив в поле controls.
                controls: ['zoomControl']
            });

        });

        $scope.dev_height = $window.innerHeight / 3;

        $scope.myStyle = {'background-image': 'url(img/background/back.jpg)', 'background-size': 'cover', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'height': $scope.dev_height+'px', 'width': '100%'};

        $scope.mapHeight = {'height': $scope.dev_height*2+'px', 'width': '100%'}

    // }

})

.controller('ShareCtrl', function($rootScope, $scope, $stateParams, $timeout, $cordovaSQLite, ionicMaterialInk, ionicMaterialMotion, $ionicPlatform, $ionicNavBarDelegate, $window, $cordovaSocialSharing, $cordovaInstagram, $cordovaCamera, $cordovaFile, $ionicPopup, $translate, $q, $ionicScrollDelegate, $http, Org) {

    $scope.$on('$ionicView.beforeLeave', function(){
        $scope.connectvar = 1;
    });

    $translate(['org_name', 'org_website', 'app_ios', 'app_android', 'dwn_app', 'get_bonuses', 'icon_path', 'instagram_hash', 'internet_connection_title', 'internet_connection_body', 'share_selfie_button1', 'share_selfie_button2']).then(function(res) {

        $scope.org_name = res.org_name;
        $scope.org_website = res.org_website;
        $scope.app_ios = res.app_ios;
        $scope.app_android = res.app_android;
        $scope.dwn_app = res.dwn_app;
        $scope.get_bonuses = res.get_bonuses;
        $scope.icon_path = res.icon_path;
        $scope.instagram_hash = res.instagram_hash;

        $scope.internet_connection_title = res.internet_connection_title;
        $scope.internet_connection_body = res.internet_connection_body;
        $scope.share_selfie_button1 = res.share_selfie_button1;
        $scope.share_selfie_button2 = res.share_selfie_button2;

    });

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 500);
    });

    $scope.orgdata = [];

    $scope.$on('$ionicView.enter', function (event, viewData) {
        
        Org.getorgnew().then(function(data) {
            $scope.orgdata = data[0];
        }, function() {});
    
    });

    if($rootScope.platform == 'iOS') {$scope.sharelink = $rootScope.iosLink;} else if($rootScope.platform == 'Android') {$scope.sharelink = $rootScope.androidLink;}
    
    $scope.intconnect = true;

    $scope.connectvar = 0;

    // Set Ink
    ionicMaterialInk.displayEffect();

    $scope.dev_height = $window.innerHeight / 2.5;

    $scope.card_height = $window.innerWidth / 3;

    $scope.selfieBtn = true;

    $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center'};

    $scope.acceptStyle = function() {
        $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};
           $ionicScrollDelegate.resize();
    }

    $scope.mapHeight = {'height': $scope.dev_height*2+'px', 'width': '100%'};

    var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
    $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {

      if(success.rows.length > 0) {
        $scope.promo = success.rows.item(0).user_real_id;
      }

    },function() {});

    var buildImage = function() {
        var deferred = $q.defer();
        // create the canvas
        var canvas = document.createElement('canvas');
        canvas.width = 700;
        canvas.height = 220;
        var context = canvas.getContext('2d');
        // draw a rectangular white frame for our content
        context.beginPath();
        context.fillStyle = "white";
        context.rect(1, 1, 698, 218);
        context.fill();
        context.stroke();
        // draw some text, leaving space for the avatar image
        context.fillStyle = "black";
        context.font = "20px Arial";
        context.fillText($scope.dwn_app+' '+$scope.org_name, 220, 40, 450);
        context.font = "20px Arial";
        context.fillText($scope.get_bonuses, 220, 80, 450);
        context.font = "72px Arial";
        context.fillStyle = "red";
        context.fillText($scope.promo, 300, 170, 450);
        // draw avatar image on the left
        var avatar = new Image();
        avatar.onload = function() {
            context.drawImage(avatar, 10, 10, 200, 200);
            deferred.resolve(canvas);
        };
        avatar.src = "img/sign.png";
        return deferred.promise;
    };

    // PROMO SHARING
    $scope.shareVK = function() {
        if($scope.selfieBtn) {

            $scope.selfieBtn = false;

            buildImage().then(function(canvas) {
                return $cordovaSocialSharing.share(null, null, canvas.toDataURL());
            })
            .then(function(result) {
                $scope.selfieBtn = true;

                var sharestr = JSON.stringify({
                    device_id: $rootScope.uuid,
                    inst_id: $rootScope.institution,
                    social: 'vk',
                    newusr: 'share'
                });

                $http.post($rootScope.generalscript, sharestr).then(function(e) {}, function() {});

            }, function(err) {
                $scope.selfieBtn = true;
            });

        }
    }

    // SOCIAL SHARING
    $scope.socialSharing = function () {
        if($scope.selfieBtn) {
        $scope.selfieBtn = false;

        $cordovaSocialSharing.share($scope.dwn_app+' '+$scope.org_name+' '+$scope.get_bonuses+$scope.promo, $scope.org_name, $scope.icon_path, $scope.sharelink) // Share via native share sheet
            .then(function(result) {
                $scope.selfieBtn = true;

                var sharestr = JSON.stringify({
                    device_id: $rootScope.uuid,
                    inst_id: $rootScope.institution,
                    social: 'social',
                    newusr: 'share'
                });

                $http.post($rootScope.generalscript, sharestr).then(function(e) {}, function() {});

            }, function(err) {
                $scope.selfieBtn = true;
            });
        }
    }

    $scope.shareTwitter = function() {
        if($scope.selfieBtn) {

            $scope.selfieBtn = false;

            buildImage().then(function(canvas) {
                return $cordovaSocialSharing.share(null, null, canvas.toDataURL());
            })
            .then(function(result) {
                $scope.selfieBtn = true;

                var sharestr = JSON.stringify({
                    device_id: $rootScope.uuid,
                    inst_id: $rootScope.institution,
                    social: 'twitter',
                    newusr: 'share'
                });

                $http.post($rootScope.generalscript, sharestr).then(function(e) {}, function() {});

            }, function(err) {
                $scope.selfieBtn = true;
            });

        }
    }

    $scope.shareWhatsApp = function() {
        if($scope.selfieBtn) {
        $scope.selfieBtn = false;
        $cordovaSocialSharing.shareViaWhatsApp($scope.dwn_app+' '+$scope.org_name+' '+$scope.get_bonuses+$scope.promo, $scope.icon_path, $scope.sharelink)
            .then(function(result) {
                $scope.selfieBtn = true;

                var sharestr = JSON.stringify({
                    device_id: $rootScope.uuid,
                    inst_id: $rootScope.institution,
                    social: 'whatsapp',
                    newusr: 'share'
                });

                $http.post($rootScope.generalscript, sharestr).then(function(e) {}, function() {});

            }, function(err) {
                $scope.selfieBtn = true;
            });
        }
    }

    $scope.shareFacebook = function() {
        if($scope.selfieBtn) {

            $scope.selfieBtn = false;

            buildImage().then(function(canvas) {
                return $cordovaSocialSharing.share(null, null, canvas.toDataURL());
            })
            .then(function(result) {
                $scope.selfieBtn = true;

                var sharestr = JSON.stringify({
                    device_id: $rootScope.uuid,
                    inst_id: $rootScope.institution,
                    social: 'facebook',
                    newusr: 'share'
                });

                $http.post($rootScope.generalscript, sharestr).then(function(e) {}, function() {});

            }, function(err) {
                $scope.selfieBtn = true;
            });

        }
    }

    $scope.shareSMS = function () {
        if($scope.selfieBtn) {
        $scope.selfieBtn = false;
        // access multiple numbers in a string like: '0612345678,0687654321'
        $cordovaSocialSharing.shareViaSMS($scope.dwn_app+' '+$scope.org_name+' '+$scope.get_bonuses+$scope.promo, null)
            .then(function(result) {
                $scope.selfieBtn = true;

                var sharestr = JSON.stringify({
                    device_id: $rootScope.uuid,
                    inst_id: $rootScope.institution,
                    social: 'sms',
                    newusr: 'share'
                });

                $http.post($rootScope.generalscript, sharestr).then(function(e) {}, function() {});

            }, function(err) {
                $scope.selfieBtn = true;
            });
        }
    }

    $scope.shareEmail = function() {
        if($scope.selfieBtn) {

            $scope.selfieBtn = false;

            buildImage().then(function(canvas) {
                return $cordovaSocialSharing.share(null, null, canvas.toDataURL());
            })
            .then(function(result) {
                $scope.selfieBtn = true;

                var sharestr = JSON.stringify({
                    device_id: $rootScope.uuid,
                    inst_id: $rootScope.institution,
                    social: 'email',
                    newusr: 'share'
                });

                $http.post($rootScope.generalscript, sharestr).then(function(e) {}, function() {});

            }, function(err) {
                $scope.selfieBtn = true;
            });

        }
    }
    
    // НАСТРОЙКИ СЕЛФИ
    var optionsselfie = {
        quality: 70,
          destinationType: Camera.DestinationType.DATA_URL,
          sourceType: Camera.PictureSourceType.CAMERA,
          allowEdit: false,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 700,
          targetHeight: 700,
          // popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false,
          correctOrientation:true
    };
    
    // СДЕЛАТЬ СЕЛФИ
    $scope.selfie = function() {

        // if($cordovaNetwork.isOnline()) {

            if($scope.selfieBtn) {

                $scope.selfieBtn = false;

                $cordovaCamera.getPicture(optionsselfie).then(function(imageData) {

                    $scope.selfieBtn = true;

                    var img = "data:image/jpeg;base64," + imageData;

                    // ОКОШКО ПРЕДЛАГАЮЩЕЕ РАЗБЛОКИРОВАТЬ ПОЛЬЗОВАТЕЛЯ
                    var picPopup = $ionicPopup.show({
                        template: '<div class="list card"><div class="item item-image"><img src="'+img+'" /></div></div>',
                        title: '',
                        subtitle: '',
                        scope: $scope,
                        buttons: [
                            { text: $scope.share_selfie_button1,
                                type: 'button-assertive',
                                onTap: function() {
                                    picPopup.close();
                                }
                            },
                            {
                                text: '<b>'+$scope.share_selfie_button2+'</b>',
                                type: 'button-balanced',
                                onTap: function(e) {
                                    $timeout(function() {$scope.shareInstagram(img);}, 500);
                                    picPopup.close();
                                }
                            }
                        ]
                    });

                }, function(err) {
                    $scope.selfieBtn = true;
                });

            }

        // } else {
        //  $ionicPopup.alert({
  //               title: $scope.internet_connection_title,
  //               template: $scope.internet_connection_body
  //           });
        // }

    }
    
    $scope.shareInstagram = function(imageData) {
        $cordovaInstagram.share({image: imageData, caption: $scope.instagram_hash}).then(function(suc) {
            // Worked
            // alert(JSON.stringify(suc))
            var sharestr = JSON.stringify({
                device_id: $rootScope.uuid,
                inst_id: $rootScope.institution,
                social: 'instagram',
                newusr: 'share'
            });

            $http.post($rootScope.generalscript, sharestr).then(function(e) {}, function() {});

        }, function(err) {
            // Didn't work
            // alert(JSON.stringify(err))
        });
    }

})

.controller('RulesCtrl', function($rootScope, $scope, $timeout, $stateParams, $ionicPlatform, $ionicNavBarDelegate, $window, $translate, ionicMaterialInk) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 500);
    });

    ionicMaterialInk.displayEffect();

})

.controller('AboutCtrl', function($rootScope, $scope, $timeout, $stateParams, $ionicPlatform, $ionicNavBarDelegate, $window, $translate, $ionicSideMenuDelegate, ionicMaterialMotion, ionicMaterialInk) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
        $ionicSideMenuDelegate.canDragContent(true);
        $timeout(function() {
            $ionicNavBarDelegate.align('center');
        }, 500);
    });

    ionicMaterialInk.displayEffect();

    $scope.dev_height = $window.innerHeight / 2.5;

    $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};
    $scope.avaStyle = {'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};

    $scope.acceptStyle = function() {
        $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};
        $scope.avaStyle = {'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};
    };

})

.controller('ProfileCtrl', function($rootScope, $scope, $state, $timeout, $stateParams, $ionicHistory, $window, $ionicPopup, $cordovaSQLite, $ionicPlatform, $http, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $ionicLoading, $ionicScrollDelegate, $ionicNavBarDelegate, $cordovaImagePicker, $translate, ionicMaterialMotion, ionicMaterialInk, Store, HtmlEnt, Org) {

    $translate(['profile_gend_0', 'profile_gend_1', 'profile_gend_2', 'profile_fromwhere_0', 'profile_fromwhere_1', 'profile_fromwhere_2', 'profile_fromwhere_3', 'profile_fromwhere_4', 'profile_fromwhere_5', 'profile_myname', 'profile_save_pic_button1', 'profile_save_pic_button2', 'profile_orderpop_title', 'profile_orderpop_body', 'profile_orderpop_button1', 'profile_orderpop_button2', 'profile_saved_title', 'profile_saved_body', 'profile_repeat_title', 'profile_repeat_body', 'profile_button_ok', 'profile_additional_points_title', 'profile_additional_points_body', 'profile_block_title', 'profile_block_body', 'profile_wrongdata_title', 'profile_wrongdata_body', 'profile_promocode_title', 'profile_promocode_button1', 'profile_promocode_button2', 'profile_phonenumber_title', 'profile_phonenumber_body', 'profile_phonenumber_button1', 'profile_phonenumber_button2', 'internet_connection_title', 'internet_connection_body', 'profile_restore_account_title', 'profile_restore_account_body', 'profile_restore_account_button1', 'profile_restore_account_button2', 'profile_cancel_order_title', 'profile_cancel_order_txt', 'profile_cancel_order_btn_1', 'profile_cancel_order_btn_2', 'profile_modal_pop_notallowed_tit', 'profile_modal_pop_notallowed_tmp', 'profile_modal_pop_notallowed_btn']).then(function(res) {

        $scope.profile_gend_0 = res.profile_gend_0;
        $scope.profile_gend_1 = res.profile_gend_1;
        $scope.profile_gend_2 = res.profile_gend_2;
        $scope.profile_fromwhere_0 = res.profile_fromwhere_0;
        $scope.profile_fromwhere_1 = res.profile_fromwhere_1;
        $scope.profile_fromwhere_2 = res.profile_fromwhere_2;
        $scope.profile_fromwhere_3 = res.profile_fromwhere_3;
        $scope.profile_fromwhere_4 = res.profile_fromwhere_4;
        $scope.profile_fromwhere_5 = res.profile_fromwhere_5;
        $scope.profile_myname = res.profile_myname;
        $scope.profile_save_pic_button1 = res.profile_save_pic_button1;
        $scope.profile_save_pic_button2 = res.profile_save_pic_button2;
        $scope.profile_orderpop_title = res.profile_orderpop_title;
        $scope.profile_orderpop_body = res.profile_orderpop_body;
        $scope.profile_restore_account_title = res.profile_restore_account_title;
        $scope.profile_restore_account_body = res.profile_restore_account_body;
        $scope.profile_restore_account_button1 = res.profile_restore_account_button1;
        $scope.profile_restore_account_button2 = res.profile_restore_account_button2;
        $scope.internet_connection_title = res.internet_connection_title;
        $scope.internet_connection_body = res.internet_connection_body;

        $scope.profile_orderpop_button1 = res.profile_orderpop_button1;
        $scope.profile_orderpop_button2 = res.profile_orderpop_button2;
        $scope.profile_saved_title = res.profile_saved_title;
        $scope.profile_saved_body = res.profile_saved_body;
        $scope.profile_repeat_title = res.profile_repeat_title;
        $scope.profile_repeat_body = res.profile_repeat_body;
        $scope.profile_button_ok = res.profile_button_ok;
        $scope.profile_additional_points_title = res.profile_additional_points_title;
        $scope.profile_additional_points_body = res.profile_additional_points_body;
        $scope.profile_block_title = res.profile_block_title;
        $scope.profile_block_body = res.profile_block_body;
        $scope.profile_wrongdata_title = res.profile_wrongdata_title;
        $scope.profile_wrongdata_body = res.profile_wrongdata_body;
        $scope.profile_promocode_title = res.profile_promocode_title;
        $scope.profile_promocode_button1 = res.profile_promocode_button1;
        $scope.profile_promocode_button2 = res.profile_promocode_button2;
        $scope.profile_phonenumber_title = res.profile_phonenumber_title;
        $scope.profile_phonenumber_body = res.profile_phonenumber_body;
        $scope.profile_phonenumber_button1 = res.profile_phonenumber_button1;
        $scope.profile_phonenumber_button2 = res.profile_phonenumber_button2;

        $scope.profile_cancel_order_title = res.profile_cancel_order_title;
        $scope.profile_cancel_order_txt = res.profile_cancel_order_txt;
        $scope.profile_cancel_order_btn_1 = res.profile_cancel_order_btn_1;
        $scope.profile_cancel_order_btn_2 = res.profile_cancel_order_btn_2;
        $scope.profile_modal_pop_notallowed_tit = res.profile_modal_pop_notallowed_tit;
        $scope.profile_modal_pop_notallowed_tmp = res.profile_modal_pop_notallowed_tmp;
        $scope.profile_modal_pop_notallowed_btn = res.profile_modal_pop_notallowed_btn;

    });

    var permissions = cordova.plugins.permissions;

    var list = [
        permissions.READ_EXTERNAL_STORAGE,
        permissions.WRITE_EXTERNAL_STORAGE
    ];

    $scope.orgdata = [];
    
    $scope.$on('$ionicView.enter', function (event, viewData) {
        
        Org.getorgnew().then(function(data) {
            $scope.orgdata = data[0];
        }, function() {});
    
    });

    $scope.$on('$ionicView.beforeLeave', function(){
        $scope.connectvar = 1;
    });

    $timeout(function() {
       $ionicScrollDelegate.resize();
    }, 2000);

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    // ORDER HISTORY
    $scope.is_orders = true;

    $scope.intconnect = true;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    // CHECK TIMEZONE (ARGUMENTS ACCEPTING: SECONDS, MILLISECONDS, NOTHING)
    function timezoneAdd(thetime) {
        if(thetime) {
          var thetime = parseInt(thetime);
          if(thetime.toString().length == 10) {
            thetime = thetime*1000;
            // var tzoff = new Date(thetime).getTimezoneOffset();
            var tzoff = new Date().getTimezoneOffset();
            var nsec = tzoff*60*1000;
            var serverzone = 3*60*60*1000;
            var timediff = nsec+serverzone;
            return ((thetime + timediff)/1000).toFixed(0);
          }
          else if(thetime.toString().length == 13) {
            // var tzoff = new Date(thetime).getTimezoneOffset();
            var tzoff = new Date().getTimezoneOffset();
            var nsec = tzoff*60*1000;
            var serverzone = 3*60*60*1000;
            var timediff = nsec+serverzone;
            return thetime + timediff;
          }
        }
        else {
          var tzoff = new Date().getTimezoneOffset();
          var nsec = tzoff*60*1000;
          var serverzone = 3*60*60*1000;
          var timediff = nsec+serverzone;
          return timediff;
        }
    }

    function timezoneSub(thetime) {
        if(thetime) {
          var thetime = parseInt(thetime);
          if(thetime.toString().length == 10) {
            thetime = thetime*1000;
            // var tzoff = new Date(thetime).getTimezoneOffset();
            var tzoff = new Date().getTimezoneOffset();
            var nsec = tzoff*60*1000;
            var serverzone = 3*60*60*1000;
            var timediff = nsec+serverzone;
            return ((thetime - timediff)/1000).toFixed(0);
          }
          else if(thetime.toString().length == 13) {
            // var tzoff = new Date(thetime).getTimezoneOffset();
            var tzoff = new Date().getTimezoneOffset();
            var nsec = tzoff*60*1000;
            var serverzone = 3*60*60*1000;
            var timediff = nsec+serverzone;
            return thetime - timediff;
          }
        }
        else {
          var tzoff = new Date().getTimezoneOffset();
          var nsec = tzoff*60*1000;
          var serverzone = 3*60*60*1000;
          var timediff = nsec+serverzone;
          return timediff;
        }
    }

    $scope.ordersAll = [];

    $scope.orderingpush = function(suc) {
        $scope.ordersAll = [];
        if(suc.order_worker > 0) {

              var order_id = suc.order_id;
              var order_name = suc.order_name;
              var order_user = suc.order_user;
              var order_office = suc.order_office;
              var order_goods = suc.order_goods;
              var order_cats = suc.order_cats;
              var order_start = suc.order_start;
              var order_end = suc.order_end;

              var order_desc = suc.order_desc;
              var order_bill = parseFloat(suc.order_bill/100).toFixed(2);
              var order_institution = suc.order_institution;
              var order_status = suc.order_status;

              var order_allday = suc.order_allday;
              var order_when = suc.order_when;
              var order_order = suc.order_order;
              var workerid = suc.order_worker;
              var order_mobile = suc.order_mobile;

              // console.log('1 =======================> '+order_id+' '+order_name)

              // alert(workerid)

              infouser(workerid).then(function(userdata) {

                // alert(JSON.stringify(userdata))

                menuepic(order_order).then(function(picdata) {

                    // alert(JSON.stringify(picdata))

                  menuename(order_order).then(function(namedata) {

                    // alert(JSON.stringify(namedata))
                    
                    $scope.ordersAll.push({"order_id":order_id, "order_user":order_user, "order_name":order_name, "order_desc":order_desc, "order_worker":userdata, "order_bill":order_bill, "order_order":namedata, "order_pic":"http://www.olegtronics.com/admin/img/menu/"+order_institution+"/300/"+picdata, "order_office":order_office, "order_goods":order_goods, "order_cats":order_cats, "order_start":order_start, "order_end":order_end, "order_status":order_status, "order_allday":order_allday, "order_mobile":order_mobile, "order_when":order_when});

                  })

                })

              })

        }
        else {

          var order_id = suc.order_id;
          var order_name = suc.order_name;
          var order_user = suc.order_user;
          var order_office = suc.order_office;
          var order_goods = suc.order_goods;
          var order_cats = suc.order_cats;
          var order_start = suc.order_start;
          var order_end = suc.order_end;
          var order_desc = suc.order_desc;
          var order_bill = parseFloat(suc.order_bill/100).toFixed(2);
          var order_institution = suc.order_institution;
          var order_status = suc.order_status;

          var order_allday = suc.order_allday;
          var order_when = suc.order_when;
          var order_order = suc.order_order;
          var workerid = suc.order_worker;
          var order_mobile = suc.order_mobile;

          // console.log('2 =======================> '+order_id+' '+order_name)

          menuepic(order_order).then(function(picdata) {

            menuename(order_order).then(function(namedata) {


                $scope.ordersAll.push({"order_id":order_id, "order_user":order_user, "order_name":order_name, "order_desc":order_desc, "order_worker":"", "order_bill":order_bill, "order_order":namedata, "order_pic":"http://www.olegtronics.com/admin/img/menu/"+order_institution+"/300/"+picdata, "order_office":order_office, "order_goods":order_goods, "order_cats":order_cats, "order_start":order_start, "order_end":order_end, "order_status":order_status, "order_allday":order_allday, "order_mobile":order_mobile, "order_when":order_when});

            })

          })

        }
    }

    $scope.ordering = function(myid) {

        var queryOrders = "SELECT * FROM ordering WHERE order_user = ? ORDER BY order_id DESC LIMIT 20";
        $cordovaSQLite.execute($rootScope.db, queryOrders, [myid]).then(function(suc) {
            // console.log('=================> '+suc.rows.length)
            if(suc.rows.length > 0) {
                for(var i=0;i<suc.rows.length;i++) {
                    // console.log('=================> '+suc.rows.item(i).order_worker)
                    if(suc.rows.item(i).order_status != 4 && suc.rows.item(i).order_del == 0) {
                        suc.rows.item(i).order_start = timezoneAdd(suc.rows.item(i).order_start);
                        suc.rows.item(i).order_end = timezoneAdd(suc.rows.item(i).order_end);
                        $scope.orderingpush(suc.rows.item(i));  
                    }
                }
            }
        }, function() {});

    }

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
        
        $timeout(function() {
            $ionicNavBarDelegate.align('center');

            $scope.usrGender = [{id: 0,name: $scope.profile_gend_0},{id: 1,name: $scope.profile_gend_1},{id: 2,name: $scope.profile_gend_2}];

            $scope.usrFromWhere = [{id: 0,name: $scope.profile_fromwhere_0},{id: 1,name: $scope.profile_fromwhere_1},{id: 2,name: $scope.profile_fromwhere_2},{id: 3,name: $scope.profile_fromwhere_3},{id: 4,name: $scope.profile_fromwhere_4},{id: 5,name: $scope.profile_fromwhere_5}];

            $scope.myName = $scope.profile_myname;
            $scope.user.gender = $scope.usrGender[0];
            $scope.user.install_where = $scope.usrFromWhere[0];

            var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
            $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {

                $scope.myPhone = success.rows.item(0).user_mob;
                $scope.mobConfirm = success.rows.item(0).user_mob_confirm;
                $scope.workPos = success.rows.item(0).user_work_pos;

                $scope.user_real_id = success.rows.item(0).user_real_id;

                $scope.ordering(success.rows.item(0).user_real_id);

                if(success.rows.item(0).user_name != '0' && success.rows.item(0).user_name != '') {
                    $scope.user.name = success.rows.item(0).user_name;
                }

                if(success.rows.item(0).user_surname != '0' && success.rows.item(0).user_surname != '') {
                    $scope.user.surname = success.rows.item(0).user_surname;
                }

                if(success.rows.item(0).user_middlename != '0' && success.rows.item(0).user_middlename != '') {
                    $scope.user.middlename = success.rows.item(0).user_middlename;
                }

                if(success.rows.item(0).user_email != '0' && success.rows.item(0).user_email != '') {
                    $scope.user.email = success.rows.item(0).user_email;
                }

                if(success.rows.item(0).user_email_confirm == '1') {
                    $scope.user.email_confirm = success.rows.item(0).user_email_confirm;
                }

                if(success.rows.item(0).user_tel != '0' && success.rows.item(0).user_tel != '') {
                    $scope.user.tel = success.rows.item(0).user_tel;
                }

                if(success.rows.item(0).user_mob_confirm != '') {
                    $scope.user.mob_confirm = success.rows.item(0).user_mob_confirm;
                }

                if(success.rows.item(0).user_mob != '0' && success.rows.item(0).user_mob != '') {
                    $scope.user.mob = success.rows.item(0).user_mob;
                }

                if(success.rows.item(0).user_institution != '0' && success.rows.item(0).user_institution != '') {
                $scope.user.institution = success.rows.item(0).user_institution;
                }

                if(success.rows.item(0).user_pic != '0' && success.rows.item(0).user_pic != '') {
                    $scope.user.pic = success.rows.item(0).user_pic;
                    $scope.myPic = 'http://www.olegtronics.com/admin/img/user/'+$scope.user.institution+'/pic/'+$scope.user.pic;
                }

                if(success.rows.item(0).user_gender != '0' && success.rows.item(0).user_gender != '') {
                    $scope.user.gender = $scope.usrGender[success.rows.item(0).user_gender];
                }

                if(success.rows.item(0).user_birthday != '0' && success.rows.item(0).user_birthday != '') {
                    
                    if(success.rows.item(0).user_birthday != '0000-00-00') {
                        // var birthdaysecs = success.rows.item(0).user_birthday * 1000;

                        // var today = new Date(birthdaysecs);
                        // var dd = today.getDate();
                        // var mm = today.getMonth()+1; //January is 0!
                        // var yyyy = today.getFullYear();

                        // if(dd<10) {
                        //     dd='0'+dd
                        // }

                        // if(mm<10) {
                        //     mm='0'+mm
                        // }

                        // $scope.user.birthday = yyyy+'-'+mm+'-'+dd;
                        $scope.user.birthday = new Date(success.rows.item(0).user_birthday);
                    }
                    else {
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth()+1; //January is 0!
                        var yyyy = today.getFullYear();

                        if(dd<10) {
                            dd='0'+dd
                        }

                        if(mm<10) {
                            mm='0'+mm
                        }

                        today = yyyy+'-'+mm+'-'+dd;

                        $scope.user.birthday = new Date(today);
                    }

                }

                if(success.rows.item(0).user_adress) {

                    if(success.rows.item(0).user_adress != '0' && success.rows.item(0).user_adress != '') {
                        var usradrsplit = success.rows.item(0).user_adress.split("|");
                        if(usradrsplit[0]) {$scope.adr.adress1 = usradrsplit[0];}
                        if(usradrsplit[1]) {$scope.adr.adress2 = usradrsplit[1];}
                        if(usradrsplit[2]) {$scope.adr.adress3 = usradrsplit[2];}
                        if(usradrsplit[3]) {$scope.adr.adress4 = usradrsplit[3];}
                        if(usradrsplit[4]) {$scope.adr.adress5 = usradrsplit[4];}
                    }

                }

                if(success.rows.item(0).install_where != '0' && success.rows.item(0).install_where != '') {
                    $scope.user.install_where = $scope.usrFromWhere[success.rows.item(0).user_install_where];
                }

                if(success.rows.item(0).user_real_id != '0' && success.rows.item(0).user_real_id != '') {
                    $scope.myID = success.rows.item(0).user_real_id;
                }

                if(success.rows.item(0).user_name != '0' && success.rows.item(0).user_name != '') {
                    $scope.myName = success.rows.item(0).user_name;
                }

            }, function() {});

        }, 100);

        Store.sumcheck().then(function(data) {
            $scope.order = data;
        });

    });

    $scope.user = {
        name: '',
        surname: '',
        middlename: '',
        gender: '',
        birthday: '',
        mob: '',
        tel: '',
        email: '',
        adress: '',
        install_where: ''
    };

    $scope.adr = {
        adress1: '',
        adress2: '',
        adress3: '',
        adress4: '',
        adress5: ''
    };

    $scope.myID = '';

    $scope.adr.adress1 = '';
    $scope.adr.adress2 = '';
    $scope.adr.adress3 = '';
    $scope.adr.adress4 = '';
    $scope.adr.adress5 = '';
    $scope.user.name = '';
    $scope.user.surname = '';
    $scope.user.middlename = '';
    $scope.user.email = '';
    $scope.user.tel = '';
    $scope.user.mob = '';
    $scope.user.institution = '';
    $scope.user.pic = '';
    $scope.user.mob_confirm = 0;
    $scope.user.email_confirm = 0;

    $scope.myPic = 'http://www.olegtronics.com/admin/images/user.png';

    $scope.promoShow = false;

    $scope.myPhone = 0;
    $scope.mobConfirm = 0;
    $scope.workPos = 0;

    var orders = [];

    function infouser(x) {

        var x = x;

        var queryUsers = "SELECT * FROM users WHERE user_real_id = ? AND user_del = '0'";
        return $cordovaSQLite.execute($rootScope.db, queryUsers, [x]).then(function(succ) {

          if(succ.rows.length > 0) {

            return succ.rows.item(0).user_name;

          }
          else {
            return true;
          }

        },function() {});

    };

    function menuepic(x) {

        var x = x;

        var queryMenueSel = "SELECT * FROM menue WHERE menue_id = ? AND menue_del = '0'";
        return $cordovaSQLite.execute($rootScope.db, queryMenueSel, [x]).then(function(success) {

          if(success.rows.length > 0) {

            return success.rows.item(0).menue_pic;

          }
          else {
            return true;
          }

        }, function() {});

    };

    function menuename(x) {

        var x = x;

        var queryMenueSel = "SELECT * FROM menue WHERE menue_when > ? AND menue_del = '0'";
        return $cordovaSQLite.execute($rootScope.db, queryMenueSel, [x]).then(function(success) {

          if(success.rows.length > 0) {

            return success.rows.item(0).menue_name;

          }
          else {
            return true;
          }

        }, function() {});

    };

    $scope.formatTime = function(x) {
        // ОПРЕДЕЛЕНИЕ ВРЕМЯ
        var nowtime = new Date();
        var gottime = x * 1000;
        // ПЕРЕВЕРНУТОЕ ВРЕМЯ, ТАК КАК ЗАПИСЬ В БУДУЩЕМ
        var nowtimediff = gottime - nowtime.getTime();
        var onltime = new Date(gottime);
        var onlDateTime;
        var onlMonth;
        var onlDay;
        var onlHour;
        var onlMin;
        var days = ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"];
        var pushday1 = days[onltime.getDay()];

        // console.log('=================> '+nowtimediff + ' ' + nowtime.getTime() + ' ' + gottime)

        // DAYS AGO
        if(nowtimediff > 86400) {
          if(onltime.getMonth() < 9) {
              onlMonth = '0' + (onltime.getMonth() + 1);
          } else {
              onlMonth = onltime.getMonth() + 1;
          }

          if(onltime.getDate() < 10) {
              onlDay = '0' + onltime.getDate();
          } else {
              onlDay = onltime.getDate();
          }

          if(onltime.getHours() < 10) {
              onlHour = '0' + onltime.getHours();
          } else {
              onlHour = onltime.getHours();
          }
          if(onltime.getMinutes() < 10) {
              onlMin = '0' + onltime.getMinutes();
          } else {
              onlMin = onltime.getMinutes();
          }

          onlDateTime = pushday1 + ' ' + onlDay + '.' + onlMonth + '.' + onltime.getFullYear() + ' ' + onlHour + ':' + onlMin;
        }
        // HOURS AND MINUTES AGO
        else {
          if(onltime.getHours() < 10) {
              onlHour = '0' + onltime.getHours();
          } else {
              onlHour = onltime.getHours();
          }
          if(onltime.getMinutes() < 10) {
              onlMin = '0' + onltime.getMinutes();
          } else {
              onlMin = onltime.getMinutes();
          }

          onlDateTime =  onlHour + ':' + onlMin;
        }

        return onlDateTime;
    }

    Store.getpoints().then(function(data) {
        $scope.pointsAll = data;
    });

    // $timeout(function() {alert(JSON.stringify(orders))}, 2000);

    // Store.getorders().then(function(data) {
    //     $scope.ordersAll = data;
    //     alert('xx '+JSON.stringify(data))
    // });

    $scope.wallet = 0;

    var queryWallet = "SELECT * FROM wallet WHERE wallet_id != ? AND wallet_del = '0' ORDER BY wallet_id DESC LIMIT 1";
    $cordovaSQLite.execute($rootScope.db, queryWallet, [0]).then(function(suc) {
      if(suc.rows.length > 0) {
        $scope.wallet = suc.rows.item(0).wallet_total;
      }
    }, function() {});

    $scope.dev_height = $window.innerHeight / 2.5;

    $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};
    $scope.avaStyle = {'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};

    $scope.acceptStyle = function() {
        $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};
        $scope.avaStyle = {'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};
    }

    // НАСТРОЙКИ СЕЛФИ
    var optionsselfie = {
        quality: 70,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: 700,
        targetHeight: 700,
        correctOrientation: true
    };

    $scope.selfieBtn = true;

    $scope.picPrev = 1;
    
    // СДЕЛАТЬ СЕЛФИ
    $scope.selfie = function() {

        permissions.hasPermission(list, function(persucc) {

            permissions.requestPermissions(list, function(status) {
                if(!status.hasPermission) {
                    // alert('ERROR PERMISSION NOT GRANTED ==================================> ')
                }
                else {

                    if($scope.selfieBtn) {

                        $scope.selfieBtn = false;

                        $cordovaCamera.getPicture(optionsselfie).then(function(imageURI) {

                            $scope.selfieBtn = true;

                            // DATE - TIME IN SECONDS
                            var when = Math.floor(new Date().getTime() / 1000);

                            var randpicname = $scope.myID + '_' + when;

                            var namefilesplit = imageURI.split('/');
                            var namefile = namefilesplit[namefilesplit.length-1];
                            var oldurlsplit = imageURI.split(namefile);
                            var oldurl = oldurlsplit[0];
                            var topath = cordova.file.dataDirectory + $rootScope.inst_dir + '/' + randpicname + '.jpg';
                            var tourl = cordova.file.dataDirectory + $rootScope.inst_dir + '/';

                            $cordovaFile.moveFile(oldurl, namefile, tourl, randpicname + '.jpg').then(function(success) {

                                $scope.savePic(topath, randpicname);

                            }, function(er) {});


                        }, function(err) {
                            $scope.selfieBtn = true;
                        });

                    }

                }

            }, function(pererror) {
                $ionicLoading.hide().then(function() {});
                // alert('ERROR PERMISSION PLUGIN ==================================> '+JSON.stringify(pererror))
            })

        }, null);

    }

    // НАСТРОЙКИ ВЫБОРА ФОТО
    var optionsImg = {
       maximumImagesCount: 1,
       width: 800,
       height: 800,
       quality: 80
      };

    // ВЫБРАТЬ ФОТО
    $scope.selPic = function() {

        permissions.hasPermission(list, function(persucc) {

            permissions.requestPermissions(list, function(status) {
                if(!status.hasPermission) {
                    // alert('ERROR PERMISSION NOT GRANTED ==================================> ')
                }
                else {

                    if($scope.selfieBtn) {

                        $scope.selfieBtn = false;

                        $cordovaImagePicker.getPictures(optionsImg)
                        .then(function (results) {

                            $scope.selfieBtn = true;

                            if(results.length > 0) {

                                for (var i = 0; i < results.length; i++) {

                                // DATE - TIME IN SECONDS
                                var when = Math.floor(new Date().getTime() / 1000);

                                var randpicname = $scope.myID + '_' + when;

                                var namefilesplit = results[i].split('/');
                                var namefile = namefilesplit[namefilesplit.length-1];
                                var oldurlsplit = results[i].split(namefile);
                                var oldurl = oldurlsplit[0];
                                var topath = cordova.file.dataDirectory + $rootScope.inst_dir + '/' + randpicname + '.jpg';
                                var tourl = cordova.file.dataDirectory + $rootScope.inst_dir + '/';

                                $cordovaFile.copyFile(oldurl, namefile, tourl, randpicname + '.jpg').then(function(success) {

                                    $scope.savePic(topath, randpicname);

                                }, function(er) {});
                                
                                }

                            }

                        }, function(error) {
                            $scope.selfieBtn = true;
                        });

                    }

                }

            }, function(pererror) {
                $ionicLoading.hide().then(function() {});
                // alert('ERROR PERMISSION PLUGIN ==================================> '+JSON.stringify(pererror))
            })

        }, null);

    }

    // ЗАГРУЗКА СЕЛФИ
    $scope.savePic = function(imgpath, randpicname) {

        // Setup the loader
        $ionicLoading.show({
            content: '<i class="icon ion-loading-c"></i><span id="loadprog"></span>',
            animation: 'fade-in',
            showBackdrop: false,
            maxWidth: 50,
            showDelay: 0
        });

        // UPLOADING SOUND
        var options = {
            fileKey: "file",
            fileName: randpicname + '.jpg',
            chunkedMode: false,
            mimeType: 'image/jpeg'
        }

        $cordovaFileTransfer.upload("http://www.olegtronics.com/admin/coms/upload.php?usrupl=1&preview="+$scope.picPrev+"&user_id=" + $scope.myID, imgpath, options).then(function(result) {
            
                var srtringify = JSON.stringify(result.response);
                var parsingres = JSON.parse(JSON.parse(srtringify));

                var messent = parsingres.user_upd;

                if(messent > 0) {

                    if($scope.picPrev == '1') {

                        $scope.picPreview = 'http://www.olegtronics.com/admin/img/user/'+$scope.user.institution+'/pic/'+randpicname + '.jpg';

                        // ОКОШКО ПРЕДЛАГАЮЩЕЕ СОХРАНИТЬ ФОТО
                        var picPopup = $ionicPopup.show({
                            template: '<div class="list card"><div class="item item-image"><img cache-src="'+$scope.picPreview+'" /></div></div>',
                            title: '',
                            subtitle: '',
                            scope: $scope,
                            buttons: [
                                { text: $scope.profile_save_pic_button1,
                                    type: 'button-assertive',
                                    onTap: function() {
                                        $cordovaFile.removeFile(cordova.file.dataDirectory + $rootScope.inst_dir + '/', randpicname + '.jpg').then(function() {
                                            picPopup.close();
                                            $scope.selfieBtn = true;
                                        }, function() {});
                                    }
                                },
                                {
                                    text: '<b>'+$scope.profile_save_pic_button2+'</b>',
                                    type: 'button-balanced',
                                    onTap: function(e) {
                                        $scope.picPrev = 0;
                                        $scope.selfieBtn = true;
                                        $timeout(function() {$scope.savePic(imgpath, randpicname);}, 500);
                                        picPopup.close();
                                    }
                                }
                            ]
                        });

                    }
                    else if($scope.picPrev == '0') {

                        // IMAGE
                        var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
                        $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(yes) {

                                // REMOVE PREVIOUS FILE FROM DISK
                                $cordovaFile.removeFile(cordova.file.dataDirectory + $rootScope.inst_dir + '/', yes.rows.item(0).user_pic).then(function () {
                                    // UPDATE IMAGE
                                    var updateBio = "UPDATE users SET user_pic=?, user_upd=? WHERE user_id = ?";
                                    $cordovaSQLite.execute($rootScope.db, updateBio, [randpicname + '.jpg', messent, 1]).then(function() {}, function() {});

                                    $scope.myPic = 'http://www.olegtronics.com/admin/img/user/'+$scope.user.institution+'/pic/'+randpicname + '.jpg';

                                    $scope.picPrev = 1;


                                }, function () {
                                    // UPDATE IMAGE
                                    var updateBio = "UPDATE users SET user_pic=?, user_upd=? WHERE user_id = ?";
                                    $cordovaSQLite.execute($rootScope.db, updateBio, [randpicname + '.jpg', messent, 1]).then(function() {}, function() {});

                                    $scope.myPic = 'http://www.olegtronics.com/admin/img/user/'+$scope.user.institution+'/pic/'+randpicname + '.jpg';

                                    $scope.picPrev = 1;

                                });

                        },
                        function(er) {});

                    }

                }

            
            $ionicLoading.hide();

        }, function(err) {

            $ionicLoading.hide();

        }, function (progress) {
            document.getElementById('loadprog').innerHTML = Math.round((progress.loaded / progress.total) * 100) + ' %';
        });

    }

    $scope.restoreAccount = function() {

        $scope.restorePopup = $ionicPopup.show({
            title: $scope.profile_restore_account_title,
            template: '<p class="padding">'+$scope.profile_restore_account_body+'</p><input placeholder="email@email.com" style="width:100%;border_bottom:1px solid #ff0000;">',
            scope: $scope,
            buttons: [
                { text: '<b>'+$scope.profile_restore_account_button1+'</b>',
                    onTap: function() {
                        $scope.restorePopup.close();
                    }},
                { text: '<b>'+$scope.profile_restore_account_button2+'</b>',
                    onTap: function(e) {
                        $scope.restorePopup.close();
                    }}
            ]
        });

    }

    $scope.orderAgain = function() {

        $scope.orderAgainPopup = $ionicPopup.show({
            title: $scope.profile_orderpop_title,
            template: '<p class="padding">'+$scope.profile_orderpop_body+'</p>',
            scope: $scope,
            buttons: [
                { text: '<b>'+$scope.profile_orderpop_button1+'</b>',
                    onTap: function() {
                        $scope.orderAgainPopup.close();
                    }},
                { text: '<b>'+$scope.profile_orderpop_button2+'</b>',
                    onTap: function(e) {
                        $scope.orderAgainPopup.close();
                    }}
            ]
        });

    }

    $scope.goBackOne = function() {
        $ionicHistory.goBack();
    }

    $scope.saved = function() {
        $ionicPopup.alert({
            title: $scope.profile_saved_title,
            template: $scope.profile_saved_body
        });
    }

    $scope.repeat = function() {
        $ionicPopup.alert({
            title: $scope.profile_repeat_title,
            template: $scope.profile_repeat_body
        });
    }

    var queryUsrPromo = "SELECT * FROM users WHERE user_id=? AND user_del = '0' LIMIT 1";
    $cordovaSQLite.execute($rootScope.db, queryUsrPromo, [1]).then(function(suc) {

            if(suc.rows.item(0).user_promo != 1) {
                if(suc.rows.item(0).user_promo < 4) {
                    $scope.promoShow = true;
                }
            }

    }, function(er) {});

    $scope.connectvar = 0;

    $scope.sendPromo = function(promotxt) {

        var promostr = JSON.stringify({
            device_id: $rootScope.uuid,
            inst_id: $rootScope.institution,
            promo: promotxt,
            newusr: 'promo'
        });

        $http.post($rootScope.generalscript, promostr).then(function(e) {

            var data = e.data;

            var promocodeOK = parseInt(data[0].promoOK);

            // console.log(promocodeOK);

            if(promocodeOK == 0) {

            }
            else if(promocodeOK == 2) {

                var queryUsrPromoUpd = "UPDATE users SET user_promo=? WHERE user_id=?";
                $cordovaSQLite.execute($rootScope.db, queryUsrPromoUpd, [1, 1]).then(function(suc) {

                    $scope.promoShow = false;

                    $scope.forbidPopup = $ionicPopup.show({
                        title: $scope.profile_block_title,
                        template: $scope.profile_block_body,
                        scope: $scope,
                        buttons: [
                          { text: $scope.profile_button_ok,
                              onTap: function() {
                                  $scope.forbidPopup.close();
                              }}
                        ]
                    });

                }, function() {});

            }
            else if(promocodeOK == 3) {

                $scope.falsePopup = $ionicPopup.show({
                    title: $scope.profile_wrongdata_title,
                    template: $scope.profile_wrongdata_body,
                    scope: $scope,
                    buttons: [
                      { text: $scope.profile_button_ok,
                          onTap: function() {
                              $scope.falsePopup.close();
                              $timeout(function() {$scope.beforePromoSend();}, 100);
                          }}
                    ]
                });

            }
            else if(promocodeOK == 4) {
    
                var errPopup = $ionicPopup.alert({
                  title: $scope.profile_modal_pop_notallowed_tit,
                  template: $scope.profile_modal_pop_notallowed_tmp,
                  cssClass: 'gifterr',
                  scope: $scope,
                  buttons: [
                    {
                      text: '<h3>'+$scope.profile_modal_pop_notallowed_btn+'</h3>',
                      type: 'button-full button-clear darkgreentxt',
                      onTap: function(e) {
                          errPopup.close();
                      }
                    }
                  ]
                });
    
            }
            else if(promocodeOK > 4) {

                var queryUsrPromoUpd = "UPDATE users SET user_promo=? WHERE user_id=?";
                $cordovaSQLite.execute($rootScope.db, queryUsrPromoUpd, [promocodeOK, 1]).then(function(suc) {

                    $scope.promoShow = false;

                    $scope.correctPopup = $ionicPopup.show({
                        title: $scope.profile_additional_points_title,
                        template: $scope.profile_additional_points_body,
                        scope: $scope,
                        buttons: [
                          { text: $scope.profile_button_ok,
                              onTap: function() {
                                  $scope.correctPopup.close();
                              }}
                        ]
                    });

                }, function() {});

            }

        }, function() {});

    };

    $scope.beforePromoSend = function() {

        if($scope.workPos >= '2') {

            $scope.forbidPopup = $ionicPopup.show({
                title: $scope.profile_block_title,
                template: $scope.profile_block_body,
                scope: $scope,
                buttons: [
                  { text: $scope.profile_button_ok,
                      onTap: function() {
                          $scope.forbidPopup.close();
                      }}
                ]
            });

        }
        else {

            if($scope.myPhone && $scope.myPhone != '0' && $scope.mobConfirm && $scope.mobConfirm == '1') {
                
                $scope.promoPopup = $ionicPopup.show({
                    title: $scope.profile_promocode_title,
                    template: '<input id="promotxt" style="width:100%;padding:5px;text-align:center;border-bottom:2px solid #ff0000;resize:none;" />',
                    scope: $scope,
                    buttons: [
                        { text: $scope.profile_promocode_button1,
                            onTap: function() {
                                $scope.promoPopup.close();
                                // var queryUsrPromoUpd = "UPDATE users SET user_promo=? WHERE user_id=?";
                                // $cordovaSQLite.execute($rootScope.db, queryUsrPromoUpd, [1, 1]).then(function(suc) {
                                //     $scope.sendPromo('1');
                                // },
                                // function() {});
                            }},
                        { text: $scope.profile_promocode_button2,
                            onTap: function(e) {
                                var promotxt = document.getElementById('promotxt').value;
                                $scope.sendPromo(promotxt);
                                $scope.promoPopup.close();
                            }}
                    ]
                });

            }
            else {

                $scope.phonePopup = $ionicPopup.show({
                    title: $scope.profile_phonenumber_title,
                    template: $scope.profile_phonenumber_body,
                    scope: $scope,
                    buttons: [
                      { text: $scope.profile_phonenumber_button1,
                          onTap: function() {
                              $scope.phonePopup.close();
                              $timeout(function() {$state.go('app.profile1');}, 100);
                          }},
                          { text: $scope.profile_phonenumber_button2,
                          onTap: function() {
                              $scope.phonePopup.close();
                          }}
                    ]
                });

            }

        }

    }

    $scope.cancelOrder = function(orderid) {

        var xjson = JSON.stringify({
            inst_id: $rootScope.institution,
            newusr: 'calender',
            device_id: $rootScope.uuid,
            getset: 2,
            orderId: orderid
        });

        $http.post($rootScope.generalscript, xjson).then(function(ordsuc) {

            // console.log('==================> '+JSON.stringify(ordsuc))

            var orderdata = ordsuc.data;

            if(orderdata[0].orderOK == '7') {
                $ionicPopup.alert({title: 'Внимание', template: 'Запись уже не доступна!'});
            }
            else if(orderdata[0].orderOK == '6') {
                // alert('success android: '+JSON.stringify(orderdata));
                // $scope.smssent = true;

                $ionicPopup.alert({title: 'Внимание', template: 'Вы отменили запись!'});
                $timeout(function() {
                    var orderingUpd = "UPDATE ordering SET order_status=? WHERE order_id=?";
                    $cordovaSQLite.execute($rootScope.db, orderingUpd, [4, orderid]).then(function() {
                        for(var i=0;i<$scope.ordersAll.length;i++) {
                            if($scope.ordersAll[i].order_id == orderid) {
                                $scope.ordersAll[i].order_status = 4;
                            }
                        }
                    }, function() {});
                }, 200);
            }
            
        }, 
        function(er) {
            $ionicPopup.alert({title: 'Связь', template: 'Проверьте Интернетсоединение.'});
            // console.log('ERROR ==========================> '+JSON.stringify(er));
        });

    }

    $scope.beforeCancelOrder = function(orderid) {
        var cancelOrderPopup = $ionicPopup.show({
            title: $scope.profile_cancel_order_title,
            template: $scope.profile_cancel_order_txt,
            scope: $scope,
            buttons: [
                { text: $scope.profile_cancel_order_btn_1,
                    onTap: function() {
                        cancelOrderPopup.close();
                        // var queryUsrPromoUpd = "UPDATE users SET user_promo=? WHERE user_id=?";
                        // $cordovaSQLite.execute($rootScope.db, queryUsrPromoUpd, [1, 1]).then(function(suc) {
                        //     $scope.sendPromo('1');
                        // },
                        // function() {});
                    }},
                { text: $scope.profile_cancel_order_btn_2,
                    onTap: function(e) {
                        cancelOrderPopup.close();
                        $timeout(function() {$scope.cancelOrder(orderid);}, 200);
                    }}
            ]
        });
    }

    $scope.htmlEntities = function(x) {
        return HtmlEnt.decodeEntities(x);
    }

})

.controller('ProfileCtrl1', function($rootScope, $scope, $state, $timeout, $stateParams, $ionicHistory, $window, $ionicPopup, $cordovaSQLite, $ionicPlatform, $http, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $ionicLoading, $ionicScrollDelegate, $ionicNavBarDelegate, $translate, $cordovaNetwork, $localStorage, ionicMaterialMotion, ionicMaterialInk, Store, HtmlEnt) {

    $scope.is_discount = false;

    $translate(['profile1_gend_0', 'profile1_gend_1', 'profile1_gend_2', 'profile1_fromwhere_0', 'profile1_fromwhere_1', 'profile1_fromwhere_2', 'profile1_fromwhere_3', 'profile1_fromwhere_4', 'profile1_fromwhere_5', 'profile1_myname', 'profile1_save_pic_button1', 'profile1_save_pic_button2', 'profile1_orderpop_title', 'profile1_orderpop_body', 'profile1_orderpop_button1', 'profile1_orderpop_button2', 'profile1_saved_title', 'profile1_saved_body', 'internet_connection_title', 'internet_connection_body', 'profile1_phonevalid_title', 'profile1_phonevalid_body', 'profile1_phonerepeat_title', 'profile1_phonerepeat_body', 'profile1_phonerepeat_button1', 'profile1_phonerepeat_button2', 'profile1_phoneenter_button1', 'profile1_phoneenter_button2', 'profile1_phoneconf_title', 'profile1_phoneconf_body', 'profile1_discount_title', 'profile1_discount_body', 'profile1_blocked_title', 'profile1_blocked_body', 'profile1_unblocked_title', 'profile1_unblocked_body', 'profile1_wrongcode_title', 'profile1_wrongcode_body', 'profile1_waitconfirm_title', 'profile1_waitconfirm_body', 'profile1_whenready_title', 'profile1_whenready_body', 'profile1_whenready_button', 'profile1_shortphone_title1', 'profile1_shortphone_title2', 'profile1_shortphone_body', 'profile1_shortphone_button', 'profile1_nophone_title1', 'profile1_whenready_title', 'profile1_nophone_title2', 'profile1_nophone_body', 'profile1_nophone_button', 'profile1_smsconf_body', 'profile1_smsconf_title', 'profile1_smsconf_button1', 'profile1_smsconf_button2', 'profile1_enter_phonenumber_title', 'profile1_enter_phonenumber_body', 'profile1_discountpop_nonum_title', 'profile1_discountpop_nonum_body', 'profile1_discountpop_nobirth_title', 'profile1_discountpop_nobirth_body', 'profile1_discountpop_nogend_title', 'profile1_discountpop_nogend_body', 'profile1_discountpop_nosurname_title', 'profile1_discountpop_nosurname_body', 'profile1_discountpop_nofamily_title', 'profile1_discountpop_nofamily_body', 'profile1_discountpop_noname_title', 'profile1_discountpop_noname_body', 'profile1_restore_account_title', 'profile1_restore_account_body', 'profile1_restore_account_button1', 'profile1_restore_account_button2', 'profile1_order_again_title', 'profile1_order_again_body', 'profile1_order_again_button1', 'profile1_order_again_button2', 'profile1_data_saved_title', 'profile1_data_saved_body', 'profile1_repeat_order_title', 'profile1_repeat_order_body']).then(function(res) {

            $scope.profile1_gend_0 = res.profile1_gend_0;
            $scope.profile1_gend_1 = res.profile1_gend_1;
            $scope.profile1_gend_2 = res.profile1_gend_2;
            $scope.profile1_fromwhere_0 = res.profile1_fromwhere_0;
            $scope.profile1_fromwhere_1 = res.profile1_fromwhere_1;
            $scope.profile1_fromwhere_2 = res.profile1_fromwhere_2;
            $scope.profile1_fromwhere_3 = res.profile1_fromwhere_3;
            $scope.profile1_fromwhere_4 = res.profile1_fromwhere_4;
            $scope.profile1_fromwhere_5 = res.profile1_fromwhere_5;
            $scope.profile1_myname = res.profile1_myname;
            $scope.profile1_save_pic_button1 = res.profile1_save_pic_button1;
            $scope.profile1_save_pic_button2 = res.profile1_save_pic_button2;
            $scope.profile1_orderpop_title = res.profile1_orderpop_title;
            $scope.profile1_orderpop_body = res.profile1_orderpop_body;

            $scope.internet_connection_title = res.internet_connection_title;
            $scope.internet_connection_body = res.internet_connection_body;

            $scope.profile1_phonevalid_title = res.profile1_phonevalid_title;
            $scope.profile1_phonevalid_body = res.profile1_phonevalid_body;
            $scope.profile1_phonerepeat_title = res.profile1_phonerepeat_title;
            $scope.profile1_phonerepeat_body = res.profile1_phonerepeat_body;
            $scope.profile1_phonerepeat_button1 = res.profile1_phonerepeat_button1;
            $scope.profile1_phonerepeat_button2 = res.profile1_phonerepeat_button2;
            $scope.profile1_phoneenter_button1 = res.profile1_phoneenter_button1;
            $scope.profile1_phoneenter_button2 = res.profile1_phoneenter_button2;
            $scope.profile1_phoneconf_title = res.profile1_phoneconf_title;
            $scope.profile1_phoneconf_body = res.profile1_phoneconf_body;
            $scope.profile1_discount_title = res.profile1_discount_title;
            $scope.profile1_discount_body = res.profile1_discount_body;
            $scope.profile1_blocked_title = res.profile1_blocked_title;
            $scope.profile1_blocked_body = res.profile1_blocked_body;
            $scope.profile1_unblocked_title = res.profile1_unblocked_title;
            $scope.profile1_unblocked_body = res.profile1_unblocked_body;
            $scope.profile1_wrongcode_title = res.profile1_wrongcode_title;
            $scope.profile1_wrongcode_body = res.profile1_wrongcode_body;
            $scope.profile1_waitconfirm_title = res.profile1_waitconfirm_title;
            $scope.profile1_waitconfirm_body = res.profile1_waitconfirm_body;
            $scope.profile1_whenready_title = res.profile1_whenready_title;
            $scope.profile1_whenready_body = res.profile1_whenready_body;
            $scope.profile1_whenready_button = res.profile1_whenready_button;
            $scope.profile1_shortphone_title1 = res.profile1_shortphone_title1;
            $scope.profile1_shortphone_title2 = res.profile1_shortphone_title2;
            $scope.profile1_shortphone_body = res.profile1_shortphone_body;
            $scope.profile1_shortphone_button = res.profile1_shortphone_button;
            $scope.profile1_nophone_title1 = res.profile1_nophone_title1;
            $scope.profile1_whenready_title = res.profile1_whenready_title;
            $scope.profile1_nophone_title2 = res.profile1_nophone_title2;
            $scope.profile1_nophone_body = res.profile1_nophone_body;
            $scope.profile1_nophone_button = res.profile1_nophone_button;
            $scope.profile1_smsconf_body = res.profile1_smsconf_body;
            $scope.profile1_smsconf_title = res.profile1_smsconf_title;
            $scope.profile1_smsconf_button1 = res.profile1_smsconf_button1;
            $scope.profile1_smsconf_button2 = res.profile1_smsconf_button2;
            $scope.profile1_enter_phonenumber_title = res.profile1_enter_phonenumber_title;
            $scope.profile1_enter_phonenumber_body = res.profile1_enter_phonenumber_body;
            $scope.profile1_discountpop_nonum_title = res.profile1_discountpop_nonum_title;
            $scope.profile1_discountpop_nonum_body = res.profile1_discountpop_nonum_body;
            $scope.profile1_discountpop_nobirth_title = res.profile1_discountpop_nobirth_title;
            $scope.profile1_discountpop_nobirth_body = res.profile1_discountpop_nobirth_body;
            $scope.profile1_discountpop_nogend_title = res.profile1_discountpop_nogend_title;
            $scope.profile1_discountpop_nogend_body = res.profile1_discountpop_nogend_body;
            $scope.profile1_discountpop_nosurname_title = res.profile1_discountpop_nosurname_title;
            $scope.profile1_discountpop_nosurname_body = res.profile1_discountpop_nosurname_body;
            $scope.profile1_discountpop_nofamily_title = res.profile1_discountpop_nofamily_title;
            $scope.profile1_discountpop_nofamily_body = res.profile1_discountpop_nofamily_body;
            $scope.profile1_discountpop_noname_title = res.profile1_discountpop_noname_title;
            $scope.profile1_discountpop_noname_body = res.profile1_discountpop_noname_body;
            $scope.profile1_restore_account_title = res.profile1_restore_account_title;
            $scope.profile1_restore_account_body = res.profile1_restore_account_body;
            $scope.profile1_restore_account_button1 = res.profile1_restore_account_button1;
            $scope.profile1_restore_account_button2 = res.profile1_restore_account_button2;
            $scope.profile1_order_again_title = res.profile1_order_again_title;
            $scope.profile1_order_again_body = res.profile1_order_again_body;
            $scope.profile1_order_again_button1 = res.profile1_order_again_button1;
            $scope.profile1_order_again_button2 = res.profile1_order_again_button2;
            $scope.profile1_data_saved_title = res.profile1_data_saved_title;
            $scope.profile1_data_saved_body = res.profile1_data_saved_body;
            $scope.profile1_repeat_order_title = res.profile1_repeat_order_title;
            $scope.profile1_repeat_order_body = res.profile1_repeat_order_body;

    });

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');

        $scope.usrGender = [{id: 0,name: $scope.profile1_gend_0},{id: 1,name: $scope.profile1_gend_1},{id: 2,name: $scope.profile1_gend_2}];

        $scope.usrFromWhere = [{id: 0,name: $scope.profile1_fromwhere_0},{id: 1,name: $scope.profile1_fromwhere_1},{id: 2,name: $scope.profile1_fromwhere_2},{id: 3,name: $scope.profile1_fromwhere_3},{id: 4,name: $scope.profile1_fromwhere_4},{id: 5,name: $scope.profile1_fromwhere_5}];

        $scope.user.gender = $scope.usrGender[0];
        $scope.user.install_where = $scope.usrFromWhere[0];
        $scope.myName = $scope.profile1_myname;

        var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
        $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {

            $scope.user_real_id = success.rows.item(0).user_real_id;

            if(success.rows.item(0).user_name != '0' && success.rows.item(0).user_name != '') {
                $scope.user.name = HtmlEnt.decodeEntities(success.rows.item(0).user_name);
            }

            if(success.rows.item(0).user_surname != '0' && success.rows.item(0).user_surname != '') {
                $scope.user.surname = HtmlEnt.decodeEntities(success.rows.item(0).user_surname);
            }

            if(success.rows.item(0).user_middlename != '0' && success.rows.item(0).user_middlename != '') {
                $scope.user.middlename = HtmlEnt.decodeEntities(success.rows.item(0).user_middlename);
            }

            if(success.rows.item(0).user_email != '0' && success.rows.item(0).user_email != '') {
                $scope.user.email = HtmlEnt.decodeEntities(success.rows.item(0).user_email);
            }

            if(success.rows.item(0).user_email_confirm == '1') {
                $scope.user.email_confirm = success.rows.item(0).user_email_confirm;
            }

            if(success.rows.item(0).user_tel != '0' && success.rows.item(0).user_tel != '') {
                $scope.user.tel = success.rows.item(0).user_tel;
            }

            if(success.rows.item(0).user_mob_confirm != '') {
                $scope.user.mob_confirm = success.rows.item(0).user_mob_confirm;
                $scope.smsConfirmed = success.rows.item(0).user_mob_confirm;
            }

            if(success.rows.item(0).user_mob != '0' && success.rows.item(0).user_mob != '') {
                $scope.user.mob = success.rows.item(0).user_mob;
            }

            if(success.rows.item(0).user_institution != '0' && success.rows.item(0).user_institution != '') {
              $scope.user.institution = success.rows.item(0).user_institution;
            }

            if(success.rows.item(0).user_pic != '0' && success.rows.item(0).user_pic != '') {
                $scope.user.pic = success.rows.item(0).user_pic;
            }

            if(success.rows.item(0).user_gender != '0' && success.rows.item(0).user_gender != '') {
                $scope.user.gender = $scope.usrGender[success.rows.item(0).user_gender];
            }

            if(success.rows.item(0).user_birthday != '0' && success.rows.item(0).user_birthday != '') {
                
                if(success.rows.item(0).user_birthday != '0000-00-00') {

                    $scope.user.birthday = new Date(success.rows.item(0).user_birthday);

                }
                else {
                    $scope.user.birthday = new Date();
                }

            }

            if(success.rows.item(0).user_adress) {

                if(success.rows.item(0).user_adress != '0' && success.rows.item(0).user_adress != '') {
                    var usradrsplit = success.rows.item(0).user_adress.split("|");
                    if(usradrsplit[0]) {$scope.adr.adress1 = HtmlEnt.decodeEntities(usradrsplit[0]);}
                    if(usradrsplit[1]) {$scope.adr.adress2 = HtmlEnt.decodeEntities(usradrsplit[1]);}
                    if(usradrsplit[2]) {$scope.adr.adress3 = HtmlEnt.decodeEntities(usradrsplit[2]);}
                    if(usradrsplit[3]) {$scope.adr.adress4 = HtmlEnt.decodeEntities(usradrsplit[3]);}
                    if(usradrsplit[4]) {$scope.adr.adress5 = HtmlEnt.decodeEntities(usradrsplit[4]);}
                }

            }

            if(success.rows.item(0).install_where != '0' && success.rows.item(0).install_where != '') {
                $scope.user.install_where = $scope.usrFromWhere[success.rows.item(0).user_install_where];
            }

            if(success.rows.item(0).user_real_id != '0' && success.rows.item(0).user_real_id != '') {
                $scope.myID = success.rows.item(0).user_real_id;
            }

            if(success.rows.item(0).user_name != '0' && success.rows.item(0).user_name != '') {
                $scope.myName = HtmlEnt.decodeEntities(success.rows.item(0).user_name);
            }

            if($scope.user.pic != '' && $scope.user.pic != '0') {
                $scope.myPic = 'http://www.olegtronics.com/admin/img/user/'+$scope.user.institution+'/pic/'+$scope.user.pic;
            }

        }, function() {});

      }, 100);
    });

    $scope.$on('$ionicView.beforeLeave', function(){
        $scope.connectvar = 1;
    });

    $scope.smsOrdered = 0;
    $scope.smsConfirmed = 1;

    if($localStorage.smsOrdered) {
        if($localStorage.smsOrdered == 1) {
            $scope.smsOrdered = 1;
        }
    }
    else {
        $localStorage.smsOrdered = 0;
    }

    $scope.connectvar = 0;

    $scope.intconnect = true;

    $timeout(function() {
       $ionicScrollDelegate.resize();
    }, 2000);

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.user = {
        name: '',
        surname: '',
        middlename: '',
        gender: '',
        birthday: '',
        mob: '',
        tel: '',
        email: '',
        adress: '',
        install_where: ''
    };

    $scope.adr = {
        adress1: '',
        adress2: '',
        adress3: '',
        adress4: '',
        adress5: ''
    };

    $scope.myID = '';
    
    $scope.myPic = 'http://www.olegtronics.com/admin/images/user.png';

    $scope.saving = true;

    $scope.adr.adress1 = '';
    $scope.adr.adress2 = '';
    $scope.adr.adress3 = '';
    $scope.adr.adress4 = '';
    $scope.adr.adress5 = '';
    $scope.user.name = '';
    $scope.user.surname = '';
    $scope.user.middlename = '';
    $scope.user.email = '';
    $scope.user.tel = '';
    $scope.user.mob = '';
    $scope.user.institution = '';
    $scope.user.pic = '';
    $scope.user.mob_confirm = 1;
    $scope.user.email_confirm = 0;

    /*
    $scope.saved = function() {

        $ionicPopup.alert({
            title: "Сохраненно",
            template: "Данные сохранены!"
        });


        $timeout(function() {savedPopup.close();}, 3000);

    } 
    */

    $scope.timecalc = function(x) {
         // ОПРЕДЕЛЕНИЕ ВРЕМЯ
            var nowtime = new Date();
            var gottime = x * 1000;
            var nowtimediff = nowtime.getTime() - gottime;
            var onltime = new Date(gottime);
            var onlDateTime;
            var onlMonth;
            var onlDay;
            var onlHour;
            var onlMin;

            // DAYS AGO
            if(nowtimediff > 86400) {
              if(onltime.getMonth() < 9) {
                  onlMonth = '0' + (onltime.getMonth() + 1);
              } else {
                  onlMonth = onltime.getMonth() + 1;
              }

              if(onltime.getDate() < 10) {
                  onlDay = '0' + onltime.getDate();
              } else {
                  onlDay = onltime.getDate();
              }

              if(onltime.getHours() < 10) {
                  onlHour = '0' + onltime.getHours();
              } else {
                  onlHour = onltime.getHours();
              }
              if(onltime.getMinutes() < 10) {
                  onlMin = '0' + onltime.getMinutes();
              } else {
                  onlMin = onltime.getMinutes();
              }

              onlDateTime = onlDay + '.' + onlMonth + '.' + onltime.getFullYear() + ' ' + onlHour + ':' + onlMin;
            }
            // HOURS AND MINUTES AGO
            else {
              if(onltime.getHours() < 10) {
                  onlHour = '0' + onltime.getHours();
              } else {
                  onlHour = onltime.getHours();
              }
              if(onltime.getMinutes() < 10) {
                  onlMin = '0' + onltime.getMinutes();
              } else {
                  onlMin = onltime.getMinutes();
              }

              onlDateTime =  onlHour + ':' + onlMin;
            }

            return onlDateTime;
    }

    $scope.wallet = 0;

    var queryWallet = "SELECT * FROM wallet WHERE wallet_id != ? AND wallet_del = '0' ORDER BY wallet_id DESC LIMIT 1";
    $cordovaSQLite.execute($rootScope.db, queryWallet, [0]).then(function(suc) {
      if(suc.rows.length > 0) {
        $scope.wallet = suc.rows.item(0).wallet_total;
      }
    }, function() {});

    $scope.birthenable = true;

    var smsConfPopup;

    $scope.orderSMS = function(user, adr) {

        if($cordovaNetwork.isOnline()) {

            $ionicLoading.show({
                content: '<i class="icon ion-loading-c"></i>',
                animation: 'fade-in',
                showBackdrop: false,
                maxWidth: 50,
                showDelay: 0
            });

            // $scope.updateData(user, adr);

            $timeout(function() {

                var smsstr = JSON.stringify({
                    device_id: $rootScope.uuid,
                    inst_id: $scope.user.institution,
                    sms: '0',
                    newusr: 'sms'
                });

                $http.post($rootScope.generalscript, smsstr).then(function(e) {

                    $ionicLoading.hide();

                    var data = e.data;

                    // alert(JSON.stringify(data))

                    if(data[0].smsOK == '2') {

                        $ionicPopup.alert({
                            title: $scope.profile1_phonevalid_title,
                            template: $scope.profile1_phonevalid_body
                        });

                    }
                    else if(data[0].smsOK == '3') {

                        $localStorage.smsOrdered = 1;

                        $scope.smsOrdered = 1;

                    }
                    else if(data[0].smsOK == '4') {
                        $ionicPopup.alert({
                            title: $scope.profile1_phoneenter_button1,
                            template: $scope.profile1_phoneenter_button2
                        });
                    
                    }
                    else if(data[0].smsOK == '5') {

                        var whens = data[0].when;

                        var updateMob = "UPDATE users SET user_mob_confirm = ?, user_upd=? WHERE user_id = ?";
                        $cordovaSQLite.execute($rootScope.db, updateMob, ['1', whens, '1']).then(function() {

                            $scope.user.mob_confirm = 1;

                            var oldUsr = [data[0].oldUsr];

                            if(data[0].oldUsr) {
                                if(typeof data[0].oldUsr !== 'undefined' && data[0].oldUsr !== null && data[0].oldUsr.length > 0) {

                                    // USER DISCOUNT
                                    if(oldUsr[0].usrData.user_discount > '0') {

                                      // alert(oldUsr[0].user_discount);

                                      var usrdiscount = oldUsr[0].usrData.user_discount;

                                      var queryUsrDiscount = "SELECT * FROM users WHERE user_id=? AND user_discount=? AND user_del = '0' LIMIT 1";
                                      $cordovaSQLite.execute($rootScope.db, queryUsrDiscount, [1, usrdiscount]).then(function(suc) {
                                        if(suc.rows.length == 0) {
                                          
                                          var queryUsrDiscountUpd = "UPDATE users SET user_discount=? WHERE user_id=?";
                                          $cordovaSQLite.execute($rootScope.db, queryUsrDiscountUpd, [usrdiscount, 1]).then(function(suc) {

                                            $ionicPopup.alert({
                                                title: $scope.profile1_discount_title,
                                                template: $scope.profile1_discount_body
                                            });

                                          }, function() {});

                                        }
                                      }, function() {});

                                    }

                                    // USER WORK POSITION
                                    if(oldUsr[0].usrData.user_work_pos == '2' || oldUsr[0].usrData.user_work_pos == '3' || oldUsr[0].usrData.user_work_pos == '4') {

                                      // alert(oldUsr[0].user_discount);

                                      var user_work_pos = oldUsr[0].usrData.user_work_pos;

                                      var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? AND user_del = '0' LIMIT 1";
                                      $cordovaSQLite.execute($rootScope.db, queryUsrWP, [1, user_work_pos]).then(function(suc) {
                                        if(suc.rows.length == 0) {
                                          
                                          var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                                          $cordovaSQLite.execute($rootScope.db, queryUsrWPUpd, [user_work_pos, 1]).then(function(suc) {

                                            $ionicPopup.alert({
                                                title: $scope.profile1_blocked_title,
                                                template: $scope.profile1_blocked_body
                                            });

                                          }, function() {});

                                        }
                                      }, function() {});

                                    }
                                    // IF NO CONNECTION TO DB OR SERVER IS DOWN
                                    else if(oldUsr[0].usrData.user_work_pos != '1000') {

                                      var user_work_pos = oldUsr[0].usrData.user_work_pos;

                                      var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? AND user_del = '0' LIMIT 1";
                                      $cordovaSQLite.execute($rootScope.db, queryUsrWP, [1, user_work_pos]).then(function(suc) {
                                        if(suc.rows.length == 0) {
                                          
                                          var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                                          $cordovaSQLite.execute($rootScope.db, queryUsrWPUpd, [user_work_pos, 1]).then(function(suc) {

                                            $ionicPopup.alert({
                                                title: $scope.profile1_unblocked_title,
                                                template: $scope.profile1_unblocked_body
                                            });

                                          }, function() {});

                                        }
                                      }, function() {});

                                    }

                                    // DB POINTS
                                    var pointsArr = oldUsr[0].pointsArr;

                                    if(pointsArr.length > 0) {

                                      var id = 0;

                                      $scope.pointsArrFunc = function(id) {

                                        var points_id = pointsArr[id]['points_id'];
                                        var points_user = pointsArr[id]['points_user'];
                                        var points_bill = pointsArr[id]['points_bill'];
                                        var points_discount = pointsArr[id]['points_discount'];
                                        var points_points = pointsArr[id]['points_points'];
                                        var points_got_spend = pointsArr[id]['points_got_spend'];
                                        var points_waiter = pointsArr[id]['points_waiter'];
                                        var points_institution = pointsArr[id]['points_institution'];
                                        var points_status = pointsArr[id]['points_status'];
                                        var points_comment = pointsArr[id]['points_comment'];
                                        var points_proofed = pointsArr[id]['points_proofed'];
                                        var points_gift = pointsArr[id]['points_gift'];
                                        var points_when = pointsArr[id]['points_when'];
                                        var points_del = pointsArr[id]['points_del'];

                                        var queryPoints = "SELECT * FROM points WHERE points_id = ?";
                                        $cordovaSQLite.execute($rootScope.db, queryPoints, [points_id]).then(function(suc) {
                                          if(suc.rows.length > 0) {
                                            var pointsUpd = "UPDATE points SET points_status=?, points_proofed=?, points_when=?, points_del=? WHERE points_id=?";
                                            $cordovaSQLite.execute($rootScope.db, pointsUpd, [points_status, points_proofed, points_when, points_id, points_del]).then(function() {
                                              id++;
                                              if(id < pointsArr.length) {
                                                $scope.pointsArrFunc(id);
                                              }
                                            }, function() {});
                                          }
                                          else {
                                            var pointsIns = "INSERT INTO points (points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_status, points_comment,  points_proofed, points_gift, points_when, points_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                            $cordovaSQLite.execute($rootScope.db, pointsIns, [points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_status, points_comment,  points_proofed, points_gift, points_when, points_del]).then(function() {
                                              id++;
                                              if(id < pointsArr.length) {
                                                $scope.pointsArrFunc(id);
                                              }
                                            }, function() {});
                                          }
                                        }, function() {});

                                      }
                                      $scope.pointsArrFunc(0);

                                    }

                                    // DB WALLET
                                    var walletArr = oldUsr[0].walletArr;

                                    if(walletArr.length > 0) {

                                        var wallet_user = walletArr[0]['wallet_user'];
                                        var wallet_institution = walletArr[0]['wallet_institution'];
                                        var wallet_total = walletArr[0]['wallet_total'];
                                        var wallet_when = walletArr[0]['wallet_when'];
                                        var wallet_del = walletArr[0]['wallet_del'];

                                        var walletUpd = "UPDATE wallet SET wallet_total=?, wallet_when=?, wallet_del=?";
                                        $cordovaSQLite.execute($rootScope.db, walletUpd, [wallet_total, wallet_when, wallet_del]).then(function() {
                                            Store.addtocart(0);
                                        }, function() {});

                                    }

                                    // DB CHAT
                                    var chatArr = oldUsr[0].chatArr;
                                    
                                    if(chatArr.length > 0) {

                                      var id = 0;

                                      $scope.chatArrFunc = function(id) {

                                        var chat_id = chatArr[id]['chat_id'];
                                        var chat_from = chatArr[id]['chat_from'];
                                        var chat_to = chatArr[id]['chat_to'];
                                        var chat_name = chatArr[id]['chat_name'];
                                        var chat_message = chatArr[id]['chat_message'];
                                        var chat_read = chatArr[id]['chat_read'];
                                        var chat_institution = chatArr[id]['chat_institution'];
                                        var chat_answered = chatArr[id]['chat_answered'];
                                        var chat_when = chatArr[id]['chat_when'];
                                        var chat_del = chatArr[id]['chat_del'];
                              
                                         var queryChat = "SELECT * FROM chat WHERE chat_id = ?";
                                        $cordovaSQLite.execute($rootScope.db, queryChat, [chat_id]).then(function(suc) {
                                        if(suc.rows.length > 0) {
                                            var chatUpd = "UPDATE chat SET chat_read=?, chat_answered=?, chat_when=?, chat_del=? WHERE chat_id=?";
                                            $cordovaSQLite.execute($rootScope.db, chatUpd, [chat_read, chat_answered, chat_when, chat_del, chat_id]).then(function() {
                                              id++;
                                              if(id < chatArr.length) {
                                                $scope.chatArrFunc(id);
                                              }
                                            }, function() {});
                                          }
                                          else {
                                            var chatIns = "INSERT INTO chat (chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                                            $cordovaSQLite.execute($rootScope.db, chatIns, [chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del]).then(function() {
                                              id++;
                                              if(id < chatArr.length) {
                                                $scope.chatArrFunc(id);
                                              }
                                            }, function() {});
                                          }
                                        }, function() {});

                                      }
                                      $scope.chatArrFunc(0);

                                    }

                                    var user_name = oldUsr[0].usrData['user_name'];
                                    var user_surname = oldUsr[0].usrData['user_surname'];
                                    var user_middlename = oldUsr[0].usrData['user_middlename'];
                                    var user_email = oldUsr[0].usrData['user_email'];
                                    var user_email_confirm = oldUsr[0].usrData['user_email_confirm'];
                                    var user_pwd = oldUsr[0].usrData['user_pwd'];
                                    var user_tel = oldUsr[0].usrData['user_tel'];
                                    var user_pic = oldUsr[0].usrData['user_pic'];
                                    var user_gender = oldUsr[0].usrData['user_gender'];
                                    var user_birthday = oldUsr[0].usrData['user_birthday'];
                                    var user_country = oldUsr[0].usrData['user_country'];
                                    var user_region = oldUsr[0].usrData['user_region'];
                                    var user_city = oldUsr[0].usrData['user_city'];
                                    var user_adress = oldUsr[0].usrData['user_adress'];
                                    var user_install_where = oldUsr[0].usrData['user_install_where'];
                                    var user_log_key = oldUsr[0].usrData['user_log_key'];
                                    var user_promo = oldUsr[0].usrData['user_promo'];
                                    var user_del = oldUsr[0].usrData['user_del'];

                                    (user_name != '0') ? $scope.user.name = user_name : $scope.user.name = '';
                                    (user_surname != '0') ? $scope.user.surname = user_surname : $scope.user.surname = '';
                                    (user_middlename != '0') ? $scope.user.middlename = user_middlename : $scope.user.middlename = '';
                                    (user_email != '0') ? $scope.user.email = user_email : $scope.user.email = '';
                                    (user_tel != '0') ? $scope.user.tel = user_tel : $scope.user.tel = '';
                                    (user_pic != '0') ? $scope.user.pic = user_pic : $scope.user.pic = '';

                                    // DB USER
                                    var usrUpd = "UPDATE users SET user_name=?, user_surname=?, user_middlename=?, user_email=?, user_email_confirm=?, user_pwd=?, user_tel=?, user_pic=?, user_gender=?, user_birthday=?, user_country=?, user_region=?, user_city=?, user_adress=?, user_install_where=?, user_log_key=?, user_promo=?, user_del=? WHERE user_id=?";
                                    $cordovaSQLite.execute($rootScope.db, usrUpd, [user_name, user_surname, user_middlename, user_email, user_email_confirm, user_pwd, user_tel, user_pic, user_gender, user_birthday, user_country, user_region, user_city, user_adress, user_install_where, user_log_key, user_promo, user_del, 1]).then(function() {}, function() {});

                                }
                            }

                            var smsConfPop = $ionicPopup.show({
                                template: $scope.profile1_phoneconf_body,
                                title: $scope.profile1_phoneconf_title,
                                scope: $scope,
                                buttons: [
                                  {
                                    text: '<b>'+$scope.profile1_whenready_button+'</b>',
                                    type: 'button-positive',
                                    onTap: function(e) {
                                        smsConfPop.close();
                                        $ionicHistory.nextViewOptions({
                                          disableAnimate: true,
                                          disableBack: true
                                        });
                                        $timeout(function (){
                                             $state.go('app.main');
                                        }, 100);
                                    }
                                  }
                                ]
                            });

                        }, function(er) {});

                    }
                    else if(data[0].smsOK == '6') {

                        $ionicPopup.alert({
                            title: $scope.profile1_wrongcode_title,
                            template: '<b class="assertive">'+$scope.profile1_wrongcode_body+'</b>'
                        });

                    }

                }, function() {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: $scope.internet_connection_title,
                        template: $scope.internet_connection_body
                    });
                });

            }, 1000);

        } else {
            $ionicPopup.alert({
                title: $scope.internet_connection_title,
                template: $scope.internet_connection_body
            });
        }

    }

    $scope.confirmSMS = function() {

        // alert('Confirm: '+JSON.stringify(user));
        
        var confstr = JSON.stringify({
            device_id: $rootScope.uuid,
            inst_id: $scope.user.institution,
            sms: $scope.user.codes,
            newusr: 'sms'
        });

        $http.post($rootScope.generalscript, confstr).then(function(e) {

            var data = e.data;

            // console.log(JSON.stringify(data));

            if(data[0].smsOK == '5') {

                $scope.user.mob_confirm = 1;
                $scope.smsConfirmed = 1;

                var whens = data[0].when;

                var updateMob = "UPDATE users SET user_mob_confirm = ?, user_upd=? WHERE user_id = ?";
                $cordovaSQLite.execute($rootScope.db, updateMob, ['1', whens, '1']).then(function() {

                    var oldUsr = [data[0].oldUsr];

                    if(data[0].oldUsr) {
                        if(oldUsr.length > 0) {
                            if(typeof data[0].oldUsr !== 'undefined' && data[0].oldUsr !== null) {
                                
                                // USER DISCOUNT
                                if(oldUsr[0].usrData.user_discount) {
                                    if(oldUsr[0].usrData.user_discount > '0') {

                                      // alert(oldUsr[0].user_discount);

                                      var usrdiscount = oldUsr[0].usrData.user_discount;

                                      var queryUsrDiscount = "SELECT * FROM users WHERE user_id=? AND user_discount=? AND user_del='0' LIMIT 1";
                                      $cordovaSQLite.execute($rootScope.db, queryUsrDiscount, [1, usrdiscount]).then(function(suc) {
                                        if(suc.rows.length == 0) {
                                          
                                          var queryUsrDiscountUpd = "UPDATE users SET user_discount=? WHERE user_id=?";
                                          $cordovaSQLite.execute($rootScope.db, queryUsrDiscountUpd, [usrdiscount, 1]).then(function(suc) {

                                            $ionicPopup.alert({
                                                title: $scope.profile1_discount_title,
                                                template: $scope.profile1_discount_body
                                            });

                                          }, function() {});

                                        }
                                      }, function() {});

                                    }
                                }

                                // USER WORK POSITION
                                if(oldUsr[0].usrData.user_work_pos) {
                                    if(oldUsr[0].usrData.user_work_pos == '2' || oldUsr[0].usrData.user_work_pos == '3' || oldUsr[0].usrData.user_work_pos == '4') {

                                      // alert(oldUsr[0].user_discount);

                                      var user_work_pos = oldUsr[0].usrData.user_work_pos;

                                      var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? AND user_del='0' LIMIT 1";
                                      $cordovaSQLite.execute($rootScope.db, queryUsrWP, [1, user_work_pos]).then(function(suc) {
                                        if(suc.rows.length == 0) {
                                          
                                          var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                                          $cordovaSQLite.execute($rootScope.db, queryUsrWPUpd, [user_work_pos, 1]).then(function(suc) {

                                            $ionicPopup.alert({
                                                title: $scope.profile1_blocked_title,
                                                template: $scope.profile1_blocked_body
                                            });

                                          }, function() {});

                                        }
                                      }, function() {});

                                    }
                                    // IF NO CONNECTION TO DB OR SERVER IS DOWN
                                    else if(oldUsr[0].usrData.user_work_pos != '1000') {

                                      var user_work_pos = oldUsr[0].usrData.user_work_pos;

                                      var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? AND user_del='0' LIMIT 1";
                                      $cordovaSQLite.execute($rootScope.db, queryUsrWP, [1, user_work_pos]).then(function(suc) {
                                        if(suc.rows.length == 0) {
                                          
                                          var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                                          $cordovaSQLite.execute($rootScope.db, queryUsrWPUpd, [user_work_pos, 1]).then(function(suc) {

                                            $ionicPopup.alert({
                                                title: $scope.profile1_unblocked_title,
                                                template: $scope.profile1_unblocked_body
                                            });

                                          }, function() {});

                                        }
                                      }, function() {});

                                    }
                                }

                                // DB POINTS
                                var pointsArr = oldUsr[0].pointsArr;

                                if(pointsArr.length > 0) {

                                  var id = 0;

                                  $scope.pointsArrFunc = function(id) {

                                    var points_id = pointsArr[id]['points_id'];
                                    var points_user = pointsArr[id]['points_user'];
                                    var points_bill = pointsArr[id]['points_bill'];
                                    var points_discount = pointsArr[id]['points_discount'];
                                    var points_points = pointsArr[id]['points_points'];
                                    var points_got_spend = pointsArr[id]['points_got_spend'];
                                    var points_waiter = pointsArr[id]['points_waiter'];
                                    var points_institution = pointsArr[id]['points_institution'];
                                    var points_status = pointsArr[id]['points_status'];
                                    var points_comment = pointsArr[id]['points_comment'];
                                    var points_proofed = pointsArr[id]['points_proofed'];
                                    var points_gift = pointsArr[id]['points_gift'];
                                    var points_when = pointsArr[id]['points_when'];
                                    var points_del = pointsArr[id]['points_del'];

                                    var queryPoints = "SELECT * FROM points WHERE points_id = ?";
                                    $cordovaSQLite.execute($rootScope.db, queryPoints, [points_id]).then(function(suc) {
                                      if(suc.rows.length > 0) {
                                        var pointsUpd = "UPDATE points SET points_status=?, points_proofed=?, points_when=?, points_del=? WHERE points_id=?";
                                        $cordovaSQLite.execute($rootScope.db, pointsUpd, [points_status, points_proofed, points_when, points_del, points_id]).then(function() {
                                          id++;
                                          if(id < pointsArr.length) {
                                            $scope.pointsArrFunc(id);
                                          }
                                        }, function() {});
                                      }
                                      else {
                                        var pointsIns = "INSERT INTO points (points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_status, points_comment,  points_proofed, points_gift, points_when, points_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                        $cordovaSQLite.execute($rootScope.db, pointsIns, [points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_status, points_comment,  points_proofed, points_gift, points_when, points_del]).then(function() {
                                          id++;
                                          if(id < pointsArr.length) {
                                            $scope.pointsArrFunc(id);
                                          }
                                        }, function() {});
                                      }
                                    }, function() {});

                                  }
                                  $scope.pointsArrFunc(0);

                                }

                                // DB WALLET
                                var walletArr = oldUsr[0].walletArr;

                                if(walletArr.length > 0) {

                                    var wallet_user = walletArr[0]['wallet_user'];
                                    var wallet_institution = walletArr[0]['wallet_institution'];
                                    var wallet_total = walletArr[0]['wallet_total'];
                                    var wallet_when = walletArr[0]['wallet_when'];
                                    var wallet_del = walletArr[0]['wallet_del'];

                                    var walletUpd = "UPDATE wallet SET wallet_total=?, wallet_when=?, wallet_del=?";
                                    $cordovaSQLite.execute($rootScope.db, walletUpd, [wallet_total, wallet_when, wallet_del]).then(function() {
                                        Store.addtocart(0);
                                    }, function() {});

                                }

                                // DB CHAT
                                var chatArr = oldUsr[0].chatArr;
                                
                                if(chatArr.length > 0) {

                                  var id = 0;

                                  $scope.chatArrFunc = function(id) {

                                    var chat_id = chatArr[id]['chat_id'];
                                    var chat_from = chatArr[id]['chat_from'];
                                    var chat_to = chatArr[id]['chat_to'];
                                    var chat_name = chatArr[id]['chat_name'];
                                    var chat_message = chatArr[id]['chat_message'];
                                    var chat_read = chatArr[id]['chat_read'];
                                    var chat_institution = chatArr[id]['chat_institution'];
                                    var chat_answered = chatArr[id]['chat_answered'];
                                    var chat_when = chatArr[id]['chat_when'];
                                    var chat_del = chatArr[id]['chat_del'];
                          
                                     var queryChat = "SELECT * FROM chat WHERE chat_id = ?";
                                    $cordovaSQLite.execute($rootScope.db, queryChat, [chat_id]).then(function(suc) {
                                    if(suc.rows.length > 0) {
                                        var chatUpd = "UPDATE chat SET chat_read=?, chat_answered=?, chat_when=?, chat_del=? WHERE chat_id=?";
                                        $cordovaSQLite.execute($rootScope.db, chatUpd, [chat_read, chat_answered, chat_when, chat_del, chat_id]).then(function() {
                                          id++;
                                          if(id < chatArr.length) {
                                            $scope.chatArrFunc(id);
                                          }
                                        }, function() {});
                                      }
                                      else {
                                        var chatIns = "INSERT INTO chat (chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                                        $cordovaSQLite.execute($rootScope.db, chatIns, [chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del]).then(function() {
                                          id++;
                                          if(id < chatArr.length) {
                                            $scope.chatArrFunc(id);
                                          }
                                        }, function() {});
                                      }
                                    }, function() {});

                                  }
                                  $scope.chatArrFunc(0);

                                }

                                var user_name = oldUsr[0].usrData['user_name'];
                                var user_surname = oldUsr[0].usrData['user_surname'];
                                var user_middlename = oldUsr[0].usrData['user_middlename'];
                                var user_email = oldUsr[0].usrData['user_email'];
                                var user_email_confirm = oldUsr[0].usrData['user_email_confirm'];
                                var user_pwd = oldUsr[0].usrData['user_pwd'];
                                var user_tel = oldUsr[0].usrData['user_tel'];
                                var user_pic = oldUsr[0].usrData['user_pic'];
                                var user_gender = oldUsr[0].usrData['user_gender'];
                                var user_birthday = oldUsr[0].usrData['user_birthday'];
                                var user_country = oldUsr[0].usrData['user_country'];
                                var user_region = oldUsr[0].usrData['user_region'];
                                var user_city = oldUsr[0].usrData['user_city'];
                                var user_adress = oldUsr[0].usrData['user_adress'];
                                var user_install_where = oldUsr[0].usrData['user_install_where'];
                                var user_log_key = oldUsr[0].usrData['user_log_key'];
                                var user_promo = oldUsr[0].usrData['user_promo'];
                                var user_del = oldUsr[0].usrData['user_del'];

                                (user_name != '0') ? $scope.user.name = user_name : $scope.user.name = '';
                                (user_surname != '0') ? $scope.user.surname = user_surname : $scope.user.surname = '';
                                (user_middlename != '0') ? $scope.user.middlename = user_middlename : $scope.user.middlename = '';
                                (user_email != '0') ? $scope.user.email = user_email : $scope.user.email = '';
                                (user_tel != '0') ? $scope.user.tel = user_tel : $scope.user.tel = '';
                                (user_pic != '0') ? $scope.user.pic = user_pic : $scope.user.pic = '';

                                // DB USER
                                var usrUpd = "UPDATE users SET user_name=?, user_surname=?, user_middlename=?, user_email=?, user_email_confirm=?, user_pwd=?, user_tel=?, user_pic=?, user_gender=?, user_birthday=?, user_country=?, user_region=?, user_city=?, user_adress=?, user_install_where=?, user_log_key=?, user_promo=?, user_del=? WHERE user_id=?";
                                $cordovaSQLite.execute($rootScope.db, usrUpd, [user_name, user_surname, user_middlename, user_email, user_email_confirm, user_pwd, user_tel, user_pic, user_gender, user_birthday, user_country, user_region, user_city, user_adress, user_install_where, user_log_key, user_promo, user_del, 1]).then(function() {}, function(er) {});

                            }
                        }
                    }

                    var smsConfPop = $ionicPopup.show({
                        template: $scope.profile1_phoneconf_body,
                        title: $scope.profile1_phoneconf_title,
                        scope: $scope,
                        buttons: [
                          {
                            text: '<b>'+$scope.profile1_whenready_button+'</b>',
                            type: 'button-positive',
                            onTap: function(e) {
                                smsConfPop.close();
                                // $ionicHistory.nextViewOptions({
                                //   disableAnimate: true,
                                //   disableBack: true
                                // });
                                // $timeout(function (){
                                //      $state.go('app.main');
                                // }, 100);
                            }
                          }
                        ]
                    });

                }, function(er) {});

            }
            else if(data[0].smsOK == '6') {

                $ionicPopup.alert({
                    title: $scope.profile1_wrongcode_title,
                    template: '<b class="assertive">'+$scope.profile1_wrongcode_body+'</b>'
                });

            }

        }, function() {});

    }

    $scope.orderDiscount = function(user, adr) {

        var mobconf = document.getElementById('mobconf');

        if(mobconf.length > 0) {

            if(mobconf.value.length >= 10) {

                $scope.updateData(user, adr);

                $timeout(function() {
                    
                    var discstr = JSON.stringify({
                        device_id: $rootScope.uuid,
                        inst_id: $scope.user.institution,
                        discount: 'req',
                        newusr: 'discount'
                    });

                    $http.post($rootScope.generalscript, discstr).then(function(e) {

                        var data = e.data;

                        // alert(JSON.stringify(data))

                        if(data[0].discountOK == '4') {

                            $ionicPopup.alert({
                                title: $scope.profile1_waitconfirm_title,
                                template: $scope.profile1_waitconfirm_body
                            });

                        }
                        else if(data[0].discountOK == '3') {

                            var discountReqPopup = $ionicPopup.show({
                                template: $scope.profile1_whenready_body,
                                title: $scope.profile1_whenready_title,
                                scope: $scope,
                                buttons: [
                                  {
                                    text: '<b>'+$scope.profile1_whenready_button+'</b>',
                                    type: 'button-positive',
                                    onTap: function(e) {
                                      discountReqPopup.close();
                                    }
                                  }
                                ]
                            });

                        }

                    }, function() {});

                }, 1000);

            }
            else {

                var lengthPopup = $ionicPopup.show({
                    template: $scope.profile1_shortphone_title1+'<br/>'+$scope.profile1_shortphone_title2,
                    title: $scope.profile1_shortphone_body,
                    scope: $scope,
                    buttons: [
                      {
                        text: '<b>'+$scope.profile1_shortphone_button+'</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                          lengthPopup.close();
                        }
                      }
                    ]
                });

            }

        }
        else {

            var notPopup = $ionicPopup.show({
                    template: $scope.profile1_nophone_title1+'<br/>'+$scope.profile1_nophone_title2,
                    title: $scope.profile1_nophone_body,
                    scope: $scope,
                    buttons: [
                      {
                        text: '<b>'+$scope.profile1_nophone_button+'</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                          notPopup.close();
                        }
                      }
                    ]
                });

        }

    }

    $scope.confirmTel = function(user, adr) {

        if(user.mob) {

            var mobconf = document.getElementById('mobconf');

            if(mobconf.value.length >= 10) {

                if($cordovaNetwork.isOnline()) {

                    var submitPopup = $ionicPopup.show({
                        template: $scope.profile1_smsconf_body,
                        title: $scope.profile1_smsconf_title,
                        subtitle: '',
                        scope: $scope,
                        buttons: [
                            { text: $scope.profile1_smsconf_button1,
                                type: 'button-assertive',
                                onTap: function() {
                                    submitPopup.close();
                                }
                            },
                            {
                                text: '<b>'+$scope.profile1_smsconf_button2+'</b>',
                                type: 'button-balanced',
                                onTap: function(e) {
                                    $timeout(function() {$scope.orderSMS(user, adr);}, 500);
                                    submitPopup.close();
                                }
                            }
                        ]
                    });
                    
                    $scope.updateData(user, adr);

                }
                else {

                    $ionicPopup.alert({
                        title: $scope.internet_connection_title,
                        template: $scope.internet_connection_body
                    });

                }

            }
            else {

                var lengthPopup = $ionicPopup.show({
                    template: $scope.profile1_shortphone_title1+'<br/>'+$scope.profile1_shortphone_title2,
                    title: $scope.profile1_shortphone_body,
                    scope: $scope,
                    buttons: [
                      {
                        text: '<b>'+$scope.profile1_shortphone_button+'</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                          lengthPopup.close();
                        }
                      }
                    ]
                });

            }

        }
        else {
            $ionicPopup.alert({
                title: $scope.profile1_enter_phonenumber_title,
                template: $scope.profile1_enter_phonenumber_body
            });
        }

    }

    $scope.getDiscount = function(user, adr) {

        // if($cordovaNetwork.isOnline()) {

            if(user.name) {
                if(user.surname) {
                    if(user.middlename) {
                        if(user.gender) {
                            if(user.birthday) {
                                if(user.mob) {

                                    if($scope.user.mob_confirm == '1') {

                                        $scope.orderDiscount(user, adr);

                                    }
                                    else {

                                        $scope.confirmTel(user, adr);

                                    }

                                }
                                else {
                                    $ionicPopup.alert({
                                        title: $scope.profile1_discountpop_nonum_title,
                                        template: $scope.profile1_discountpop_nonum_body
                                    });
                                }
                            }
                            else {
                                $ionicPopup.alert({
                                    title: $scope.profile1_discountpop_nobirth_title,
                                    template: $scope.profile1_discountpop_nobirth_body
                                });
                            }
                        }
                        else {
                            $ionicPopup.alert({
                                title: $scope.profile1_discountpop_nogend_title,
                                template: $scope.profile1_discountpop_nogend_body
                            });
                        }
                    }
                    else {
                        $ionicPopup.alert({
                            title: $scope.profile1_discountpop_nosurname_title,
                            template: $scope.profile1_discountpop_nosurname_body
                        });
                    }
                }
                else {
                    $ionicPopup.alert({
                        title: $scope.profile1_discountpop_nofamily_title,
                        template: $scope.profile1_discountpop_nofamily_body
                    });
                }
            }
            else {
                $ionicPopup.alert({
                    title: $scope.profile1_discountpop_noname_title,
                    template: $scope.profile1_discountpop_noname_body
                });
            }

        // } else {
        //     $ionicPopup.alert({
        //         title: $scope.internet_connection_title,
        //         template: $scope.internet_connection_body
        //     });
        // }

    };

    $scope.updateData = function(user, adr) {

        if($cordovaNetwork.isOnline()) {

            var useradress = adr.adress1 + '|' + adr.adress2 + '|' + adr.adress3 + '|' + adr.adress4 + '|' + adr.adress5;
            
            var updstr = JSON.stringify({
                device: $rootScope.model,
                device_id: $rootScope.uuid,
                device_version: $rootScope.version,
                device_os: $rootScope.platform,
                user_name: (user.name != '' && user.name != 0) ? user.name : "0",
                user_surname: (user.surname != '' && user.surname != 0) ? user.surname : "0",
                user_middlename: (user.middlename != '' && user.middlename != 0) ? user.middlename : "0",
                user_email: (user.email != '' && user.email != 0) ? user.email : "0",
                user_tel: (user.tel != '' && user.tel != 0) ? user.tel : "0",
                user_mob: (user.mob != '' && user.mob != 0) ? user.mob : "0",
                user_gender: (user.gender['id'] != '' && user.gender['id'] != 0) ? user.gender['id'] : "0",
                user_birthday: user.birthday,
                user_country: 0,
                user_region: 0,
                user_city: 0,
                user_adress: (useradress != '' && useradress != 0) ? useradress : "0",
                user_install_where: (user.install_where['id'] != '' && user.install_where['id'] != 0) ? user.install_where['id'] : "0",
                inst_id: $scope.user.institution,
                newusr: 'upd'
            });

            $http.post($rootScope.generalscript, updstr).then(function(e) {

                var data = e.data;

                if(data[0].upd == '1') {

                    $scope.saving = false;

                    $timeout(function() {$scope.saving = true;}, 2000);

                    var updateBio = "UPDATE users SET user_name=?, user_surname=?, user_middlename=?, user_email=?, user_tel=?, user_mob=?, user_gender=?, user_birthday=?, user_adress=?, user_install_where=?, user_device=?, user_device_id=?, user_device_version=?, user_device_os=?, user_upd=? WHERE user_id = ?";
                    $cordovaSQLite.execute($rootScope.db, updateBio, [(user.name != '' && user.name != 0) ? user.name : "0", (user.surname != '' && user.surname != 0) ? user.surname : "0", (user.middlename != '' && user.middlename != 0) ? user.middlename : "0", (user.email != '' && user.email != 0) ? user.email : "0", (user.tel != '' && user.tel != 0) ? user.tel : "0", (user.mob != '' && user.mob != 0) ? user.mob : "0", (user.gender['id'] != '' && user.gender['id'] != 0) ? user.gender['id'] : "0", user.birthday, (useradress != '' && useradress != 0) ? useradress : "0", (user.install_where['id'] != '' && user.install_where['id'] != 0) ? user.install_where['id'] : "0", $rootScope.model, $rootScope.uuid, $rootScope.version, $rootScope.platform, data[0].when, 1]).then(function() {}, function(er) {});

                }

            }, function() {});

        } else {
            $ionicPopup.alert({
                title: $scope.internet_connection_title,
                template: $scope.internet_connection_body
            });
        }

    }

    $scope.dev_height = $window.innerHeight / 2.5;

    $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center'};

    $scope.acceptStyle = function() {
        $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};
    }

    $scope.onlyNumbers = /^[0-9]+$/;

    $scope.restoreAccount = function() {

        $scope.restorePopup = $ionicPopup.show({
            title: $scope.profile1_restore_account_title,
            template: '<p class="padding">'+$scope.profile1_restore_account_body+'</p><input placeholder="email@email.com" style="width:100%;border_bottom:1px solid #ff0000;">',
            scope: $scope,
            buttons: [
                { text: '<b>'+$scope.profile1_restore_account_button1+'</b>',
                    onTap: function() {
                        $scope.restorePopup.close();
                    }},
                { text: '<b>'+$scope.profile1_restore_account_button2+'</b>',
                    onTap: function(e) {
                        $scope.restorePopup.close();
                    }}
            ]
        });

    }

    $scope.orderAgain = function() {

        $scope.orderAgainPopup = $ionicPopup.show({
            title: $scope.profile1_order_again_title,
            template: '<p class="padding">'+$scope.profile1_order_again_body+'</p>',
            scope: $scope,
            buttons: [
                { text: '<b>'+$scope.profile1_order_again_button1+'</b>',
                    onTap: function() {
                        $scope.orderAgainPopup.close();
                    }},
                { text: '<b>'+$scope.profile1_order_again_button2+'</b>',
                    onTap: function(e) {
                        $scope.orderAgainPopup.close();
                    }}
            ]
        });

    }

    $scope.goBackOne = function() {
        $ionicHistory.goBack();
    }

    $scope.saved = function() {
        // if($scope.user.mob.length == 0 || $scope.user.mob.length >= 10) {
            $ionicPopup.alert({
                title: $scope.profile1_data_saved_title,
                template: $scope.profile1_data_saved_body
            });
        // }
    }

    $scope.repeat = function() {
        $ionicPopup.alert({
            title: $scope.profile1_repeat_order_title,
            template: $scope.profile1_repeat_order_body
        });
    }

    $scope.htmlEntities = function(x) {
        return HtmlEnt.decodeEntities(x);
    }

})

.controller('ContactCtrl', function($rootScope, $scope, $timeout, $stateParams, $ionicPlatform, $window, $ionicNavBarDelegate, $translate, $cordovaGeolocation, $ionicSideMenuDelegate, ionicMaterialInk) {

    $translate(['org_name']).then(function(loadingmes) {

        $scope.org_name = loadingmes.org_name;

        // $scope.languages = LanguageFac.all();
        // $scope.genders = [{id:1, name: $scope.male_mes}, {id:2, name: $scope.female_mes}];
        // $scope.ages = AgeFac.all();

    });

    function yainit() {

        var myMap = new ymaps.Map('map', {
            center: [53.9019, 27.5553],
            zoom: 15
        });

        myMap.controls.add('smallZoomControl', { right: 15, top: 50 });

        // Создаем многоугольник
       // var myPolygon = new ymaps.Polygon([[
       //      [53.8595, 27.6819],
       //      [53.8692, 27.6472],
       //      [53.8807, 27.6563],
       //      [53.9037, 27.6359],
       //      [53.9070, 27.6305],
       //      [53.9071, 27.6132],
       //      [53.9153, 27.6040],
       //      [53.9231, 27.6003],
       //      [53.9247, 27.5982],
       //      [53.9315, 27.5766],
       //      [53.9320, 27.5486],
       //      [53.9298, 27.5342],
       //      [53.9287, 27.5108],
       //      [53.9280, 27.5084],
       //      [53.9167, 27.4952],
       //      [53.8952, 27.4977],
       //      [53.8835, 27.5050],
       //      [53.8801, 27.4995],
       //      [53.8687, 27.4910],
       //      [53.8574, 27.4778],
       //      [53.8425, 27.4697],
       //      [53.8352, 27.5333],
       //      [53.8328, 27.5554],
       //      [53.8337, 27.6422],
       //      [53.8360, 27.6544],
       //      [53.8407, 27.6634],
       //      [53.8595, 27.6819]
       //  ]]);

       //  myMap.geoObjects.add(myPolygon);

        var rest1 = new ymaps.GeoObject({
        geometry: {type: "Point", coordinates: [53.9077, 27.5847]},
        properties: {iconContent: '<div style="background-color:#fff;"><img src="img/sign.png" width="120px"></div><p style="font-size:10px; text-align:center;color:#000;">ул. Козлова, 8</p>', balloonContent: '<strong>Штолле</strong><br>пн-сб: 10.00 - 21.00<br>вс: 10.00 - 20.00<br>тел. +375291624443'}}, {preset: 'twirl#redStretchyIcon', draggable: false});
    
        var rest2 = new ymaps.GeoObject({
        geometry: {type: "Point", coordinates: [53.9050, 27.5465]},
        properties: {iconContent: '<div style="background-color:#fff;"><img src="img/sign.png" width="120px"></div><p style="font-size:10px; text-align:center;color:#000;">ул.Раковская, 23</p>', balloonContent: '<strong>Штолле</strong><br>пн-вс: 10.00-23.00<br>тел. +375447508555'}}, {preset: 'twirl#redStretchyIcon', draggable: false});

        var rest3 = new ymaps.GeoObject({
        geometry: {type: "Point", coordinates: [53.9178, 27.5869]},
        properties: {iconContent: '<div style="background-color:#fff;"><img src="img/sign.png" width="120px"></div><p style="font-size:10px; text-align:center;color:#000;">пр. Независимости, 53</p>', balloonContent: '<strong>Штолле</strong><br>пн-вс: 10.00-23.00<br>тел. +375293317777'}}, {preset: 'twirl#redStretchyIcon', draggable: false});

        var rest4 = new ymaps.GeoObject({
        geometry: {type: "Point", coordinates: [53.9039, 27.5587]},
        properties: {iconContent: '<div style="background-color:#fff;"><img src="img/sign.png" width="120px"></div><p style="font-size:10px; text-align:center;color:#000;">ул.Интернациональная,23</p>', balloonContent: '<strong>Штолле</strong><br>пн-сб: 10.00 - 23.00<br>тел. +375447710051'}}, {preset: 'twirl#redStretchyIcon', draggable: false});

        var rest5 = new ymaps.GeoObject({
        geometry: {type: "Point", coordinates: [53.9078, 27.5758]},
        properties: {iconContent: '<div style="background-color:#fff;"><img src="img/sign.png" width="120px"></div><p style="font-size:10px; text-align:center;color:#000;">пр.Независимости,38</p>', balloonContent: '<strong>Штолле</strong><br>пн-вс: 08.00-21.00<br>тел. +375447212020'}}, {preset: 'twirl#redStretchyIcon', draggable: false});

        var rest6 = new ymaps.GeoObject({
        geometry: {type: "Point", coordinates: [53.8936, 27.5536]},
        properties: {iconContent: '<div style="background-color:#fff;"><img src="img/sign.png" width="120px"></div><p style="font-size:10px; text-align:center;color:#000;">ул. Свердлова,22</p>', balloonContent: '<strong>Штолле</strong><br>пн-сб: 10.00 - 22.00<br>тел. +375296779012'}}, {preset: 'twirl#redStretchyIcon', draggable: false});

        var rest7 = new ymaps.GeoObject({
        geometry: {type: "Point", coordinates: [53.8864, 27.5369]},
        properties: {iconContent: '<div style="background-color:#fff;"><img src="img/sign.png" width="120px"></div><p style="font-size:10px; text-align:center;color:#000;">ул. Московская,22 (ст.метро "Институт Культуры")</p>', balloonContent: '<strong>Штолле</strong><br>пн-вс: 09.00 - 21.00<br>тел. +375293223340'}}, {preset: 'twirl#redStretchyIcon', draggable: false});

        var rest8 = new ymaps.GeoObject({
        geometry: {type: "Point", coordinates: [53.9197, 27.5768]},
        properties: {iconContent: '<div style="background-color:#fff;"><img src="img/sign.png" width="120px"></div><p style="font-size:10px; text-align:center;color:#000;">ул. В. Хоружей,8 (возле Комаровского рынка)</p>', balloonContent: '<strong>Штолле</strong><br>пн-сб: 09.00 - 21.00<br>тел. +375296778868'}}, {preset: 'twirl#redStretchyIcon', draggable: false});

        var rest9 = new ymaps.GeoObject({
        geometry: {type: "Point", coordinates: [53.8982, 27.5586]},
        properties: {iconContent: '<div style="background-color:#fff;"><img src="img/sign.png" width="120px"></div><p style="font-size:10px; text-align:center;color:#000;">ул. К. Маркса, 20</p>', balloonContent: '<strong>Штолле</strong><br>пн-сб: 09.00 - 22.00<br>тел. +375296555350'}}, {preset: 'twirl#redStretchyIcon', draggable: false});

        var rest10 = new ymaps.GeoObject({
        geometry: {type: "Point", coordinates: [53.8997, 27.5567]},
        properties: {iconContent: '<div style="background-color:#fff;"><img src="img/sign.png" width="120px"></div><p style="font-size:10px; text-align:center;color:#000;">пр. Независимоcти, 19</p>', balloonContent: '<strong>Штолле</strong><br>пн-вс: 09.00 - 22.00<br>тел. +375296098889'}}, {preset: 'twirl#redStretchyIcon', draggable: false});

        var rest11 = new ymaps.GeoObject({
        geometry: {type: "Point", coordinates: [53.9288, 27.5879]},
        properties: {iconContent: '<div style="background-color:#fff;"><img src="img/sign.png" width="120px"></div><p style="font-size:10px; text-align:center;color:#000;">ул. Сурганова, 50</p>', balloonContent: '<strong>Штолле</strong><br>пн-вс: 09.00 - 22.00<br>тел. +375293254090'}}, {preset: 'twirl#redStretchyIcon', draggable: false});

        // Добавляем многоугольник на карту.
        myMap.geoObjects.add(rest1);
        myMap.geoObjects.add(rest2);
        myMap.geoObjects.add(rest3);
        myMap.geoObjects.add(rest4);
        myMap.geoObjects.add(rest5);
        myMap.geoObjects.add(rest6);
        myMap.geoObjects.add(rest7);
        myMap.geoObjects.add(rest8);
        myMap.geoObjects.add(rest9);
        myMap.geoObjects.add(rest10);
        myMap.geoObjects.add(rest11);

        // myMap.balloon.open([53.8627, 27.5537], "Зона доставки", {
        //     closeButton: false
        // });

        // myPlacemark.balloon.open();

        var posOptions = {timeout: 10000, enableHighAccuracy: false};
        $cordovaGeolocation.getCurrentPosition(posOptions).then(
        function(position) {
            myMap.setCenter([position.coords.latitude, position.coords.longitude], 15);
        }, function() {});

    }

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
    
        $ionicSideMenuDelegate.canDragContent(false);

        $timeout(function() {
            $ionicNavBarDelegate.align('center');

            // if($cordovaNetwork.isOnline()) {
                $timeout(function() {
                    ymaps.ready(yainit);
                }, 300);
            // }

        }, 100);

    });
    
    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.dev_height = $window.innerHeight;

    $scope.myStyle = {'height': $scope.dev_height+'px', 'width': '100%'};

})

.controller('Contact2Ctrl', function($rootScope, $scope, $window, $stateParams, $ionicPlatform, $state, $timeout, $cordovaFile, $cordovaFileTransfer, $localStorage, $cordovaSQLite, $ionicScrollDelegate, $ionicNavBarDelegate, $translate, $ionicModal, ionicMaterialInk, ionicMaterialMotion, Store, HtmlEnt) {

    $scope.$on('$ionicView.enter', function(){
        $timeout(function() {
            $ionicNavBarDelegate.align('center');
        });
    });

    $translate(['store_code_terms_top', 'store_code_terms_body', 'store_code_terms_button']).then(function(res) {

        $scope.store_code_terms_top = res.store_code_terms_top;
        $scope.store_code_terms_body = res.store_code_terms_body;
        $scope.store_code_terms_button = res.store_code_terms_button;
        
    });

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
        $ionicNavBarDelegate.align('center');
    });

    $timeout(function() {

        var maxHeight = 0;
        var elements = document.getElementsByClassName("equalex");
        for (var i=0; i<elements.length; i++) {
            if (elements[i].offsetHeight > maxHeight) { maxHeight = elements[i].offsetHeight; }
        }
        $scope.heightDiv = {'height': maxHeight+'px'};

       $ionicScrollDelegate.resize();

    }, 1000);

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.dev_height = $window.innerHeight / 3.8;

    $scope.myStyle = {'height': $scope.dev_height+'px'};

})

.controller('SupportCtrl', function($rootScope, $scope, $stateParams, $window, $timeout, $ionicPopup, ionicMaterialMotion, $ionicPlatform, $ionicNavBarDelegate, $cordovaSQLite, $ionicScrollDelegate, $translate, $http, ionicMaterialInk, HtmlEnt) {

    $translate(['internet_connection_title', 'internet_connection_body', 'support_first_message', 'support_request_discount_name', 'support_request_discount_message', 'support_pop_title', 'support_pop_body', 'support_pop_button1', 'support_pop_button2']).then(function(res) {


            $scope.internet_connection_title = res.internet_connection_title;
            $scope.internet_connection_body = res.internet_connection_body;
            $scope.support_first_message = res.support_first_message;
            $scope.support_request_discount_name = res.support_request_discount_name;
            $scope.support_request_discount_message = res.support_request_discount_message;
            $scope.support_pop_title = res.support_pop_title;
            $scope.support_pop_body = res.support_pop_body;
            $scope.support_pop_button1 = res.support_pop_button1;
            $scope.support_pop_button2 = res.support_pop_button2;

    });

    $scope.$on("$ionicView.leave", function(event, data){
       
        // clearInterval(chatInter);
        $timeout.cancel(chatInter);
       
    });

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');

        $scope.messages = [{
            chat_message: $scope.support_first_message,
            chat_from: '1'
        }];
        $scope.data = [{
            message: ''
        }];

        $scope.getMessages();

      }, 500);

      $scope.canmessage = true;


    });

    $scope.$on('$ionicView.beforeLeave', function(){
        $scope.connectvar = 1;
    });

    var chatInter;

    $scope.connectvar = 0;

    $scope.canmessage = true;

    var versionmin = parseInt($rootScope.version) * 100;
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && versionmin < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.dev_height = $window.innerHeight / 2.5;

    $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center'};

    $scope.acceptStyle = function() {
        $scope.myStyle = {'height': $scope.dev_height+'px', 'background-repeat': 'no-repeat', 'background-position': 'center center', 'background-size': 'cover'};
    }

    $scope.mapHeight = {'height': $scope.dev_height*2+'px', 'width': '100%'};

    $scope.myID = '';

    var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
    $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {

      if(success.rows.length > 0) {
        $scope.myID = success.rows.item(0).user_real_id;
      }

    },function() {});

    var lastchat = 1;

    $scope.intconnect = true;

    $scope.canmessage = true;

    $scope.sendMessage = function() {

        if($scope.canmessage) {

            // alert($scope.data.message)

            if($scope.data.message != '' && typeof $scope.data.message != 'undefined') {

                $scope.canmessage = false;

                var supportstr = JSON.stringify({
                    device_id: $rootScope.uuid,
                    inst_id: $rootScope.institution,
                    getsend: 'send',
                    name: 'support',
                    txt: $scope.data.message,
                    newusr: 'chat'
                });

                // console.log("SEND =========================> "+JSON.stringify(supportstr))

                $scope.writeMessages(supportstr);
                            
            }

        }

    }

    $scope.getMessages = function() {

        var nonChatName = $scope.support_request_discount_name;
        var nonChatMessage = $scope.support_request_discount_message;

        var getChat = "SELECT * FROM chat WHERE chat_name != ? AND chat_del='0' ORDER BY chat_id ASC";
        $cordovaSQLite.execute($rootScope.db, getChat, [nonChatName]).then(function(success) {
            // console.log("CHATS LENGTH ======================> "+success.rows.length);
          if(success.rows.length > 0) {
            for (var i = 0;i < success.rows.length;i++) {
                if(success.rows.item(i).chat_message != nonChatMessage) {
                
                    $scope.messages.push({
                        chat_message: HtmlEnt.decodeEntities(success.rows.item(i).chat_message),
                        chat_from: success.rows.item(i).chat_from,
                    });

                    if(success.rows.item(i).chat_when > lastchat) {
                        lastchat = success.rows.item(i).chat_when;
                    }

                    if(i == success.rows.length - 1) {
                        $ionicScrollDelegate.scrollBottom(true);
                        $scope.checkMessages();
                    }

                }
            }

          }
          else {
            $scope.checkMessages();
          }

        },function(er) {});

    }

    $scope.checkMessages = function() {

        var checkmesstr = JSON.stringify({
            device_id: $rootScope.uuid,
            inst_id: $rootScope.institution,
            getsend: 'get',
            lastchat: lastchat,
            newusr: 'chat'
        });

        // console.log("CHECK ================================> "+JSON.stringify(checkmesstr))

        $scope.writeMessages(checkmesstr);
        
    }

    $scope.writeMessages = function(messtr) {

        $http.post($rootScope.generalscript, messtr).then(function(e) {

            $scope.canmessage = true;

            var data = e.data;

            // console.log("WRITE ====================================> "+JSON.stringify(data));

            if(data[0].chatOK == '1') {

                // DB CHAT
                var chatArr = data[0].chatArr;

                $scope.chatArrFunc = function(id) {

                  var chat_id = chatArr[id]['chat_id'];
                  var chat_from = chatArr[id]['chat_from'];
                  var chat_to = chatArr[id]['chat_to'];
                  var chat_name = chatArr[id]['chat_name'];
                  var chat_message = chatArr[id]['chat_message'];
                  var chat_read = chatArr[id]['chat_read'];
                  var chat_institution = chatArr[id]['chat_institution'];
                  var chat_answered = chatArr[id]['chat_answered'];
                  var chat_when = chatArr[id]['chat_when'];
                  var chat_del = chatArr[id]['chat_del'];

                  lastchat = chat_when;

                    var queryChat = "SELECT * FROM chat WHERE chat_id = ? AND chat_del='0'";
                    $cordovaSQLite.execute($rootScope.db, queryChat, [chat_id]).then(function(suc) {

                        if(suc.rows.length > 0) {
                          var chatUpd = "UPDATE chat SET chat_read=?, chat_answered=?, chat_when=?, chat_del=? WHERE chat_id=?";
                          $cordovaSQLite.execute($rootScope.db, chatUpd, [chat_read, chat_answered, chat_when, chat_del, chat_id]).then(function() {
                            id++;
                            if(id < chatArr.length) {
                              $scope.chatArrFunc(id);
                            }
                          }, function() {});
                        }
                        else {
                          var chatIns = "INSERT INTO chat (chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                          $cordovaSQLite.execute($rootScope.db, chatIns, [chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del]).then(function() {

                                $scope.messages.push({
                                    chat_message: HtmlEnt.decodeEntities(chat_message),
                                    chat_from: chat_from
                                });

                                $ionicScrollDelegate.scrollBottom(true);

                            id++;
                            if(id < chatArr.length) {
                              $scope.chatArrFunc(id);
                            }
                          }, function() {});
                        }

                    }, function() {});

                }

                $timeout(function() {$scope.chatArrFunc(0);}, 200);


            }
            else if(data[0].chatOK > '1') {

                var chatIns = "INSERT INTO chat (chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                $cordovaSQLite.execute($rootScope.db, chatIns, [data[0].chatID, $scope.myID, '1', 'support', $scope.data.message, '0', $rootScope.institution, '0', data[0].chatOK, '0']).then(function() {

                    $scope.messages.push({
                        chat_message: HtmlEnt.decodeEntities($scope.data.message),
                        chat_from: $scope.myID,
                    });

                    $scope.data.message = '';
                    $ionicScrollDelegate.scrollBottom(true);

                }, function() {});

            }

            chatInter = $timeout(function () {
                $scope.checkMessages();
            }, 3000);

        }, function() {
            chatInter = $timeout(function () {
                $scope.checkMessages();
            }, 3000);
        });

    }

})

.controller('ActivityCtrl', function($rootScope, $scope, $state, $stateParams, $timeout, $ionicPlatform, $ionicNavBarDelegate, $translate, ionicMaterialMotion, ionicMaterialInk) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 500);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

})

.controller('FabCtrl', function($rootScope, $scope, $stateParams, $ionicPlatform, $state, $timeout, $cordovaFile, $cordovaFileTransfer, $localStorage, $window, $cordovaNetwork, $cordovaSQLite, $ionicScrollDelegate, $ionicPosition, $ionicNavBarDelegate, $ionicModal, $ionicPopup, $http, ionicMaterialMotion, ionicMaterialInk, Store) {

    $scope.$parent.clearFabs();

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    $scope.intconnect = true;

    $scope.comment = {
        category: '',
        orderid: '',
        worker: 0,
        date: new Date(),
        time: new Date(),
        text: ''
    };

    $scope.categories = '';

    $scope.menues = '';

    $scope.employees = '';

    $scope.zeronull = "0";

    $scope.emptyspace = "";

    $scope.menueinterval = 30;

    $scope.schedules = '';

    $scope.schedulearr = [];

    Store.getcats(3).then(function(data) {
        $scope.categories = data;
    });

    $scope.getServices = function(x) {
        Store.selcat(x.cat_id).then(function(data) {
            $scope.menues = data;
        });
        $scope.employees = '';
        // var elt = document.getElementById('servicesel');
        // $scope.orderform_0.street = elt.options[elt.selectedIndex].text;
    }

    $scope.getEmployee = function(x) {
        Store.getwaiters(x.menue_id).then(function(data) {
            $scope.employees = data;
        });
        // var elt = document.getElementById('servicesel');
        // $scope.orderform_0.street = elt.options[elt.selectedIndex].text;
    }

    $scope.getScheduleTimes = function() {
        Store.getschedule().then(function(data) {
            $scope.schedulearr = data;
        });
    }

    $scope.getScheduleTimes();

    $scope.scheduletime = [{
        hours: 0,
        mins: 0
    }];

    $scope.scheduleselect = function(x) {

        $scope.schedulePopup = $ionicPopup.show({
            title: 'Выбрать время',
            template: '<p class="padding" style="text-align:center;"><select ng-model="scheduletime.hours" ng-value="cats.cat_id" ng-options="cats.cat_name for cats in categories" class="item-text-wrap"></select><select ng-model="scheduletime.mins" ng-value="cats.cat_id" ng-options="cats.cat_name for cats in categories" class="item-text-wrap"></select></p>',
            scope: $scope,
            buttons: [
                { text: 'ОТМЕНА',
                    onTap: function() {
                        $scope.schedulePopup.close();
                    }},
                { text: 'ПРИНЯТЬ',
                    onTap: function(e) {
                        var rtxt = document.getElementById('ratetxt').value;
                        $scope.sendRate($scope.rating.rate, rtxt);
                        $scope.schedulePopup.close();
                    }}
            ]
        });

    }

    $scope.modalOrder = $ionicModal.fromTemplate('<ion-modal-view><ion-header-bar class="bar-assertive"><a class="button button-clear pull-right" ng-click="closeModal()"><i class="icon ion-close"></i></a><h1 class="title">Запись</h1></ion-header-bar><ion-content has-bouncing="true"><div class="padding"><div class="list"><label class="item item-input item-select"><span class="input-label">Вид услуги</span><select ng-change="getServices(comment.category)" ng-model="comment.category" ng-value="cats.cat_id" ng-options="cats.cat_name for cats in categories" class="item-text-wrap"></select></label><label class="item item-input item-select item-text-wrap"><span class="input-label">Услуга</span><select ng-change="getEmployee(comment.orderid)" ng-model="comment.orderid" ng-value="menue.menue_id" ng-options="menue.menue_name for menue in menues"></select></label></div><div class="list"><h4 ng-if="employees">Выбрать мастера</h4><ion-radio style="padding-left:0px;" class="item-avatar" ng-repeat="employee in employees" ng-value="employee.user_real_id" ng-model="comment.worker"> <img ng-if="employee.user_pic != zeronull && employee.user_pic != emptyspace" cache-src="http://www.communiloca.com/admin/img/user/'+$rootScope.institution+'/pic/{{employee.user_pic}}" /> <img ng-if="employee.user_pic == zeronull || employee.user_pic == emptyspace" cache-src="http://www.communiloca.com/admin/img/user/'+$rootScope.institution+'/pic/user.png" /> <h4><span ng-if="employee.user_surname != 0">{{ employee.user_surname }}</span> <span ng-if="employee.user_name != 0">{{ employee.user_name }}</span> <span ng-if="employee.user_middlename != 0">{{ employee.user_middlename }}</span></h4><p>{{employee.prof_name}}</p> </ion-radio></div><div class="list"><div class="item item-icon-left" ion-datetime-picker date ng-model="comment.date"><i class="icon ion-calendar positive"></i><strong>{{comment.date| date: "dd.MM.yyyy"}}</strong></div><div class="item item-icon-left" ion-datetime-picker time minute-step="menueinterval" ng-model="comment.time"><i class="icon ion-ios-clock positive"></i><strong>{{comment.time| date: "H:mm"}}</strong></div></label><label class="item item-input item-floating-label"><span class="input-label">Примечания</span><input type="text" ng-model="comment.text" placeholder="Примечания"></label><div class="item"><button class="button button-balanced button-block" ng-click="sendOrder();"><i class="icon ion-archive"></i>Записаться</button></div></div></div></ion-content></ion-modal-view>', {
      scope: $scope,
      animation: 'slide-in-up'
    });

    $scope.subscribe = function() {

        // if($cordovaNetwork.isOnline()) {
            $scope.modalOrder.show();
        // }
        // else {
            // $scope.intconnect = false;
            // $ionicPopup.alert({
            //     title: "Сеть",
            //     template: "Соединение с интернетом прерванно!"
            // });
        // }

    }

    $scope.closeModal = function() {
        $scope.modalOrder.hide();
    }

    $scope.sendOrder = function() {
        
        var calstr = JSON.stringify({
            device_id: $rootScope.uuid,
            inst_id: $rootScope.institution,
            orderid: $scope.comment.orderid,
            worker: $scope.comment.worker,
            start: ($scope.comment.date.getTime() / 1000).toFixed(0),
            end: ($scope.comment.time.getTime() / 1000).toFixed(0),
            text: $scope.comment.text,
            newusr: 'calender'
        });

        $http.post($rootScope.generalscript, calstr).then(function(e) {

            var data = JSON.parse(e.data);

            if(data[0].orderOK == '0') {

            }
            else if(data[0].orderOK == '1') {

                // alert(JSON.stringify(data[0]))

                var order_id = data[0].order_id;
                var order_user = data[0].order_user;
                var order_name = data[0].order_name;
                var order_desc = data[0].order_desc;
                var order_worker = data[0].order_worker;
                var order_institution = data[0].order_institution;
                var order_bill = data[0].order_bill;
                var order_order = data[0].order_order;
                var order_status = data[0].order_status;
                var order_year = data[0].order_year;
                var order_year_end = data[0].order_year_end;
                var order_month = data[0].order_month;
                var order_month_end = data[0].order_month_end;
                var order_day = data[0].order_day;
                var order_day_end = data[0].order_day_end;
                var order_hour = data[0].order_hour;
                var order_hour_end = data[0].order_hour_end;
                var order_min = data[0].order_min;
                var order_min_end = data[0].order_min_end;
                var order_allday = data[0].order_allday;
                var order_mobile = data[0].order_mobile;
                var order_when = data[0].order_when;
                var order_del = data[0].order_del;

                var orderIns = "INSERT INTO ordering (order_id, order_user, order_name, order_desc, order_worker, order_institution, order_bill, order_order, order_status, order_year, order_year_end, order_month, order_month_end, order_day, order_day_end, order_hour, order_hour_end, order_min, order_min_end, order_allday, order_mobile, order_when, order_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $cordovaSQLite.execute($rootScope.db, orderIns, [order_id, order_user, order_name, order_desc, order_worker, order_institution, order_bill, order_order, order_status, order_year, order_year_end, order_month, order_month_end, order_day, order_day_end, order_hour, order_hour_end, order_min, order_min_end, order_allday, order_mobile, order_when, order_del]).then(function(ins) {}, function() {});

                $ionicPopup.alert({
                    title: "Вы успешно записаны!",
                    template: "Ожидайте одобрения в \"личном кабинете\"!"
                });
            }
            else if(data[0].orderOK == '2') {
                $ionicPopup.alert({
                    title: "Внимание",
                    template: "Вы уже записались на выбранную услугу. Ожидайте одобрения в \"личном кабинете\"!"
                });
            }
            else if(data[0].orderOK == '3') {
                $ionicPopup.alert({
                    title: "Внимание",
                    template: "Такой услуги уже нет!"
                });
            }

        }, function() {});

    }

})

.controller('OrderCatCtrl', function($rootScope, $scope, $state, $stateParams, $timeout, $ionicPlatform, $ionicNavBarDelegate, $window, $ionicScrollDelegate, ionicMaterialMotion, ionicMaterialInk, Order) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 100);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.dev_height = $window.innerHeight / 3.8;

    $scope.htmlEntities = function(x) {
        return Order.decodeEntities(x);
    }

    $scope.categories = [];

    Order.getOrderCategories().then(function(data) {
        $scope.categories = data;
    });

})

.controller('OrderMenueCtrl', function($rootScope, $scope, $state, $stateParams, $timeout, $ionicPlatform, $ionicNavBarDelegate, $window, $ionicScrollDelegate, ionicMaterialMotion, ionicMaterialInk, Order) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 100);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.htmlEntities = function(x) {
        return Order.decodeEntities(x);
    }

    $scope.menues = [];

    Order.getMenue($stateParams.catId, $stateParams.goodId).then(function(data) {
        $scope.menues = data;
        $timeout(function() {$ionicScrollDelegate.resize();}, 50);
    });

    $scope.setMenue = function(menueId, menueName, menueCost) {
        Order.setMenue(menueId, menueName, menueCost);
    }

})

.controller('OrderWorkerCtrl', function($rootScope, $scope, $state, $stateParams, $timeout, $ionicPlatform, $ionicNavBarDelegate, $window, $ionicScrollDelegate, ionicMaterialMotion, ionicMaterialInk, Order) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 100);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.htmlEntities = function(x) {
        return Order.decodeEntities(x);
    }

    $scope.workers = [];

    $scope.menId = $stateParams.menId;

    Order.getWorker($stateParams.menId).then(function(data) {
        $scope.workers = data;
        $timeout(function() {$ionicScrollDelegate.resize();}, 50);
    });

    $scope.setWorker = function(workerId, workerName, workerPic, workerProfession) {
        Order.setWorker(workerId, workerName, workerPic, workerProfession);
    }

})

.controller('OrderTimeCtrl', function($rootScope, $scope, $state, $stateParams, $timeout, $ionicPlatform, $ionicNavBarDelegate, $window, $ionicScrollDelegate, $ionicPopup, ionicMaterialMotion, ionicMaterialInk, Order) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
        $ionicScrollDelegate.resize();
      }, 100);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.days7 = Order.getDates($stateParams.menId, $stateParams.workId);

    // console.log(JSON.stringify($scope.days7));

    $scope.menId = $stateParams.menId;

    $scope.workId = $stateParams.workId;

    $scope.indexid = 0;

    $scope.isGroupShown = 0;

    $scope.toggleGroup = function(day, id) {
        $scope.indexid = id;
        $scope.days7[id].items = [];
        if ($scope.isGroupShown == day) {
          $scope.isGroupShown = null;
          $scope.days7[$scope.indexid].items = [];
        } else {
          $scope.isGroupShown = day;
          Order.getTimes(day, $stateParams.menId, $stateParams.workId).then(function(data) {
            $scope.days7[id].items = data;
            $timeout(function() {$ionicScrollDelegate.resize();}, 50);
            // console.log(JSON.stringify($scope.days7[id]));
          }, function() {});
        }
    };

    $scope.timecalc = function(x) {

        // ОПРЕДЕЛЕНИЕ ВРЕМЯ
        var nowtime = new Date();
        var gottime = x * 1000;
        var nowtimediff = nowtime.getTime() - gottime;
        var onltime = new Date(gottime);
        var onlDateTime;
        var onlHour;
        var onlMin;

        // HOURS AND MINUTES AGO
        if(onltime.getHours() < 10) {
            onlHour = '0' + onltime.getHours();
        } else {
            onlHour = onltime.getHours();
        }
        if(onltime.getMinutes() < 10) {
            onlMin = '0' + onltime.getMinutes();
        } else {
            onlMin = onltime.getMinutes();
        }

        onlDateTime =  onlHour + ':' + onlMin;

        return onlDateTime;
    }

    $scope.setOrderHour = function(orderHour, orderHourName, day) {
        Order.setOrderHour(orderHour, orderHourName, day);
    }

})

.controller('OrderCtrl', function($rootScope, $scope, $state, $stateParams, $timeout, $ionicPlatform, $ionicNavBarDelegate, $window, $ionicScrollDelegate, $ionicPopup, ionicMaterialMotion, ionicMaterialInk, Order) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
        $ionicScrollDelegate.resize();
      }, 100);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.menId = $stateParams.menId;

    $scope.workId = $stateParams.workId;

    $scope.orderHour = $stateParams.orderHour;

    $scope.notphone = false;

    $scope.data = {
        name: '',
        phone: '',
        email: '',
        comments: '',
        reminder: '',
        barSelected: 0
    }

    $scope.setSelected = function(x) {
        $scope.data.barSelected = parseInt(x);
        switch(x) {
            case 0:
                $scope.data.reminder = 'Нет';
                break;
            case 1:
                $scope.data.reminder = 'SMS';
                break;
            case 2:
                $scope.data.reminder = 'Email';
                break;
            default:
                $scope.data.reminder = 'Нет';
                break;
        }
    }

    $scope.placeOrder = function() {
        $scope.notphone = false;
        var mobconf = document.getElementById('phone');
        if(mobconf.value.length >= 10) {
            // console.log($scope.data.name + ' ' + $scope.data.phone + ' ' + $scope.data.email + ' ' + $scope.data.comments + ' ' + $scope.data.reminder);
            Order.setOrder($scope.data.name, $scope.data.phone, $scope.data.email, $scope.data.comments, $scope.data.reminder);
            $timeout(function() {$state.go('app.order_final', {menId: $scope.menId, workId: $scope.workId, orderHour: $scope.orderHour});}, 300);
        }
        else {
            var lengthPopup = $ionicPopup.show({
                title: "Длина номера телефона",
                template: 'Слишком короткий номер.<br/>Пример: 71234567890',
                scope: $scope,
                buttons: [
                  {
                    text: '<b>ОК</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                      lengthPopup.close();
                    }
                  }
                ]
            });
            $scope.notphone = true;
        }
    }

    $scope.onlyNumbers = /^[0-9]+$/;

})

.controller('OrderFinalCtrl', function($rootScope, $scope, $state, $stateParams, $timeout, $ionicPlatform, $ionicNavBarDelegate, $window, $ionicScrollDelegate, $http, $ionicLoading, $ionicHistory, $ionicPopup, ionicMaterialMotion, ionicMaterialInk, Order) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
        $ionicScrollDelegate.resize();
      }, 100);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.menId = $stateParams.menId;

    $scope.workId = $stateParams.workId;

    $scope.orderHour = $stateParams.orderHour;

    $scope.finalOrder = Order.getOrder();

    $scope.ordersum = ($scope.finalOrder.menueCost/100).toFixed(2);

    $scope.smssent = false;

    $scope.notconfirmed = true;

    $scope.datas = {
        smsconf: ''
    }

    var orderstr = JSON.stringify(Order.getOrder());

    $scope.sendOrder = function() {

        $ionicLoading.show({
            content: '<i class="icon ion-loading-c"></i>',
            animation: 'fade-in',
            showBackdrop: false,
            maxWidth: 50,
            showDelay: 0
        });

        $http.post($rootScope.generalscript, orderstr).then(function(ordsuc) {

            $ionicLoading.hide();
            var orderdata = ordsuc.data;

            // alert(JSON.stringify(data))

            if(orderdata[0].orderOK == '3') {
                $ionicPopup.alert({title: 'Внимание', template: 'Повторный запрос только через 5 минут!'});
            }
            else if(orderdata[0].orderOK == '1') {
                // alert('success android: '+JSON.stringify(orderdata));
                // $scope.smssent = true;
                $ionicPopup.alert({title: 'Спасибо!', template: 'Вы записались! Ожидайте звонка.'});
                $timeout(function() {
                    Order.emptyOrder();
                    $ionicHistory.nextViewOptions({
                        disableAnimate: true,
                        disableBack: true
                    });
                    $state.go('app.main');
                }, 200);
            }
            else if(orderdata[0].orderOK == '5') {
                $ionicPopup.alert({title: 'Внимание', template: 'Укажите Ваш номер телефона!'});
            }

        }, 
        function(er) {
            // alert('error: '+JSON.stringify(er));
            $ionicLoading.hide();
        });

    }

    $scope.confSMS = function() {

        Order.setSMSconf($scope.datas.smsconf);

        $timeout(function() {

            orderstr = JSON.stringify(Order.getOrder());

            $ionicLoading.show({
                content: '<i class="icon ion-loading-c"></i>',
                animation: 'fade-in',
                showBackdrop: false,
                maxWidth: 50,
                showDelay: 0
            });

            $http.post($rootScope.generalscript, orderstr).then(function(ordsuc) {

                $ionicLoading.hide();

                var orderdata = ordsuc.data;
                // alert('success: '+JSON.stringify(orderdata));
                if(orderdata[0].orderOK == '4') {
                    $ionicPopup.alert({title: 'Спасибо!', template: 'Вы записались! Статус отслеживайте в личном кабинете.'});
                }
                else if(orderdata[0].orderOK == '5') {
                    $ionicPopup.alert({title: 'Ошибка', template: 'Запись к данной услуге невозможна!'});
                }
                else if(orderdata[0].orderOK == '2') {
                    $ionicPopup.alert({title: 'Спасибо', template: 'Запись уже внесена!'});
                }

                $timeout(function() {
                    Order.emptyOrder();
                    $ionicHistory.nextViewOptions({
                        disableAnimate: true,
                        disableBack: true
                    });
                    $state.go('app.main');
                }, 200);
                

            }, 
            function(er) {
                // alert('error: '+JSON.stringify(er));
                $ionicLoading.hide();
            });

        }, 200);

    }

})

.controller('OrderBSCatCtrl', function($rootScope, $scope, $state, $stateParams, $timeout, $ionicPlatform, $ionicNavBarDelegate, $window, $ionicScrollDelegate, ionicMaterialMotion, ionicMaterialInk, Order) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 100);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.dev_height = $window.innerHeight / 3.8;

    $scope.htmlEntities = function(x) {
        return Order.decodeEntities(x);
    }

    $scope.categories = [];

    Order.getOrderBSCategories().then(function(data) {
        $scope.categories = data;
        $timeout(function() {$ionicScrollDelegate.resize();}, 50);
    });

})

.controller('OrderBSStartCtrl', function($rootScope, $scope, $state, $stateParams, $timeout, $ionicPlatform, $ionicNavBarDelegate, $window, $ionicScrollDelegate, ionicMaterialMotion, ionicMaterialInk, Order) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
      }, 100);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.dev_height = $window.innerHeight / 3.8;

    $scope.htmlEntities = function(x) {
        return Order.decodeEntities(x);
    }

    $scope.categories = [];

    var cart = [];

    $scope.checks = false;

    Order.getOrderBSMenue($stateParams.catId, $stateParams.goodId).then(function(data) {
        $scope.categories = data;
        $timeout(function() {
            // console.log('--------------------------->'+JSON.stringify(data));

            var cartlength = data.length;
            // console.log('==========================> DATA LENGTH '+cartlength)
            if(cartlength > 0) {
                for (var i = 0;i < cartlength; i++) {
                    // console.log('==========================> CHECKED '+data[i].checked)
                    if(data[i].checked) {
                        cart.push(data[i]);
                        $scope.checks = true;
                    }
                }
            }

            $timeout(function() {$ionicScrollDelegate.resize();}, 50);

        }, 200);
    });

    $scope.checking = function(cat) {
        if(!cat.category) {
            if(cat.checked) {
                var cartlength = cart.length;
                if(cartlength > 0) {
                    for (var i = 0;i < cartlength; i++) {
                        if(parseInt(cart[i].menue_id) == parseInt(cat.menue_id)) {
                            Order.removeCheckedOrder(cat.menue_id);
                            if(cartlength == 1) {
                                $scope.checks = false;
                                cart.splice(i, 1);
                            }
                            else {
                                cart.splice(i, 1);
                                break;
                            }
                        }
                    }
                }
                return cat.checked = false;
            }
            else {
                cart.push(cat);
                $scope.checks = true;
                return cat.checked = true;
            }
        }
    }

    $scope.checkout = function() {
        if(cart.length > 0) {
            for(var i=0;i<cart.length;i++) {
                // console.log(JSON.stringify(cart[i]));
                Order.setCheckedOrder(cart[i]);
            }
            $state.go('app.order_bs_worker', {menId: cart[0].menue_id});
        }
    }

})

.controller('OrderBSWorkerCtrl', function($rootScope, $scope, $state, $stateParams, $timeout, $ionicPlatform, $ionicNavBarDelegate, $window, $ionicScrollDelegate, ionicMaterialMotion, ionicMaterialInk, Order) {

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.htmlEntities = function(x) {
        return Order.decodeEntities(x);
    }

    $scope.workers = [];

    $scope.menId = $stateParams.menId;

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {

        Order.getBSWorker($stateParams.menId).then(function(data) {
            $scope.workers = data;
            $timeout(function() {$ionicScrollDelegate.resize();}, 50);
            // console.log("==================> CONTROLLER WORKER: "+JSON.stringify(data))
        });

        $ionicNavBarDelegate.align('center');

      }, 1000);
    });

    $scope.setBSWorker = function(workerId, workerName, workerPic, workerProfession) {
        Order.setBSWorker(workerId, workerName, workerPic, workerProfession);
    }

})

.controller('OrderBSTimeCtrl', function($rootScope, $scope, $state, $stateParams, $timeout, $ionicPlatform, $ionicNavBarDelegate, $window, $ionicScrollDelegate, $ionicPopup, ionicMaterialMotion, ionicMaterialInk, Order) {

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {
      $timeout(function() {
        $ionicNavBarDelegate.align('center');
        $ionicScrollDelegate.resize();
      }, 100);
    });

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.days7 = Order.getBSDates($stateParams.menId, $stateParams.workId);

    // console.log(JSON.stringify($scope.days7));

    $scope.menId = $stateParams.menId;

    $scope.workId = $stateParams.workId;

    $scope.indexid = 0;

    $scope.isGroupShown = 0;

    $scope.toggleGroup = function(day, id) {
        $scope.indexid = id;
        $scope.days7[id].items = [];
        if ($scope.isGroupShown == day) {
          $scope.isGroupShown = null;
          $scope.days7[$scope.indexid].items = [];
        } else {
          $scope.isGroupShown = day;
          Order.getBSTimes(day, $stateParams.menId, $stateParams.workId).then(function(data) {
            $scope.days7[id].items = data;
            $timeout(function() {$ionicScrollDelegate.resize();}, 50);
            // console.log(JSON.stringify($scope.days7[id]));
          }, function() {});
        }
    };

    $scope.timecalc = function(x) {

        // ОПРЕДЕЛЕНИЕ ВРЕМЯ
        var nowtime = new Date();
        var gottime = x * 1000;
        var nowtimediff = nowtime.getTime() - gottime;
        var onltime = new Date(gottime);
        var onlDateTime;
        var onlHour;
        var onlMin;

        // HOURS AND MINUTES AGO
        if(onltime.getHours() < 10) {
            onlHour = '0' + onltime.getHours();
        } else {
            onlHour = onltime.getHours();
        }
        if(onltime.getMinutes() < 10) {
            onlMin = '0' + onltime.getMinutes();
        } else {
            onlMin = onltime.getMinutes();
        }

        onlDateTime =  onlHour + ':' + onlMin;

        return onlDateTime;
    }

    $scope.setOrderHour = function(orderHour, orderHourName, day) {
        Order.setBSOrderHour(orderHour, orderHourName, day);
    }

})

.controller('OrderBSCtrl', function($rootScope, $scope, $state, $stateParams, $timeout, $ionicPlatform, $ionicNavBarDelegate, $window, $ionicScrollDelegate, $http, $ionicPopup, $ionicLoading, $ionicHistory, $cordovaSQLite, ionicMaterialMotion, ionicMaterialInk, Order, HtmlEnt) {

    var version = parseInt($rootScope.version) * 100;
    
    var minvers = 4.3*100;

    if($rootScope.platform == 'Android' && version < minvers) {

    }
    else {
        ionicMaterialInk.displayEffect();
    }

    $scope.menId = $stateParams.menId;

    $scope.workId = $stateParams.workId;

    $scope.orderHour = $stateParams.orderHour;

    $scope.notphone = false;

    $scope.data = {
        name: '',
        phone: '',
        email: '',
        comments: '',
        reminder: '',
        barSelected: 0
    }

    $scope.finalOrder = Order.getBSOrder();

    $scope.onlyNumbers = /^[0-9]+$/;

    function orderArrFuncIns(gotdata) {

        var order_id = gotdata['order_id'];
        var order_user = gotdata['order_user'];
        var order_name = gotdata['order_name'];
        var order_desc = gotdata['order_desc'];
        var order_worker = gotdata['order_worker'];
        var order_institution = gotdata['order_institution'];
        var order_office = gotdata['order_office'];
        var order_bill = gotdata['order_bill'];
        var order_goods = gotdata['order_goods'];
        var order_cats = gotdata['order_cats'];
        var order_order = gotdata['order_order'];
        var order_status = gotdata['order_status'];
        var order_start = gotdata['order_start'];
        var order_end = gotdata['order_end'];
        var order_allday = gotdata['order_allday'];
        var order_mobile = gotdata['order_mobile'];
        var order_when = gotdata['order_when'];
        var order_del = gotdata['order_del'];

        var queryOrdering = "SELECT * FROM ordering WHERE order_id = ?";
        $cordovaSQLite.execute($rootScope.db, queryOrdering, [order_id]).then(function(suc) {
            if(suc.rows.length > 0) {

                // DB ORDERS
                var orderingUpd = "UPDATE ordering SET order_user=?, order_name=?, order_desc=?, order_worker=?, order_institution=?, order_office=?, order_bill=?, order_goods=?, order_cats=?, order_order=?, order_status=?, order_start=?, order_end=?, order_allday=?, order_mobile=?, order_when=?, order_del=? WHERE order_id=?";
                $cordovaSQLite.execute($rootScope.db, orderingUpd, [order_user, order_name, order_desc, order_worker, order_institution, order_office, order_bill, order_goods, order_cats, order_order, order_status, order_start, order_end, order_allday, order_mobile, order_when, order_del, order_id]).then(function() {}, function() {});

            }
            else {

                var orderIns = "INSERT INTO ordering (order_id, order_user, order_name, order_desc, order_worker, order_institution, order_office, order_bill, order_goods, order_cats, order_order, order_status, order_start, order_end, order_allday, order_mobile, order_when, order_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $cordovaSQLite.execute($rootScope.db, orderIns, [order_id, order_user, order_name, order_desc, order_worker, order_institution, order_office, order_bill, order_goods, order_cats, order_order, order_status, order_start, order_end, order_allday, order_mobile, order_when, order_del]).then(function(ins) {}, function() {});

            }
        }, function() {});

    };

    function uploadOrder(x, y) {

        var xjson = JSON.stringify(x);

        $http.post($rootScope.generalscript, xjson).then(function(ordsuc) {

            // console.log(JSON.stringify(ordsuc))

            $ionicLoading.hide();
            var orderdata = ordsuc.data;


            if(orderdata[0].orderOK == '3' && y == 1) {
                $ionicPopup.alert({title: 'Внимание', template: 'Повторный запрос только через 5 минут!'});
            }
            else if(orderdata[0].orderOK == '1' && y == 1) {
                // alert('success android: '+JSON.stringify(orderdata));
                // $scope.smssent = true;

                orderArrFuncIns(orderdata[0]);

                $ionicPopup.alert({title: 'Спасибо!', template: 'Вы записались! Ожидайте звонка.'});
                $timeout(function() {
                    Order.emptyBSOrder();
                    $ionicHistory.nextViewOptions({
                        disableAnimate: true,
                        disableBack: true
                    });
                    $state.go('app.main');
                }, 200);
            }
            else if(orderdata[0].orderOK == '1') {
                orderArrFuncIns(orderdata[0]);
            }
            else if(orderdata[0].orderOK == '5' && y == 1) {
                $ionicPopup.alert({title: 'Внимание', template: 'Укажите Ваш номер телефона!'});
            }
            
        }, 
        function(er) {
            if(y == 1) {
                $ionicPopup.alert({title: 'Связь', template: 'Проверьте Интернетсоединение.'});
                // alert('error: '+JSON.stringify(er));
                $ionicLoading.hide();
            }
        });

    }

    $scope.sendOrder = function() {

        var orderstr = Order.getBSOrder();

        if(orderstr.length > 0) {

            $ionicLoading.show({
                content: '<i class="icon ion-loading-c"></i>',
                animation: 'fade-in',
                showBackdrop: false,
                maxWidth: 50,
                showDelay: 0
            });

            // console.log(orderstr);

            for(var i=0;i<orderstr.length;i++) {
                if(i == orderstr.length-1) {
                    // console.log('===================> ORDER STR 1: '+JSON.stringify(orderstr[i]));
                    uploadOrder(orderstr[i], 1);
                }
                else {
                    // console.log('===================> ORDER STR 0: '+JSON.stringify(orderstr[i]));
                    uploadOrder(orderstr[i], 0);
                }
            }

        }
        else {
            $ionicPopup.alert({title: 'Пустая запись!', template: 'Добавьте минимум одну запись.'});
        }

    }

    $scope.setSelected = function(x) {
        $scope.data.barSelected = parseInt(x);
        switch(x) {
            case 0:
                $scope.data.reminder = 'Нет';
                break;
            case 1:
                $scope.data.reminder = 'SMS';
                break;
            case 2:
                $scope.data.reminder = 'Email';
                break;
            default:
                $scope.data.reminder = 'Нет';
                break;
        }
    }

    $scope.placeOrder = function() {
        $scope.notphone = false;
        var numbersOnly = /^[0-9]+$/;
        var mobconf = document.getElementById('phone');
        if(mobconf.value.match(numbersOnly) && mobconf.value.length >= 10) {
            // console.log($scope.data.name + ' ' + $scope.data.phone + ' ' + $scope.data.email + ' ' + $scope.data.comments + ' ' + $scope.data.reminder);
            Order.setBSOrder($scope.data.name, $scope.data.phone, $scope.data.email, $scope.data.comments, $scope.data.reminder);
            $timeout(function() {$scope.sendOrder();}, 500);
        }
        else {
            var lengthPopup = $ionicPopup.show({
                title: "Номер телефона",
                template: 'Неправильный номер.<br/>Пример: 71234567890',
                scope: $scope,
                buttons: [
                  {
                    text: '<b>ОК</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                      lengthPopup.close();
                    }
                  }
                ]
            });
            $scope.notphone = true;
        }
    }

    $scope.removeOrder = function(order) {
        Order.removeCheckedOrder(order);
    }

    $scope.$on('$ionicView.afterEnter', function (event, viewData) {

        // Disable BACK button on home
        $ionicPlatform.registerBackButtonAction(function (event) {
            if($state.current.name=="app.order_bs"){
                
            }
            else {
                navigator.app.backHistory();
            }
        }, 100);

      $timeout(function() {

        $ionicNavBarDelegate.align('center');

        var queryMeSel = "SELECT * FROM users WHERE user_id = ? AND user_del = '0'";
        $cordovaSQLite.execute($rootScope.db, queryMeSel, [1]).then(function(success) {

            if(success.rows.item(0).user_name != '0' && success.rows.item(0).user_name != '') {
                $scope.data.name = HtmlEnt.decodeEntities(success.rows.item(0).user_name);
            }

            if(success.rows.item(0).user_email != '0' && success.rows.item(0).user_email != '') {
                $scope.data.email = HtmlEnt.decodeEntities(success.rows.item(0).user_email);
            }

            if(success.rows.item(0).user_mob != '0' && success.rows.item(0).user_mob != '') {
                $scope.data.phone = success.rows.item(0).user_mob;
            }

        }, function() {});

        $ionicScrollDelegate.resize();
        
      }, 100);

    });

})
;
